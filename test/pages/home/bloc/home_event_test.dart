/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

// Todo: implement event tests.
import 'dart:math';

import 'package:flutter_test/flutter_test.dart';
import 'package:tc_frontend/pages/home/bloc/home_bloc.dart';
import 'package:tc_frontend/pages/home/model/module_list.dart';

void main() {
  int pos;
  int newPos;
  HomeState baseState;

  setUp(() {
    final baseStateIdList = List.generate(
      moduleList.length,
      (index) => moduleList[index].id,
    );
    baseState = HomeState.fromIdList(baseStateIdList);

    final rng = Random();
    final max = baseState.modules.length - 1;
    pos = rng.nextInt(max);
    newPos = rng.nextInt(max);

    final disabledStateIdList = baseState.asIdList;
    disabledStateIdList[pos] = disabledStateIdList[pos] * -1;

    var reorderedStateIdList = baseState.asIdList;
    reorderedStateIdList.insert(newPos, reorderedStateIdList.removeAt(pos));
  });

  /*
  group("HomePageEnableEvent", () {
    test("Enables correctly", () {
      expect(
        HomePageEnableEvent(pos).performAction(disabledState),
        baseState,
      );
    });
    test("Already enabled caught", () {
      expect(
        () => HomePageEnableEvent(pos).performAction(baseState),
        throwsArgumentError,
      );
    });
  });

  group("HomePageDisableEvent", () {
    test("Disables correctly", () {
      expect(
        HomePageDisableEvent(pos).performAction(baseState),
        disabledState,
      );
    });
    test("Already disabled caught", () {
      expect(
        () => HomePageDisableEvent(pos).performAction(disabledState),
        throwsArgumentError,
      );
    });
  });

  group("HomePageReorderEvent", () {
    test("Reorders correctly", () {
      expect(
        HomePageReorderEvent(pos, newPos).performAction(baseState),
        reorderedState,
      );
    });
  });
   */
}
