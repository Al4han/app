/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:tc_frontend/pages/timeline/model/timeline_entry.dart';
import 'package:tc_frontend/pages/timeline/model/timeline_source.dart';

const String parsedJSONResponseAllEntriesIsLast = """{
  "content": [
    {
      "id": 43,
      "author": "Murderface von Murderface geb. Murderface",
      "parentSource": "parentSource",
      "avatar": "MNI.png",
      "text": "My precious.",
      "route": "/",
      "routeArgs": "1",
      "date": "2019-07-18T08:29:33.511+0000",
      "faved": true,
      "pinned": false,
      "heart": {
        "count": 1,
        "liked": true
      }
    },
    {
      "id": 41,
      "author": "Murderface von Murderface geb. Murderface",
      "parentSource": "parentSource",
      "avatar": "MNI.png",
      "text": "That'll do, pig. That'll do.",
      "route": "/",
      "routeArgs": "1",
      "date": "2019-07-18T08:29:33.511+0000",
      "faved": false,
      "pinned": false,
      "heart": {
        "count": 1,
        "liked": false
      }
    }
  ],
  "pageable": {
    "sort": {
      "sorted": true,
      "unsorted": false,
      "empty": false
    },
    "pageSize": 2,
    "pageNumber": 0,
    "offset": 0,
    "paged": true,
    "unpaged": false
  },
  "totalPages": 2,
  "last": true,
  "totalElements": 4,
  "first": false,
  "sort": {
    "sorted": true,
    "unsorted": false,
    "empty": false
  },
  "number": 1,
  "numberOfElements": 2,
  "size": 2,
  "empty": false
}""";

const String parsedJSONResponseAllEntriesIsNotLast = """{
  "content": [
    {
      "id": 46,
      "author": "Murderface von Murderface geb. Murderface",
      "parentSource": "parentSource",
      "avatar": "MNI.png",
      "text": "My precious.",
      "route": "/",
      "routeArgs": "1",
      "date": "2019-07-18T08:29:33.511+0000",
      "faved": true,
      "pinned": false,
      "heart": {
        "count": 32,
        "liked": true
      }
    },
    {
      "id": 49,
      "author": "Murderface von Murderface geb. Murderface",
      "parentSource": "parentSource",
      "avatar": "MNI.png",
      "text": "That'll do, pig. That'll do.",
      "route": "/",
      "routeArgs": "1",
      "date": "2019-07-18T08:29:33.511+0000",
      "faved": false,
      "pinned": false,
      "heart": {
        "count": 4,
        "liked": false
      }
    }
  ],
  "pageable": {
    "sort": {
      "sorted": true,
      "unsorted": false,
      "empty": false
    },
    "pageSize": 2,
    "pageNumber": 1,
    "offset": 0,
    "paged": true,
    "unpaged": false
  },
  "totalPages": 2,
  "last": false,
  "totalElements": 4,
  "first": true,
  "sort": {
    "sorted": true,
    "unsorted": false,
    "empty": false
  },
  "number": 0,
  "numberOfElements": 2,
  "size": 2,
  "empty": false
}""";

const String parsedJSONResponseFavedEntries = """{
  "content": [
    {
      "id": 46,
      "author": "Murderface von Murderface geb. Murderface",
      "parentSource": "parentSource",
      "avatar": "MNI.png",
      "text": "My precious.",
      "route": "/",
      "routeArgs": "1",
      "date": "2019-07-18T08:29:33.511+0000",
      "faved": true,
      "pinned": false,
      "heart": {
        "count": 32,
        "liked": true
      }
    },
    {
      "id": 49,
      "author": "Murderface von Murderface geb. Murderface",
      "parentSource": "parentSource",
      "avatar": "MNI.png",
      "text": "That'll do, pig. That'll do.",
      "route": "/",
      "routeArgs": "1",
      "date": "2019-07-18T08:29:33.511+0000",
      "faved": true,
      "pinned": true,
      "heart": {
        "count": 4,
        "liked": false
      }
    }
  ],
  "pageable": {
    "sort": {
      "sorted": true,
      "unsorted": false,
      "empty": false
    },
    "pageSize": 2,
    "pageNumber": 0,
    "offset": 0,
    "paged": true,
    "unpaged": false
  },
  "totalPages": 1,
  "last": true,
  "totalElements": 2,
  "first": true,
  "sort": {
    "sorted": true,
    "unsorted": false,
    "empty": false
  },
  "number": 1,
  "numberOfElements": 2,
  "size": 2,
  "empty": false
}""";

const String pinnedEntriesJSON = """[
  {
    "id": 1,
    "author": "Grillbot",
    "parentSource": "parentSource",
    "avatar": "MNI.png",
    "text": "Das vegane 'Fleisch' schmeckt lecker, nur ist nicht mehr warm!",
    "route": "/",
    "routeArgs": "1",
    "date": "2019-07-18T08:29:33.538+0000",
    "faved": false,
    "pinned": true,
    "heart": {
        "count": 4,
        "liked": true
      }
  },
  {
    "id": 5,
    "author": "Grillbot",
    "parentSource": "parentSource",
    "avatar": "MNI.png",
    "text": "Tja, hättet ihr euch mal beeilt.",
    "route": "/",
    "routeArgs": "1",
    "date": "2019-07-18T08:29:33.511+0000",
    "faved": true,
    "pinned": true,
    "heart": {
        "count": 6,
        "liked": false
      }
  },
  {
    "id": 52,
    "author": "Murderface von Murderface geb. Murderface",
    "parentSource": "parentSource",
    "avatar": "MNI.png",
    "text": "Keep your friends close, but your enemies closer.",
    "route": "/",
    "routeArgs": "1",
    "date": "2019-07-18T08:29:33.511+0000",
    "faved": true,
    "pinned": true,
    "heart": {
        "count": 745,
        "liked": true
      }
  }
]""";

const String parsedSourcesJSON = """[
  {
    "id": 1,
    "name": "Fachschaften",
    "description": "Informationskanal der Fachschaften",
    "logo": "MNI.png",
    "subscribed": true
  },
  {
    "id": 3,
    "name": "Fachschaft MNI",
    "description": "Informationskanal der Fachschaft MNI",
    "logo": "MNI.png",
    "subscribed": false
  },
  {
    "id": 4,
    "name": "Fachschaft MNI Grillfest",
    "description": "Alles zum Grillfest der FSMNI",
    "logo": "MNI.png",
    "subscribed": false
  },
  {
    "id": 5,
    "campus": null,
    "department": null,
    "parent": null,
    "name": "Parkplatzsituation",
    "description": "Infos zu Parkplätzen an der THM",
    "logo": "LSE.png",
    "subscribed": true
  },
  {
    "id": 8,
    "name": "Allgemein",
    "description": "Allgemeine Mitteilungen",
    "logo": "WIRTSCHAFT.png",
    "subscribed": true
  },
  {
    "id": 10,
    "name": "Systembenachrichtigungen",
    "description": "Systembenachrichtigungen",
    "logo": "MNI.png",
    "subscribed": true
  }
]""";

final TLEntryList expectedFavedEntries = TLEntryList.from(
  [
    TLEntry(
      id: 46,
      author: "Murderface von Murderface geb. Murderface",
      parentSource: "parentSource",
      avatar: AssetImage('MNI.png'),
      text: 'My precious.',
      route: "/",
      routeArgs: "1",
      date: DateTime.parse("2019-07-18T08:29:33.511+0000"),
      favored: true,
      pinned: false,
    ),
    TLEntry(
      id: 49,
      author: "Murderface von Murderface geb. Murderface",
      parentSource: "parentSource",
      avatar: AssetImage("MNI.png"),
      text: "That'll do, pig. That'll do.",
      route: "/",
      routeArgs: "1",
      date: DateTime.parse("2019-07-18T08:29:33.511+0000"),
      favored: true,
      pinned: true,
    ),
  ],
);

final TLEntryList expectedEntriesLastPage = TLEntryList.from([
  TLEntry(
    id: 43,
    author: "Murderface von Murderface geb. Murderface",
    parentSource: "parentSource",
    avatar: AssetImage('MNI.png'),
    text: 'My precious.',
    route: "/",
    routeArgs: "1",
    date: DateTime.parse("2019-07-18T08:29:33.511+0000"),
    favored: true,
    pinned: false,
  ),
  TLEntry(
    id: 41,
    author: "Murderface von Murderface geb. Murderface",
    parentSource: "parentSource",
    avatar: AssetImage("MNI.png"),
    text: "That'll do, pig. That'll do.",
    route: "/",
    routeArgs: "1",
    date: DateTime.parse("2019-07-18T08:29:33.511+0000"),
    favored: false,
    pinned: false,
  ),
]);

// DatedId is 44

final TLEntryList expectedEntries = TLEntryList.from([
  TLEntry(
    id: 46,
    author: "Murderface von Murderface geb. Murderface",
    parentSource: "parentSource",
    avatar: AssetImage('MNI.png'),
    text: 'My precious.',
    route: "/",
    routeArgs: "1",
    date: DateTime.parse("2019-07-18T08:29:33.511+0000"),
    favored: true,
    pinned: false,
  ),
  TLEntry(
    id: 49,
    author: "Murderface von Murderface geb. Murderface",
    parentSource: "parentSource",
    avatar: AssetImage("MNI.png"),
    text: "That'll do, pig. That'll do.",
    route: "/",
    routeArgs: "1",
    date: DateTime.parse("2019-07-18T08:29:33.511+0000"),
    favored: false,
    pinned: false,
  ),
]);

final TLEntryList expectedPinnedEntries = TLEntryList.from([
  TLEntry(
    id: 1,
    author: "Grillbot",
    parentSource: "parentSource",
    avatar: AssetImage("MNI.png"),
    text: "Das vegane 'Fleisch' schmeckt lecker, nur ist nicht mehr warm!",
    route: "/",
    routeArgs: "1",
    date: DateTime.parse("2019-07-18T08:29:33.538+0000"),
    favored: false,
    pinned: true,
  ),
  TLEntry(
    id: 5,
    author: "Grillbot",
    parentSource: "parentSource",
    avatar: AssetImage("MNI.png"),
    text: "Tja, hättet ihr euch mal beeilt.",
    route: "/",
    routeArgs: "1",
    date: DateTime.parse("2019-07-18T08:29:33.511+0000"),
    favored: true,
    pinned: true,
  ),
  TLEntry(
    id: 52,
    author: "Murderface von Murderface geb. Murderface",
    parentSource: "parentSource",
    avatar: AssetImage("MNI.png"),
    text: "Keep your friends close, but your enemies closer.",
    route: "/",
    routeArgs: "1",
    date: DateTime.parse("2019-07-18T08:29:33.511+0000"),
    favored: true,
    pinned: true,
  ),
]);

final List<TLSource> expectedSources = [
  TLSource(
    name: "Fachschaften",
    id: 1,
    description: "Informationskanal der Fachschaften",
    subscribed: true,
    logo: AssetImage('assets/images/logos/MNI.png'),
  ),
  TLSource(
    name: "Fachschaft MNI",
    id: 3,
    description: "Informationskanal der Fachschaft MNI",
    subscribed: false,
    logo: AssetImage('assets/images/logos/MNI.png'),
  ),
  TLSource(
    name: "Fachschaft MNI Grillfest",
    id: 4,
    description: "Alles zum Grillfest der FSMNI",
    subscribed: false,
    logo: AssetImage('assets/images/logos/MNI.png'),
  ),
  TLSource(
    name: "Parkplatzsituation",
    id: 5,
    description: "Infos zu Parkplätzen an der THM",
    subscribed: true,
    logo: AssetImage('assets/images/logos/LSE.png'),
  ),
  TLSource(
    name: "Allgemein",
    id: 8,
    description: "Allgemeine Mitteilungen",
    subscribed: true,
    logo: AssetImage('assets/images/logos/WIRTSCHAFT.png'),
  ),
  TLSource(
    name: "Systembenachrichtigungen",
    id: 10,
    description: "Systembenachrichtigungen",
    subscribed: true,
    logo: AssetImage('assets/images/logos/MNI.png'),
  ),
];
