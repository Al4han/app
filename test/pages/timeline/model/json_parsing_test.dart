/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:tc_frontend/common/tc_utility_functions.dart';
import 'package:tc_frontend/pages/timeline/model/timeline_entry.dart';
import 'package:tc_frontend/pages/timeline/model/timeline_source.dart';

import 'json_strings.dart' as surrogate;

void main() {
  group('timeline json parsing tests', () {
    late String parsedJSONResponseAllEntries;
    late String parsedSourcesJSON;
    late String pinnedEntriesJSON;
    late List<TLEntry> expectedEntries;
    late List<TLEntry> expectedPinnedEntries;
    late List<TLSource> expectedSources;
    setUp(() {
      parsedJSONResponseAllEntries =
          surrogate.parsedJSONResponseAllEntriesIsNotLast;
      parsedSourcesJSON = surrogate.parsedSourcesJSON;
      expectedEntries = surrogate.expectedEntries.data;
      expectedSources = surrogate.expectedSources;
      pinnedEntriesJSON = surrogate.pinnedEntriesJSON;
      expectedPinnedEntries = surrogate.expectedPinnedEntries.data;
    });
    test('parses json entry input correctly', () {
      final data = Map<String, dynamic>.from(
        json.decode(parsedJSONResponseAllEntries) ?? {},
      );
      var dataMap = buildEntitiesFromList(data['content'], TLEntry.fromJson);
      expect(dataMap, expectedEntries);
    });
    test('parses pinned entries correctly', () {
      var dataList = buildEntitiesFromList(
        json.decode(pinnedEntriesJSON),
        TLEntry.fromJson,
      );
      expect(dataList, expectedPinnedEntries);
    });
    test('parses source info correctly', () {
      var dataList = buildEntitiesFromList(
        json.decode(parsedSourcesJSON),
        TLSource.fromJson,
      );
      expect(dataList, expectedSources);
    });
  });
}
