/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter_test/flutter_test.dart';
import 'package:tc_frontend/common/constants/api_constants.dart';
import 'package:tc_frontend/pages/timeline/helper/json_helper.dart' as requests;

void main() {
  group('json request generator: getTLEntries()', () {
    test('getTLEntries with default values', () {
      expect(requests.getTLEntries(), '$baseUrl/timeline?page=0');
    });
    test('getTLEntries with [user] = 532 and [page] = 6', () {
      expect(requests.getTLEntries(page: 6), '$baseUrl/timeline?page=6');
    });
    test('getTLEntries with [user] = 23 and [limit] = 23', () {
      expect(requests.getTLEntries(), '$baseUrl/timeline?page=0');
    });
    test('getTLEntries with [user] = 2, [page] = 2 and [limit] = 5', () {
      expect(requests.getTLEntries(page: 2), '$baseUrl/timeline?page=2');
    });
    test('getTLEntries throws exception as expected with [page] = -1', () {
      expect(() => requests.getTLEntries(page: -1), throwsFormatException);
    });
    test('getTLEntries throws exception as expected with [page] = -12', () {
      expect(() => requests.getTLEntries(page: -12), throwsFormatException);
    });

    test('getFavedTLEntries returns correct string', () {
      expect(requests.getFavedTLEntries(), '$baseUrl/timeline/faved?page=0');
    });
    test('getHiddenTLEntries returns correct string as expected', () {
      expect(requests.getHiddenTLEntries(), '$baseUrl/timeline/hidden');
    });
    test('deleteAllHiddenStates returns correct string as expected', () {
      expect(requests.deleteAllHiddenStates(), '$baseUrl/timeline/hidden');
    });
    test('getUnpinnedTLEntries returns correct string as expected', () {
      expect(requests.getUnpinnedTLEntries(), '$baseUrl/timeline/pinned/unpin');
    });
    test('togglePinnedTLEntry returns correct string as expected', () {
      expect(requests.togglePinnedTLEntry(entryID: 7),
          '$baseUrl/timeline/pinned/unpin/7');
    });
    test('toggleTLSourceSubscription returns correct string as expected', () {
      expect(
          requests.toggleTLSourceSubscription(
              subscriptionType: 'SUBSCRIBED', sourceID: 5),
          '$baseUrl/timeline/sources/5/SUBSCRIBED');
    });
    test('getTLSources returns correct string as expected', () {
      expect(requests.getTLSources(), '$baseUrl/timeline/sources/settings');
    });
  });
}
