/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' show Response;
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tc_frontend/pages/timeline/bloc/entries/timeline_bloc.dart';
import 'package:tc_frontend/pages/timeline/helper/http_helper.dart';
import 'package:tc_frontend/pages/timeline/helper/json_helper.dart' as requests;
import 'package:tc_frontend/pages/timeline/model/timeline_entry.dart';
import 'package:tc_frontend/repositories/http/http_repository.dart';

import '../../model/json_strings.dart';
import 'timeline_bloc_test.mocks.dart';

@GenerateMocks([TEvent, Response, HttpRepository])
class MockStorage extends Mock implements HydratedStorage {
  @override
  Future<void> delete(String key) => Future<void>.delayed(
        Duration(microseconds: 1),
      );

  @override
  Future<void> clear() => Future<void>.delayed(
        Duration(microseconds: 1),
      );

  @override
  Future<void> write(String key, dynamic value) => Future<void>.delayed(
        Duration(microseconds: 1),
      );

  @override
  dynamic read(String key) {}
}

class MockBlocObserver extends Mock implements BlocObserver {
  /// Provide debugPrints on every bloc event.
  @override
  void onEvent(Bloc bloc, Object? event) {
    debugPrint("MockDelegate.onEvent: "
        "${bloc.toString()}: ${event?.toString() ?? ''}");
  }

  /// Provide debugPrints on every bloc transition.
  @override
  // ignore: strict_raw_type
  void onTransition(Bloc bloc, Transition transition) {
    debugPrint("MockDelegate.onTransition: "
        "${bloc.toString()}: {\n"
        "${transition.currentState.toString()}\n"
        "${transition.event.toString()}\n"
        "${transition.nextState.toString()}\n"
        "}");
  }

  /// Provide debugPrints on every bloc error and throw the error for catcher.
  @override
  // ignore: strict_raw_type
  void onError(BlocBase bloc, Object error, StackTrace stacktrace) {
    debugPrint("MockDelegate.onError: "
        "${bloc.toString()}: ${error.toString()}: "
        "${stacktrace.toString()}");
    // throw error;
  }
}

//Siehe https://github.com/felangel/hydrated_bloc/blob/master/test/hydrated_bloc_test.dart
void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  MockBlocObserver observer;
  MockStorage storage;

  setUp(() {
    observer = MockBlocObserver();
    Bloc.observer = observer;
    storage = MockStorage();
    HydratedBloc.storage = storage;
  });

  group('Timeline Bloc Test: General Behaviour', () {
    late MockHttpRepository httpRepo;
    //FakeTEvent badTimelineEvent;
    late TimelineBloc timelineBloc;
    BlocHttpHelper bhh;

    setUp(() async {
      SharedPreferences.setMockInitialValues(<String, int>{
        'timeline.lastShownEntry': 200,
      });
      httpRepo = MockHttpRepository();
      bhh = BlocHttpHelper(httpRepo: httpRepo);
      timelineBloc = TimelineBloc(thh: bhh);
      //badTimelineEvent = FakeTEvent();
    });

    tearDown(() async {
      httpRepo.close();
      timelineBloc.clear();
    });

    test('initial state is correct', () {
      expect(timelineBloc.state, TStateUninitialized());
    });

    test('close does not emit new states', () {
      expectLater(
        timelineBloc.stream,
        emitsInOrder([]),
      );
      timelineBloc.close();
    });

    test('Bloc returns [TimelineStateError] on server time out', () {
      when(httpRepo.read(anything)).thenThrow(
        TimeoutException("ServerTimedOut"),
      );
      expectLater(
        timelineBloc.stream,
        emitsInOrder([
          TStateError(error: "TimeoutException: ServerTimedOut"),
        ]),
      );
      timelineBloc.add(TEventFetchAll());
    });

    /* test('Bloc returns [TStateWarning] on bad timeline event', () {
      expectLater(
          timelineBloc,
          emitsInOrder([
            TStateUninitialized(),
            TStateWarning(
                warning: '******************\n'
                    '* Unexpected event: { Instance of \'FakeTEvent\' }\n'
                    '* THIS DEFINETLY SHOULD NOT HAPPEN\n'
                    '******************')
          ]));
      timelineBloc.add(badTimelineEvent);
    });*/
  });

  group('Timeline Bloc Test: TimelineEvents', () {
    late MockHttpRepository httpRepo;
    late TimelineBloc timelineBloc;
    BlocHttpHelper bhh;
    late MockResponse httpResponse1;
    late MockResponse httpResponse2;
    // FIXME
    //MockHttpResponse httpResponse3;

    setUp(() async {
      httpRepo = MockHttpRepository();
      httpResponse1 = MockResponse();
      httpResponse2 = MockResponse();
      // FIXME
      //httpResponse3 = MockHttpResponse();

      SharedPreferences.setMockInitialValues(<String, int>{
        'timeline.lastShownEntry': 44,
      });

      bhh = BlocHttpHelper(httpRepo: httpRepo);

      timelineBloc = TimelineBloc(thh: bhh);
    });

    tearDown(() async {
      httpRepo.close();
      //timelineBloc.clear();
    });

    test(
        'Bloc returns [TimelineStateError] '
        'on bad response to [TimelineEventFetchAll]', () {
      // TODO: provide message
      when(httpRepo.read(anything)).thenThrow(NotFoundException(''));

      expectLater(
        timelineBloc.stream,
        emitsInOrder([
          TStateError(error: "NotFoundException: { message: , uri: null }"),
        ]),
      );
      timelineBloc.add(TEventFetchAll());
    });

    test('Bloc returns [TStateLoaded] on [TEventFetchFaved]', () {
      when(httpResponse1.statusCode).thenReturn(200);
      when(httpResponse1.body).thenReturn(parsedJSONResponseFavedEntries);
      // FIXME Remove headers matcher once 'accept' header is handled by wrapper
      when(httpRepo.read(anything, headers: anyNamed('headers')))
          .thenAnswer((_) => Future.value(httpResponse1.body));

      expectLater(
          timelineBloc.stream,
          emitsInOrder([
            TStateLoaded(
              entries: expectedFavedEntries.copyWith(last: true),
              hasReachedMax: true,
            )
          ]));
      timelineBloc.add(TEventFetchFaved());
    });

    test(
        'Bloc returns [TimelineStateError] '
        'on [TimelineEventFetchFaved] on bad server response', () {
      // FIXME Remove headers matcher once 'accept' header is handled by wrapper
      // TODO: provide message
      when(httpRepo.read(anything, headers: anyNamed('headers')))
          .thenThrow(NotFoundException(''));
      expectLater(
        timelineBloc.stream,
        emitsInOrder([
          TStateError(error: "NotFoundException: { message: , uri: null }"),
        ]),
      );
      timelineBloc.add(TEventFetchFaved());
    });

    /*test('Bloc returns TStateLoaded on TEventFetchAll with two pages', () {
      when(httpResponse1.statusCode).thenReturn(200);
      when(httpResponse1.body)
          .thenReturn(parsedJSONResponseAllEntriesIsNotLast);

      when(httpResponse2.statusCode).thenReturn(200);
      when(httpResponse2.body).thenReturn(pinnedEntriesJSON);

      when(httpResponse3.statusCode).thenReturn(200);
      when(httpResponse3.body).thenReturn(parsedJSONResponseAllEntriesIsLast);

      // FIXME Remove headers matcher once 'accept' header is handled by wrapper
      when(httpRepo.read(anything, headers: anyNamed('headers')))
          .thenAnswer((_) => Future.value(httpResponse3.body));

      // FIXME Remove headers matcher once 'accept' header is handled by wrapper
      when(httpRepo.read(requests.getPinnedTLEntries(),
              headers: anyNamed('headers')))
          .thenAnswer((_) => Future.value(httpResponse2.body));

      // FIXME Remove headers matcher once 'accept' header is handled by wrapper
      when(httpRepo.read(requests.getTLEntries(), headers: anyNamed('headers')))
          .thenAnswer((_) => Future.value(httpResponse1.body));

      expectLater(
          timelineBloc,
          emitsInOrder([
            TStateUninitialized(),
            TStateLoaded(
              entries: expectedPinnedEntries + expectedEntries,
              hasReachedMax: false,
              idDatedContent: 44,
            ),
            TStateLoaded(
              entries: expectedPinnedEntries +
                  expectedEntries +
                  expectedEntriesLastPage,
              hasReachedMax: true,
              idDatedContent: 44,
            ),
          ]));

      timelineBloc.add(TEventFetchAll());
      timelineBloc.add(TEventFetchAll());
    });
*/
    test(
        'Bloc returns [TimelineStateLoaded] '
        'on [TimelineEventFetchAll] with single page', () async {
      when(httpResponse1.statusCode).thenReturn(200);
      when(httpResponse1.body).thenReturn(parsedJSONResponseAllEntriesIsLast);

      when(httpResponse2.statusCode).thenReturn(200);
      when(httpResponse2.body).thenReturn(pinnedEntriesJSON);
      // FIXME Remove headers matcher once 'accept' header is handled by wrapper
      when(httpRepo.read(anything, headers: anyNamed('headers')))
          .thenAnswer((_) => Future.value(httpResponse1.body));
      // FIXME Remove headers matcher once 'accept' header is handled by wrapper
      when(httpRepo.read(requests.getPinnedTLEntries(),
              headers: anyNamed('headers')))
          .thenAnswer((_) => Future.value(httpResponse2.body));
      expectLater(
          timelineBloc.stream,
          emitsInOrder([
            TStateLoaded(
              entries: expectedPinnedEntries
                  .add(expectedEntriesLastPage)
                  .copyWith(last: true),
              hasReachedMax: true,
              idDatedContent: 44,
            ),
          ]));
      timelineBloc.add(TEventFetchAll());
    });
  });

  group('TimelineState tests', () {
    test('Getter \'isEmpty\' returns true if \'entries\' is empty', () {
      var tlStateLoaded = TStateLoaded(
        entries: TLEntryList(data: [], last: true),
        hasReachedMax: false,
      );
      expect(tlStateLoaded.isEmpty, equals(true));
    });

    test('Getter \'isEmpty\' returns false if \'entries\' is filled', () {
      var tlStateLoaded = TStateLoaded(
        entries: expectedEntries,
        hasReachedMax: false,
      );
      expect(tlStateLoaded.isEmpty, equals(false));
    });
  });
}
