/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:convert';

import 'package:tc_frontend/common/tc_utility_functions.dart';
import 'package:tc_frontend/modules/sport/model/exercise.dart';

const String cachedExerciseJson = """
[
    {
        "name": {
            "de": "Durchatmen",
            "en": "Breathe deeply"
        },
        "id": 0,
        "imageUrl": "https://example.org/example.jpg",
        "imageAltText": {
            "de": "Platzhalter",
            "en": "Placeholder"
        },
        "category": "mobility",
        "tags": [
            "anything",
            "shoulder",
            "neck",
            "biceps",
            "legs",
            "back"
        ],
        "mode": "none",
        "repetition": 8,
        "showDuration": true,
        "singleDuration": 1000,
        "totalDuration": 60000,
        "sets": 3,
        "instructions": {
            "effect": {
                "de": "Entspannung, Schultermobilisation",
                "en": "Relaxation, shoulder mobilization"
            },
            "startingPosition": {
                "de": "aufrechter Stand, Arme hängen locker seitlich herunter",
                "en": "upright stance, arms hanging loosely down at the sides"
            },
            "motionSequence": {
                "de": "Arme vor dem Körper nach oben führen und dabei einatmen. Arme über dem Kopf ausstrecken und seitlich am Körper wieder nach unten führen und dabei ausatmen.",
                "en": "Bring arms up in front of the body while inhaling. Extend arms above head and bring them back down to the side of the body while exhaling."
            },
            "repetitions": {
                "de": "8x durchatmen",
                "en": "Breathe deeply 8 times"
            }
        }
    },
    {
        "name": {
            "de": "Schultern kreisen",
            "en": "Circle shoulders"
        },
        "id": 1,
        "imageUrl": "https://example.org/example.jpg",
        "imageAltText": {
            "de": "Platzhalter",
            "en": "Placeholder"
        },
        "category": "mobility",
        "tags": [
            "anything",
            "shoulder",
            "neck",
            "biceps",
            "legs",
            "back"
        ],
        "mode": "none",
        "repetition": 8,
        "showDuration": true,
        "singleDuration": 1000,
        "totalDuration": 60000,
        "sets": 3,
        "instructions": {
            "effect": {
                "de": "Mobilisation und Lockerung der Schultern",
                "en": "Mobilization and loosening of the shoulders"
            },
            "startingPosition": {
                "de": "aufrechter Stand, Füße hüftbreit aufstellen",
                "en": "upright stance, place feet hip-width apart"
            },
            "motionSequence": {
                "de": "Schultern zu den Ohren ziehen und anschließend wieder nach unten drücken",
                "en": "Pull shoulders to ears and then push them back down"
            },
            "repetitions": {
                "de": "3x 8 Wiederholungen",
                "en": "3x 8 repetitions"
            },
            "variation": {
                "de": "auch im Sitzen möglich",
                "en": "also possible in a sitting position"
            }
        }
    }
]
""";

const String serverProvidedExerciseJson = """
[
    {
        "name": {
            "de": "Durchatmen",
            "en": "Breathe deeply"
        },
        "id": 0,
        "imageUrl": "https://example.org/example.jpg",
        "imageAltText": {
            "de": "Platzhalter",
            "en": "Placeholder"
        },
        "category": "mobility",
        "tags": [
            "anything",
            "shoulder",
            "neck",
            "biceps",
            "legs",
            "back"
        ],
        "mode": "none",
        "repetition": 8,
        "showDuration": true,
        "singleDuration": 1000,
        "totalDuration": 60000,
        "sets": 3,
        "instructions": {
            "effect": {
                "de": "Entspannung, Schultermobilisation",
                "en": "Relaxation, shoulder mobilization"
            },
            "startingPosition": {
                "de": "aufrechter Stand, Arme hängen locker seitlich herunter",
                "en": "upright stance, arms hanging loosely down at the sides"
            },
            "motionSequence": {
                "de": "Arme vor dem Körper nach oben führen und dabei einatmen. Arme über dem Kopf ausstrecken und seitlich am Körper wieder nach unten führen und dabei ausatmen.",
                "en": "Bring arms up in front of the body while inhaling. Extend arms above head and bring them back down to the side of the body while exhaling."
            },
            "repetitions": {
                "de": "8x durchatmen",
                "en": "Breathe deeply 8 times"
            }
        }
    },
    {
        "name": {
            "de": "Schultern heben & senken",
            "en": "Lift & lower shoulders"
        },
        "id": 1,
        "imageUrl": "https://example.org/example.jpg",
        "imageAltText": {
            "de": "Platzhalter",
            "en": "Placeholder"
        },
        "category": "mobility",
        "tags": [
            "anything",
            "shoulder",
            "neck",
            "biceps",
            "legs",
            "back"
        ],
        "modes": "none",
        "repetition": 8,
        "showDuration": true,
        "singleDuration": 1000,
        "totalDuration": 60000,
        "sets": 3,
        "instructions": {
            "effect": {
                "de": "Mobilisation und Lockerung der Schultern",
                "en": "Mobilization and loosening of the shoulders"
            },
            "startingPosition": {
                "de": "aufrechter Stand, Füße hüftbreit aufstellen",
                "en": "upright stance, place feet hip-width apart"
            },
            "motionSequence": {
                "de": "Schultern zu den Ohren ziehen und anschließend wieder nach unten drücken",
                "en": "Pull shoulders to ears and then push back down"
            },
            "repetitions": {
                "de": "3x 8 Wiederholungen",
                "en": "3x 8 repetitions"
            },
            "variation": {
                "de": "auch im Sitzen möglich",
                "en": "also possible in a sitting position"
            }
        }
    },
    {
        "name": {
            "de": "Schultern kreisen",
            "en": "Circle shoulders"
        },
        "id": 2,
        "imageUrl": "https://example.org/example.jpg",
        "imageAltText": {
            "de": "Platzhalter",
            "en": "Placeholder"
        },
        "category": "mobility",
        "tags": [
            "anything",
            "shoulder",
            "neck",
            "biceps",
            "legs",
            "back"
        ],
        "mode": "none",
        "repetition": 8,
        "showDuration": true,
        "singleDuration": 1000,
        "totalDuration": 60000,
        "sets": 3,
        "instructions": {
            "effect": {
                "de": "Mobilisation und Lockerung der Schultern",
                "en": "Mobilization and loosening of the shoulders"
            },
            "startingPosition": {
                "de": "aufrechter Stand, Füße hüftbreit aufstellen",
                "en": "upright stance, place feet hip-width apart"
            },
            "motionSequence": {
                "de": "Schultern zu den Ohren ziehen und anschließend wieder nach unten drücken",
                "en": "Pull shoulders to ears and then push them back down"
            },
            "repetitions": {
                "de": "3x 8 Wiederholungen",
                "en": "3x 8 repetitions"
            },
            "variation": {
                "de": "auch im Sitzen möglich",
                "en": "also possible in a sitting position"
            }
        }
    },
    {
        "name": {
            "de": "Schultern vor & zurück ziehen",
            "en": "Pull shoulders back & forth"
        },
        "id": 3,
        "imageUrl": "https://example.org/example.jpg",
        "imageAltText": {
            "de": "Platzhalter",
            "en": "Placeholder"
        },
        "category": "mobility",
        "tags": [
            "anything",
            "shoulder",
            "neck",
            "biceps",
            "legs",
            "back"
        ],
        "mode": "none",
        "repetition": 8,
        "showDuration": true,
        "singleDuration": 1000,
        "totalDuration": 60000,
        "sets": 3,
        "instructions": {
            "effect": {
                "de": "Mobilisation und Lockerung der Schultern",
                "en": "Mobilization and loosening of the shoulders"
            },
            "startingPosition": {
                "de": "aufrechter Stand, Füße hüftbreit aufstellen",
                "en": "upright stance, place feet hip-width apart"
            },
            "motionSequence": {
                "de": "Schultern nach vorne und anschließend nach hinten ziehen, die Schulterblätter nähern sich dabei an",
                "en": "Pull the shoulders forward and then back, bringing the shoulder blades closer together."
            },
            "repetitions": {
                "de": "3x 8 Wiederholungen",
                "en": "3x 8 repetitions"
            },
            "variation": {
                "de": "auch im Sitzen möglich",
                "en": "also possible in a sitting position"
            }
        }
    },
    {
        "name": {
            "de": "Fuß-Gym",
            "en": "Foot Gym"
        },
        "id": 4,
        "imageUrl": "https://example.org/example.jpg",
        "imageAltText": {
            "de": "Platzhalter",
            "en": "Placeholder"
        },
        "category": "mobility",
        "tags": [
            "anything",
            "shoulder",
            "neck",
            "biceps",
            "legs",
            "back"
        ],
        "mode": "none",
        "repetition": 8,
        "showDuration": true,
        "singleDuration": 1000,
        "totalDuration": 60000,
        "sets": 3,
        "instructions": {
            "effect": {
                "de": "Mobilisation der Fußgelenke",
                "en": "Mobilization of the ankles"
            },
            "startingPosition": {
                "de": "aufrecht sitzen, linkes Bein leicht anheben",
                "en": "sit upright, lift left leg slightly"
            },
            "motionSequence": {
                "de": "linken Fuß in der Luft kreisen, dann Bein/ Fuß wechseln",
                "en": "circle left foot in the air, then change leg/foot"
            },
            "repetitions": {
                "de": "3x je 4 Kreise rechts/ links (pro Seite)",
                "en": "3x each 4 circles right/ left (per side)"
            },
            "variation": {
                "de": "die Fußspitze Richtung Knie ziehen, dann den Fuß wieder strecken",
                "en": "pull the tip of the foot towards the knee, then stretch the foot again"
            }
        }
    }
]""";

final List<Exercise> cachedExerciseList = buildEntitiesFromList(
  jsonDecode(cachedExerciseJson),
  Exercise.fromJson,
).toList(growable: false);

final List<Exercise> serverProvidedExerciseList = buildEntitiesFromList(
  jsonDecode(serverProvidedExerciseJson),
  Exercise.fromJson,
).toList(growable: false);
