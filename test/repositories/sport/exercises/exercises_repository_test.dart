/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tc_frontend/common/assets_adapter.dart';
import 'package:tc_frontend/common/tc_utility_functions.dart';
import 'package:tc_frontend/modules/sport/model/exercise.dart';
import 'package:tc_frontend/repositories/http/http_repository.dart';
import 'package:tc_frontend/repositories/sport/exercises/exercises_repository.dart';
import 'package:tc_frontend/repositories/sport/exercises/exercises_repository_constants.dart';

import '../../../shared/mock_repo.dart';
import 'exercises_repository_assets.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  late MockHttpRepo httpRepo;

  late List<Exercise> expectedExercises;
  late MockHttpResponse httpResponse;

  late String expectedExerciseString;

  late DateTime expiredTimestamp;

  setUp(() async {
    expiredTimestamp = DateTime.now().subtract(Duration(seconds: 61));
    httpRepo = MockHttpRepo();
    httpResponse = MockHttpResponse();

    HttpRepository.mockRepo = httpRepo;

    return rootBundle
        .loadString(
          AssetAdapter.exercisesJson(),
          cache: true,
        )
        .then<String>((value) => expectedExerciseString = value)
        .then(
          (value) => expectedExercises =
              buildEntitiesFromList(jsonDecode(value), Exercise.fromJson),
        );
  });

  tearDown(() async {
    httpResponse = MockHttpResponse();
    HttpRepository.mockRepo = MockHttpRepo();
    return await SharedPreferences.getInstance().then((value) => value.clear());
  });

  /*test('fetch data from cache if time threshold is not yet reached', () async
   {
    when(httpResponse.statusCode).thenReturn(200);
    when(httpResponse.bodyBytes)
        .thenReturn(utf8.encode(serverProvidedExerciseJson));
    when(httpRepo.get(anything, secured: false))
        .thenAnswer((_) => Future.value(httpResponse));

    var nonExpiredTimestamp = DateTime.now().subtract(Duration(seconds: 5));
    SharedPreferences.setMockInitialValues({
      sportUpdateTimestampKey: nonExpiredTimestamp.toIso8601String(),
      sportExercisePrefsKey: cachedExerciseJson
    });

    return await expectLater(
            ExerciseRepository().getTimestamp().then((t) {
              expect(t, isNotNull);
              return t;
            }).then((value) => value.millisecondsSinceEpoch),
            completion(equals(nonExpiredTimestamp.millisecondsSinceEpoch)))
        .whenComplete(() => expectLater(ExerciseRepository().getExercises(),
            completion(unorderedEquals(cachedExerciseList))))
        .whenComplete(() => expect(
            ExerciseRepository()
                .getTimestamp()
                .then((value) => value.millisecondsSinceEpoch),
            completion(equals(nonExpiredTimestamp.millisecondsSinceEpoch))));
  }, skip: true);

   */

  // test('fetch data from server with empty cache ', () async {
  //   when(httpResponse.bodyBytes)
  //       .thenReturn(utf8.encode(serverProvidedExerciseJson));
  //   when(httpResponse.statusCode).thenReturn(200);
  //   when(httpRepo.get(anything, secured: false))
  //       .thenAnswer((_) => Future.value(httpResponse));
  //   Future<List<int>> actualSerializedExercises;
  //   Future<String> actualTimestamp;

  //   var actualExerciseList = ExerciseRepository()
  //       .getExercises()
  //       .then((value) => value.map((e) => e.hashCode)
  //.toList(growable: false))
  //       .whenComplete(() => actualSerializedExercises =
  //           SharedPreferences.getInstance()
  //               .then<String>((pr) => pr.getString(sportExercisePrefsKey))
  //               .then((v) => (jsonDecode(v) as List<dynamic>)
  //                   .map((e) => Exercise.fromJson(e)))
  //               .then((value) =>
  //                   value.map((e) => e.hashCode).toList(growable: false)))
  //       .whenComplete(() => actualTimestamp = SharedPreferences.getInstance()
  //           .then((prefs) => prefs.getString(sportUpdateTimestampKey)));

  //   var serverListMatcher = completion(equals(serverProvidedExerciseList
  //       .map((e) => e.hashCode)
  //       .toList(growable: false)));

  //   return await expectLater(actualExerciseList, serverListMatcher)
  //       .whenComplete(
  //           () => expectLater(actualSerializedExercises, serverListMatcher))
  //       .whenComplete(
  //           () => expectLater(actualTimestamp, completion(isNotNull)));
  // });

  test('try to fetch data without internet connection or cache', () {
    when(httpResponse.statusCode).thenReturn(404);
    when(httpRepo.get(anything, secured: false))
        .thenAnswer((_) => Future.value(httpResponse));

    expectLater(ExerciseRepository().fetchDataFromServer(),
            completion(equals(expectedExerciseString)))
        .whenComplete(() => expect(
            SharedPreferences.getInstance()
                .then((pr) => pr.getString(sportExercisePrefsKey)),
            completion(equals(expectedExerciseString))));
  }, skip: true);

  test('fetch data from server with expired cache', () async {
    when(httpResponse.bodyBytes).thenReturn(
      Uint8List.fromList(utf8.encode(serverProvidedExerciseJson)),
    );
    when(httpResponse.statusCode).thenReturn(200);
    when(httpRepo.get(anything, secured: false))
        .thenAnswer((_) => Future.value(httpResponse));

    SharedPreferences.setMockInitialValues({
      sportUpdateTimestampKey: expiredTimestamp.toIso8601String(),
      sportExercisePrefsKey: cachedExerciseJson
    });

    return await expectLater(
            ExerciseRepository().fetchDataFromServer().then<List<int>>((json) =>
                buildEntitiesFromList(jsonDecode(json), Exercise.fromJson)
                    .map((e) => e.hashCode)
                    .toList(growable: false)),
            completion(unorderedMatches(serverProvidedExerciseList
                .map((e) => e.hashCode)
                .toList(growable: false))))
        .whenComplete(() => expectLater(
            SharedPreferences.getInstance()
                .then((pr) => pr.getString(sportExercisePrefsKey)),
            completion(equals(serverProvidedExerciseJson))))
        .whenComplete(() => expectLater(
              SharedPreferences.getInstance()
                  .then((pr) => pr.getString(sportUpdateTimestampKey))
                  .then<DateTime>((str) => DateTime.parse(str ?? ''))
                  .then<int>((d) => d.millisecondsSinceEpoch),
              completion(greaterThan(expiredTimestamp.millisecondsSinceEpoch)),
            ));
  }, skip: true);

  test('fetch data from fallback without cached data', () {
    when(httpResponse.statusCode).thenReturn(404);
    when(httpRepo.get(anything, secured: false))
        .thenAnswer((_) => Future.value(httpResponse));
    expectLater(ExerciseRepository().getExercises(),
        completion(equals(expectedExercises)));
  }, skip: true);
}
