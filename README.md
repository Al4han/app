# tinyCampus App (Android & iOS)

## Features

- Homescreen
- Timeline
- Profile with unlockable badges
- Dark mode (coming soon!)
- i18n support (German and English)
- Authentication (guest mode and verified users)
- Automatic error reporting via [Sentry](https://sentry.io)
  (opt-in, anonymous and hosted on our servers)
- Modules:
  - Business Cards (currently inactive)
  - Cafeteria Menu
  - Important Links
  - Practice Phase (in cooperation with FB Wirtschaftsingenieurwesen)
  - Questions and Answers
  - Semester Fee Reminder
  - Zentrale Studienberatung

## Build instructions

### Prepare `.env` files

It is recommended to keep separate `.env` files for local dev and production
builds in a directory named `.env/` at the root of the project directory and
create symlinks into the `assets/env` directory like this:

```sh
$ mkdir .env
$ cp assets/env/example/.env.dev.example .env/.env.dev
$ ln -s .env/.env.dev assets/env/.env

# (Optional) Only needed for production builds
$ cp assets/env/example/.env.production.example .env/.env.production
```

Please note that the provided build script (see ["How to build"](#how-to-build))
relies on the exact locations of these files. For detailed information about the
available environment variables see
["Environment Variables"](#environment-variables) below.

### How to build

**Local development:** Fetch the dependencies and run the app on your
preferred virtual or physical device:

```sh
$ flutter pub get
$ flutter run
```

**Production:** It is recommended to use the provided shell script under
`scripts/build-release.sh`. The script automatically symlinks the correct
production `.env` file, performs a clean build and restores the `.env` file for
local dev builds afterwards.

```sh
# Android
$ scripts/build-release.sh appbundle

# iOS
$ scripts/build-release.sh ios
```

### Environment variables

Suggested environment variables for `.env.dev`:

- `BLOC_DEBUG`: Filter debug prints by Bloc (see [below](#bloc-debug-print-filtering))
- `SENTRY_DSN_DEBUG`: Sentry DSN for local dev builds in debug mode
- `SENTRY_DSN_RELEASE`: Sentry DSN for local dev builds in release mode

Suggested environment variables for `.env.production`:

- `BLOC_DEBUG`: Set to `{}` to disable Bloc debug prints (see [below](#bloc-debug-print-filtering))
- `OAUTH2_CLIENT_ID`: Your OAuth2 client ID (Default: `client`)
- `OAUTH2_CLIENT_SECRET`: Your OAuth2 client secret (Default: `secret`)
- `SENTRY_DSN_RELEASE`: Sentry DSN for store/production builds

### Bloc debug print filtering

Unclutter your debug logs by specifying the Blocs and events for which you want
to receive debug prints using JSON syntax:

```sh
# Example 1
# Disable all Bloc debug prints (default)
BLOC_DEBUG={}

# Example 2
# Custom settings:
# - Print all events for AuthBloc
# - Only print onEvent and onTransition events for UserBloc
BLOC_DEBUG={"AuthBloc": true, "UserBloc": {"onEvent": true, "onTransition": true}}

# Example 3
# Disable filter, i.e. print everything
BLOC_DEBUG=
```

### Sentry

If you want to use [Sentry](https://sentry.io) to collect bug reports, you need
to specify the DSNs via the `SENTRY_DSN_DEBUG` or `SENTRY_DSN_RELEASE`
environment variables. 

## Licence

tinyCampus is licenced under
[EUPL](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12) version
1.2 or later.

The name "tinyCampus" and the tinyCapmus logo are trademarked by
[Technische Hochschule Mittelhessen](https://thm.de).
