// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'status.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension StatusCopyWith on Status {
  Status copyWith({
    bool? blockApp,
    List<StatusNotification>? entries,
    bool? hideHeaderLogo,
    bool? hideNotificationIfUpdated,
    String? latestAndroidVersion,
    String? latestIosVersion,
    String? minimumVersionRequired,
  }) {
    return Status(
      blockApp: blockApp ?? this.blockApp,
      entries: entries ?? this.entries,
      hideHeaderLogo: hideHeaderLogo ?? this.hideHeaderLogo,
      hideNotificationIfUpdated:
          hideNotificationIfUpdated ?? this.hideNotificationIfUpdated,
      latestAndroidVersion: latestAndroidVersion ?? this.latestAndroidVersion,
      latestIosVersion: latestIosVersion ?? this.latestIosVersion,
      minimumVersionRequired:
          minimumVersionRequired ?? this.minimumVersionRequired,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Status _$StatusFromJson(Map<String, dynamic> json) => Status(
      blockApp: json['blockApp'] as bool? ?? false,
      hideNotificationIfUpdated:
          json['hideNotificationIfUpdated'] as bool? ?? false,
      hideHeaderLogo: json['hideHeaderLogo'] as bool? ?? false,
      latestAndroidVersion: json['latestAndroidVersion'] as String? ?? "0.0.0",
      latestIosVersion: json['latestIosVersion'] as String? ?? "0.0.0",
      minimumVersionRequired:
          json['minimumVersionRequired'] as String? ?? "0.0.0",
      entries: (json['entries'] as List<dynamic>?)
              ?.map(
                  (e) => StatusNotification.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
    );

Map<String, dynamic> _$StatusToJson(Status instance) => <String, dynamic>{
      'blockApp': instance.blockApp,
      'hideNotificationIfUpdated': instance.hideNotificationIfUpdated,
      'hideHeaderLogo': instance.hideHeaderLogo,
      'latestAndroidVersion': instance.latestAndroidVersion,
      'latestIosVersion': instance.latestIosVersion,
      'minimumVersionRequired': instance.minimumVersionRequired,
      'entries': instance.entries.map((e) => e.toJson()).toList(),
    };
