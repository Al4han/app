// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'status_notification.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension StatusNotificationCopyWith on StatusNotification {
  StatusNotification copyWith({
    Map<String, String>? body,
    Map<String, String>? title,
  }) {
    return StatusNotification(
      body: body ?? this.body,
      title: title ?? this.title,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StatusNotification _$StatusNotificationFromJson(Map json) => StatusNotification(
      body: (json['body'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
      title: (json['title'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
    );

Map<String, dynamic> _$StatusNotificationToJson(StatusNotification instance) =>
    <String, dynamic>{
      'title': instance.title,
      'body': instance.body,
    };
