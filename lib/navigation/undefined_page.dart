/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

/// Page constructed when `router.dart` encounters an unknown route name.
class UndefinedPage extends StatelessWidget {
  /// Name of the unknown route.
  final String name;

  UndefinedPage({
    Key? key,
    String? name,
  })  : name = name ?? "Undefined",
        super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                'Route for:\n'
                '"$name"\n'
                'is not defined',
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    ?.copyWith(fontWeight: FontWeight.bold),
              ),
              if (!kReleaseMode)
                Text(
                  'Please add the path constant at:\n'
                  '"lib/common/constants/routing_constants.dart"\n'
                  'and generator function at:\n'
                  '"lib/navigation/router.dart".',
                  style: Theme.of(context)
                      .textTheme
                      .headline6
                      ?.copyWith(color: Colors.red),
                ),
            ],
          ),
        ),
      );
}
