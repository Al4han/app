/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../common/tinycampus_icons.dart';
import '../../pages/home/home_page.dart';
import '../../pages/profile/profile_page.dart';
import '../../pages/settings/settings_page.dart';
import '../../pages/timeline/ui/timeline_page.dart';

/// Available Tabs in BottomNavigation
enum TabItem {
  home,
  timeline,
  profile,
  settings,
}

// TODO: Cleanup
/// Helper class to better differentiate between Tabs
// TODO: use inheritance instead maybe?
abstract class TabHelper {
  static String description(TabItem tabItem) {
    switch (tabItem) {
      case TabItem.home:
        return 'home';
      case TabItem.timeline:
        return 'timeline';
      case TabItem.profile:
        return 'profile';
      case TabItem.settings:
        return 'settings';
      default:
        return '';
    }
  }

  static IconData icon(TabItem tabItem) {
    switch (tabItem) {
      case TabItem.home:
        return TinyCampusIcons.home;
      case TabItem.timeline:
        return TinyCampusIcons.timeline;
      case TabItem.profile:
        return Icons.account_circle;
      case TabItem.settings:
        return TinyCampusIcons.settings;
      default:
        return Icons.adb;
    }
  }

  static WidgetBuilder route(TabItem tabItem) {
    switch (tabItem) {
      case TabItem.home:
        return (context) => HomePage();
      case TabItem.timeline:
        return (context) => TimelinePage(key: ValueKey('timelineMainPage'));
      case TabItem.profile:
        return (context) => ProfilePage();
      case TabItem.settings:
        return (context) => SettingsPage();
      default:
        return (context) => HomePage();
    }
  }
}
