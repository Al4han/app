/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../common/tc_markdown_stylesheet.dart';

class BrandInfoPage extends StatelessWidget {
  const BrandInfoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: I18nText('settings.brand_info'),
        ),
        backgroundColor: Theme.of(context).primaryColor,
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                  bottom: 18.0, top: 24.0, left: 16, right: 16),
              child: Container(
                height: 140,
                child: Image.asset(
                  "assets/images/tinycampus/logofont_block_default.png",
                  fit: BoxFit.fill,
                ),
                alignment: Alignment.center,
              ),
            ),
            Divider(
              indent: 15,
              endIndent: 15,
            ),
            Material(
              color: Theme.of(context).primaryColor,
              child: ListTile(
                onTap: () => _launchUrl(FlutterI18n.translate(
                    context, 'settings.brand_info_page.url')),
                title: I18nText('settings.brand_info_page.url_description'),
                subtitle: I18nText('settings.learn_more'),
                trailing: Icon(Icons.chevron_right),
              ),
            ),
            Divider(
              indent: 15,
              endIndent: 15,
            ),
            Expanded(
              child: Markdown(
                data: FlutterI18n.translate(
                  context,
                  'settings.brand_info_page.markdown_body',
                ),
                styleSheet: TinyCampusMarkdownStylesheet(context),
                physics: BouncingScrollPhysics(),
              ),
            ),
          ],
        ),
      );

  Future<void> _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
