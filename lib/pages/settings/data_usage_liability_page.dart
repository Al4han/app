/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../common/tc_markdown_stylesheet.dart';

class DataUsageLiabilityPage extends StatelessWidget {
  const DataUsageLiabilityPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: I18nText('settings.data_usage_and_liability'),
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        body: Column(
          children: [
            Expanded(
              child: Card(
                color: Theme.of(context).primaryColor,
                child: Markdown(
                  onTapLink: (_, url, __) async {
                    if (url == null) return;

                    if (!url.toLowerCase().startsWith(RegExp(r'^https?://'))) {
                      url = 'https://$url';
                    }

                    if (await canLaunch(url)) {
                      await launch(url);
                    }
                  },
                  data: FlutterI18n.translate(
                    context,
                    'settings.data_usage_and_liability_page.markdown_body',
                  ),
                  styleSheet: TinyCampusMarkdownStylesheet(context),
                  physics: BouncingScrollPhysics(),
                ),
              ),
            ),
          ],
        ),
      );
}
