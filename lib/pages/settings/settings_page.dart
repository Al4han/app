/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:dynamic_themes/dynamic_themes.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wakelock/wakelock.dart';

import '../../common/constants/api_constants.dart';
import '../../common/constants/localization_constants.dart'
    show overrideLanguage;
import '../../common/constants/routing_constants.dart';
import '../../common/tc_theme.dart';
import '../../localization/fallback_loaders.dart';
import 'account_settings_widget.dart';
import 'language_widget.dart';
import 'theme/settings_page_theme.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  late Future<SharedPreferences> _prefs;
  bool acceptErrorReporting = false;
  bool dialogMode = false;
  final int forceDebugModeThreshold = 17;
  int forceDebugModeValue = 0;
  bool forceDebugMode = false;

  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  @override
  void initState() {
    super.initState();
    _prefs = SharedPreferences.getInstance();
    _initPackageInfo();
    _initErrorReporting();
    _initReportMode();
    _initForceDebugMode();
  }

  Future<void> _initErrorReporting() async {
    var sp = await _prefs;
    // TODO: maybe add ? operators for optimization
    acceptErrorReporting = sp.getBool('accept_reporting') ?? false;
    setState(() {});
  }

  Future<void> _initReportMode() async {
    var sp = await _prefs;
    // TODO: maybe add ? operators for optimization
    dialogMode = sp.getBool('dialog_mode') ?? false;
    setState(() {});
  }

  Future<void> _initForceDebugMode() async {
    var sp = await _prefs;
    // TODO: maybe add ? operators for optimization
    forceDebugMode = sp.getBool('force_debug_mode') ?? false;
    setState(() {});
  }

  Future<void> _forceDebugModeStep() async {
    setState(() {
      if (++forceDebugModeValue >= forceDebugModeThreshold) {
        setForceDebugMode(value: true);
        forceDebugMode = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          title: I18nText('settings.title'),
        ),
        body: Container(
          child: listBuilder(context, getSettingsListTiles()),
        ),
      );

  Future<void> setErrorReporting({required bool value}) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool('accept_reporting', value);
  }

  Future<void> setErrorReportMode({required bool value}) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool('dialog_mode', value);
  }

  Future<void> setForceDebugMode({required bool value}) async {
    forceDebugMode = false;
    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool('force_debug_mode', value);
  }

  List<Widget> getSettingsListTiles() {
    var buttonList = <Widget>[];

    buttonList.add(
      Divider(
        color: Colors.transparent,
        height: 1,
      ),
    );

    I18nText currentLanguage;
    if (FlutterI18n.currentLocale(context).toString() == 'en') {
      currentLanguage = I18nText('settings.languages.english');
    } else {
      currentLanguage = I18nText('settings.languages.german');
    }

    // Language
    buttonList.add(
      Material(
        color: Theme.of(context).primaryColor,
        child: Column(
          children: <Widget>[
            ListTile(
              title: I18nText(
                'settings.language_settings',
                child: Text(
                  "Spracheinstellungen",
                  style: Theme.of(context).textTheme.headline3,
                ),
              ),
            ),
            ListTile(
              leading: LanguageWidget(
                FlutterI18n.currentLocale(context)
                    .toString()
                    .toUpperCase()
                    .substring(0, 2),
              ),
              title: currentLanguage,
              subtitle: I18nText(
                'settings.tap_change',
                child: Text(
                  '',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
              ),
              onTap: () => Navigator.pushNamed(context, settingsLanguageRoute)
                  .then((context) => setState(() {})),
            ),
          ],
        ),
      ),
    );

    // Account settings
    buttonList.add(
      Material(
        color: Theme.of(context).primaryColor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              title: Text(
                FlutterI18n.translate(context, "settings.account_settings"),
                style: Theme.of(context).textTheme.headline3,
              ),
            ),
            AccountSettingsWidget(),
          ],
        ),
      ),
    );

    // Theme change, remove !kReleaseMode when ready
    if (!kReleaseMode) {
      buttonList.add(
        Material(
          color: Theme.of(context).primaryColor,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListTile(
                title: I18nText(
                  'settings.display_settings',
                  child: Text(
                    "Anzeigeeinstellungen",
                    style: Theme.of(context).textTheme.headline3,
                  ),
                ),
              ),
              SettingsPageTheme(),
            ],
          ),
        ),
      );
    }

    // Games
    buttonList.add(
      Material(
        color: Theme.of(context).primaryColor,
        child: Column(
          children: [
            ListTile(
              title: Text(
                FlutterI18n.translate(context, 'settings.games.title'),
                style: Theme.of(context).textTheme.headline3,
              ),
            ),
            Ink(
              color: Theme.of(context).primaryColor,
              child: ListTile(
                onTap: () {
                  Navigator.of(context).pushNamed(offlineGame);
                },
                title: Text(
                  FlutterI18n.translate(context, "login.bouncy_ball"),
                ),
              ),
            ),
            Container(height: 16),
          ],
        ),
      ),
    );

    // Bug Reporting
    buttonList.add(
      Ink(
        color: Theme.of(context).primaryColor,
        child: Column(
          children: <Widget>[
            ListTile(
              onTap: _forceDebugModeStep,
              title: I18nText(
                'settings.error_reporting.title',
                child: Text(
                  "Error Reporting",
                  style: Theme.of(context).textTheme.headline3,
                ),
              ),
            ),
            Ink(
              color: Theme.of(context).primaryColor,
              child: SwitchListTile(
                  activeColor: CurrentTheme().tcDarkBlue,
                  title: I18nText(
                    'settings.error_reporting.description',
                    child: Text(
                      "Error Reporting",
                    ),
                  ),
                  value: acceptErrorReporting,
                  onChanged: (value) {
                    setState(() {
                      acceptErrorReporting = value;
                    });
                    setErrorReporting(value: value);
                  }),
            ),
            if (kReleaseMode && !forceDebugMode) Container(height: 16),
            if (kDebugMode || forceDebugMode)
              Divider(
                indent: 16,
                endIndent: 16,
              ),
            if (kDebugMode || forceDebugMode)
              Ink(
                color: Theme.of(context).primaryColor,
                child: SwitchListTile(
                    activeColor: CurrentTheme().tcDarkBlue,
                    title: I18nText(
                      'settings.error_reporting.show_dialogs',
                      child: Text(
                        "Error Reporting",
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                    // TODO: use ? operator here
                    value: dialogMode,
                    onChanged: acceptErrorReporting == true
                        ? (value) {
                            setState(() {
                              dialogMode = value;
                            });
                            setErrorReportMode(value: value);
                          }
                        : null),
              ),
            if (kDebugMode || forceDebugMode)
              Divider(
                indent: 16,
                endIndent: 16,
              ),
            if (kDebugMode || forceDebugMode)
              Ink(
                color: Theme.of(context).primaryColor,
                child: ListTile(
                  leading: Icon(
                    Icons.error,
                    color: Theme.of(context).iconTheme.color,
                  ),
                  title: I18nText(
                    'settings.error_reporting.throw_exception',
                    child: Text(
                      "Error Reporting",
                    ),
                  ),
                  onTap: () => throw Exception(),
                ),
              ),
            if (kDebugMode || forceDebugMode)
              Divider(
                indent: 16,
                endIndent: 16,
              ),
            if (kDebugMode || forceDebugMode)
              Ink(
                color: Theme.of(context).primaryColor,
                child: ListTile(
                  leading: Icon(
                    Icons.error,
                    color: Theme.of(context).iconTheme.color,
                  ),
                  title: I18nText(
                    'settings.error_reporting.throw_exception_loc',
                    child: Text(
                      "Error Reporting",
                    ),
                  ),
                  onTap: () => throw LocalizationException(
                    "purposely.wrong.key.DUMMY",
                    FlutterI18n.currentLocale(context)?.languageCode ?? "??",
                  ),
                ),
              ),
            if (forceDebugMode)
              Divider(
                indent: 16,
                endIndent: 16,
              ),
            if (forceDebugMode)
              Ink(
                color: Theme.of(context).primaryColor,
                child: ListTile(
                  leading: Icon(Icons.settings_backup_restore),
                  title: I18nText(
                    'settings.error_reporting.exit_forced_debug_mode',
                    child: Text(
                      "Error Reporting",
                    ),
                  ),
                  onTap: () => setState(
                    () {
                      forceDebugModeValue = 0;
                      setForceDebugMode(value: false);
                    },
                  ),
                ),
              ),
          ],
        ),
      ),
    );
    if (kDebugMode) {
      buttonList.add(Material(
        color: Theme.of(context).primaryColor,
        child: Column(
          children: <Widget>[
            ListTile(
              title: I18nText(
                'settings.debug',
                child: Text(
                  "Debug",
                  style: Theme.of(context).textTheme.headline3,
                ),
              ),
            ),
            FutureBuilder<bool>(
              initialData: false,
              future: Wakelock.enabled,
              builder: (context, snapshot) => SwitchListTile(
                title: I18nText('settings.wakelock'),
                value: snapshot.data ?? false,
                onChanged: (value) => setState(() {
                  Wakelock.toggle(enable: value);
                }),
              ),
            ),
            Divider(
              indent: 16,
              endIndent: 16,
            ),
            ListTile(
              title: Text(
                'Locale version',
              ),
              subtitle: Text(
                '${FlutterI18n.currentLocale(context)?.languageCode}'
                '-v${FlutterI18n.translate(
                  context,
                  '_version',
                )}',
              ),
            ),
          ],
        ),
      ));
    }

    // Über die App
    buttonList.add(Material(
      color: Theme.of(context).primaryColor,
      child: Column(
        children: <Widget>[
          ListTile(
            title: I18nText(
              'settings.about',
              child: Text(
                'Über tinyCampus',
                style: Theme.of(context).textTheme.headline3,
              ),
            ),
          ),
          ListTile(
            onTap: () => {Navigator.pushNamed(context, settingsCreditsRoute)},
            title: I18nText('settings.credits'),
            subtitle: I18nText('settings.learn_more'),
          ),
          Divider(
            indent: 16,
            endIndent: 16,
          ),
          ListTile(
            onTap: () => _launchUrl(
                FlutterI18n.translate(context, 'settings.legal_notice_url')),
            title: I18nText('settings.legal_notice'),
            subtitle: I18nText('settings.legal_notice_url'),
            trailing: Icon(
              Icons.chevron_right,
              color: Theme.of(context).iconTheme.color,
            ),
          ),
          Divider(
            indent: 16,
            endIndent: 16,
          ),
          ListTile(
            onTap: () => _launchUrl(
                FlutterI18n.translate(context, 'settings.privacy_policy_url')),
            title: I18nText('settings.privacy_policy'),
            subtitle: I18nText('settings.privacy_policy_url'),
            trailing: Icon(
              Icons.chevron_right,
              color: Theme.of(context).iconTheme.color,
            ),
          ),
          Divider(
            indent: 16,
            endIndent: 16,
          ),
          ListTile(
            onTap: () =>
                {Navigator.pushNamed(context, settingsDataDeletionRoute)},
            title: I18nText('settings.data_delete_process'),
            subtitle: I18nText('settings.learn_more'),
          ),
          Divider(
            indent: 16,
            endIndent: 16,
          ),
          ListTile(
            onTap: () => _launchUrl('mailto:$tinyCampusEmail'),
            title: I18nText('settings.contact'),
            subtitle: Text(tinyCampusEmail),
          ),
          Divider(
            indent: 16,
            endIndent: 16,
          ),
          ListTile(
            onTap: () => _launchUrl(
                FlutterI18n.translate(context, 'settings.website_link')),
            title: I18nText('settings.website'),
            subtitle: I18nText('settings.website_link'),
            trailing: Icon(Icons.chevron_right,
                color: Theme.of(context).iconTheme.color),
          ),
          Divider(
            indent: 16,
            endIndent: 16,
          ),
          ListTile(
            onTap: () => {Navigator.pushNamed(context, settingsBrandInfoRoute)},
            title: I18nText('settings.brand_info'),
            subtitle: I18nText('settings.learn_more'),
          ),
          Divider(
            indent: 16,
            endIndent: 16,
          ),
          ListTile(
            onTap: () => _launchUrl(
                FlutterI18n.translate(context, 'settings.licence_url')),
            title: I18nText('settings.licence'),
            subtitle: I18nText('settings.licence_name'),
            trailing: Icon(
              Icons.chevron_right,
              color: Theme.of(context).iconTheme.color,
            ),
          ),
          Divider(
            indent: 16,
            endIndent: 16,
          ),
          ListTile(
            onTap: () => _launchUrl(
                FlutterI18n.translate(context, 'settings.source_code_url')),
            title: I18nText('settings.source_code'),
            subtitle: I18nText('settings.source_code_url'),
            trailing: Icon(
              Icons.chevron_right,
              color: Theme.of(context).iconTheme.color,
            ),
          ),
          Divider(
            indent: 16,
            endIndent: 16,
          ),
          ListTile(
            onTap: () => _launchUrl(FlutterI18n.translate(
                context, 'settings.source_code_backend_url')),
            title: I18nText('settings.source_code_backend'),
            subtitle: I18nText('settings.source_code_backend_url'),
            trailing: Icon(
              Icons.chevron_right,
              color: Theme.of(context).iconTheme.color,
            ),
          ),
        ],
      ),
    ));

    // Build info
    buttonList.add(Container(
      color: Theme.of(context).primaryColor,
      child: Column(children: <Widget>[
        ListTile(
          title: I18nText(
            'settings.build_info',
            child: Text(
              'Build-Informationen',
              style: Theme.of(context).textTheme.headline3,
            ),
          ),
        ),
        ListTile(
          title: I18nText('settings.app_name'),
          subtitle: Text(_packageInfo.appName),
        ),
        Divider(
          indent: 16,
          endIndent: 16,
        ),
        ListTile(
          title: I18nText('settings.package_name'),
          subtitle: Text(_packageInfo.packageName),
        ),
        Divider(
          indent: 16,
          endIndent: 16,
        ),
        ListTile(
          title: I18nText('settings.app_version'),
          subtitle: Text(_packageInfo.version),
        ),
        Divider(
          indent: 16,
          endIndent: 16,
        ),
        ListTile(
          title: I18nText('settings.build_number'),
          subtitle: Text(_packageInfo.buildNumber),
        )
      ]),
    ));

    return buttonList;
  }

  ListView listBuilder(BuildContext context, List<Widget> items) =>
      ListView.separated(
        physics: BouncingScrollPhysics(),
        separatorBuilder: (context, index) => Divider(
          indent: 18.0,
          color: Colors.transparent,
          height: 10,
        ),
        itemBuilder: (context, index) => items[index],
        itemCount: items.length,
      );

  Future<void> _initPackageInfo() async {
    final info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  bool iconButtonToggle = false;

  // Theme testing stuff
  void changeBrightness() {
    DynamicTheme.of(context)?.setTheme(
      Theme.of(context).brightness == Brightness.dark
          ? TCThemes.light.index
          : TCThemes.dark.index,
    );
  }

  void changeLanguage(BuildContext context, Locale locale) {
    _prefs.then((sp) => sp.setString(overrideLanguage, locale.languageCode));
    // DO NOT DELETE! Needed to reload Settings page with new localization
    FlutterI18n.refresh(context, locale).then((_) => setState(() {}));
  }

  Future<void> _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
