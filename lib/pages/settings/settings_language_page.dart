/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../common/constants/localization_constants.dart'
    show overrideLanguage;
import 'language_widget.dart';

class SettingsLanguage extends StatefulWidget {
  const SettingsLanguage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => SettingsLanguageState();
}

class SettingsLanguageState extends State<SettingsLanguage> {
  late SharedPreferences _prefs;
  final ValueNotifier<bool> loading = ValueNotifier<bool>(false);
  final ValueNotifier<Map<String, bool>> loadMap =
      ValueNotifier<Map<String, bool>>(<String, bool>{});

  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((value) {
      if (mounted) {
        setState(() => _prefs = value);
      }
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(
            FlutterI18n.translate(context, 'settings.title'),
          ),
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        body: Column(
          children: <Widget>[
            Divider(
              color: Colors.transparent,
              height: 10,
            ),
            Container(
              child: Builder(
                  builder: (builderContext) => ListView(
                        shrinkWrap: true,
                        children: <Widget>[
                          ListTile(
                            tileColor: Theme.of(context).primaryColor,
                            title: I18nText(
                              'settings.language_settings',
                              child: Text(
                                "Spracheinstellungen",
                                style: Theme.of(context).textTheme.headline3,
                              ),
                            ),
                          ),
                          buildLanguageMaterialTile(context, builderContext,
                              i18nKey: 'settings.languages.german',
                              locale: "de"),
                          Divider(
                            thickness: 1,
                            height: 1,
                            indent: 20,
                            endIndent: 20,
                          ),
                          buildLanguageMaterialTile(context, builderContext,
                              i18nKey: 'settings.languages.english',
                              locale: "en"),
                        ],
                      )),
              color: Theme.of(context).primaryColor,
            ),
          ],
        ),
      );

  Material buildLanguageMaterialTile(
    BuildContext context,
    BuildContext builderContext, {
    required String i18nKey,
    required String locale,
  }) =>
      Material(
        color: Theme.of(context).primaryColor,
        child: InkWell(
          splashFactory: InkRipple.splashFactory,
          splashColor: Colors.grey[100],
          highlightColor: Colors.grey[300],
          child: SizedBox(
            height: 75,
            child: Center(
              child: ListTile(
                leading: ValueListenableBuilder(
                  valueListenable: loadMap,
                  builder: (context, value, child) => ValueListenableBuilder(
                    valueListenable: loading,
                    builder: (context, value, child) => LanguageWidget(
                      locale.toUpperCase(),
                      isLoading: loadMap.value[locale] ?? false,
                    ),
                  ),
                ),
                title: I18nText(i18nKey),
              ),
            ),
          ),
          onTap: () async {
            if (!loading.value) {
              loadMap.value[locale] = true;
              loading.value = true;
              try {
                await changeLanguage(builderContext, Locale(locale));
              } on Exception catch (e) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text("Error: $e"),
                    duration: Duration(seconds: 2),
                  ),
                );
              }
              loadMap.value[locale] = false;
              loading.value = false;
            }
          },
        ),
      );

  Future<void> changeLanguage(BuildContext context, Locale locale) async {
    _prefs.setString(overrideLanguage, locale.languageCode);
    // DO NOT DELETE! Needed to reload Settings page with new Localization.
    await FlutterI18n.refresh(context, locale).then((_) {
      if (mounted) {
        setState(() {
          ScaffoldMessenger.of(context).removeCurrentSnackBar();
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(
              FlutterI18n.translate(
                  context, 'settings.snackbar_language_changed'),
            ),
            duration: Duration(seconds: 2),
          ));
        });
      }
    });
  }
}
