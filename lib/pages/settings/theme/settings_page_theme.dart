/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../../common/tc_theme.dart';
import '../../../modules/organizer/ui/organizer/o_drawer/i18n_common.dart';
import 'theme_widget.dart';

class SettingsPageTheme extends StatefulWidget {
  const SettingsPageTheme({Key? key}) : super(key: key);

  @override
  _SettingsPageThemeState createState() => _SettingsPageThemeState();
}

class _SettingsPageThemeState extends State<SettingsPageTheme> {
  @override
  Widget build(BuildContext context) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(height: 16),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                Container(
                  width: 15,
                ),
                ThemeWidget(
                  CorporateColors.passiveBackgroundLight,
                  Colors.white,
                  FlutterI18n.translate(context, 'settings.themes.light'),
                  TCThemes.light,
                ),
                Container(
                  width: 20,
                ),
                ThemeWidget(
                  CorporateColors.darkModeBackground,
                  CorporateColors.darkModeContent,
                  FlutterI18n.translate(context, 'settings.themes.dark'),
                  TCThemes.dark,
                ),
                Container(
                  width: 15,
                ),
              ],
            ),
          ),
          Container(height: 24)
        ],
      );
}
