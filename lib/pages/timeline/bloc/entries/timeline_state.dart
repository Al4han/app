/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'timeline_bloc.dart';

/// Archetype for all states regarding the core functionality of the timeline.
@Immutable()
abstract class TState extends Equatable {
  static const String versionUID = '{tc-timeline-v0002}';

  final String attemptedVersion;

  final String type;

  TLEntryList get entries => TLEntryList.empty();

  /// Indicates whether or not all entries have been fetched from the server
  bool get hasReachedMax => true;

  /// marks the position of old content
  int get idDatedContent => -1;

  int get page => 0;

  bool get favoredOnly => false;

  bool get isEmpty => true;

  TState(this.attemptedVersion, this.type) {
    if (attemptedVersion != versionUID) {
      throw WrongJSONVersionException(
        expectedVersionString: versionUID,
        attemptedVersion: attemptedVersion,
        state: type,
      );
    }
  }

  @override
  List<Object> get props => [versionUID];

  @override
  bool get stringify => true;
}

/// Indicates that the [TimelineBloc] hasn't been initialized yet.
@JsonSerializable(anyMap: true, explicitToJson: true)
class TStateUninitialized extends TState {
  final String version;

  TStateUninitialized([this.version = TState.versionUID])
      : super(version, 'TStateUninitialized');

  factory TStateUninitialized.fromJson(Map<String, dynamic> json) =>
      _$TStateUninitializedFromJson(json);

  Map<String, dynamic> toJson() => _$TStateUninitializedToJson(this);
}

/// Indicates that an error occurred during the handling of a [TEvent]
class TStateError extends TState {
  /// Error message
  final String error;

  /// Constructor accepts an optional error message
  TStateError({required this.error}) : super(TState.versionUID, '');

  @override
  List<Object> get props => super.props + [error];
}

/// Indicates that the timeline was fetched successfully
@CopyWith()
@JsonSerializable(anyMap: true, explicitToJson: true, checked: true)
class TStateLoaded extends TState {
  @override
  final TLEntryList entries;

  /// Indicates whether or not all entries have been fetched from the server
  @override
  final bool hasReachedMax;

  /// marks the position of old content
  @override
  final int idDatedContent;

  final String version;

  @override
  final int page;

  @override
  final bool favoredOnly;

  /// Constructor requires [entries]
  /// but accepts [hasReachedMax] as an optional parameter.
  TStateLoaded({
    required this.entries,
    required this.hasReachedMax,
    this.idDatedContent = -1,
    this.page = 0,
    this.version = TState.versionUID,
    this.favoredOnly = false,
  }) : super(version, 'TStateLoaded');

  factory TStateLoaded.fromJson(Map<String, dynamic> json) =>
      _$TStateLoadedFromJson(json);

  Map<String, dynamic> toJson() => _$TStateLoadedToJson(this);

  TStateLoaded removeEntry(TLEntry entry) {
    var _newList = entries.data;
    var _newDatedContentId = -1;
    if (entry.id == idDatedContent) {
      _newDatedContentId = _newList[_newList.indexOf(entry) + 1].id;
    }
    _newList.removeWhere((element) => element.id == entry.id);
    return copyWith(
        entries: TLEntryList.from(_newList),
        idDatedContent: _newDatedContentId);
  }

  TLEntryList get pinnedEntries => TLEntryList.from(
      entries.data.where((element) => element.pinned).toList());

  /// Nullsafe getter to check weather or not the included entries list is empty
  @override
  bool get isEmpty => entries.isEmpty;

  TLEntryList get recentEntries => TLEntryList.from(entries.data
      .where((element) => (element.id > idDatedContent) && !element.pinned)
      .toList());

  TLEntryList get datedEntries => TLEntryList.from(entries.data
      .where((element) => element.id <= idDatedContent && !element.pinned)
      .toList());

  @override
  List<Object> get props =>
      super.props +
      [
        hasReachedMax,
        idDatedContent,
        entries,
      ];

  bool entryIsRecent(TLEntry entry) => entry.id > idDatedContent;

  bool entryIsDated(TLEntry entry) => entry.id > idDatedContent;
}
