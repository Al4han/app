/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'timeline_bloc.dart';

const String _lastShownEntry = 'timeline.lastShownEntry';

///Abstract class for timeline events.
///All events regarding the timeline have to extend this class or they will be
///ignored by [TimelineBloc].
@Immutable()
abstract class TEvent extends Equatable {
  const TEvent();

  @override
  List<Object> get props => [];

  Future<TState> _interaction(TimelineBloc bloc);

  static TStateError _errorDuringProcessing(TEvent event) => TStateError(
      error: '******************\n'
          '* Error while processing event: { $event }\n'
          '* THIS DEFINETLY SHOULD NOT HAPPEN\n'
          '******************');

  @override
  bool get stringify => true;
}

/// Defines an interaction event for an entry
abstract class TInteractionEvent extends TEvent {
  final TLEntry entry;

  TInteractionEvent(this.entry);
}

class TFavoredToggle extends TInteractionEvent {
  TFavoredToggle({required TLEntry entry}) : super(entry);

  @override
  Future<TState> _interaction(TimelineBloc bloc) async {
    try {
      var st = (bloc.state as TStateLoaded);
      if (entry.favored) {
        // Toggle off
        bloc.thh
            .sendDeleteRequest(request: toggleFavedTLEntry(entryID: entry.id))
            .catchError((e) {
          bloc.add(TEventError(error: e.toString()));
        });
      } else {
        bloc.thh
            .sendPostRequest(request: toggleFavedTLEntry(entryID: entry.id))
            .catchError((e) {
          bloc.add(TEventError(error: e.toString()));
        });
      }
      var entryList = st.entries;
      return st.copyWith(entries: entryList.replace(entry));
    } on Exception catch (e) {
      return TStateError(error: e.toString());
    }
  }
}

class TPinToggle extends TInteractionEvent {
  TPinToggle({required TLEntry entry}) : super(entry);

  @override
  Future<TState> _interaction(TimelineBloc bloc) async {
    try {
      var st = (bloc.state as TStateLoaded);
      bloc.thh
          .sendPostRequest(request: togglePinnedTLEntry(entryID: entry.id))
          .catchError((e) {
        bloc.add(TEventError(error: e.toString()));
      });

      var entryList = st.entries;
      return st.copyWith(
          entries: entryList.replace(entry.copyWith(pinned: !entry.pinned)));
    } on Exception catch (e) {
      return TStateError(error: e.toString());
    }
  }
}

class THide extends TInteractionEvent {
  THide({required TLEntry entry}) : super(entry);

  @override
  Future<TState> _interaction(TimelineBloc bloc) async {
    try {
      var st = (bloc.state as TStateLoaded);
      bloc.thh
          .sendPostRequest(request: postHideTLEntry(entryID: entry.id))
          .catchError((e) {
        bloc.add(TEventError(error: e.toString()));
      });
      var entryList = st.entries;
      return st.copyWith(entries: entryList.remove(entry));
    } on Exception catch (e) {
      return TStateError(error: e.toString());
    }
  }
}

class TReport extends TInteractionEvent {
  TReport({required TLEntry entry}) : super(entry);

  @override
  Future<TState> _interaction(TimelineBloc bloc) async => bloc.state;
}

///Abstract class for timeline entry specific events.
///Timeline entry events have to extend this class.
abstract class TEventEntry extends TEvent {}

class TEventError extends TEvent {
  final String error;

  TEventError({required this.error});

  @override
  List<Object> get props => [error];

  @override
  Future<TState> _interaction(TimelineBloc bloc) async =>
      TStateError(error: error);
}

///[TimelineBloc] event for resetting the whole timeline causing
///it to reload its contents from the server.
class TEventReset extends TEventEntry {
  @override
  Future<TState> _interaction(TimelineBloc bloc) async => TStateUninitialized();
}

///Causes [TimelineBloc] to fetch all entries from the server
class TEventFetchAll extends TEventEntry {
  @override
  Future<TState> _interaction(TimelineBloc bloc) async {
    try {
      if (bloc.state is! TStateLoaded ||
          (bloc.state is TStateLoaded &&
              ((bloc.state as TStateLoaded).favoredOnly))) {
        final pinnedEntries =
            await bloc.thh.fetchPinnedEntries(request: getPinnedTLEntries());
        var idDatedContent =
            await bloc._sp.then<int>((pr) => pr.getInt(_lastShownEntry) ?? -1);
        final entries = await bloc.thh.fetchEntries(request: getTLEntries());

        // Called if there are neither pinned entries nor regular entries
        if (pinnedEntries.isEmpty && entries.isEmpty) {
          return TStateLoaded(
            hasReachedMax: true,
            entries: TLEntryList.empty(),
          );
          // Called if there are pinned entries but no regular entries
        } else if (pinnedEntries.isNotEmpty && entries.isEmpty) {
          return TStateLoaded(
            hasReachedMax: true,
            entries: pinnedEntries,
          );
        }

        if (idDatedContent <= entries[0].id) {
          (await bloc._sp).setInt(_lastShownEntry, entries[0].id);
        }

        return TStateLoaded(
          entries: pinnedEntries.add(entries),
          idDatedContent: idDatedContent,
          hasReachedMax: entries.last,
        );
      }
      if (bloc.state is TStateLoaded) {
        var st = (bloc.state as TStateLoaded);
        var page = st.page + 1;
        final entries =
            await bloc.thh.fetchEntries(request: getTLEntries(page: page));

        if (entries.isEmpty) {
          return st.copyWith(
            hasReachedMax: true,
          );
        } else {
          return st.copyWith(
            entries: (bloc.state as TStateLoaded).entries.add(entries),
            hasReachedMax: entries.last,
            page: page,
          );
        }
      }
    } on Exception catch (e) {
      return TStateError(error: e.toString());
    }
    return TEvent._errorDuringProcessing(this);
  }
}

///Causes [TimelineBloc] to fetch all pinned entries from the server
@Deprecated('Seems it is nowhere in use.')
class TEventFetchPinned extends TEventEntry {
  @override
  Future<TState> _interaction(TimelineBloc bloc) async {
    try {
      final entries =
          await bloc.thh.fetchPinnedEntries(request: getPinnedTLEntries());
      return TStateLoaded(
        entries: entries,
        hasReachedMax: false,
      );
    } on Exception catch (e) {
      return TStateError(error: e.toString());
    }
  }
}

///Causes [TimelineBloc] to fetch all favored entries from the server
class TEventFetchFaved extends TEventEntry {
  @override
  List<Object> get props => [];

  @override
  Future<TState> _interaction(TimelineBloc bloc) async {
    try {
      final entries = await bloc.thh.fetchEntries(request: getFavedTLEntries());
      return TStateLoaded(
        entries: entries,
        hasReachedMax: true,
        favoredOnly: true,
      );
    } on Exception catch (e) {
      return TStateError(error: e.toString());
    }
  }
}

class TimelineResetEvent extends TEvent {
  const TimelineResetEvent();

  @override
  Future<TState> _interaction(TimelineBloc bloc) =>
      Future.value(TStateUninitialized());
}
