// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'timeline_bloc.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension TStateLoadedCopyWith on TStateLoaded {
  TStateLoaded copyWith({
    TLEntryList? entries,
    bool? favoredOnly,
    bool? hasReachedMax,
    int? idDatedContent,
    int? page,
    String? version,
  }) {
    return TStateLoaded(
      entries: entries ?? this.entries,
      favoredOnly: favoredOnly ?? this.favoredOnly,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      idDatedContent: idDatedContent ?? this.idDatedContent,
      page: page ?? this.page,
      version: version ?? this.version,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TStateUninitialized _$TStateUninitializedFromJson(Map json) =>
    TStateUninitialized(
      json['version'] as String? ?? TState.versionUID,
    );

Map<String, dynamic> _$TStateUninitializedToJson(
        TStateUninitialized instance) =>
    <String, dynamic>{
      'version': instance.version,
    };

TStateLoaded _$TStateLoadedFromJson(Map json) => $checkedCreate(
      'TStateLoaded',
      json,
      ($checkedConvert) {
        final val = TStateLoaded(
          entries: $checkedConvert('entries',
              (v) => TLEntryList.fromJson(Map<String, dynamic>.from(v as Map))),
          hasReachedMax: $checkedConvert('hasReachedMax', (v) => v as bool),
          idDatedContent:
              $checkedConvert('idDatedContent', (v) => v as int? ?? -1),
          page: $checkedConvert('page', (v) => v as int? ?? 0),
          version: $checkedConvert(
              'version', (v) => v as String? ?? TState.versionUID),
          favoredOnly:
              $checkedConvert('favoredOnly', (v) => v as bool? ?? false),
        );
        return val;
      },
    );

Map<String, dynamic> _$TStateLoadedToJson(TStateLoaded instance) =>
    <String, dynamic>{
      'entries': instance.entries.toJson(),
      'hasReachedMax': instance.hasReachedMax,
      'idDatedContent': instance.idDatedContent,
      'version': instance.version,
      'page': instance.page,
      'favoredOnly': instance.favoredOnly,
    };
