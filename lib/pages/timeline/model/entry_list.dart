/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'timeline_entry.dart';

/// Contains a list of [TLEntry] as well as its
/// page number and if it is the last page
@Immutable()
@CopyWith()
@JsonSerializable(anyMap: true, explicitToJson: true)
class TLEntryList extends Equatable {
  static const int expectedPageSize = 20;

  /// List of [TLEntry]
  final List<TLEntry> data;

  /// Indicates if the list was the last one
  final bool last;

  @JsonKey(name: 'number')
  final int page;

  /// Creates a new [TLEntryList]
  TLEntryList({
    this.data = const [],
    required this.last,
    this.page = 0,
  });

  /// Special constructor for a list of pinned entries.
  /// Has to be the first by definition.
  TLEntryList.pinned({
    this.data = const [],
    this.last = false,
    this.page = 0,
  });

  TLEntryList.empty()
      : data = <TLEntry>[],
        last = false,
        page = 0;

  factory TLEntryList.from(
    List<TLEntry> iterable, {
    bool tailing = false,
  }) =>
      TLEntryList(
        data: iterable,
        last: tailing,
      );

  factory TLEntryList.fromJson(Map<String, dynamic> json) =>
      _$TLEntryListFromJson(json);

  Map<String, dynamic> toJson() => _$TLEntryListToJson(this);

  int get length => data.length;

  bool get isEmpty => data.isEmpty;

  bool get isNotEmpty => data.isNotEmpty;

  TLEntry operator [](int index) => data[index];

  void operator []=(int index, TLEntry value) {
    data[index] = value;
  }

  TLEntryList add(TLEntryList other) => other.copyWith(data: data + other.data);

  TLEntryList remove(TLEntry entry) {
    var d = [...data];
    d.removeWhere((element) => element == entry);
    return copyWith(data: d);
  }

  Iterable<T> map<T>(T Function(TLEntry e) f) => data.map<T>(f);

  @override
  List<Object> get props => [data, page, last];

  @override
  bool get stringify => true;

  TLEntryList replace(TLEntry entry) {
    var list = [...data];
    var i = list.indexWhere((element) => element == entry);
    list[i] = entry;
    return copyWith(data: list);
  }
}
