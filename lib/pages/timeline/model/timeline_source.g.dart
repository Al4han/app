// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'timeline_source.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension TLSourceCopyWith on TLSource {
  TLSource copyWith({
    String? description,
    int? id,
    AssetImage? logo,
    String? name,
    bool? subscribed,
  }) {
    return TLSource(
      description: description ?? this.description,
      id: id ?? this.id,
      logo: logo ?? this.logo,
      name: name ?? this.name,
      subscribed: subscribed ?? this.subscribed,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TLSource _$TLSourceFromJson(Map<String, dynamic> json) => TLSource(
      name: json['name'] as String,
      id: json['id'] as int,
      description: json['description'] as String,
      subscribed: json['subscribed'] as bool,
      logo: TLSource._logoFromJson(json['logo'] as String),
    );

Map<String, dynamic> _$TLSourceToJson(TLSource instance) => <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'description': instance.description,
      'subscribed': instance.subscribed,
      'logo': TLSource._logoToJson(instance.logo),
    };
