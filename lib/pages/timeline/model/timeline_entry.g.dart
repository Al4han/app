// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'timeline_entry.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension TLEntryCopyWith on TLEntry {
  TLEntry copyWith({
    String? author,
    AssetImage? avatar,
    DateTime? date,
    bool? favored,
    int? id,
    bool? isReported,
    String? parentSource,
    bool? pinned,
    String? route,
    String? routeArgs,
    String? text,
  }) {
    return TLEntry(
      author: author ?? this.author,
      avatar: avatar ?? this.avatar,
      date: date ?? this.date,
      favored: favored ?? this.favored,
      id: id ?? this.id,
      isReported: isReported ?? this.isReported,
      parentSource: parentSource ?? this.parentSource,
      pinned: pinned ?? this.pinned,
      route: route ?? this.route,
      routeArgs: routeArgs ?? this.routeArgs,
      text: text ?? this.text,
    );
  }
}

extension TLEntryListCopyWith on TLEntryList {
  TLEntryList copyWith({
    List<TLEntry>? data,
    bool? last,
    int? page,
  }) {
    return TLEntryList(
      data: data ?? this.data,
      last: last ?? this.last,
      page: page ?? this.page,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TLEntry _$TLEntryFromJson(Map json) => TLEntry(
      id: json['id'] as int,
      author: json['author'] as String,
      parentSource: json['parentSource'] as String,
      date: DateTime.parse(json['date'] as String),
      text: json['text'] as String? ?? 'ERROR: No Content',
      avatar: TLEntry._avatarFromJson(json['avatar'] as String),
      route: json['route'] as String,
      routeArgs: json['routeArgs'] as String,
      favored: json['faved'] as bool? ?? false,
      pinned: json['pinned'] as bool? ?? false,
      isReported: json['reported'] as bool? ?? false,
    );

Map<String, dynamic> _$TLEntryToJson(TLEntry instance) => <String, dynamic>{
      'id': instance.id,
      'author': instance.author,
      'parentSource': instance.parentSource,
      'avatar': TLEntry._avatarToJson(instance.avatar),
      'date': instance.date.toIso8601String(),
      'route': instance.route,
      'routeArgs': instance.routeArgs,
      'text': instance.text,
      'faved': instance.favored,
      'pinned': instance.pinned,
      'reported': instance.isReported,
    };

TLEntryList _$TLEntryListFromJson(Map json) => TLEntryList(
      data: (json['data'] as List<dynamic>?)
              ?.map(
                  (e) => TLEntry.fromJson(Map<String, dynamic>.from(e as Map)))
              .toList() ??
          const [],
      last: json['last'] as bool,
      page: json['number'] as int? ?? 0,
    );

Map<String, dynamic> _$TLEntryListToJson(TLEntryList instance) =>
    <String, dynamic>{
      'data': instance.data.map((e) => e.toJson()).toList(),
      'last': instance.last,
      'number': instance.page,
    };
