/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of '../timeline_settings_page.dart';

/// Widget which references a source
class _SourceEntryWidget extends StatefulWidget {
  /// Source which is referenced
  final TLSource source;

  /// Creates a new Widget
  const _SourceEntryWidget({Key? key, required this.source}) : super(key: key);

  @override
  _SourceEntryWidgetState createState() => _SourceEntryWidgetState();
}

class _SourceEntryWidgetState extends State<_SourceEntryWidget> {
  late TLSource _source;

  _SourceEntryWidgetState();

  @override
  void initState() {
    super.initState();
    _source = widget.source;
  }

  @override
  Widget build(BuildContext context) {
    final _sourceBloc = BlocProvider.of<SourceBloc>(context);
    return BlocBuilder<SourceBloc, SourceState>(
      bloc: _sourceBloc,
      builder: (context, state) {
        if (state is SourceStateSubscribed && state.source.id == _source.id) {
          return CheckboxListTile(
            activeColor: CurrentTheme().tcDarkBlue,
            title: I18nText(_source.name),
            subtitle: I18nText(
              _source.description,
              child: Text(
                _source.description,
                softWrap: true,
              ),
            ),
            secondary: CircleAvatar(
              backgroundImage: _source.logo,
            ),
            value: _source.subscribed,
            onChanged: (_) {
              setState(() {
                _sourceBloc.add(SourceEventUnsubscribe(source: _source));
              });
              _source = _source.copyWith(subscribed: !_source.subscribed);
            },
          );
        }
        if (state is SourceStateUnsubscribed && state.source.id == _source.id) {
          return CheckboxListTile(
            activeColor: CurrentTheme().tcDarkBlue,
            title: I18nText(_source.name),
            subtitle: I18nText(
              _source.description,
              child: Text(
                _source.description,
                softWrap: true,
              ),
            ),
            secondary: CircleAvatar(
              backgroundImage: _source.logo,
            ),
            value: _source.subscribed,
            onChanged: (_) {
              setState(() {
                _sourceBloc.add(SourceEventSubscribe(source: _source));
              });
              _source = _source.copyWith(subscribed: !_source.subscribed);
            },
          );
        }
        return CheckboxListTile(
          activeColor: CurrentTheme().tcDarkBlue,
          title: I18nText(_source.name),
          subtitle: I18nText(
            _source.description,
            child: Text(
              _source.description,
              softWrap: true,
            ),
          ),
          secondary: CircleAvatar(
            backgroundImage: _source.logo,
          ),
          value: _source.subscribed,
          onChanged: (_) {
            setState(() {
              _sourceBloc.add(
                _source.subscribed
                    ? SourceEventUnsubscribe(source: _source)
                    : SourceEventSubscribe(source: _source),
              );
            });
            _source = _source.copyWith(subscribed: !_source.subscribed);
          },
        );
      },
    );
  }
}
