/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../../../common/constants/api_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../../../common/tinycampus_icons.dart';
import '../../../../common/widgets/dialog/tc_dialog.dart';
import '../../../../common/widgets/reporting/reported_blur_wrapper.dart';
import '../../../../common/widgets/tc_link_text.dart';
import '../../../../repositories/http/http_repository.dart';
import '../../bloc/entries/timeline_bloc.dart';
import '../../model/timeline_entry.dart';
import '../common.dart';

part 'timeline_entry_widget.dart';

/// Wrapper for handling the interaction of an [TLEntryWidget] with [Slidable]
class SlideWrapper extends StatefulWidget {
  /// [TLEntry]-Widget to wrapped with [Slidable]
  final TLEntry entry;
  final BlocCallback listUpdateCallback;

  /// Constructor which accepts a [key] and a [entry]
  SlideWrapper({
    required Key key,
    required this.entry,
    required this.listUpdateCallback,
  }) : super(key: key);

  @override
  _SlideWrapperState createState() => _SlideWrapperState();
}

class _SlideWrapperState extends State<SlideWrapper> {
  late TLEntry _entry;
  static final SlidableController slidableController = SlidableController();
  static const _duration = Duration(milliseconds: 260);
  late bool visibility;

  @override
  void initState() {
    super.initState();
    _entry = widget.entry;
    visibility = true;
  }

  @override
  Widget build(BuildContext context) => AnimatedCrossFade(
        duration: _duration,
        sizeCurve: Curves.easeOutQuint,
        secondChild: Container(),
        crossFadeState:
            visibility ? CrossFadeState.showFirst : CrossFadeState.showSecond,
        firstChild: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(vertical: 4.0),
              child: Slidable(
                movementDuration: Duration(milliseconds: 70),
                key: Key('tlEntrySlideWidget${_entry.id}'),
                controller: slidableController,
                actionPane: SlidableDrawerActionPane(),
                closeOnScroll: true,
                actionExtentRatio: 0.235,
                child: TLEntryWidget(
                  key: Key('tlEntryWidget${_entry.id}'),
                  tlEntry: _entry,
                  blocCallback: (event) => setState(() {
                    visibility = false;
                    widget.listUpdateCallback(event);
                  }),
                ),
                secondaryActions: <Widget>[
                  IconSlideAction(
                    closeOnTap: false,
                    caption:
                        FlutterI18n.translate(context, 'common.actions.fave'),
                    color: Colors.amber[600],
                    foregroundColor: Colors.white,
                    iconWidget: Container(
                      child: Icon(
                        _entry.favored
                            ? TinyCampusIcons.star_100p
                            : TinyCampusIcons.star_0p,
                        color: Colors.white,
                        size: 40.0,
                      ),
                      margin: EdgeInsets.only(bottom: 12.0),
                    ),
                    onTap: () {
                      //_tlBloc.add(TFavoredToggle(entry: _entry));
                      setState(() {
                        widget
                            .listUpdateCallback(TFavoredToggle(entry: _entry));
                        _entry = _entry.copyWith(favored: !_entry.favored);
                      });
                    },
                  ),
                  IconSlideAction(
                    caption: FlutterI18n.translate(
                        context, 'common.report.button_label'),
                    color: Colors.grey[600],
                    iconWidget: Container(
                      child: Icon(
                        TinyCampusIcons.report,
                        color: Colors.white,
                        size: 40.0,
                      ),
                      margin: EdgeInsets.only(bottom: 12.0),
                    ),
                    onTap: () => TCDialog.showCustomDialog(
                      context: context,
                      onConfirm: () => _reportPost(_entry.id),
                      headlineText: FlutterI18n.translate(
                        context,
                        'common.report.post.headline',
                      ),
                      bodyText: FlutterI18n.translate(
                        context,
                        'common.report.post.body',
                      ),
                    ),
                  ),
                  IconSlideAction(
                    caption:
                        FlutterI18n.translate(context, 'common.actions.remove'),
                    iconWidget: Container(
                      child: Icon(
                        TinyCampusIcons.trash,
                        color: Colors.white,
                        size: 40.0,
                      ),
                      margin: EdgeInsets.only(bottom: 12.0),
                    ),
                    color: CorporateColors.cafeteriaCautionRed,
                    onTap: () {
                      setState(() {
                        visibility = false;
                        widget.listUpdateCallback.call(THide(
                          entry: _entry,
                        ));
                      });
                    },
                  )
                ],
              ),
            ),
          ],
        ),
      );

  Future<void> _reportPost(int id) async {
    try {
      final messenger = ScaffoldMessenger.of(context);
      final successText = FlutterI18n.translate(
        context,
        'common.report.post.success',
      );
      final errorText = FlutterI18n.translate(
        context,
        'common.report.post.error',
      );

      try {
        await HttpRepository().post('$timelineUrl/${_entry.id}/report');
        messenger.showSnackBar(SnackBar(content: Text(successText)));
      } on Exception catch (e) {
        debugPrint(e.toString());
        messenger.showSnackBar(SnackBar(content: Text(errorText)));
      }
    } on Exception catch (e) {
      // Don't rethrow for the users sanity sake
      debugPrint(e.toString());
    }
  }
}
