// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'highscores.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HighScores _$HighScoresFromJson(Map json) => HighScores(
      global: (json['global'] as List<dynamic>?)
              ?.map((e) => Score.fromJson(Map<String, dynamic>.from(e as Map)))
              .toList() ??
          const <Score>[],
      recent: (json['recent'] as List<dynamic>?)
              ?.map((e) => Score.fromJson(Map<String, dynamic>.from(e as Map)))
              .toList() ??
          const <Score>[],
      personal: json['personal'] == null
          ? const Score()
          : Score.fromJson(Map<String, dynamic>.from(json['personal'] as Map)),
    );

Map<String, dynamic> _$HighScoresToJson(HighScores instance) =>
    <String, dynamic>{
      'global': instance.global.map((e) => e.toJson()).toList(),
      'recent': instance.recent.map((e) => e.toJson()).toList(),
      'personal': instance.personal.toJson(),
    };
