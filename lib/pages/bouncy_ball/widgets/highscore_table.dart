/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/tc_theme.dart';
import '../model/highscores.dart';
import '../model/score.dart';
import 'highscore_text_field.dart';

class HighScoreTable extends StatelessWidget {
  final bool sendScore;
  final TextEditingController controller;
  final int recentPosition;
  final int globalPosition;
  final int points;
  final HighScores? tables;
  final void Function() close;
  final bool showPersonal;
  final int personalHighscore;

  const HighScoreTable({
    Key? key,
    required this.sendScore,
    required this.controller,
    required this.recentPosition,
    required this.globalPosition,
    required this.points,
    required this.tables,
    required this.close,
    required this.showPersonal,
    required this.personalHighscore,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Card(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        color: Theme.of(context).primaryColor,
        elevation: 4,
        margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "HIGHSCORE",
                        style: Theme.of(context).textTheme.headline1?.copyWith(
                              color: CurrentTheme().textColor,
                              fontWeight: FontWeight.w900,
                            ),
                      ),
                      Transform.translate(
                        offset: Offset(16, 0),
                        child: FlatButton(
                          minWidth: 10,
                          child: Icon(
                            Icons.close,
                            size: 25,
                            color: Theme.of(context).iconTheme.color,
                          ),
                          onPressed: close,
                        ),
                      ),
                    ],
                  ),
                  showPersonal
                      ? Text(
                          points != -1
                              ? FlutterI18n.translate(
                                  context, "modules.bouncy_ball.points",
                                  translationParams: {"points": "$points"})
                              : FlutterI18n.translate(
                                  context, "modules.bouncy_ball.not_played"),
                          style:
                              Theme.of(context).textTheme.headline3?.copyWith(
                                    color: CurrentTheme().textColor,
                                  ),
                        )
                      : Container(),
                  points >= personalHighscore && showPersonal
                      ? Text(FlutterI18n.translate(
                          context, "modules.bouncy_ball.new_record"))
                      : Container(),
                ],
              ),
              Container(
                height: 20,
              ),
              tables != null
                  ? Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              showPersonal
                                  ? Text(
                                      globalPosition != -1
                                          ? "Top $globalPosition"
                                          : FlutterI18n.translate(context,
                                              "modules.bouncy_ball.no_rank"),
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline2
                                          ?.copyWith(
                                            fontWeight: FontWeight.bold,
                                          ),
                                    )
                                  : Container(),
                              Text(
                                FlutterI18n.translate(
                                    context, "modules.bouncy_ball.overall"),
                                style: Theme.of(context)
                                    .textTheme
                                    .headline3
                                    ?.copyWith(
                                      color: CurrentTheme().textColor,
                                    ),
                              ),
                              Container(
                                height: 10,
                              ),
                              (!sendScore && tables != null)
                                  ? _createTable(tables!.global, context)
                                  : Container(),
                            ],
                          ),
                        ),
                        Container(width: 16),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              showPersonal
                                  ? Text(
                                      recentPosition != -1
                                          ? "Top $recentPosition"
                                          : FlutterI18n.translate(context,
                                              "modules.bouncy_ball.no_rank"),
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline2
                                          ?.copyWith(
                                              fontWeight: FontWeight.bold),
                                    )
                                  : Container(),
                              Text(
                                FlutterI18n.translate(
                                    context, "modules.bouncy_ball.last_day"),
                                style: Theme.of(context)
                                    .textTheme
                                    .headline3
                                    ?.copyWith(
                                      color: CurrentTheme().textColor,
                                    ),
                              ),
                              Container(
                                height: 10,
                              ),
                              (!sendScore && tables != null)
                                  ? _createTable(tables!.recent, context)
                                  : Container(),
                            ],
                          ),
                        ),
                      ],
                    )
                  : Center(child: CircularProgressIndicator()),
              sendScore && tables != null
                  ? HighScoreTextField(controller: controller)
                  : Container(),
            ],
          ),
        ),
      );

  Widget _createTable(List<Score> table, BuildContext context) => Table(
        // defaultColumnWidth: IntrinsicColumnWidth(),
        children: table.map((e) => _tableEntry(e, context)).toList(),
      );

  TableRow _tableEntry(Score score, BuildContext context) => TableRow(
        children: [
          Row(
            children: [
              Text(
                score.name,
                style: score.isSelf
                    ? Theme.of(context).textTheme.bodyText1?.copyWith(
                        color: CurrentTheme().tcBlueFont,
                        fontWeight: FontWeight.bold)
                    : Theme.of(context)
                        .textTheme
                        .bodyText1
                        ?.copyWith(color: CurrentTheme().textPassive),
              ),
              Expanded(
                child: Container(),
              ),
              Text(
                "${score.score}",
                textAlign: TextAlign.right,
                style: score.isSelf
                    ? Theme.of(context).textTheme.bodyText1?.copyWith(
                        color: CurrentTheme().textColor,
                        fontWeight: FontWeight.bold)
                    : Theme.of(context)
                        .textTheme
                        .bodyText1
                        ?.copyWith(color: CurrentTheme().textPassive),
              ),
            ],
          ),
        ],
      );
}
