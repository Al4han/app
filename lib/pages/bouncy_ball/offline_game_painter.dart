/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'game_object.dart';

class Painter extends CustomPainter {
  DrawingController? controller;
  final List<GameObject> gameObjects;
  // final ui.Image imageRef;

  Painter(
    this.controller,
    this.gameObjects,
    // this.imageRef,
  ) : super(repaint: controller); //This is important

  final pPlayer = Paint()..color = Colors.green;
  final pPlayerStroke = Paint()
    ..color = Colors.black
    ..style = PaintingStyle.stroke
    ..strokeWidth = 4;
  final pEnemy = Paint()
    ..color = Colors.red
    ..style = PaintingStyle.fill;
  final pEnemyHeavy = Paint()
    ..color = Colors.red[900] ?? Colors.red
    ..style = PaintingStyle.fill;
  final pEnemySand = Paint()
    ..color = Color(0xffE8AE2F)
    ..style = PaintingStyle.fill;
  final pEnemyStroke = Paint()
    ..color = Colors.black
    ..strokeCap = StrokeCap.round
    ..style = PaintingStyle.stroke
    ..strokeWidth = 4;
  final pPolygon = Paint()
    ..color = Colors.black
    ..strokeCap = StrokeCap.square
    ..style = PaintingStyle.fill
    ..strokeWidth = 4;
  final pPlayerLandingParticle = Paint()
    ..color = const Color(0xffF6C350).withOpacity(1.0);
  final pPlayerLandingParticleStroke = Paint()
    // ..color = const Color(0xff000000).withOpacity(1.0)
    ..color = const Color(0xffF7CB81).withOpacity(1.0)
    ..strokeCap = StrokeCap.round
    ..style = PaintingStyle.stroke
    ..strokeWidth = 4;
  final pParticle = Paint()..color = Colors.yellow.withOpacity(0.2);
  // ..shader = RadialGradient(
  //   colors: [
  //     Colors.green,
  //     Colors.red,
  //     Colors.black,
  //   ],
  // ).createShader(Rect.fromCircle(
  //   center: Offset(0, 0),
  //   radius: 20,
  // ));
  final pDebug = Paint()..color = Colors.black.withOpacity(0.4);
  final particle = Offset(0, 0);

  @override
  void paint(Canvas canvas, Size size) {
    //Paint function

    for (var i = 0; i < gameObjects.length; i++) {
      final go = gameObjects[i];
      if (go.type == EType.player) {
        drawPlayerParticle(go.particles, canvas, size);

        // canvas.drawCircle(Offset(go.pos.x, -go.pos.y), 20, pPlayer);
        drawPlayer(go, canvas, size);
        drawPlayerParticle(go.frontParticles, canvas, size);
      } else {
        // drawParticle(go, canvas, size);
        drawPlayerParticle(go.particles, canvas, size);
        drawEnemy(go, canvas, size);
        // drawImage(go, canvas, size);
      }
    }
    // drawPolygon(gameObjects, canvas);
    // drawPolygonLaser(gameObjects, canvas);
  }

  void drawPolygon(List<GameObject> points, Canvas canvas) {
    final vertices = points.map((e) => Offset(e.pos.x, -e.pos.y)).toList();
    canvas.drawPoints(ui.PointMode.polygon, vertices, pPolygon);
    var path = Path();
    path.addPolygon(vertices, true);
    // final paint = Paint();
    // paint.style = PaintingStyle.fill;
    // paint.color = Color.fromRGBO(255, 40, 0, 0.5);
    // canvas.drawPath(path, paint);
  }

  void drawPolygonLaser(List<GameObject> points, Canvas canvas) {
    for (var i = 50; i < 255; i += 20) {
      final pPolygon = Paint()
        ..color = Color.fromRGBO(255, 50 + i, 50 + i, 1.0)
        ..strokeCap = StrokeCap.round
        ..style = PaintingStyle.fill
        ..strokeWidth = 60 - ((i / 2));
      final vertices = points.map((e) => Offset(e.pos.x, -e.pos.y)).toList();
      canvas.drawPoints(ui.PointMode.polygon, vertices, pPolygon);
      var path = Path();
      path.addPolygon(vertices, true);
    }

    // final paint = Paint();
    // paint.style = PaintingStyle.fill;
    // paint.color = Color.fromRGBO(255, 40, 0, 0.5);
    // canvas.drawPath(path, paint);
  }

  // void drawImage(GameObject go, Canvas canvas, Size size) {
  //   canvas.save();
  //   const scaleTra = 4.0;
  //   canvas.scale(1 / scaleTra);
  //   canvas.translate(go.pos.x * scaleTra, -go.pos.y * scaleTra);
  //   // canvas.
  //   canvas.drawImage(
  //       imageRef,
  //       Offset(-80, -80),
  //       // Offset.zero,
  //       // Offset(go.pos.x, -go.pos.y),
  //       Paint()
  //         ..filterQuality = FilterQuality.high
  //         ..blendMode = BlendMode.plus
  //         ..imageFilter = ui.ImageFilter.blur(
  //           sigmaX: 1,
  //           sigmaY: 1,
  //         ));

  //   canvas.restore();
  // }

  void rotate(Canvas canvas, double cx, double cy, double angle) {
    canvas.translate(cx, cy);
    canvas.rotate(angle);
    canvas.translate(-cx, -cy);
  }

  void drawEnemy(GameObject go, Canvas canvas, Size size) {
    var enemyPaint = pEnemy;
    var radius = 20.0;
    switch (go.type) {
      case EType.fly:
      case EType.upper:
      case EType.lower:
      case EType.bouncer:
      case EType.liftBouncer:
      case EType.dropBouncer:
      case EType.player:
      case EType.particle:
        enemyPaint = pEnemy;
        break;
      case EType.heavy:
        enemyPaint = pEnemyHeavy;
        radius = 26;
        // radius = go.alive;
        break;
    }
    canvas.drawCircle(Offset(go.pos.x, -go.pos.y), radius, enemyPaint);
    canvas.drawCircle(Offset(go.pos.x, -go.pos.y), radius, pEnemyStroke);
  }

  void drawPlayer(GameObject go, Canvas canvas, Size size) {
    // pPlayer.color = lighten(pPlayer.color, (go.vel.y) * 3);
    // pPlayerStroke.color = lighten(pPlayerStroke.color, (go.vel.y) * 5);
    canvas.save();
    canvas.translate(go.pos.x, -go.pos.y);
    canvas.rotate(-atan(go.vel.clone().y));
    // canvas.skew(go.vel.y/10,0.0);
    // pPlayer.color = (pPlayer.color & 0xfefefe) >> 1;
    canvas.drawOval(
        Rect.fromCenter(
            center: Offset.zero,
            width: 40 + go.vel.clone().normalize() * 5,
            height: 40 - go.vel.clone().normalize()),
        pPlayer);
    canvas.drawOval(
        Rect.fromCenter(
            center: Offset.zero,
            width: 40 + go.vel.clone().normalize() * 5,
            height: 40 - go.vel.clone().normalize()),
        pPlayerStroke);
    canvas.restore();
  }

  Color lighten(Color c, [double percent = 10]) {
    var p = percent / 100;
    return Color.fromARGB(
        c.alpha,
        (c.red + ((255 - c.red) * p).round()) % 255,
        (c.green + ((255 - c.green) * p).round()) % 255,
        (c.blue + ((255 - c.blue) * p).round()) % 255);
  }

  void drawPlayerParticle(
      List<GameObject> particles, Canvas canvas, Size size) {
    for (var particle in particles) {
      canvas.drawCircle(
          Offset(particle.pos.x, -particle.pos.y - (particle.alive / 3) + 24),
          particle.alive.toDouble() / 2,
          pPlayerLandingParticleStroke);
    }
    for (var particle in particles) {
      canvas.drawCircle(
          Offset(particle.pos.x, -particle.pos.y - (particle.alive / 3) + 24),
          particle.alive.toDouble() / 2,
          pPlayerLandingParticle);
    }
    // final points =
    //     particles.map((e) => Offset(e.pos.x, -e.pos.y + 15)).toList();
    // drawPolygonLaser(particles, canvas);
    // canvas.drawPoints(ui.PointMode.polygon, points, pPolygon);
  }

  void drawParticle(GameObject go, Canvas canvas, Size size) {
    for (var particle in go.particles) {
      canvas.drawCircle(Offset(particle.pos.x, -particle.pos.y),
          particle.alive.toDouble() / 10, pParticle);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

class DrawingController extends ChangeNotifier {
  final List<Offset> _points = [];

  /// Perform operations on _points (data)

  void add(Offset point) {
    _points.add(point);
    notifyListeners();
  }
}
