/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'splash_page_nav_obstacle.g.dart';

/// Obstacles this page could encounter when trying to navigate.
@CopyWith()
class NavObstacle extends Equatable {
  static void emptyVoidCallback() {}

  /// Whether a [Dialog] or [SnackBar] is displayed.
  final bool dialog, snack;

  /// Call to [Navigator] for routing.
  final VoidCallback decision;

  /// Creates a [ValueNotifier] friendly representation of the above values.
  NavObstacle({
    this.dialog = false,
    this.snack = false,
    this.decision = emptyVoidCallback,
  });

  /// Evaluate if everything is ready for navigation.
  bool get evaluate => (!dialog && !snack && decision != emptyVoidCallback);

  /// Provide listener for [ValueNotifier].
  /// Executes [decision] base on [evaluate]
  static void navigateConditionally(NavObstacle other) =>
      other.evaluate ? other.decision() : null;

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [dialog, snack, decision];
}
