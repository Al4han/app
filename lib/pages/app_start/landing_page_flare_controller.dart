/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import 'package:flare_flutter/flare.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:flutter/foundation.dart';

class LandingPageFlareController extends FlareControls {
  LandingPageFlareController({
    required this.initCallback,
    this.mixSeconds = 4.0,
    this.lastPlayedAnimation = "",
  });

  FlutterActorArtboard? artboard;
  String _animationName = "";
  double mixSeconds;
  String lastPlayedAnimation;
  VoidCallback initCallback;
  bool loop = false;

  final List<FlareAnimationLayer> _animationLayers = [];

  @override
  void initialize(FlutterActorArtboard artboard) {
    this.artboard = artboard;
    initCallback.call();
  }

  void replayLastAnimation() {
    if (lastPlayedAnimation.isNotEmpty) {
      play(lastPlayedAnimation);
    }
  }

  @override
  void play(
    String name, {
    double mix = 1.0,
    double mixSeconds = 0.2,
    bool overrideSameAnimation = true,
  }) {
    if (lastPlayedAnimation == name && !overrideSameAnimation) {
      return;
    }
    _animationLayers.clear();
    _animationName = name;
    lastPlayedAnimation = name;
    if (_animationName.isNotEmpty) {
      var animation = artboard?.getAnimation(_animationName);
      if (animation != null) {
        _animationLayers.add(
          FlareAnimationLayer(_animationName, animation)
            ..mix = mix
            ..mixSeconds = 0.02,
        );
        isActive.value = true;
      }
    }
  }

  bool hasCurrentAnimation(String value) {
    if (_animationName.isEmpty || value.isEmpty) return false;
    return _animationName == value;
  }

  List<String> animations(String check) =>
      artboard?.animations.map((e) => e.name).toList() ?? <String>[];

  /// Add the [FlareAnimationLayer] of the animation named [name],
  /// to the end of the list of currently playing animation layers.
  void addAnimation(String name, {double mix = 1.0, double mixSeconds = 0.2}) {
    _animationName = name;
    if (_animationName.isNotEmpty) {
      var animation = artboard?.getAnimation(_animationName);
      if (animation != null) {
        _animationLayers.add(
          FlareAnimationLayer(_animationName, animation)
            ..mix = mix
            ..mixSeconds = mixSeconds,
        );
        isActive.value = true;
      }
    }
  }

  void forceAdvance() {
    var completed = <FlareAnimationLayer>[];

    for (var i = 0; i < _animationLayers.length; i++) {
      var layer = _animationLayers[i];
      layer.mix += 800000;
      layer.time += 800000;

      var mix = (mixSeconds == 0.0) ? 1.0 : min(1.0, layer.mix / mixSeconds);

      if (layer.animation.isLooping) {
        layer.time %= layer.animation.duration;
      }
      if (artboard != null) {
        layer.animation.apply(layer.time, artboard!, mix);
      }

      if (layer.time > layer.animation.duration) {
        completed.add(layer);
      }
    }

    addToComplete(completed);
  }

  @override
  bool advance(FlutterActorArtboard artboard, double elapsed) {
    if (_animationLayers.isEmpty) return false;
    var completed = <FlareAnimationLayer>[];

    for (var i = 0; i < _animationLayers.length; i++) {
      var layer = _animationLayers[i];
      layer.mix += elapsed;
      layer.time += elapsed;

      var mix = (mixSeconds == 0.0) ? 1.0 : min(1.0, layer.mix / mixSeconds);

      if (layer.animation.isLooping) {
        layer.time %= layer.animation.duration;
      }

      layer.animation.apply(layer.time, artboard, mix);

      if (layer.time > layer.animation.duration) {
        completed.add(layer);
      }
    }

    addToComplete(completed);

    return _animationLayers.isNotEmpty;
  }

  void addToComplete(List<FlareAnimationLayer> completed) {
    for (var i = 0; i < completed.length; i++) {
      _animationLayers.remove(completed[i]);
      onCompleted(completed[i].name);
    }
  }
}
