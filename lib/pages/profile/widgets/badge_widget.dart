/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../bloc/user/model/badge_model.dart';
import '../controller/badge_loop_controller.dart';
import '../profile_badge_card.dart';
import 'badge_progress_bar.dart';

/// Widget for displaying Badges on a user's profile
@immutable
class BadgeWidget extends StatelessWidget {
  final Badge badge;

  final FlareControls _ctrl = BadgeLoopController();

  final int currentConditions;
  final int neededConditions;

  BadgeWidget({Key? key, required this.badge})
      : neededConditions = badge.conditions[0].needed,
        currentConditions =
            min(badge.conditions[0].current, badge.conditions[0].needed),
        super(key: key);

  /// Constructor for a placeholder
  BadgeWidget.placeholder({
    Key? key,
  })  : badge = Badge.placeholder(),
        currentConditions = 0,
        neededConditions = 0,
        super(key: key);

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: () {
          Navigator.of(context).push(PageRouteBuilder(
            opaque: false,
            transitionDuration: Duration(milliseconds: 0),
            pageBuilder: (_, __, ___) => ProfileBadgeCard(badge),
          ));
        },
        child: Card(
          elevation: 3.0,
          margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          color: Theme.of(context).primaryColor,
          child: ListTile(
            leading: SizedBox(
              height: 55.0,
              width: 55.0,
              child: FlareActor(
                "assets/flare/${badge.asset}",
                alignment: Alignment.center,
                antialias: false,
                fit: BoxFit.contain,
                animation: badge.animationLevel,
                controller: _ctrl,
                callback: _ctrl.onCompleted,
                isPaused: false,
              ),
            ),
            title: Text(
              FlutterI18n.translate(context,
                  'badges.content.id_${badge.badgeId.toString()}.title'),
              style: Theme.of(context).textTheme.headline3,
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  FlutterI18n.translate(
                      context,
                      'badges.content.'
                      'id_${badge.badgeId.toString()}.description'),
                  style: Theme.of(context).textTheme.bodyText2,
                ),
                Divider(
                  height: 5,
                  color: Colors.transparent,
                ),
                BadgeProgressBar(badge: badge)
              ],
              mainAxisAlignment: MainAxisAlignment.spaceAround,
            ),
            isThreeLine: true,
          ),
        ),
      );
}
