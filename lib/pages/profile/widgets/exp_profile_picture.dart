/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../../bloc/user/model/user_model.dart';
import '../../../common/constants/routing_constants.dart';
import '../../../common/tc_theme.dart';
import 'profile_picture.dart';

/// A ProfilePicture with Exp chart
class ExpProfilePicture extends StatelessWidget {
  ExpProfilePicture({Key? key, required this.user, required this.hearts})
      : super(key: key);

  final User user;

  final int hearts;

  @override
  Widget build(BuildContext context) =>
      Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
        Stack(
          children: <Widget>[
            ProfilePicture(avatar: user.avatar),
            Container(
              height: 140,
              width: 140,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.transparent,
                border: Border.all(
                    color: Theme.of(context).primaryColor, width: 20),
              ),
            ),
            Container(
              height: 120,
              width: 120,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                      color: CorporateColors.tinyCampusBlue, width: 5)),
            ),
            Positioned(
              top: 80,
              left: 14,
              child: _buildHeartCounterButton(
                context,
                hearts,
              ),
            ),
            SizedBox(
              height: 10,
              width: 200,
            )
          ],
          alignment: Alignment.center,
        ),
      ]);

  Stack _buildHeartCounterButton(BuildContext context, int likes) {
    var sizedBoxWidth = 65.0;
    if (likes > 99) {
      sizedBoxWidth = 85.0;
    }
    return Stack(
      alignment: Alignment.centerLeft,
      children: <Widget>[
        SizedBox(
          width: sizedBoxWidth,
          height: 55,
          child: FloatingActionButton(
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            child: Container(
              width: 45,
              height: 45,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Theme.of(context).primaryColor),
              child: Icon(
                Icons.favorite,
                color: Colors.red,
                size: 30,
              ),
            ),
            onPressed: () => Navigator.pushNamed(context, profileHeartsRoute),
          ),
        ),
        Text(
          '$likes',
          style: Theme.of(context).textTheme.bodyText1,
        ),
      ],
    );
  }
}
