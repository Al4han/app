/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'home_bloc.dart';

/// A quasi interface for [HomeBloc]s supported events.
@immutable
abstract class HomeEvent extends Equatable {
  const HomeEvent();

  /// Performs the action on the current [HomeState]
  /// and returns corresponding next state.
  HomeState _performAction(HomeBloc bloc);

  @override
  String toString() => runtimeType.toString();

  @override
  List<Object> get props => [];
}

/// An event to enable a specified [Module].
class HomeEnableEvent extends HomeEvent {
  final int _pos;

  HomeEnableEvent(this._pos) : assert(_pos >= 0);

  @override
  HomeState _performAction(HomeBloc bloc) {
    final currentModuleList = bloc.state.modules;
    var newModuleList = List<Module>.from(bloc.state.modules);
    currentModuleList[_pos].enabled
        ? throw ArgumentError.value(_pos, "_pos", "already enabled")
        : newModuleList[_pos] = currentModuleList[_pos].toToggled();
    return HomeState(newModuleList);
  }

  @override
  String toString() => "${runtimeType.toString()}: { pos: $_pos }";

  @override
  List<Object> get props => [_pos];
}

/// An event to disable a specified [Module].
class HomeDisableEvent extends HomeEvent {
  final int _pos;

  HomeDisableEvent(this._pos) : assert(_pos >= 0);

  @override
  HomeState _performAction(HomeBloc bloc) {
    final currentModuleList = bloc.state.modules;
    var newModuleList = List<Module>.from(bloc.state.modules);
    currentModuleList[_pos].enabled
        ? newModuleList[_pos] = currentModuleList[_pos].toToggled()
        : throw ArgumentError.value(_pos, "_pos", "already disabled");
    return HomeState(newModuleList);
  }

  @override
  String toString() => "${runtimeType.toString()}: { pos: $_pos }";

  @override
  List<Object> get props => [_pos];
}

/// An event to reorder a specified [Module].
class HomeReorderEvent extends HomeEvent {
  final int _oldPos;
  final int _newPos;

  HomeReorderEvent(this._oldPos, this._newPos)
      : assert(_oldPos >= 0),
        assert(_newPos >= 0);

  @override
  HomeState _performAction(HomeBloc bloc) {
    var newModuleList = List<Module>.from(bloc.state.modules);
    newModuleList.insert(_newPos, newModuleList.removeAt(_oldPos));
    return HomeState(newModuleList);
  }

  @override
  String toString() => "${runtimeType.toString()}: "
      "{ oldPos: $_oldPos, newPos: $_newPos }";

  @override
  List<Object> get props => [_oldPos, _newPos];
}

/// An event to post a specified [HomeState] to the backend.
class HomePostEvent extends HomeEvent {
  @override
  HomeSyncingState _performAction(HomeBloc bloc) {
    _post(bloc);
    return HomeSyncingState(bloc.state.modules);
  }

  /// Tries to send next [HomeState] to backend for synchronisation.
  /// Dispatches a new [HomeEvent] based on the returned status code.
  Future<void> _post(HomeBloc bloc) async {
    try {
      final idList = bloc.state.asIdList;
      final body = jsonEncode(idList);
      await bloc._httpRepo.post(homePageUrl, body: body);
      bloc.add(HomeSyncedEvent());
    } on Exception catch (e) {
      bloc.add(HomeErrorEvent.post(e.toString()));
    }
  }
}

/// An event to fetch the [HomeState] from the backend.
class HomeFetchEvent extends HomeEvent {
  @override
  HomeSyncingState _performAction(HomeBloc bloc) {
    _fetch(bloc);
    return HomeSyncingState(bloc.state.modules);
  }

  /// Tries to load previous [HomeState] from backend for synchronisation.
  /// Dispatches a new [HomeEvent] based on the returned status code.
  Future<void> _fetch(HomeBloc bloc) async {
    try {
      final currentIdList = bloc.state.asIdList;
      final Map<String, dynamic> body =
          jsonDecode(await bloc._httpRepo.read(homePageUrl));

      final newIdList = List<int>.from(body["order"] ?? <int>[]);
      if (newIdList.isEmpty) {
        // Response is valid
        if (listEquals(newIdList, currentIdList)) {
          bloc.add(HomeSyncedEvent(newIdList));
        } else {
          var mergedIdList = <int>[];
          mergedIdList
            // Ignore so that both lines have same code style.
            // ignore: unnecessary_lambdas
            ..addAll(newIdList.where((id) => currentIdList.contains(id)))
            ..addAll(currentIdList.where((id) => !newIdList.contains(id)));
          bloc.add(HomeSyncedEvent(mergedIdList));
          Future(() => bloc.add(HomePostEvent()));
        }
      } else {
        bloc.add(HomeSyncedEvent(currentIdList));
        Future(() => bloc.add(HomePostEvent()));
      }
    } on NotFoundException catch (e) {
      bloc.add(
        HomeErrorEvent.notFound(e.toString()),
      );
    } on Exception catch (e) {
      bloc.add(
        HomeErrorEvent.fetch(e.toString()),
      );
    }
  }
}

/// An even to finish synchronisation.
class HomeSyncedEvent extends HomeEvent {
  final List<int> _idList;

  /// If no [_idList] is provided, current list of modules in state is used.
  HomeSyncedEvent([this._idList = const []]);

  @override
  HomeSyncedState _performAction(HomeBloc bloc) => (_idList.isEmpty)
      ? HomeSyncedState(bloc.state.modules)
      : HomeSyncedState.fromIdList(_idList);

  @override
  String toString() => "${runtimeType.toString()}"
      "${_idList.isEmpty ? '' : ': { idList: $_idList }'}";

  @override
  List<Object> get props => [_idList];
}

/// An event to handle errors occurring during synchronisation.
class HomeErrorEvent extends HomeEvent {
  final HomeError _errorType;
  final String _errorMsg;

  HomeErrorEvent.post(this._errorMsg) : _errorType = HomeError.post;
  HomeErrorEvent.fetch(this._errorMsg) : _errorType = HomeError.fetch;
  HomeErrorEvent.read(this._errorMsg) : _errorType = HomeError.read;

  HomeErrorEvent.write(this._errorMsg) : _errorType = HomeError.write;

  HomeErrorEvent.notFound(this._errorMsg) : _errorType = HomeError.notFound;

  @override
  HomeErrorState _performAction(HomeBloc bloc) {
    if (_errorType == HomeError.notFound) bloc.add(HomePostEvent());
    return HomeErrorState(bloc.state.modules, _errorType, _errorMsg);
  }

  @override
  String toString() => "${runtimeType.toString()}: "
      "{ errorType: ${_errorType.toString()}, errorMsg: $_errorMsg }";

  @override
  List<Object> get props => [_errorType, _errorMsg];
}

/// An event to reset the [HomeBloc] to its [initialState].
class HomeResetEvent extends HomeEvent {
  const HomeResetEvent();
  @override
  HomeState _performAction(HomeBloc bloc) => HomeState.baseState();
}
