/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../bloc/perm_hyd_bloc/perm_hyd_bloc.dart';
import '../../../common/constants/api_constants.dart';
import '../../../repositories/http/http_repository.dart';
import '../model/module.dart';
import '../model/module_list.dart';

part 'home_bloc.g.dart';
part 'home_event.dart';
part 'home_state.dart';

/// A [Bloc] that provides data to `HomePage` and `HomeSettingsPage`,
/// also allows to modify the [HomeState] via [HomeEvent]s.
///
/// Persistence is handled by extending [PermHydBloc] and our backend.
/// This relays [fromJson] and [toJson] to [HomeState].
class HomeBloc extends PermHydBloc<HomeEvent, HomeState> {
  final HttpRepository _httpRepo;

  /// Create this with a [HttpRepository] for internal calls to our Backend.
  /// Loads locally persisted state first via [PermHydBloc].
  /// If no such state is found uses [HomeState.baseState]
  /// and tries to load from server.
  HomeBloc()
      : _httpRepo = HttpRepository(),
        super(HomeState.baseState()) {
    _fix(state);
  }

  /// If the persisted state was an [HomeErrorState] tries to fix the error.
  /// Otherwise everything is fine and nothing has to be done.
  void _fix(HomeState persistedState) {
    // Merge the persisted `ModuleList` with the one used in
    // `HomePageState.baseState` to make updates somewhat possible.
    var persistedIdList = persistedState.asIdList;
    var moduleIdList = HomeState.baseState().asIdList;
    if (!listEquals(persistedIdList, moduleIdList)) {
      var mergedIdList = <int>[
        ...persistedIdList.where(
          (id) => moduleIdList.contains(id.abs()),
        ),
        ...moduleIdList.where(
          (mId) => !persistedIdList.map((pId) => pId.abs()).contains(mId),
        ),
      ];

      add(HomeSyncedEvent(mergedIdList));
    }

    // Check for error of previous execution and maybe try to fix them,
    // else check for updates from our backend.
    HomeEvent fix;
    switch (persistedState.errorType) {
      case HomeError.post:
      case HomeError.notFound:
        fix = HomePostEvent();
        break;
      case HomeError.none:
      case HomeError.fetch:
      case HomeError.write:
      case HomeError.read:
        fix = HomeFetchEvent();
    }
    Future(() => add(fix));
  }

  /// Lets the [HomeEvent._performAction] method handle the business logic
  /// and returns a new [HomeState] based on the changed values.
  ///
  /// Triggers backend synchronisation if next state is [HomeState].
  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    var newState = event._performAction(this);

    switch (newState.runtimeType) {
      case HomeState:
        if (event is! HomeResetEvent) {
          Future(() => add(HomePostEvent()));
        }
        break;
    }

    yield newState;
  }

  /// Deserializes the [HomeState] as support for [PermHydBloc].
  ///
  /// If no state can be generated, [HomeState.baseState] is used.
  @override
  HomeState fromJson(Map<String, dynamic> json) {
    try {
      return HomeState.fromJson(json);
    } on Exception catch (e) {
      add(HomeErrorEvent.read(e.toString()));
      return HomeState.baseState(
        HomeError.read,
        e.toString(),
      );
    }
  }

  /// Serializes the [HomeState] as support for [PermHydBloc].
  ///
  /// If no json can be generated, [HomeState.baseState] is used.
  @override
  Map<String, dynamic> toJson(HomeState state) {
    try {
      return state.toJson();
    } on Exception catch (e) {
      add(HomeErrorEvent.write(e.toString()));
      return HomeState.baseState(
        HomeError.write,
        e.toString(),
      ).toJson();
    }
  }

  @override
  String toString() => runtimeType.toString();
}
