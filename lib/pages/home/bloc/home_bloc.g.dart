// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_bloc.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeState _$HomeStateFromJson(Map json) => HomeState(
      (json['modules'] as List<dynamic>)
          .map((e) => Module.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
      $enumDecodeNullable(_$HomeErrorEnumMap, json['errorType']) ??
          HomeError.none,
      json['errorMsg'] as String? ?? "",
    );

Map<String, dynamic> _$HomeStateToJson(HomeState instance) => <String, dynamic>{
      'modules': instance.modules.map((e) => e.toJson()).toList(),
      'errorType': _$HomeErrorEnumMap[instance.errorType],
      'errorMsg': instance.errorMsg,
    };

const _$HomeErrorEnumMap = {
  HomeError.none: 'none',
  HomeError.post: 'post',
  HomeError.fetch: 'fetch',
  HomeError.notFound: 'notFound',
  HomeError.read: 'read',
  HomeError.write: 'write',
};
