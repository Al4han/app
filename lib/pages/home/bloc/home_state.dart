/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'home_bloc.dart';

// TODO: Make this a class, which extends error or exception?
/// A variety of errors which can occur while using [HomeState].
enum HomeError {
  /// No error occurred
  none,

  /// Error while posting to backend
  post,

  /// Error while fetching from backend
  fetch,

  /// Error while fetching from backend, no screen was persisted
  notFound,

  /// Error while reading from local persistence
  read,

  /// Error while writing to local persistence
  write,
}

/// A state object for [HomeBloc]
/// with support for [PermHydBloc] via
@immutable
@JsonSerializable(anyMap: true, explicitToJson: true)
class HomeState extends Equatable {
  final List<Module> modules;
  final HomeError errorType;
  final String errorMsg;

  /// Creates a new instance from a given list of [modules].
  HomeState(
    this.modules, [
    this.errorType = HomeError.none,
    this.errorMsg = "",
  ]) : assert(modules.isNotEmpty);

  /// Creates a new instance as specified in [moduleList].
  HomeState.baseState([
    this.errorType = HomeError.none,
    this.errorMsg = "",
  ]) : modules = moduleList;

  /// Creates a new instance from a given list of [Module.id]s.
  HomeState.fromIdList(
    List<int> idList, [
    this.errorType = HomeError.none,
    this.errorMsg = "",
  ])  : assert(idList.isNotEmpty),
        modules = idList
            .map(
              (id) => id > 0
                  ? moduleList.singleWhere((module) => module.id == id)
                  : moduleList
                      .singleWhere((module) => module.id == (-1 * id))
                      .toToggled(),
            )
            .toList();

  /// Returns a list of [Module.id]s.
  ///
  /// A negative value indicates the [Module] is disabled.
  List<int> get asIdList =>
      modules.map((module) => module.id * (module.enabled ? 1 : -1)).toList();

  /// Creates new instance from JSON as part of [JsonSerializable].
  factory HomeState.fromJson(Map<String, dynamic> json) =>
      _$HomeStateFromJson(json);

  /// Serializes this to JSON as part of [JsonSerializable].
  Map<String, dynamic> toJson() => _$HomeStateToJson(this);

  @override
  String toString() => errorType == HomeError.none
      ? "${runtimeType.toString()}: { modules: $modules }"
      : "${runtimeType.toString()}: { modules: $modules, "
          "${errorType.toString()}, errorMsg: $errorMsg }";

  @override
  List<Object> get props => [modules, errorType, errorMsg];
}

/// A [HomeState] indicating that a synchronisation is running.
class HomeSyncingState extends HomeState {
  /// Creates a new instance from a given list of [modules].
  HomeSyncingState(List<Module> modules) : super(modules);
}

/// A [HomeState] indicating that synchronisation has finished.
class HomeSyncedState extends HomeState {
  /// Creates a new instance from a given list of [modules].
  HomeSyncedState(List<Module> modules) : super(modules);

  /// Creates a new instance from a given list of [Module.id]s.
  HomeSyncedState.fromIdList(List<int> idList) : super.fromIdList(idList);
}

/// A [HomeState] indicating that an error occurred while synchronising.
class HomeErrorState extends HomeState {
  /// Creates a new instance from a given list of [modules].
  HomeErrorState(
    List<Module> modules,
    HomeError errorType,
    String errorMsg,
  ) : super(modules, errorType, errorMsg);
}
