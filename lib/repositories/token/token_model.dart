/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'token_repository.dart';

/// Model for OAuth2-Token.
@immutable
@JsonSerializable()
class Token extends Equatable {
  /// The access token as a String.
  @JsonKey(name: "access_token")
  final String accessToken;

  /// The token type as a String.
  @JsonKey(name: "token_type")
  final String tokenType;

  /// The refresh token as a String.
  @JsonKey(name: "refresh_token")
  final String refreshToken;

  /// The expiration date as TODO: [Unix Time Stamp?].
  @JsonKey(name: "expires_in")
  final int expiresIn;

  /// Governs the permissions of the user.
  final String scope;

  Token(
    this.accessToken,
    this.tokenType,
    this.refreshToken,
    this.expiresIn,
    this.scope,
  );

  /// Autogenerated Factory which creates a Token from a JSON Map.
  factory Token.fromJson(Map<String, dynamic> json) => _$TokenFromJson(json);

  /// Autogenerated method to Translate this Token into a JSON Map.
  Map<String, dynamic> toJson() => _$TokenToJson(this);

  @override
  String toString() => 'Token: { '
      '${toJson().entries.map((entry) => "${entry.key}: "
          "${entry.value.toString()}, ")}'
      ' }';

  @override
  List<Object> get props => [
        accessToken,
        tokenType,
        refreshToken,
        expiresIn,
        scope,
      ];
}
