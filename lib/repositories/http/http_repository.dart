/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart';

import '../../bloc/authentication/auth_bloc.dart' show AuthError;
import '../token/token_repository.dart';

part 'http_exceptions.dart';
part 'http_methods.dart';
part 'http_request.dart';
part 'http_response.dart';

/// Duration after which every call through [HttpRepository] gets capped.
const _globalTimeoutDuration = Duration(seconds: 10);

/// Repository for every call to backend.
/// Injects user tokens into calls so this doesn't have to be done elsewhere.
/// Retries failed requests after updating tokens.
class HttpRepository extends _HttpRepoBase {
  /// Singleton instance to be retrieved.
  static final HttpRepository _singleton =
      _mockRepo ?? HttpRepository._internal();

  /// Factory constructor to return singleton instance.
  factory HttpRepository() => _singleton;

  /// TokenRepository to inject token and handle refreshes etc.
  static final TokenRepository _tokenRepo = TokenRepository();

  /// Callback to logout the user if a call fails after [_retry] is run.
  static final ValueNotifier<void Function(AuthError t, Exception e)?>
      _authFailedNotifier = ValueNotifier(null);

  /// Allow [_authFailedCallback] to only be set once.
  void setAuthFailedCallback(
          void Function(AuthError t, Exception e) authFailedCallback) =>
      _authFailedNotifier.value ??= authFailedCallback;

  /// Actual constructor to initiate this.
  HttpRepository._internal();

  /// Use for spoofing a global http repo
  static HttpRepository? _mockRepo;

  /// Use [Connectivity] to check for online status.
  static Future<bool> _checkOnlineState() async =>
      (await Connectivity().checkConnectivity()) != ConnectivityResult.none;

  /// Making sure there is a a Callback and [AuthBloc] present
  /// to handle failed authentication attempts.
  static void _secureAuthCallback(AuthError t, Exception e) {
    if (_authFailedNotifier.value != null) {
      // Callback is already set and can be called.
      _authFailedNotifier.value!(t, e);
    } else {
      // Callback is not already set!
      // => register a self removing listener to call Callback when set.

      void _authFailedNotificationCallback() {
        _authFailedNotifier.value?.call(t, e);
        _authFailedNotifier.removeListener(_authFailedNotificationCallback);
      }

      _authFailedNotifier.addListener(_authFailedNotificationCallback);
    }
  }

  /// Resolve the actual call to [Client].
  ///
  /// For every call there is a separate [Client] instance
  /// which also gets disposed of.
  ///
  /// Retries once if [UnauthorizedException] is encountered.
  /// If the error repeats the user is logged out.
  /// This also happens if [RefreshTokenExpiredException]
  /// is encountered while retrying.
  ///
  /// For every other [Exception] the message is wrapped in
  /// an [OfflineException] if the call happened while offline.
  /// This is part of a bigger TODO: OfflineMode!
  @override
  Future<Response> _retry(
    _HttpRequest request, [
    bool refreshed = false,
    Client? _client,
  ]) async {
    final Request req;
    final Response res;
    final client = _client ?? Client();

    try {
      req = await request._buildRequest();
      res = await Response.fromStream(await client.send(req)).timeout(
        _globalTimeoutDuration,
        onTimeout: () => throw TimeoutException(
          "Request ${req.method} at ${req.url} "
          "did not receive a response",
          _globalTimeoutDuration,
        ),
      )
        .._checkResponseStatus();
      return res;
    } on UnauthorizedException catch (e, _) {
      if (!refreshed) {
        try {
          if (request.secured && e.message.contains('invalid_token')) {
            await _tokenRepo.refreshAccessToken();
          }
          return await _retry(request, true, client);
        } on RefreshTokenExpiredException catch (e) {
          _secureAuthCallback(AuthError.refreshExpired, e);
          rethrow;
        }
      } else {
        _secureAuthCallback(AuthError.unauthorized, e);
        rethrow;
      }
    } on Exception catch (e, _) {
      if (await _checkOnlineState()) {
        rethrow;
      } else {
        final localUrl = request.url;
        // TODO: global Offline State?
        throw OfflineException(
          'Tried to send $request while offline. Encountered: $e',
          localUrl is String ? Uri.parse(localUrl) : (localUrl as Uri),
        );
      }
    } finally {
      client.close();
    }
  }

  /// Dispose of allocated stuff.
  void close() {
    _authFailedNotifier.dispose();
  }

  /// Initializes the http repo with a mock repo
  @visibleForTesting
  static set mockRepo(HttpRepository httpRepository) =>
      _mockRepo = httpRepository;

  @visibleForTesting
  static HttpRepository get mockRepo => _mockRepo!;
}
