/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import '../../../../bloc/local_user_hyd_bloc/local_user_hyd_bloc.dart';
import 'bookmark_links_event.dart';
import 'bookmark_links_state.dart';

export 'bookmark_links_event.dart';
export 'bookmark_links_state.dart';

class BookmarkLinksBloc
    extends LocalUserHydBloc<BookmarkLinksEvent, BookmarkLinksState> {
  BookmarkLinksBloc() : super(InitialBookmarkLinksState());

  @override
  Stream<BookmarkLinksState> mapEventToState(BookmarkLinksEvent event) async* {
    yield event.performAction(state);
  }

  @override
  BookmarkLinksState fromJson(Map<String, dynamic> json) =>
      BookmarkLinksState.fromJson(json);

  @override
  Map<String, dynamic> toJson(BookmarkLinksState state) => state.toJson();
}
