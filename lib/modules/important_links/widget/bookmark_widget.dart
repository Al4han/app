/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../common/tc_theme.dart';
import '../bloc/bookmark_links/bookmark_links_bloc.dart';
import '../model/item.dart';

class BookmarkWidget extends StatelessWidget {
  final Item item;
  final bool bookmarked;

  BookmarkWidget({
    Key? key,
    required this.item,
    required this.bookmarked,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => IconButton(
        key: Key(item.toString()),
        icon: TweenAnimationBuilder<double>(
          duration: Duration(milliseconds: bookmarked ? 620 : 960),
          curve: Curves.elasticOut,
          tween: Tween<double>(
            begin: 1.0,
            end: bookmarked ? 1.0 : 0.85,
          ),
          builder: (context, i, child) => Transform.scale(
            scale: i,
            child: TweenAnimationBuilder<Color?>(
              duration: Duration(milliseconds: 160),
              tween: ColorTween(
                begin: bookmarked
                    ? CurrentTheme().tcBlueFont
                    : CurrentTheme().textPassive,
                end: bookmarked
                    ? CurrentTheme().tcBlueFont
                    : CurrentTheme().textPassive,
              ),
              builder: (context, color, child) => Icon(
                bookmarked ? Icons.bookmark : Icons.bookmark_border,
                color: color,
              ),
            ),
          ),
        ),
        onPressed: () => bookmarkToggle(context),
      );

  void bookmarkToggle(BuildContext context) {
    if (!bookmarked) {
      BlocProvider.of<BookmarkLinksBloc>(context)
          .add(AddBookmarkLinksEvent(item: item));
    } else {
      BlocProvider.of<BookmarkLinksBloc>(context)
          .add(RemoveBookmarkLinksEvent(item: item));
    }
  }
}
