/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:json_annotation/json_annotation.dart';

part 'zs_item.g.dart';

@JsonSerializable()
class ZsItem {
  @JsonKey(defaultValue: '')
  final String title;

  @JsonKey(defaultValue: '')
  final String subtitle;

  @JsonKey(defaultValue: '')
  final String imageUrl;

  @JsonKey(defaultValue: '')
  final String url;

  ZsItem({
    required this.title,
    required this.subtitle,
    required this.imageUrl,
    required this.url,
  });

  factory ZsItem.fromJson(Map<String, dynamic> json) => _$ZsItemFromJson(json);

  Map<String, dynamic> toJson() => _$ZsItemToJson(this);
}
