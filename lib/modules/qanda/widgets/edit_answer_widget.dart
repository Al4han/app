/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/tc_theme.dart';
import '../../../common/widgets/code_of_conduct/coc_info_text.dart';
import '../../../common/widgets/tc_form_field/tc_form_field.dart';
import '../../../common/widgets/tc_form_field/tc_form_field_model.dart';
import '../bloc/answer/answer_bloc.dart';
import '../bloc/question/question_bloc.dart';
import '../model/answer.dart';

///Widget that let's the user post an answer to a question
class EditAnswerWidget extends StatefulWidget {
  final Answer answer;
  final TextEditingController textEditingController;
  final VoidCallback closeCallback;
  final VoidCallback confirmCallback;

  EditAnswerWidget(
    this.answer,
    this.textEditingController,
    this.closeCallback,
    this.confirmCallback, {
    Key? key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => EditAnswerWidgetState();
}

class EditAnswerWidgetState extends State<EditAnswerWidget> {
  @override
  Widget build(BuildContext context) {
    var _answerBloc = BlocProvider.of<AnswerBloc>(context);
    var _questionBloc = BlocProvider.of<QuestionBloc>(context);
    return BlocConsumer(
      bloc: _answerBloc,
      listenWhen: (prevState, currentState) =>
          prevState is LoadingAnswersState &&
          currentState is AnswersLoadedState,
      listener: (context, state) => _questionBloc.add(
        UpdateSingleQuestionEvent(
          widget.answer.questionId,
        ),
      ),
      builder: (context, state) => Container(
        margin: EdgeInsets.symmetric(vertical: 5),
        child: Card(
          margin: EdgeInsets.zero,
          elevation: 0,
          child: Container(
            margin: EdgeInsets.only(top: 10),
            padding: EdgeInsets.symmetric(
              vertical: 20,
              horizontal: 10,
            ),
            color: Colors.white,
            child: Column(
              children: <Widget>[
                TCFormField(
                  formFieldModel: TCFormFieldModel(
                    title: FlutterI18n.translate(
                        context, 'modules.q_and_a.your_answer'),
                    hintText: FlutterI18n.translate(
                        context, 'modules.q_and_a.answer_empty'),
                    textInputType: TextInputType.multiline,
                    maxLength: 270,
                    valid: [
                      (value) {
                        if (value.trim().isEmpty) {
                          return FlutterI18n.translate(
                              context, 'modules.q_and_a.number_of_characters');
                        }
                        return null;
                      }
                    ],
                    controller: widget.textEditingController,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 24, left: 5, right: 5),
                  child: CocInfoTextWidget(),
                ),
                Container(
                  margin: EdgeInsets.only(
                    bottom: 10,
                    left: 5,
                    right: 10,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: RaisedButton(
                          onPressed: widget.closeCallback,
                          padding: EdgeInsets.symmetric(vertical: 16.0),
                          color: Colors.white,
                          textColor: CorporateColors.tinyCampusBlue,
                          clipBehavior: Clip.antiAlias,
                          child: I18nText(
                            'common.actions.cancel',
                            child: Text(
                              '',
                              style: Theme.of(context).textTheme.headline4,
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                            side: BorderSide(
                              color: CorporateColors.tinyCampusBlue,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: 20,
                      ),
                      Expanded(
                        child: RaisedButton(
                          onPressed: widget.confirmCallback,
                          padding: EdgeInsets.symmetric(vertical: 16.0),
                          color: CorporateColors.tinyCampusBlue,
                          textColor: Colors.white,
                          clipBehavior: Clip.antiAlias,
                          child: I18nText(
                            'common.actions.confirm',
                            child: Text(
                              '',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  ?.copyWith(color: Colors.white),
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                            side: BorderSide(
                              color: CorporateColors.tinyCampusBlue,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
