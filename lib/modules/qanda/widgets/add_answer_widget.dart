/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/tc_theme.dart';
import '../../../common/widgets/code_of_conduct/coc_info_text.dart';
import '../../../common/widgets/tc_form_field/tc_form_field.dart';
import '../../../common/widgets/tc_form_field/tc_form_field_model.dart';
import '../bloc/answer/answer_bloc.dart';
import '../bloc/edit/edit_bloc.dart';
import '../bloc/question/question_bloc.dart';

///Widget that let's the user post an answer to a question
class AddAnswerWidget extends StatefulWidget {
  final int questionID;
  final int index;
  AddAnswerWidget(
    this.questionID,
    this.index, {
    Key? key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => AddAnswerWidgetState();
}

class AddAnswerWidgetState extends State<AddAnswerWidget>
    with SingleTickerProviderStateMixin {
  final _textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var _answerBloc = BlocProvider.of<AnswerBloc>(context);
    var _questionBloc = BlocProvider.of<QuestionBloc>(context);
    return BlocConsumer<AnswerBloc, AnswerState>(
      bloc: _answerBloc,
      listenWhen: (prevState, currentState) =>
          prevState is LoadingAnswersState &&
          currentState is AnswersLoadedState,
      listener: (context, state) => _questionBloc.add(
        UpdateSingleQuestionEvent(
          widget.questionID,
        ),
      ),
      builder: (context, state) => BlocBuilder<EditBloc, EditState>(
        builder: (context, eState) => Container(
          margin: EdgeInsets.symmetric(vertical: 5),
          child: AnimatedSize(
            alignment: Alignment.topCenter,
            duration: Duration(
              milliseconds: 260,
            ),
            curve: Curves.easeInOut,
            child: Card(
              margin: EdgeInsets.zero,
              elevation: 0,
              child: eState.editing[widget.index] ?? false
                  ? Container(
                      margin: EdgeInsets.only(top: 10),
                      padding: EdgeInsets.symmetric(
                        vertical: 20,
                        horizontal: 10,
                      ),
                      color: Colors.white,
                      child: Column(
                        children: <Widget>[
                          TCFormField(
                            formFieldModel: TCFormFieldModel(
                              title: FlutterI18n.translate(
                                  context, 'modules.q_and_a.your_answer'),
                              hintText: FlutterI18n.translate(
                                  context, 'modules.q_and_a.answer_empty'),
                              textInputType: TextInputType.multiline,
                              maxLength: 270,
                              valid: [
                                (value) {
                                  if (value.trim().isEmpty) {
                                    return FlutterI18n.translate(
                                        context,
                                        'modules.q_and_a'
                                        '.number_of_characters');
                                  }
                                  return null;
                                }
                              ],
                              controller: _textController,
                            ),
                          ),
                          Container(
                            margin:
                                EdgeInsets.only(bottom: 24, left: 5, right: 5),
                            child: CocInfoTextWidget(),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              bottom: 10,
                              left: 5,
                              right: 10,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Expanded(
                                  child: RaisedButton(
                                    onPressed: () {
                                      setState(() {
                                        _textController.text = "";
                                        BlocProvider.of<EditBloc>(context)
                                            .add(CloseEditEvent(widget.index));
                                      });
                                    },
                                    padding:
                                        EdgeInsets.symmetric(vertical: 16.0),
                                    color: Colors.white,
                                    textColor: CorporateColors.tinyCampusBlue,
                                    clipBehavior: Clip.antiAlias,
                                    child: I18nText(
                                      'common.actions.cancel',
                                      child: Text(
                                        '',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline4,
                                      ),
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8),
                                      side: BorderSide(
                                        color: CorporateColors.tinyCampusBlue,
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 20,
                                ),
                                Expanded(
                                  child: RaisedButton(
                                    onPressed: () {
                                      if (_textController.text
                                          .trim()
                                          .isNotEmpty) {
                                        _answerBloc.add(
                                          PostAnswerEvent(widget.questionID,
                                              _textController.text.trim()),
                                        );
                                        _textController.text = "";
                                        BlocProvider.of<EditBloc>(context)
                                            .add(CloseEditEvent(widget.index));
                                      }
                                    },
                                    padding:
                                        EdgeInsets.symmetric(vertical: 16.0),
                                    color: CorporateColors.tinyCampusBlue,
                                    textColor: Colors.white,
                                    clipBehavior: Clip.antiAlias,
                                    child: I18nText(
                                      'common.actions.confirm',
                                      child: Text(
                                        '',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline4
                                            ?.copyWith(color: Colors.white),
                                      ),
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8),
                                      side: BorderSide(
                                        color: CorporateColors.tinyCampusBlue,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  : FlatButton(
                      onPressed: () {
                        setState(() {
                          BlocProvider.of<EditBloc>(context)
                              .add(OpenEditEvent(widget.index));
                        });
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(0.0)),
                      child: Container(
                        padding: EdgeInsets.symmetric(
                          vertical: 20,
                          horizontal: 10,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                              height: 30,
                              width: 50,
                              child: FlareActor(
                                "assets/flare/badge_qanda_answers.flr",
                                alignment: Alignment.center,
                                animation: "level3",
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                            I18nText(
                              'modules.q_and_a.write_answer',
                              child: Text(
                                '',
                                style: TextStyle(
                                  color: CorporateColors.tinyCampusBlue,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 20,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
            ),
          ),
        ),
      ),
    );
  }
}
