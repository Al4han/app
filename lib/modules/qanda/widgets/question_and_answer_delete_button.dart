/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/tc_theme.dart';
import '../../../common/tinycampus_icons.dart';
import '../../../common/widgets/dialog/tc_dialog.dart';
import '../bloc/answer/answer_bloc.dart';
import '../bloc/category/category_bloc.dart';
import '../bloc/question/question_bloc.dart';

///Button which allows the user to delete a question or an answer, if they
///are the author of it.
class QuestionAndAnswerDeleteButton extends StatelessWidget {
  final bool isQuestion;
  final int id;
  final int questionId;
  QuestionAndAnswerDeleteButton({
    Key? key,
    required this.isQuestion,
    required this.id,
    this.questionId = -1,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _questionBloc = BlocProvider.of<QuestionBloc>(context);
    var _answerBloc = BlocProvider.of<AnswerBloc>(context);
    return Container(
      width: 24,
      height: 24,
      margin: EdgeInsets.only(right: 8.0),
      child: IconButton(
        visualDensity: VisualDensity.compact,
        tooltip: FlutterI18n.translate(context, 'common.actions.delete'),
        padding: EdgeInsets.all(0),
        onPressed: () {
          TCDialog.showCustomDialog(
              context: context,
              functionActionColor: CorporateColors.cafeteriaCautionRed,
              onConfirm: () {
                if (isQuestion) {
                  _questionBloc.add(DeleteQuestionEvent(
                    id,
                    BlocProvider.of<CategoryBloc>(context).state.current.id,
                  ));
                  Navigator.pop(context);
                } else {
                  _answerBloc.add(DeleteAnswerEvent(questionId, id));
                  _questionBloc.add(UpdateSingleQuestionEvent(questionId));
                }
              },
              functionActionText:
                  FlutterI18n.translate(context, 'common.actions.delete'),
              headlineText: FlutterI18n.translate(
                  context,
                  isQuestion
                      ? 'modules.q_and_a.delete_question'
                      : 'modules.q_and_a.delete_answer'),
              bodyText: FlutterI18n.translate(
                  context,
                  isQuestion
                      ? 'modules.q_and_a.delete_question_text'
                      : 'modules.q_and_a.delete_answer_text'));
        },
        icon: Icon(
          TinyCampusIcons.trash,
          color: CorporateColors.tinyCampusIconGrey.withOpacity(0.5),
          size: 20,
        ),
      ),
    );
  }
}
