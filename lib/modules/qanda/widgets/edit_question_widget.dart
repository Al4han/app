/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/tc_theme.dart';
import '../../../common/widgets/code_of_conduct/coc_info_text.dart';
import '../../../common/widgets/tc_form_field/tc_form_field.dart';
import '../../../common/widgets/tc_form_field/tc_form_field_model.dart';

// TODO: This is eerily similar to edit_answer_widget.
///Widget that let's the user edit a question to a category
class EditQuestionWidget extends StatefulWidget {
  final VoidCallback closeCallback;
  final VoidCallback confirmCallback;
  final TextEditingController textController;

  EditQuestionWidget({
    Key? key,
    required this.closeCallback,
    required this.textController,
    required this.confirmCallback,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => EditQuestionWidgetState();
}

class EditQuestionWidgetState extends State<EditQuestionWidget> {
  @override
  Widget build(BuildContext context) => Card(
        margin: EdgeInsets.zero,
        elevation: 0,
        child: Container(
          margin: EdgeInsets.only(top: 10),
          padding: EdgeInsets.symmetric(
            vertical: 20,
            horizontal: 10,
          ),
          color: Colors.white,
          child: Column(
            children: <Widget>[
              TCFormField(
                formFieldModel: TCFormFieldModel(
                  title: FlutterI18n.translate(
                      context, 'modules.q_and_a.your_question'),
                  hintText: FlutterI18n.translate(
                      context, 'modules.q_and_a.enter_question'),
                  textInputType: TextInputType.multiline,
                  maxLength: 1024,
                  valid: [
                    (value) {
                      if (value.trim().isEmpty) {
                        return FlutterI18n.translate(
                            context, 'modules.q_and_a.number_of_characters');
                      }
                      return null;
                    }
                  ],
                  controller: widget.textController,
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 24, left: 5, right: 5),
                child: CocInfoTextWidget(),
              ),
              Container(
                margin: EdgeInsets.only(
                  bottom: 10,
                  left: 5,
                  right: 10,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                        onPressed: widget.closeCallback,
                        padding: EdgeInsets.symmetric(vertical: 16.0),
                        elevation: 0.0,
                        color: Colors.white,
                        textColor: CorporateColors.tinyCampusBlue,
                        clipBehavior: Clip.antiAlias,
                        child: I18nText(
                          'common.actions.cancel',
                          child: Text(
                            '',
                            style: Theme.of(context).textTheme.headline4,
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8),
                          side: BorderSide(
                            color: CorporateColors.tinyCampusBlue,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: 20,
                    ),
                    Expanded(
                      child: RaisedButton(
                        padding: EdgeInsets.symmetric(vertical: 16.0),
                        elevation: 0.0,
                        onPressed: widget.confirmCallback,
                        color: CorporateColors.tinyCampusBlue,
                        textColor: Colors.white,
                        clipBehavior: Clip.antiAlias,
                        child: I18nText(
                          'common.actions.confirm',
                          child: Text(
                            '',
                            style: Theme.of(context)
                                .textTheme
                                .headline4
                                ?.copyWith(color: Colors.white),
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8),
                          side: BorderSide(
                            color: CorporateColors.tinyCampusBlue,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
}
