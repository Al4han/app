/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/tc_theme.dart';
import '../model/flair.dart';

class FlairWidget extends StatelessWidget {
  final List<Flair> flairs;

  FlairWidget({
    Key? key,
    required this.flairs,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        child: flairs.isNotEmpty
            ? Text(
                _buildText(context),
                style: Theme.of(context)
                    .textTheme
                    .bodyText2
                    ?.copyWith(color: CorporateColors.tinyCampusIconGrey),
              )
            : Container(),
      );

  String _buildText(BuildContext context) {
    var text = "";
    for (var i in flairs) {
      if (flairs.indexOf(i) == 0) {
        text += FlutterI18n.translate(context, i.title);
        continue;
      }
      text += " | ${FlutterI18n.translate(context, i.title)}";
    }
    return text;
  }
}
