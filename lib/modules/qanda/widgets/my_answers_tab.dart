/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/widgets/I18nText.dart';

import '../bloc/answer/answer_bloc.dart';
import 'my_replies_widget.dart';
import 'qanda_loading_indicator.dart';

///TabView for the users own replies
class MyAnswersTab extends StatelessWidget {
  const MyAnswersTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<AnswerBloc>(context).add(LoadMyAnswersEvent());
    return BlocBuilder<AnswerBloc, AnswerState>(
      builder: (context, state) {
        switch (state.runtimeType) {
          case LoadingAnswersState:
            return QAndALoadingIndicator();
          case HelpfulState:
          case AnswerToggledState:
          case AnswersLoadedState:
            final entries = state.myReplies.entries.toList();
            if (entries.isNotEmpty) {
              return ListView.builder(
                physics: BouncingScrollPhysics(),
                shrinkWrap: true,
                itemCount: entries.length,
                itemBuilder: (context, index) {
                  final entry = entries[index];
                  return Column(
                    children: entry.value
                        .map((answer) => MyRepliesWidget(entry.key, answer))
                        .toList(),
                  );
                },
              );
            } else {
              return Padding(
                padding: const EdgeInsets.all(12.0),
                child: Center(
                  child: I18nText(
                    "modules.q_and_a.no_replies",
                    child: Text("You have not replied to anyone yet"),
                  ),
                ),
              );
            }
          default:
            return Center(
              child: Text("Something went wrong"),
            );
        }
      },
    );
  }
}
