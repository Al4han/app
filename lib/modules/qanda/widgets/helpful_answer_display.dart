/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/tc_theme.dart';
import '../../../common/tinycampus_icons.dart';
import '../../../common/widgets/tc_link_text.dart';
import '../model/answer.dart';

///Display, which highlights a Helpful Answer in the QuestionWidget or the
///MyAnswersWidget
class InlineAnswerDisplay extends StatelessWidget {
  final Answer? _answer;
  final int maxLines;

  const InlineAnswerDisplay(this._answer, {Key? key, this.maxLines = 6})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final answer = _answer;
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (answer?.isHelpful ?? false)
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Icon(
                TinyCampusIcons.star_100p,
                color: CorporateColors.tinyCampusOrange,
                size: 24,
              ),
            ),
          if (answer != null)
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (answer.isHelpful)
                    Text(
                      FlutterI18n.translate(
                        context,
                        'modules.q_and_a.helpful.marked_helpful',
                      ).toUpperCase(),
                      style: Theme.of(context).textTheme.bodyText1?.copyWith(
                            color: CorporateColors.tinyCampusOrange
                                .withOpacity(0.6),
                            fontSize: 12.0,
                          ),
                    ),
                  TCLinkText(
                    text: answer.body,
                    textAlign: TextAlign.start,
                    softWrap: true,
                    maxLines: maxLines,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.bodyText1?.copyWith(
                          color: answer.isHelpful
                              ? CorporateColors.tinyCampusOrange
                              : CurrentTheme().textSoftWhite,
                        ),
                    linkStyle: Theme.of(context).textTheme.bodyText1?.copyWith(
                          decoration: TextDecoration.underline,
                          color: answer.isHelpful
                              ? CorporateColors.tinyCampusOrange
                              : CorporateColors.tinyCampusTextSoft,
                        ),
                  ),
                ],
              ),
            ),
        ],
      ),
    );
  }
}
