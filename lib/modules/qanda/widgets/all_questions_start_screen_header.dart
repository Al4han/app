/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/constants/routing_constants.dart';
import '../../../common/tc_theme.dart';
import '../../../common/tinycampus_icons.dart';
import '../bloc/category/category_bloc.dart';
import '../model/category.dart';
import '../qanda_question_screen.dart';

///Header widget which routes to an overview of all questions
class AllQuestionsStartScreenHeader extends StatelessWidget {
  const AllQuestionsStartScreenHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final activity = BlocProvider.of<CategoryBloc>(context).state.all.fold<int>(
          0,
          (previousValue, element) => previousValue + element.countActivities,
        );

    return GestureDetector(
      onTap: () {
        BlocProvider.of<CategoryBloc>(context).add(ChangeCurrentCategoryEvent(
          current: Category.placeholder(),
        ));
        Navigator.pushNamed(context, qaAllQuestions,
            arguments: QAndAQuestionScreenArguments(showCategory: true));
      },
      child: Card(
        margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0))),
        elevation: 1.0,
        clipBehavior: Clip.antiAlias,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 20,
            horizontal: 8,
          ),
          child: Center(
            child: Column(
              children: <Widget>[
                Divider(
                  thickness: 0,
                  color: Colors.transparent,
                  height: 10,
                ),
                I18nText(
                  'modules.q_and_a.all_categories',
                  child: Text(
                    '',
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
                Divider(
                  thickness: 0,
                  color: Colors.transparent,
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      TinyCampusIcons.twentyfourHours,
                      size: 32,
                      color:
                          CorporateColors.tinyCampusIconGrey.withOpacity(0.6),
                    ),
                    activity > 0
                        ? Container(
                            margin: const EdgeInsets.only(right: 10, left: 20),
                            child: Text(
                              "+$activity",
                              style: TextStyle(
                                color: CorporateColors.qAndAActivityTextGreen,
                                fontSize: 20,
                                letterSpacing: -0.7,
                              ),
                            ),
                          )
                        : Container(
                            margin: const EdgeInsets.only(left: 20),
                          ),
                    I18nPlural(
                      'modules.q_and_a.activity.times',
                      activity,
                      child: Text(
                        '',
                        style: Theme.of(context).textTheme.bodyText1?.copyWith(
                              fontSize: 20,
                              color: activity > 0
                                  ? CurrentTheme().textSoftWhite
                                  : CorporateColors.tinyCampusIconGrey
                                      .withOpacity(0.5),
                            ),
                      ),
                    ),
                  ],
                ),
                Divider(
                  thickness: 0,
                  color: Colors.transparent,
                  height: 10,
                ),
              ],
            ),
          ),
        ),
        color: Theme.of(context).primaryColor,
      ),
    );
  }
}
