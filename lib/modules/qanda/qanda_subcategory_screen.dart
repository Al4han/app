/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../common/tc_theme.dart';
import 'bloc/category/category_bloc.dart';
import 'model/category.dart';

class QAndASubcategoryScreenArguments {
  QAndASubcategoryScreenArguments(this.category);

  Category category;
}

class QAndASubcategoryScreen extends StatelessWidget {
  final Category category;
  QAndASubcategoryScreen({
    Key? key,
    required this.category,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Color.fromARGB(255, 243, 243, 243),
        appBar: AppBar(
          title: I18nText(
            'common.actions.back',
          ),
        ),
        body: Container(
          // color: Colors.white,
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 8),
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Card(
                  margin: EdgeInsets.symmetric(horizontal: 0),
                  elevation: 0.0,
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: 38.0,
                    ),
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(bottom: 10),
                          child: Text(
                            category.title,
                            style: Theme.of(context)
                                .textTheme
                                .headline4
                                ?.copyWith(
                                    color: CorporateColors.tinyCampusIconGrey),
                          ),
                        ),
                        I18nText(
                          'modules.q_and_a.choose_subcategory',
                          child: Text(
                            '',
                            style: Theme.of(context).textTheme.headline2,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(height: 50),
                Column(
                  children: category.subcategories
                      .map((e) => _buildSingleCategory(e, context))
                      .toList(),
                ),
                Container(height: 50),
              ],
            ),
          ),
        ),
      );

  Widget _buildSingleCategory(Category category, BuildContext context) =>
      Container(
        margin: EdgeInsets.symmetric(vertical: 4.0),
        child: Card(
          elevation: 1.0,
          margin: EdgeInsets.symmetric(horizontal: 0.0),
          clipBehavior: Clip.antiAlias,
          child: Ink(
            color: Colors.white,
            child: ListTile(
              trailing: Icon(Icons.chevron_right),
              onTap: () {
                BlocProvider.of<CategoryBloc>(context).add(
                  SubscribeCategoryEvent(categoryID: category.id),
                );
                var count = 0;
                Navigator.popUntil(context, (_) => count++ >= 2);
              },
              title: Text(
                category.title,
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
          ),
        ),
      );
}
