/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../common/tc_theme.dart';
import 'widgets/my_answers_tab.dart';
import 'widgets/my_questions_tab.dart';

///Page which display both the users own questions and answers in separate tabs
class MyQuestionsPage extends StatelessWidget {
  const MyQuestionsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: I18nText(
              "modules.q_and_a.my_contributions",
              child: Text('Own contributions'),
            ),
            bottom: TabBar(
              indicatorColor: CorporateColors.tinyCampusBlue,
              tabs: [
                Tab(
                  child: I18nText(
                    "modules.q_and_a.my_questions",
                    child: Text(
                      'MY\nQUESTIONS',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        letterSpacing: -0.7,
                        color: CorporateColors.tinyCampusBlue,
                      ),
                    ),
                  ),
                ),
                Tab(
                  child: I18nText(
                    "modules.q_and_a.my_replies",
                    child: Text(
                      'MY\nANSWERS',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        letterSpacing: -0.7,
                        color: CorporateColors.tinyCampusBlue,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          body: Container(
            color: Theme.of(context).backgroundColor,
            child: TabBarView(
              children: [
                MyQuestionsTab(),
                MyAnswersTab(),
              ],
            ),
          ),
        ),
      );
}
