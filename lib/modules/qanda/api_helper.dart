/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import '../../common/constants/api_constants.dart';

/// URL for all questions
const String qAndABaseUrl = '$baseUrl/questions';

///URL for the questions the user has asked
const String myQuestionsUrl = '$qAndABaseUrl/my-questions';

///URL for the replies the user has given
const String myRepliesUrl = '$qAndABaseUrl/my-replies';

/// URL for question by ID
String questionByIdUrl(int questionId) => '$qAndABaseUrl/$questionId';

/// URL for answers by question ID
String answersByQuestionIdUrl(int questionId) =>
    '${questionByIdUrl(questionId)}/answers';

///URL for finding a single answer by ID
String answerByAnswerIdUrl(int answerId) => '$qAndABaseUrl/answers/$answerId';

///URL for marking and un-marking answers as helpful
String helpfulUrl(int questionID) =>
    '$qAndABaseUrl/answers/$questionID/helpful';

/// URL for question categories
const String questionsCategoriesUrl = "$qAndABaseUrl/categories";

/// URL for the number of new replies to own questions and question categories
const String questionsCategoriesOverviewUrl = "$qAndABaseUrl/overview";

///URL for liking questions
String likeQuestionByIdUrl(int id) => "$qAndABaseUrl/$id/like";

///URL for unliking questions
String unlikeQuestionByIdUrl(int id) => "$qAndABaseUrl/$id/unlike";

///URL for Liking answers
String likeAnswerByIdUrl(int id) => "$qAndABaseUrl/answers/$id/like";

///URL for unliking answers
String unlikeAnswerByIdUrl(int id) => "$qAndABaseUrl/answers/$id/unlike";

/// URL for reporting questions
String reportQuestionByIdUrl(int id) => "$qAndABaseUrl/$id/report";

/// URL for reporting answers
String reportAnswerByIdUrl(int id) => "$qAndABaseUrl/answers/$id/report";
