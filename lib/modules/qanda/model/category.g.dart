// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Category _$CategoryFromJson(Map json) => Category(
      id: json['id'] as int,
      title: json['title'] as String,
      description: json['description'] as String,
      countActivities: json['countActivities'] as int,
      logo: json['logo'] as String,
      subscribed: json['subscribed'] as bool,
      subcategories: (json['subcategories'] as List<dynamic>?)
              ?.map(
                  (e) => Category.fromJson(Map<String, dynamic>.from(e as Map)))
              .toList() ??
          const [],
    );

Map<String, dynamic> _$CategoryToJson(Category instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'countActivities': instance.countActivities,
      'logo': instance.logo,
      'subscribed': instance.subscribed,
      'subcategories': instance.subcategories.map((e) => e.toJson()).toList(),
    };
