/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'edit_bloc.dart';

@immutable
abstract class EditEvent {
  Future<void> _performAction(EditBloc bloc);

  EditState _nextState(EditBloc bloc);
}

class LoadEditableEvent extends EditEvent {
  final Map<int, bool> _editing;
  LoadEditableEvent(this._editing);

  @override
  EditState _nextState(EditBloc bloc) => LoadEditableState(_editing);

  @override
  Future<void> _performAction(EditBloc bloc) async {}
}

class OpenEditEvent extends EditEvent {
  final int index;
  OpenEditEvent(this.index);

  @override
  EditState _nextState(EditBloc bloc) =>
      LoadingEditableState(bloc.state.editing);

  @override
  Future<void> _performAction(EditBloc bloc) async {
    var ele = {...bloc.state.editing};
    ele.updateAll((key, value) => false);
    ele.update(index, (value) => true);
    bloc.add(LoadEditableEvent(ele));
  }
}

class CloseEditEvent extends EditEvent {
  final int index;
  CloseEditEvent(this.index);

  @override
  EditState _nextState(EditBloc bloc) =>
      LoadingEditableState(bloc.state.editing);

  @override
  Future<void> _performAction(EditBloc bloc) async {
    var ele = {...bloc.state.editing};
    ele.updateAll((key, value) => false);
    bloc.add(LoadEditableEvent(ele));
  }
}
