/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'answer_bloc.dart';

///Abstract class which defines the basis of any Answer State.
///Some private helper Methods are included to allow manipulation of the
///state without waiting for server/client communication
@immutable
abstract class AnswerState extends Equatable {
  final int questionId;
  final List<Answer> answers;
  final Map<Question, List<Answer>> myReplies;

  AnswerState({
    required this.questionId,
    required this.answers,
    required this.myReplies,
  });

  void _toggleAnswer(Answer answer) {
    var id = answer.id;
    var index = answers.indexWhere((element) => element.id == id);
    var newAnswer = answer.copyWith(
      likes: answer.isLiked ? answer.likes - 1 : answer.likes + 1,
      isLiked: !answer.isLiked,
    );
    answers
      ..removeWhere((ans) => ans.id == id)
      ..insert(
        index == -1 ? 0 : index,
        newAnswer,
      );

    for (final value in myReplies.values) {
      for (final element in value) {
        if (element.id == id && element != newAnswer) {
          index = value.indexOf(element);
          value
            ..remove(element)
            ..insert(index, newAnswer);
        }
      }
    }
  }

  List<Answer> _markHelpfulById(int id) {
    var newAnswers = [..._unmarkHelpful()];
    var helpfulIndex = answers.indexWhere((element) => element.id == id);
    newAnswers[helpfulIndex] = answers[helpfulIndex].copyWith(isHelpful: true);
    return newAnswers;
  }

  List<Answer> _unmarkHelpful() {
    var helpfulIndex =
        answers.indexWhere((element) => element.isHelpful == true);
    var newAnswers = [...answers];
    if (helpfulIndex >= 0) {
      newAnswers[helpfulIndex] =
          answers[helpfulIndex].copyWith(isHelpful: false);
    }
    return newAnswers;
  }

  int get replyCount => myReplies.values.fold<int>(0, (v, e) => v + e.length);

  @override
  List<Object> get props => [questionId, ...answers, myReplies];
}

///State which represents loading Answers for a specific Question or all of
///the users own replies.
@CopyWith()
class LoadingAnswersState extends AnswerState {
  LoadingAnswersState({
    required int questionId,
    required List<Answer> answers,
    required Map<Question, List<Answer>> myReplies,
  }) : super(
          answers: answers,
          questionId: questionId,
          myReplies: myReplies,
        );
}

///State which represents that the loading of Answers has concluded
@CopyWith()
class AnswersLoadedState extends AnswerState {
  AnswersLoadedState({
    int questionId = -1,
    List<Answer> answers = const [],
    Map<Question, List<Answer>> myReplies = const {},
  }) : super(
          answers: answers,
          questionId: questionId,
          myReplies: myReplies,
        );
}

///State which represents that changing the helpful-Value of an Answer was
///successful
@CopyWith()
class HelpfulState extends AnswersLoadedState {
  HelpfulState({
    required int questionId,
    required List<Answer> answers,
    required Map<Question, List<Answer>> myReplies,
  }) : super(
          answers: answers,
          questionId: questionId,
          myReplies: myReplies,
        );
}

///State which represents that changing the [liked] and [likes] value of an
///Answer was successful
@CopyWith()
class AnswerToggledState extends AnswersLoadedState {
  AnswerToggledState({
    required int questionId,
    required List<Answer> answers,
    required Map<Question, List<Answer>> myReplies,
  }) : super(
          answers: answers,
          questionId: questionId,
          myReplies: myReplies,
        );
}
