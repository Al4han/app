// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'answer_bloc.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension LoadingAnswersStateCopyWith on LoadingAnswersState {
  LoadingAnswersState copyWith({
    List<Answer>? answers,
    Map<Question, List<Answer>>? myReplies,
    int? questionId,
  }) {
    return LoadingAnswersState(
      answers: answers ?? this.answers,
      myReplies: myReplies ?? this.myReplies,
      questionId: questionId ?? this.questionId,
    );
  }
}

extension AnswersLoadedStateCopyWith on AnswersLoadedState {
  AnswersLoadedState copyWith({
    List<Answer>? answers,
    Map<Question, List<Answer>>? myReplies,
    int? questionId,
  }) {
    return AnswersLoadedState(
      answers: answers ?? this.answers,
      myReplies: myReplies ?? this.myReplies,
      questionId: questionId ?? this.questionId,
    );
  }
}

extension HelpfulStateCopyWith on HelpfulState {
  HelpfulState copyWith({
    List<Answer>? answers,
    Map<Question, List<Answer>>? myReplies,
    int? questionId,
  }) {
    return HelpfulState(
      answers: answers ?? this.answers,
      myReplies: myReplies ?? this.myReplies,
      questionId: questionId ?? this.questionId,
    );
  }
}

extension AnswerToggledStateCopyWith on AnswerToggledState {
  AnswerToggledState copyWith({
    List<Answer>? answers,
    Map<Question, List<Answer>>? myReplies,
    int? questionId,
  }) {
    return AnswerToggledState(
      answers: answers ?? this.answers,
      myReplies: myReplies ?? this.myReplies,
      questionId: questionId ?? this.questionId,
    );
  }
}
