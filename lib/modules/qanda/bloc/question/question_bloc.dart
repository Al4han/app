/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:collection';
import 'dart:convert';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../common/tc_utility_functions.dart';
import '../../../../repositories/http/http_repository.dart';
import '../../api_helper.dart' as api;
import '../../model/question.dart';

part 'question_bloc.g.dart';
part 'question_event.dart';
part 'question_state.dart';

class QuestionBloc extends Bloc<QuestionEvent, QuestionState> {
  QuestionBloc() : super(EmptyQuestionState());

  final _eventQueue = Queue<QuestionEvent>();

  @override
  Stream<QuestionState> mapEventToState(QuestionEvent event) async* {
    if (_eventQueue.isEmpty) {
      yield* event._performAction(this);
    } else {
      _eventQueue.add(event);
    }
  }
}
