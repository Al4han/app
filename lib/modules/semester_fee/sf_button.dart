/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../common/constants/api_constants.dart';
import '../../common/tc_theme.dart';
import '../../common/widgets/tc_button.dart';

class ViewPaymentDataButton extends StatelessWidget {
  const ViewPaymentDataButton({
    Key? key,
    required this.label,
    this.iconData = Icons.credit_card,
  }) : super(key: key);

  final String label;
  final IconData iconData;

  @override
  Widget build(BuildContext context) => TCButton(
        elevation: 4,
        alternativeWidget: Container(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Wrap(
                children: [
                  Icon(
                    iconData,
                    color: CurrentTheme().tcBlue,
                  ),
                  Container(width: 8),
                  Text(label, style: Theme.of(context).textTheme.headline3),
                ],
              ),
            ],
          ),
        ),
        backgroundColor: Theme.of(context).primaryColor,
        onPressedCallback: () {
          var url = thmSemesterFeePage(useProxy: false);
          var emergencyUrl = FlutterI18n.translate(
              context, 'modules.semester_fee.emergency_url');
          try {
            debugPrint("[SF_EMERGENCY_URL] Try to read emergencyURL :"
                "$emergencyUrl");
            if (emergencyUrl.startsWith("http")) {
              url = emergencyUrl;
            } else {
              debugPrint("[SF_EMERGENCY_URL_NOT_VALID] No valid url");
              debugPrint("[SF_EMERGENCY_URL_NOT_VALID] read instead: $url");
            }
          } on Exception catch (e) {
            debugPrint("[SF_NO_EMERGENCY_URL] "
                "Could not read emergency URL: ${e.toString()}");
          }
          _launchUrl(url);
        },
      );

  Future<void> _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
