/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../common/tc_theme.dart';

class SFDetailViewIcon extends StatelessWidget {
  const SFDetailViewIcon({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            IconRowWidget(),
            IconRowWidget(highlightRow: true),
            IconRowWidget(),
          ],
        ),
      );
}

class IconRowWidget extends StatelessWidget {
  const IconRowWidget({
    Key? key,
    this.highlightRow = false,
  }) : super(key: key);

  final bool highlightRow;

  @override
  Widget build(BuildContext context) => Container(
        padding: EdgeInsets.symmetric(vertical: 1.5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 3,
              height: 3,
              margin: EdgeInsets.only(right: 3),
              decoration: BoxDecoration(
                color: CorporateColors.tinyCampusIconGrey,
                shape: BoxShape.circle,
              ),
            ),
            Container(
              width: 9,
              height: 3,
              decoration: BoxDecoration(
                  color: highlightRow
                      ? CurrentTheme().tcBlue
                      : CorporateColors.tinyCampusIconGrey,
                  borderRadius: BorderRadius.all(Radius.circular(4.0))),
            ),
          ],
        ),
      );
}
