/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../common/tc_theme.dart';
import '../../common/widgets/dialog/tc_dialog.dart';
import 'sf_helper_functions.dart';
import 'sf_interval.dart';

class InnerCircleWidget extends StatelessWidget {
  const InnerCircleWidget({
    Key? key,
    required this.hideHint,
    required this.nearestInterval,
    required this.now,
    required this.context,
    required this.color,
    required this.state,
  }) : super(key: key);

  final ValueNotifier<bool> hideHint;
  final SFInterval nearestInterval;
  final DateTime now;
  final BuildContext context;
  final Color color;
  final SFState state;

  @override
  Widget build(BuildContext context) {
    final dayDifference = determineDayDifference() + 1;
    final endPaymentOnWeekend = nearestInterval.endPayment.weekday >= 6;
    return Column(
      children: [
        Text(
          determineBigNumberString(dayDifference),
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline1?.copyWith(
              fontSize: 72.0, fontWeight: FontWeight.w800, color: color),
        ),
        SizedBox(
          width: 210,
          child: Text(
            buildOtherString(context, dayDifference),
            textAlign: TextAlign.center,
            style:
                Theme.of(context).textTheme.bodyText1?.copyWith(fontSize: 20.0),
          ),
        ),
        if (endPaymentOnWeekend && state != SFState.waitingForBegin)
          ValueListenableBuilder(
            valueListenable: hideHint,
            builder: (context, value, child) => AnimatedContainer(
              duration: Duration(milliseconds: 400),
              curve: Curves.easeInOut,
              margin: EdgeInsets.only(top: hideHint.value ? 0 : 12.0),
              child: Material(
                borderRadius: BorderRadius.all(
                  Radius.circular(20.0),
                ),
                color: hideHint.value
                    ? Colors.transparent
                    : CorporateColors.tinyCampusOrange,
                child: InkWell(
                  splashColor: Colors.white10,
                  onTap: () {
                    TCDialog.showCustomDialog(
                      context: context,
                      onConfirm: () {},
                      functionActionText:
                          FlutterI18n.translate(context, 'common.actions.ok'),
                      alternativeWidget: Container(
                        margin: EdgeInsets.symmetric(vertical: 16),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(height: 24),
                            Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    FlutterI18n.translate(
                                      context,
                                      'modules.semester_fee'
                                      '.notice_weekend_card.description',
                                    ),
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        ?.copyWith(
                                          color: CorporateColors
                                              .tinyCampusTextSoft,
                                        ),
                                  ),
                                ),
                              ],
                            ),
                            ValueListenableBuilder<bool>(
                              valueListenable: hideHint,
                              builder: (context, value, child) =>
                                  CheckboxListTile(
                                contentPadding: EdgeInsets.zero,
                                onChanged: (newValue) {
                                  hideHint.value = !hideHint.value;
                                  SFUtil.setHideHint(val: newValue ?? false);
                                },
                                value: hideHint.value,
                                title: Text(
                                  FlutterI18n.translate(
                                    context,
                                    'modules.semester_fee'
                                    '.notice_weekend_card.grey_out_notice',
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      headlineText: FlutterI18n.translate(context,
                          'modules.semester_fee.notice_weekend_card.header'),
                      bodyText: '',
                    );
                  },
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      // color: CorporateColors.tinyCampusOrange,
                      borderRadius: BorderRadius.all(
                        Radius.circular(20.0),
                      ),
                    ),
                    child: Wrap(
                      alignment: WrapAlignment.center,
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.zero,
                          child: Icon(
                            Icons.warning_amber_rounded,
                            size: 18,
                            color: hideHint.value
                                ? CorporateColors.tinyCampusIconGrey
                                    .withOpacity(0.3)
                                : Theme.of(context).primaryColor,
                          ),
                        ),
                        Text(
                            FlutterI18n.translate(
                              context,
                              'modules.semester_fee.notice_weekend_card.header',
                            ).toUpperCase(),
                            style: TextStyle(
                              color: hideHint.value
                                  ? CorporateColors.tinyCampusIconGrey
                                      .withOpacity(0.3)
                                  : Theme.of(context).primaryColor,
                            )),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
      ],
    );
  }

  int determineDayDifference() {
    switch (state) {
      case SFState.waitingForBegin:
        return nearestInterval.beginPayment.difference(now).inDays < 0
            ? 0
            : nearestInterval.beginPayment.difference(now).inDays;
      case SFState.beginPayment:
        return nearestInterval.endPayment.difference(now).inDays < 0
            ? 0
            : nearestInterval.endPayment.difference(now).inDays;
      case SFState.urgentPayment:
        return nearestInterval.endPayment.difference(now).inDays < 0
            ? 0
            : nearestInterval.endPayment.difference(now).inDays;
      case SFState.error:
        return -1;
      default:
        return 0;
    }
  }

  String buildOtherString(BuildContext context, int dayDifference) {
    switch (state) {
      case SFState.waitingForBegin:
        return dayDifference < 0
            ? "?"
            : FlutterI18n.plural(context,
                'modules.semester_fee.count_days_begin_pay.day', dayDifference);
      case SFState.beginPayment:
        return dayDifference < 0
            ? "?"
            : FlutterI18n.plural(
                context, 'modules.semester_fee.count_days.day', dayDifference);
      case SFState.urgentPayment:
        return dayDifference < 0
            ? "?"
            : FlutterI18n.plural(
                context, 'modules.semester_fee.count_days.day', dayDifference);
      case SFState.error:
        return "Error";
      default:
        return "?";
    }
  }

  String determineBigNumberString(int dayDifference) {
    switch (state) {
      case SFState.waitingForBegin:
        return dayDifference.toString();
      case SFState.beginPayment:
        return dayDifference.toString();
      case SFState.urgentPayment:
        return dayDifference.toString();
      case SFState.error:
        return "Error";
      default:
        return "?";
    }
  }
}
