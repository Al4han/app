/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:convert';

import '../../common/constants/api_constants.dart';

var _codec = utf8.fuse(base64);

/// Cafeteria base URL
const String cafeteriaBaseUrl = '$baseUrl/cafeteria';

/// Image URL for cafeteria announcements returned by MaxManager that
/// always results in HTTP 403
const String invalidImageUrl =
    '$cafeteriaImageHost/daten-extern/sw-giessen/html/fotos/big/';

/// URL for Cafeteria comments by item ID
String commentsByItemIdUrl(String itemId) =>
    '$cafeteriaBaseUrl/${_codec.encode(itemId)}/comments';

/// URL for Cafeteria ratings by item ID
String ratingsByItemIdUrl(String itemId) =>
    '$cafeteriaBaseUrl/${_codec.encode(itemId)}/ratings';

/// URL for deleting CafeteriaItem Comment
String deleteItemCommentByIdUrl(int commentId) =>
    '$cafeteriaBaseUrl/comments/$commentId';

/// URL for liking CafeteriaItem Comment
String likeItemCommentByIdUrl(int commentId) =>
    '$cafeteriaBaseUrl/comments/$commentId/like';

/// URL for un-liking CafeteriaItem Comment
String unlikeItemCommentByIdUrl(int commentId) =>
    '$cafeteriaBaseUrl/comments/$commentId/unlike';

/// URL for all Cafeteria Opening Times
String getAllOpeningTimesUrl() => '$cafeteriaBaseUrl/hours';

/// URL for reporting CafeteriaItem Comment
String reportCommentByIdUrl(int commentId) =>
    "$cafeteriaBaseUrl/comments/$commentId/report";
