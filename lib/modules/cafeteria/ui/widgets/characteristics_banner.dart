/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/constants/api_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../model/characteristic.dart';

class CharacteristicsBanner extends StatelessWidget {
  final Characteristic _characteristic;

  ///Defines the background gradient of the CharacteristicsBanner
  final Gradient backgroundGradient;

  ///Public constructor which takes a characteristic, to display in a LunchCard
  const CharacteristicsBanner({
    Key? key,
    required Characteristic characteristic,
    required this.backgroundGradient,
  })  : _characteristic = characteristic,
        super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        height: 50,
        child: Align(
          alignment: Alignment.center,
          child: I18nText(
            _characteristic.name,
            child: Text(
              '',
              maxLines: 1,
              overflow: TextOverflow.fade,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 14.0,
                  letterSpacing: -1.0,
                  fontStyle: FontStyle.italic),
            ),
          ),
        ),
        decoration: BoxDecoration(
          gradient: backgroundGradient,
          borderRadius: BorderRadius.circular(7),
        ),
      );
}

/// Wrapper class for the Vegetarian banner on the LunchCards
class VegetarianBanner extends CharacteristicsBanner {
  VegetarianBanner({
    Key? key,
  }) : super(
          key: key,
          characteristic: const Characteristic(
            abbreviation: "V",
            name: 'modules.cafeteria.vegetarian_uppercase',
            image:
                "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/V.png",
          ),
          backgroundGradient: LinearGradient(
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
            colors: [
              CorporateColors.tinyCampusBlue,
              CorporateColors.tinyCampusBlue,
            ],
          ),
        );
}

/// Wrapper class for the Vegan banner on the LunchCards
class VeganBanner extends CharacteristicsBanner {
  VeganBanner({Key? key})
      : super(
          key: key,
          characteristic: const Characteristic(
            abbreviation: "VEG",
            name: 'modules.cafeteria.vegan_uppercase',
            image:
                "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/VEG.png",
          ),
          backgroundGradient: LinearGradient(
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
            colors: [
              CorporateColors.cafeteriaVeganGreen,
              CorporateColors.cafeteriaVeganGreen,
            ],
          ),
        );
}
