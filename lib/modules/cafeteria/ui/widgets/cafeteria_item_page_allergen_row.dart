/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../model/cafeteria_item.dart';
import '../../model/cafeteria_settings.dart';

/// A Widget that takes the current [BoxConstraints], a [CafeteriaItem]
/// and the current [CafeteriaSettings] of the [User]
/// It checks the [CafeteriaItem] for elements the User has decided to avoid
/// and marks these ingredients as red
class CafeteriaItemPageAllergenRow extends StatelessWidget {
  final BoxConstraints constraints;
  final CafeteriaItem item;
  final CafeteriaSettings settings;

  const CafeteriaItemPageAllergenRow({
    Key? key,
    required this.constraints,
    required this.item,
    required this.settings,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 30,
                ),
                child: I18nText(
                  'modules.cafeteria.items.allergens',
                  child: Text(
                    'Allergene',
                    textAlign: TextAlign.start,
                    style: Theme.of(context).textTheme.headline3,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(
                  30,
                  0,
                  30,
                  30,
                ),
                width: constraints.maxWidth / 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    for (var entry in item.allergens.entries)
                      settings.allergens
                              .map((e) => e.id)
                              .toList()
                              .contains(entry.key.substring(0, 2))
                          ? Text(
                              '${entry.key}:'
                              ' ${entry.value}',
                              style: TextStyle(
                                color: Colors.red,
                              ),
                            )
                          : Text(
                              '${entry.key}:'
                              ' ${entry.value}',
                            )
                  ],
                ),
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                child: I18nText(
                  'modules.cafeteria.items.additives',
                  child: Text(
                    'Zusatzstoffe',
                    textAlign: TextAlign.start,
                    style: Theme.of(context).textTheme.headline3,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(
                  20,
                  0,
                  30,
                  30,
                ),
                width: constraints.maxWidth / 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    for (var entry in item.additives.entries)
                      Text(
                        '${entry.key}: ${entry.value}',
                        style: TextStyle(),
                      )
                  ],
                ),
              ),
            ],
          ),
        ],
      );
}
