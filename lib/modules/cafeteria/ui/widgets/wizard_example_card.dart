/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';
import '../../model/cafeteria_settings.dart';
import 'characteristics_banner.dart';

enum Category { vegan, vegetarian, none, price, other }

/// A Widget that shows the User what happens to the UI if they make
/// [CafeteriaSettings] choices
class WizardExampleCard extends StatelessWidget {
  final Category category;
  final CafeteriaSettings settings;
  final bool scaleDown;

  const WizardExampleCard({
    Key? key,
    required this.category,
    required this.settings,
    this.scaleDown = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Card(
        elevation: 3.0,
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        margin: const EdgeInsets.all(6),
        child: AspectRatio(
          aspectRatio: 1,
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Row(
                      children: <Widget>[
                        _generateAllergenHint(category, settings, context),
                        Expanded(
                          flex: 1,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  clipBehavior: Clip.antiAlias,
                                  decoration: BoxDecoration(),
                                  child: Image.asset(
                                    _getImagePath(category, settings),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.fromLTRB(
                                  10,
                                  0,
                                  0,
                                  0,
                                ),
                                child: Container(
                                  width: MediaQuery.of(context).size.width / 3 -
                                      (scaleDown ? 40 : 8),
                                  height: scaleDown ? 8 : 14,
                                  decoration: BoxDecoration(
                                    color: Color.fromRGBO(
                                      216,
                                      216,
                                      216,
                                      100,
                                    ),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(20),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(
                                  10,
                                  0,
                                  0,
                                  0,
                                ),
                                child: Container(
                                  width: MediaQuery.of(context).size.width / 3 -
                                      45,
                                  height: scaleDown ? 8 : 14,
                                  decoration: BoxDecoration(
                                    color: Color.fromRGBO(
                                      216,
                                      216,
                                      216,
                                      100,
                                    ),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(20),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              !scaleDown
                                  ? Expanded(
                                      flex: 2,
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                          10,
                                          0,
                                          10,
                                          0,
                                        ),
                                        child: category != Category.price
                                            ? Container(
                                                width: MediaQuery.of(context)
                                                            .size
                                                            .width /
                                                        6 -
                                                    20,
                                                height: 14,
                                                decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                    216,
                                                    216,
                                                    216,
                                                    100,
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                    Radius.circular(20),
                                                  ),
                                                ),
                                              )
                                            : Text(
                                                _getPrice(settings),
                                                style: TextStyle(
                                                    fontSize: 20.0,
                                                    letterSpacing: -1.0,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                      ),
                                    )
                                  : Expanded(flex: 2, child: Container()),
                              Expanded(
                                flex: scaleDown ? 9 : 3,
                                child: _generateBanner(category),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              _generateOpaqueLayer(category, settings),
            ],
          ),
        ),
      );

  Widget _generateOpaqueLayer(Category category, CafeteriaSettings settings) {
    switch (category) {
      case Category.vegan:
        return Container();
      case Category.vegetarian:
        if (settings.filter == CafeteriaFilter.vegan) {
          return Center(
            child: Container(
              color: Color.fromRGBO(255, 255, 255, 0.6),
            ),
          );
        } else {
          return Container();
        }
      case Category.none:
        if (settings.filter == CafeteriaFilter.vegan ||
            settings.filter == CafeteriaFilter.vegetarian) {
          return Center(
            child: Container(
              color: Color.fromRGBO(255, 255, 255, 0.6),
            ),
          );
        } else {
          return Container();
        }
      case Category.other:
        return Container();
      default:
        return Container();
    }
  }

  String _getPrice(CafeteriaSettings settings) {
    switch (settings.priceClass) {
      case PriceClass.student:
        return "2,60€";
      case PriceClass.staff:
        return "4,30€";
      case PriceClass.guest:
        return "5,10€";
      default:
        return "2,60€";
    }
  }

  Widget _generateBanner(Category category) {
    switch (category) {
      case Category.vegan:
        return VeganBanner();
      case Category.vegetarian:
        return VegetarianBanner();
      case Category.none:
        return Container();
      case Category.other:
        return Container();
      default:
        return Container();
    }
  }

  String _getImagePath(Category category, CafeteriaSettings settings) {
    switch (category) {
      case Category.vegan:
        return 'assets/images/cafeteria/wizard/example-vegan.png';
      case Category.vegetarian:
        if (settings.filter == CafeteriaFilter.vegan) {
          return 'assets/images/cafeteria/wizard/example-vegetarian-outline.png';
        } else {
          return 'assets/images/cafeteria/wizard/example-vegetarian.png';
        }
      case Category.none:
        if (settings.filter == CafeteriaFilter.vegan ||
            settings.filter == CafeteriaFilter.vegetarian) {
          return 'assets/images/cafeteria/wizard/example-meat-outline.png';
        } else {
          return 'assets/images/cafeteria/wizard/example-meat.png';
        }
      case Category.other:
        return 'assets/images/cafeteria/wizard/example-dish.png';
      default:
        return 'assets/images/cafeteria/wizard/example-dish.png';
    }
  }

  Widget _generateAllergenHint(
    Category category,
    CafeteriaSettings settings,
    BuildContext context,
  ) {
    if (category == Category.other) {
      var entries = <String>[];
      for (var allergen in settings.allergens) {
        entries.add(FlutterI18n.translate(context, allergen.name));
      }

      for (var dislike in settings.dislikedIngredients) {
        entries.add(FlutterI18n.translate(context, dislike.name));
      }

      if (entries.isNotEmpty) {
        const fontSize = 14.0;
        return Expanded(
          flex: 1,
          child: Container(
            clipBehavior: Clip.antiAlias,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(bottomRight: Radius.circular(0)),
              color: settings.allergens.isNotEmpty
                  ? CorporateColors.cafeteriaCautionRed
                  : CorporateColors.cafeteriaCautionYellow,
            ),
            padding: EdgeInsets.all(4.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  entries[0],
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: fontSize,
                  ),
                ),
                entries.length > 1
                    ? Text(
                        entries[1],
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: fontSize,
                        ),
                      )
                    : Container(
                        height: 0,
                      ),
                entries.length > 2
                    ? Text(
                        '...',
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: fontSize,
                        ),
                      )
                    : Container(
                        height: 0,
                      ),
              ],
            ),
          ),
        );
      } else {
        return Expanded(
          flex: 0,
          child: Container(),
        );
      }
    } else {
      return Expanded(
        flex: 0,
        child: Container(),
      );
    }
  }
}
