/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../../common/tc_theme.dart';
import '../../../../../pages/profile/controller/badge_loop_controller.dart';
import '../../../bloc/cafeteria_opening/cafeteria_opening_bloc.dart';

/// A Widget that takes a bool [_isOpen], a [List] of [eatingHours],
/// [nearestClosingTime] and [nearestOpeningTime]
/// It displays on the [CafeteriaHeaderCard] whether the [Cafeteria]
/// is currently open or closed and when it either opens or closes
class OpeningWidget extends StatelessWidget {
  const OpeningWidget({
    Key? key,
    required bool isOpen,
    required this.eatingHours,
    required this.isLoaded,
    required this.nearestClosingTime,
    required this.nearestOpeningTime,
  })  : _isOpen = isOpen,
        super(key: key);

  final bool _isOpen;
  final bool isLoaded;
  final String nearestClosingTime;
  final String nearestOpeningTime;
  final List<Map<String, String>> eatingHours;

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<CafeteriaOpeningBloc, CafeteriaOpeningState>(
          builder: (context, state) {
        if (state is LoadedOpeningState) {
          return Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  // Flare(),
                  SizedBox(
                    width: 48,
                    height: 48,
                    child: FlareActor(
                      "assets/flare/mensaplan_door.flr",
                      alignment: Alignment.center,
                      fit: BoxFit.contain,
                      animation: _isOpen ? "open" : "closed",
                      controller: BadgeLoopController(),
                      isPaused: false,
                    ),
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Center(
                    child: I18nText(
                      _isOpen
                          ? 'modules.cafeteria.header.is_open'
                          : 'modules.cafeteria.header.is_closed',
                      child: Text(
                        '',
                        style: TextStyle(
                            fontSize: 14,
                            color: _isOpen
                                ? CorporateColors.cafeteriaDarkGreen
                                : CorporateColors.tinyCampusIconGrey,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                  Center(
                    child: _isOpen
                        ? Text(
                            eatingHours.isEmpty
                                ? ''
                                : '${FlutterI18n.translate(
                                    context,
                                    'modules.cafeteria.header.closes_at',
                                  )}${isLoaded ? " $nearestClosingTime" : ''}',
                            style: TextStyle(
                                fontSize: 14,
                                color: CorporateColors.tinyCampusIconGrey,
                                fontWeight: FontWeight.w400),
                          )
                        : Text(
                            eatingHours.isEmpty
                                ? FlutterI18n.translate(
                                    context,
                                    'modules.cafeteria.header.unknown',
                                  )
                                : '${FlutterI18n.translate(
                                    context,
                                    'modules.cafeteria.header.opens_at',
                                  )}${isLoaded ? " $nearestOpeningTime" : ""}',
                            style: TextStyle(
                                fontSize: 14,
                                color: CorporateColors.tinyCampusIconGrey,
                                fontWeight: FontWeight.w400),
                          ),
                  ),
                ],
              ),
            ],
          );
        } else {
          return Container(
            height: 32,
            width: 32,
            margin: EdgeInsets.symmetric(vertical: 60.0),
            child: CircularProgressIndicator(
              strokeWidth: 2,
              valueColor: AlwaysStoppedAnimation<Color>(
                  CorporateColors.tinyCampusIconGrey),
            ),
          );
        }
      });
}
