/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import '../../model/building.dart';
import '../../model/cafeteria.dart';
import 'cafeteria_item_generator.dart';

// TODO: Cleanup
class CafeteriaGenerator {
  static Cafeteria generateCafeteria({required String name}) => Cafeteria(
      building: Building.example(name),
      items: CafeteriaItemGenerator.generateRandomItemList(
          input: 5 + Random().nextInt(50)));

  static List<Cafeteria> generateMultipleCafeteria() {
    var cafeteriaList = <Cafeteria>[];
    var amount = Random().nextInt(4) + 3;
    for (var i = 1; i <= amount; i++) {
      cafeteriaList.add(
        Cafeteria(
          building: Building.example("Beispielmensa ${i.toString()}"),
          items: CafeteriaItemGenerator.generateRandomItemList(
            input: 60 + Random().nextInt(55),
          ),
        ),
      );
    }
    return cafeteriaList;
  }
}
