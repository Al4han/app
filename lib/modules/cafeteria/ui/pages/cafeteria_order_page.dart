/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/widgets/I18nText.dart';

import '../../bloc/cafeteria_bloc.dart';
import '../../bloc/cafeteria_settings/cafeteria_settings_bloc.dart';

/// A Page that lets the user change the order of the cafeterias
class CafeteriaOrderPage extends StatefulWidget {
  const CafeteriaOrderPage({Key? key}) : super(key: key);

  @override
  _CafeteriaOrderPageState createState() => _CafeteriaOrderPageState();
}

class _CafeteriaOrderPageState extends State<CafeteriaOrderPage> {
  @override
  Widget build(BuildContext context) =>
      BlocBuilder<CafeteriaBloc, CafeteriaState>(
        builder: (context, state) => Scaffold(
          body: Column(
            children: <Widget>[
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8, horizontal: 16.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 8,
                      horizontal: 16,
                    ),
                    child: I18nText(
                      'modules.cafeteria.selection.order_hint',
                      child: Text(
                        "In der Hauptansicht werden die Mensen nacheinander in"
                        "der gewählten Reihenfolge angezeigt.",
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: ReorderableListView(
                  children: <Widget>[
                    for (var cafeteria
                        in BlocProvider.of<CafeteriaSettingsBloc>(context)
                            .state
                            .cafeteriaSelection)
                      Padding(
                        key: ValueKey(cafeteria),
                        padding: const EdgeInsets.symmetric(
                          vertical: 4,
                          horizontal: 16,
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: ListTile(
                            title: Text(cafeteria),
                            trailing: Icon(Icons.drag_handle),
                          ),
                        ),
                      )
                  ],
                  onReorder: (oldIndex, newIndex) {
                    setState(() {
                      BlocProvider.of<CafeteriaSettingsBloc>(context).add(
                          SelectCafeteriaEvent(
                              _updateCafeteriaList(oldIndex, newIndex)));
                    });
                  },
                ),
              ),
            ],
          ),
        ),
      );

  /// A function that reorders the cafeteria selection according to user input
  List<String> _updateCafeteriaList(int oldIndex, int newIndex) {
    var list = [
      for (var cafeteria in BlocProvider.of<CafeteriaSettingsBloc>(context)
          .state
          .cafeteriaSelection)
        cafeteria,
    ];
    var old = list[oldIndex];
    if (oldIndex > newIndex) {
      for (var i = oldIndex; i > newIndex; i--) {
        list[i] = list[i - 1];
      }
      list[newIndex] = old;
    } else {
      for (var i = oldIndex; i < newIndex - 1; i++) {
        list[i] = list[i + 1];
      }
      list[newIndex - 1] = old;
    }
    return list;
  }
}
