/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../bloc/cafeteria_bloc.dart';
import '../../bloc/cafeteria_opening/cafeteria_opening_bloc.dart';
import '../../bloc/cafeteria_settings/cafeteria_settings_bloc.dart';
import '../../model/cafeteria.dart';
import '../widgets/cafeteria_day.dart';
import '../widgets/lunch_card.dart';

///Page which shows Tabs for every weekday, containing [LunchCard]s for each day
class CafeteriaOverviewPage extends StatefulWidget {
  CafeteriaOverviewPage({Key? key}) : super(key: key);

  @override
  _CafeteriaOverviewPageState createState() => _CafeteriaOverviewPageState();
}

class _CafeteriaOverviewPageState extends State<CafeteriaOverviewPage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  late List<String> _weekDays;

  @override
  void initState() {
    super.initState();
    _weekDays = _getAllWeekdays(
      BlocProvider.of<CafeteriaBloc>(context).state.cafeteriaList,
    );
    _tabController = TabController(
      initialIndex: _getInitialIndex(_weekDays),
      length: _weekDays.length,
      vsync: this,
    );
    BlocProvider.of<CafeteriaOpeningBloc>(context).add(LoadOpeningEvent());
  }

  @override
  Widget build(BuildContext context) {
    var _dates = _getDatesForWeek(_weekDays);
    return BlocBuilder<CafeteriaSettingsBloc, CafeteriaSettingsState>(
      builder: (context, state) => Scaffold(
        appBar: AppBar(
          title: I18nText('modules.cafeteria.overview.title'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.more_vert),
              onPressed: () =>
                  Navigator.pushNamed(context, cafeteriaSettingsRoute),
            ),
            if (kDebugMode)
              IconButton(
                  icon: Icon(FontAwesomeIcons.hatWizard),
                  onPressed: () =>
                      Navigator.pushNamed(context, cafeteriaWizardRoute)),
          ],
          bottom: TabBar(
            isScrollable: true,
            labelStyle: TextStyle(fontSize: 18.0),
            labelPadding: EdgeInsets.symmetric(horizontal: 14, vertical: 5),
            tabs: [
              for (var day in _weekDays)
                Tab(
                  child: Column(
                    children: <Widget>[
                      Builder(
                        builder: (context) {
                          var date = "";
                          try {
                            date = DateFormat.E(
                              FlutterI18n.currentLocale(context)
                                      ?.languageCode ??
                                  'de',
                            ).format(DateTime.parse(day));
                          } on Exception {
                            date = "?";
                          }
                          return Text(date, style: _getStyle(true, day));
                        },
                      ),
                      Text(
                        _dates[_weekDays.lastIndexOf(day)],
                        style: _getStyle(false, day),
                      ),
                    ],
                  ),
                ),
            ],
            controller: _tabController,
          ),
        ),
        body: TabBarView(
          physics: BouncingScrollPhysics(),
          children: [
            for (var day in _weekDays)
              state.cafeteriaSelection.isNotEmpty
                  ? RefreshIndicator(
                      child: ListView(
                        physics: BouncingScrollPhysics(),
                        children: <Widget>[
                          for (var cafeteria in _getSelectedCafeterias(
                            BlocProvider.of<CafeteriaBloc>(context)
                                .state
                                .cafeteriaList,
                            state.cafeteriaSelection,
                          ))
                            CafeteriaDay(
                              cafeteria.weekday(day),
                              cafeteria.building.name,
                              DateTime.parse(day).weekday,
                            ),
                        ],
                      ),
                      onRefresh: () async {
                        BlocProvider.of<CafeteriaBloc>(context).add(
                            LoadingEvent(
                                loadTest:
                                    BlocProvider.of<CafeteriaSettingsBloc>(
                                            context)
                                        .state
                                        .settings
                                        .showTestApi));
                      },
                    )
                  : Center(
                      child: Padding(
                        padding: const EdgeInsets.all(30.0),
                        child: I18nText('modules.cafeteria.overview.subtitle'),
                      ),
                    )
          ],
          controller: _tabController,
        ),
      ),
    );
  }

  /// A function that takes [selectionList] from [CafeteriaSettings]
  /// and [cafeteriaList] from [CafeteriaBloc] state
  /// It returns a [List] of [Cafeteria] of all entries of [cafeteriaList]
  /// that match [selectionList]
  List<Cafeteria> _getSelectedCafeterias(
    List<Cafeteria> cafeteriaList,
    List<String> selectionList,
  ) =>
      cafeteriaList
          .where((e) => selectionList.contains(e.building.name))
          .toList();

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  /// A function that returns a [List] of timestamps for the current week
  /// with the current day being called "today" (localized)
  List<String> _getDatesForWeek(List<String> weekDays) => weekDays
      .map((e) => _isToday(e)
          ? FlutterI18n.translate(context, 'modules.cafeteria.overview.today')
          : '${e.substring(8)}.${e.substring(5, 7)}')
      .toList();

  TextStyle _getStyle(bool isAbove, String day) {
    var isToday = _isToday(day);
    var _style = isAbove
        ? TextStyle(
            color: isToday
                ? CorporateColors.tinyCampusBlue
                : CorporateColors.tinyCampusIconGrey,
            fontSize: 20,
            fontWeight: FontWeight.w400,
            letterSpacing: -1.0,
          )
        : TextStyle(
            color: isToday
                ? CorporateColors.tinyCampusBlue
                : CorporateColors.tinyCampusIconGrey,
            fontSize: isToday ? 14.0 : 12.0,
            fontWeight: isToday ? FontWeight.w600 : FontWeight.w400,
            letterSpacing: -0.5,
          );
    return _style;
  }

  /// A function that returns a [List] of every unique formatted date String
  /// available in all selected [Cafeteria]s
  List<String> _getAllWeekdays(List<Cafeteria> cafeteriaList) =>
      cafeteriaList.expand((e) => e.items).map((e) => e.date).toSet().toList();

  /// Returns the initial index for the [OverviewPage], default first
  /// future day or last, preferred current day
  int _getInitialIndex(List<String> weekDays) {
    for (var day in weekDays) {
      if (_isToday(day) ||
          DateTime.parse(day).difference(DateTime.now()).inDays >= 0) {
        return weekDays.indexOf(day);
      }
    }
    return (weekDays.length - 1);
  }

  /// Returns a bool that reflects if the chosen day is the current day
  bool _isToday(String date) =>
      (DateTime.parse(date).difference(DateTime.now()).inDays == 0 &&
          DateTime.parse(date).weekday == DateTime.now().weekday);
}
