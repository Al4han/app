/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:assorted_layout_widgets/assorted_layout_widgets.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/assets_adapter.dart';
import '../../../../common/tc_theme.dart';
import '../../api_helper.dart';
import '../../bloc/cafeteria_feedback/cafeteria_feedback_bloc.dart';
import '../../bloc/cafeteria_settings/cafeteria_settings_bloc.dart';
import '../../model/cafeteria_item.dart';
import '../../model/cafeteria_settings.dart';
import '../widgets/cafeteria_item_page_drawer.dart';
import '../widgets/characteristics_banner.dart';

/// A Page that displays a [CafeteriaItem]
class CafeteriaItemPage extends StatefulWidget {
  final CafeteriaItem _item;
  final String _name;

  CafeteriaItemPage({
    Key? key,
    required CafeteriaItem item,
    required String name,
  })  : _item = item,
        _name = name,
        super(key: key);

  @override
  _CafeteriaItemPageState createState() => _CafeteriaItemPageState();
}

class _CafeteriaItemPageState extends State<CafeteriaItemPage>
    with SingleTickerProviderStateMixin {
  final _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    var _snackBarShowing = false;
    var _settings =
        BlocProvider.of<CafeteriaSettingsBloc>(context).state.settings;
    var detectedAllergens =
        _getDetectedAllergens(_settings, widget._item, context);
    var detectedDislikes =
        _getDetectedDislikes(_settings, widget._item, context);
    return BlocBuilder<CafeteriaFeedbackBloc, CafeteriaFeedbackState>(
      builder: (context, state) => Scaffold(
        body: BlocListener<CafeteriaFeedbackBloc, CafeteriaFeedbackState>(
          listenWhen: (_, state) => state is CafeteriaFeedbackErrorState,
          listener: (context, state) {
            if (!_snackBarShowing) {
              _snackBarShowing = true;

              final message = (state as CafeteriaFeedbackErrorState).message;
              String content;

              switch (message) {
                case "AlreadyPosted":
                  content = FlutterI18n.translate(
                    context,
                    'modules.cafeteria.items.too_many_requests',
                  );
                  break;
                case "RemoveError":
                  content = FlutterI18n.translate(
                    context,
                    // TODO: make own key for cafeteria
                    'modules.organizer.ui.selected_program.try_again',
                  );
                  break;
                default:
                  content = message;
              }

              ScaffoldMessenger.of(context)
                  .showSnackBar(SnackBar(content: Text(content)))
                  .closed
                  .then((_) => _snackBarShowing = false);
            }
          },
          child: Stack(
            children: <Widget>[
              ListView(
                padding: EdgeInsets.zero,
                physics: ClampingScrollPhysics(),
                controller: _scrollController,
                children: <Widget>[
                  _generateWarningBanner(
                    detectedAllergens,
                    detectedDislikes,
                    context,
                  ),
                  ColumnSuper(
                    innerDistance: -30,
                    invert: false,
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          (widget._item.image == invalidImageUrl)
                              ? Image.asset(
                                  AssetAdapter.module(ModuleModel.cafeteria),
                                  fit: BoxFit.fitHeight,
                                )
                              : AnimatedSize(
                                  duration: Duration(milliseconds: 300),
                                  child: CachedNetworkImage(
                                    placeholderFadeInDuration:
                                        Duration(milliseconds: 350),
                                    fadeOutDuration:
                                        Duration(milliseconds: 350),
                                    fadeInDuration: Duration(milliseconds: 350),
                                    imageUrl: widget._item.image,
                                    placeholder: (context, url) => Container(
                                      height: 300,
                                      color: Colors.grey[800],
                                      child: Center(
                                        child: CircularProgressIndicator(
                                          backgroundColor: Colors.grey[600],
                                          valueColor:
                                              AlwaysStoppedAnimation<Color>(
                                                  Colors.white54),
                                        ),
                                      ),
                                    ),
                                    fit: BoxFit.cover,
                                    errorWidget: (context, url, _) =>
                                        Image.asset(
                                      AssetAdapter.module(
                                          ModuleModel.cafeteria),
                                      fit: BoxFit.fitHeight,
                                    ),
                                  ),
                                ),
                        ],
                      ),
                      CafeteriaItemPageDrawer(
                        item: widget._item,
                        settings: _settings,
                        name: widget._name,
                        detectedAllergens: detectedAllergens.join(', '),
                        detectedDislikes: detectedDislikes.join(', '),
                        getPosition: (pos) => _scrollController.animateTo(
                          pos + 20,
                          duration: Duration(milliseconds: 200),
                          curve: Curves.easeIn,
                        ),
                      ),
                    ],
                    separator: Padding(
                      padding: const EdgeInsets.only(bottom: 30.0),
                      child: ConstrainedBox(
                        constraints:
                            BoxConstraints(maxWidth: 90, maxHeight: 20),
                        child: _generateBanner(),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 30.0, horizontal: 0.0),
                child: Card(
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      bottomRight: Radius.circular(20),
                    ),
                  ),
                  margin: EdgeInsets.zero,
                  child: BackButton(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _generateBanner() {
    var _scaleSize = 2.0;
    if (widget._item.characteristics.isNotEmpty) {
      for (var characteristic in widget._item.characteristics) {
        if (characteristic.abbreviation == "V") {
          return Transform.scale(scale: _scaleSize, child: VegetarianBanner());
        }
        if (characteristic.abbreviation == "VEG") {
          return Transform.scale(scale: _scaleSize, child: VeganBanner());
        }
      }
      return Container(
        height: 0.01,
      );
    }
    return Container(height: 0.01);
  }

  Widget _generateWarningBanner(
    List<String> detectedAllergens,
    List<String> detectedDislikes,
    BuildContext context,
  ) {
    if (detectedAllergens.isNotEmpty || detectedDislikes.isNotEmpty) {
      return Container(
        color: detectedAllergens.isNotEmpty
            ? CorporateColors.cafeteriaCautionRed
            : CorporateColors.cafeteriaCautionYellow,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.width / 4,
        child: Align(
          alignment: Alignment.center,
          child: SizedBox(
            width: 230,
            child: Text(
              (detectedAllergens + detectedDislikes).toSet().join(', '),
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
              ),
            ),
          ),
        ),
      );
    } else {
      return Container(height: 0.01);
    }
  }

  List<String> _getDetectedAllergens(
    CafeteriaSettings settings,
    CafeteriaItem item,
    BuildContext context,
  ) =>
      settings.allergens
          .where((e) => item.allergens.containsKey(e.id))
          .map((e) => FlutterI18n.translate(context, e.name))
          .toList();

  List<String> _getDetectedDislikes(
    CafeteriaSettings settings,
    CafeteriaItem item,
    BuildContext context,
  ) {
    final abbreviations = item.characteristics.map((e) => e.abbreviation);
    return settings.dislikedIngredients
        .where((e) => abbreviations.contains(e.abbreviation))
        .map((e) => FlutterI18n.translate(context, e.name))
        .toList();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
