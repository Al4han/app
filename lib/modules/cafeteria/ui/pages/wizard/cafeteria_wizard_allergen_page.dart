/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../bloc/cafeteria_bloc.dart';
import '../../../model/cafeteria_settings.dart';
import '../../widgets/settings_widget.dart';
import '../../widgets/wizard_example_card.dart';

/// A wizard page that lets the user chose ingredients which they want to
/// be marked as allergic
class CafeteriaWizardAllergenPage extends StatefulWidget {
  final ValueChanged<CafeteriaSettings> onSettingsChanged;
  final CafeteriaSettings settings;

  const CafeteriaWizardAllergenPage({
    Key? key,
    required this.onSettingsChanged,
    required this.settings,
  }) : super(key: key);

  @override
  _CafeteriaWizardAllergenPageState createState() =>
      _CafeteriaWizardAllergenPageState();
}

class _CafeteriaWizardAllergenPageState
    extends State<CafeteriaWizardAllergenPage> {
  @override
  Widget build(BuildContext context) =>
      BlocBuilder<CafeteriaBloc, CafeteriaState>(
        builder: (context, state) => Column(
          children: [
            Container(
              margin: const EdgeInsets.symmetric(vertical: 20),
              height: 150,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: (widget.settings.allergens.isNotEmpty ||
                        widget.settings.dislikedIngredients.isNotEmpty)
                    ? <Widget>[
                        AspectRatio(
                          aspectRatio: 1,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Image.asset(
                                'assets/images/cafeteria/wizard/cafetaria_arrow@4x.png',
                                scale: 15,
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 10,
                                  vertical: 20,
                                ),
                                child: Text(
                                  FlutterI18n.translate(
                                    context,
                                    'modules.cafeteria.wizard.allergies_hint',
                                  ),
                                  style: TextStyle(
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        ConstrainedBox(
                          child: WizardExampleCard(
                            category: Category.other,
                            settings: widget.settings,
                            scaleDown: true,
                          ),
                          constraints: BoxConstraints(
                            maxWidth:
                                MediaQuery.of(context).size.width / 2 - 10,
                            maxHeight:
                                MediaQuery.of(context).size.width / 2 - 10,
                          ),
                        ),
                      ]
                    : <Widget>[
                        SizedBox(
                          width: MediaQuery.of(context).size.width - 100,
                          child: Text(
                            FlutterI18n.translate(
                              context,
                              'modules.cafeteria.wizard.allergies_explanation',
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 5,
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 15,
                            ),
                          ),
                        ),
                      ],
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: [
                    SettingsWidget(
                      settings: widget.settings,
                      title: 'modules.cafeteria.settings.allergic_to',
                      type: SettingsType.allergens,
                      onSettingsChanged: (_settings) =>
                          widget.onSettingsChanged(_settings),
                      children: Allergens.values.toList(),
                    ),
                    Container(
                      height: 80.0,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
}
