/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_i18n/widgets/I18nText.dart';

import '../../../../common/tc_theme.dart';
import '../../bloc/cafeteria_bloc.dart';
import '../../bloc/cafeteria_settings/cafeteria_settings_bloc.dart';
import '../../model/cafeteria_settings.dart';
import '../widgets/cafeteria_settings_selection_widget.dart';
import '../widgets/settings_widget.dart';

/// A page that displays the current [CafeteriaSettings]
/// as well as changing them
class CafeteriaSettingsPage extends StatefulWidget {
  const CafeteriaSettingsPage({Key? key}) : super(key: key);

  @override
  _CafeteriaSettingsPageState createState() => _CafeteriaSettingsPageState();
}

class _CafeteriaSettingsPageState extends State<CafeteriaSettingsPage> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<CafeteriaBloc, CafeteriaState>(
        builder: (context, state) => Scaffold(
          backgroundColor: Color.fromRGBO(250, 250, 250, 10),
          appBar: AppBar(
            title: I18nText('modules.cafeteria.settings.title'),
            actions: <Widget>[
              if (kDebugMode)
                IconButton(
                  icon: Icon(Icons.fiber_new),
                  onPressed: () {
                    BlocProvider.of<CafeteriaSettingsBloc>(context).add(
                      SettingsChangedEvent(
                        cafeteriaSelection: [],
                      ),
                    );
                    BlocProvider.of<CafeteriaBloc>(context)
                        .add(LoadTestDataEvent());
                  },
                ),
            ],
          ),
          body: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  if (kDebugMode)
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 5,
                        vertical: 15,
                      ),
                      child: Card(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 0,
                            vertical: 15,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 15),
                                child: I18nText(
                                  'modules.cafeteria.wizard.test_data',
                                  child: Text(
                                    'Test Data',
                                    style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                      color: CorporateColors.tinyCampusDarkBlue,
                                    ),
                                  ),
                                ),
                              ),
                              SwitchListTile(
                                value: BlocProvider.of<CafeteriaSettingsBloc>(
                                        context)
                                    .state
                                    .settings
                                    .showTestApi,
                                onChanged: (toggled) {
                                  BlocProvider.of<CafeteriaSettingsBloc>(
                                          context)
                                      .add(
                                    SettingsChangedEvent(
                                      settings: BlocProvider.of<
                                              CafeteriaSettingsBloc>(context)
                                          .state
                                          .settings
                                          .copyWith(
                                            showTestApi: toggled,
                                          ),
                                      cafeteriaSelection: [],
                                    ),
                                  );
                                  BlocProvider.of<CafeteriaBloc>(context)
                                      .add(LoadingEvent(loadTest: toggled));
                                },
                                title: I18nText(
                                  'modules.cafeteria.wizard.test_data_hint',
                                  child: Text(
                                    'Show Test Data',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: CorporateColors.tinyCampusDarkBlue,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  CafeteriaSettingsSelectionWidget(),
                  SettingsWidget(
                    settings: BlocProvider.of<CafeteriaSettingsBloc>(context)
                        .state
                        .settings,
                    title: 'modules.cafeteria.prices.title',
                    type: SettingsType.price,
                    onSettingsChanged: (_settings) => setState(() {
                      BlocProvider.of<CafeteriaSettingsBloc>(context)
                          .add(ChangeSettingsEvent(settings: _settings));
                    }),
                    children: PriceClass.values.toList(),
                  ),
                  SettingsWidget(
                    settings: BlocProvider.of<CafeteriaSettingsBloc>(context)
                        .state
                        .settings,
                    title: 'modules.cafeteria.settings.diet',
                    type: SettingsType.filter,
                    onSettingsChanged: (_settings) => setState(() {
                      BlocProvider.of<CafeteriaSettingsBloc>(context)
                          .add(ChangeSettingsEvent(settings: _settings));
                    }),
                    children: CafeteriaFilter.values.toList(),
                  ),
                  SettingsWidget(
                    settings: BlocProvider.of<CafeteriaSettingsBloc>(context)
                        .state
                        .settings,
                    title: 'modules.cafeteria.settings.allergens',
                    type: SettingsType.allergens,
                    onSettingsChanged: (_settings) => setState(() {
                      BlocProvider.of<CafeteriaSettingsBloc>(context)
                          .add(ChangeSettingsEvent(settings: _settings));
                    }),
                    children: Allergens.values.toList(),
                  ),
                  SettingsWidget(
                    settings: BlocProvider.of<CafeteriaSettingsBloc>(context)
                        .state
                        .settings,
                    title: 'modules.cafeteria.settings.dont_like',
                    type: SettingsType.disliked,
                    onSettingsChanged: (_settings) => setState(() {
                      BlocProvider.of<CafeteriaSettingsBloc>(context)
                          .add(ChangeSettingsEvent(settings: _settings));
                    }),
                    children: DislikedIngredients.values.toList(),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
