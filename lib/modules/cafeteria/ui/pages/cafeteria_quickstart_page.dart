/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../bloc/cafeteria_bloc.dart';
import '../../bloc/cafeteria_settings/cafeteria_settings_bloc.dart';
import '../../model/cafeteria_settings.dart';
import '../widgets/cafeteria_wizard_recommendation_widget.dart';

/// A page that gives the user the choice of skipping the Wizard
class CafeteriaQuickstartPage extends StatelessWidget {
  const CafeteriaQuickstartPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: I18nText(
            'modules.cafeteria.overview.title',
            child: Text(''),
          ),
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              flex: 6,
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: CafeteriaWizardRecommendationWidget(),
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 5),
                child: GestureDetector(
                  onTap: () {
                    BlocProvider.of<CafeteriaSettingsBloc>(context).add(
                        SettingsChangedEvent(
                            settings: CafeteriaSettings.initialSettings()
                                .copyWith(hasDoneWizard: true)));
                    BlocProvider.of<CafeteriaSettingsBloc>(context)
                        .add(SelectCafeteriaEvent([
                      for (var cafeteria
                          in BlocProvider.of<CafeteriaBloc>(context)
                              .state
                              .cafeteriaList)
                        cafeteria.building.name
                    ]));
                    Navigator.of(context)
                        .pushReplacementNamed(cafeteriaOverviewRoute);
                  },
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(12),
                      ),
                    ),
                    child: Container(
                      constraints: BoxConstraints.expand(),
                      alignment: Alignment.center,
                      child: I18nText(
                        'modules.cafeteria.wizard.quickstart',
                        child: Text(
                          '',
                          style: TextStyle(
                            fontSize: 18,
                            color: CorporateColors.tinyCampusBlue,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(10, 5, 10, 10),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context)
                          .pushReplacementNamed(cafeteriaWizardRoute);
                    },
                    child: Card(
                      color: CorporateColors.tinyCampusBlue,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(12),
                        ),
                      ),
                      child: Container(
                        alignment: Alignment.center,
                        constraints: BoxConstraints.expand(),
                        child: I18nText(
                          'modules.cafeteria.wizard.launch',
                          child: Text(
                            '',
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ))
          ],
        ),
      );
}
