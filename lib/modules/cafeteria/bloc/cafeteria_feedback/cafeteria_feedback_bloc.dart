/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../repositories/http/http_repository.dart';
import '../../api_helper.dart';
import '../../model/cafeteria_item_comment.dart';
import '../../model/cafeteria_item_feedback.dart';
import '../../model/cafeteria_item_rating.dart';

part 'cafeteria_feedback_bloc.g.dart';
part 'cafeteria_feedback_event.dart';
part 'cafeteria_feedback_state.dart';

/// BloC used for loading and posting [CafeteriaItemFeedback]
class CafeteriaFeedbackBloc
    extends Bloc<CafeteriaFeedbackEvent, CafeteriaFeedbackState> {
  final HttpRepository _httpRepo;

  CafeteriaFeedbackBloc()
      : _httpRepo = HttpRepository(),
        super(InitialCafeteriaFeedbackState());

  @override
  Stream<CafeteriaFeedbackState> mapEventToState(
      CafeteriaFeedbackEvent event) async* {
    yield event._getNextState(this);
    event._performAction(this);
  }
}
