// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cafeteria_feedback_bloc.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension CafeteriaFeedbackStateCopyWith on CafeteriaFeedbackState {
  CafeteriaFeedbackState copyWith({
    bool? commenting,
    CafeteriaItemFeedback? itemFeedback,
  }) {
    return CafeteriaFeedbackState(
      commenting: commenting ?? this.commenting,
      itemFeedback: itemFeedback ?? this.itemFeedback,
    );
  }
}
