/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'cafeteria_bloc.dart';

/// [CafeteriaEvent]s handle the raw data for various Cafeteria Data
@immutable
abstract class CafeteriaEvent extends Equatable {
  const CafeteriaEvent();

  CafeteriaState _getNextState(CafeteriaBloc bloc);

  Future<void> _performAction(CafeteriaBloc bloc);

  @override
  List<Object> get props => [];
}

/// A [CafeteriaEvent] that is called when opening the module or refreshing
/// It parses the Data to a [List] of [Cafeteria] to then construct a
/// [CafeteriaState].
/// Finally it adds a [LoadedEvent] with the new [CafeteriaState]
class LoadingEvent extends CafeteriaEvent {
  final bool loadTest;

  LoadingEvent({required this.loadTest});
  @override
  CafeteriaState _getNextState(CafeteriaBloc bloc) =>
      LoadingCafeteriaState.fromState(bloc.state);

  @override
  Future<void> _performAction(CafeteriaBloc bloc) async {
    var url = loadTest ? cafeteriaTestingUrl : cafeteriaUrl;
    debugPrint(url);
    debugPrint("Trying to fetch data");
    var result = await bloc._httpRepo.read(url, secured: false);
    var json = <String, dynamic>{};
    List<Cafeteria> cafeteriaList;
    if (jsonDecode(result) is! List) {
      debugPrint("Success!");
      json = jsonDecode(result) as Map<String, dynamic>;
      cafeteriaList =
          json.keys.map((key) => Cafeteria.fromJson(json[key])).toList();
    } else {
      debugPrint("Data received is empty!");
      cafeteriaList = [];
    }

    var loadingState = CafeteriaState._fromList(cafeteriaList);

    bloc.add(LoadedEvent(loadingState));
  }

  @override
  List<Object> get props => [];
}

/// A [CafeteriaEvent] that generates testing data in case of API failure
/// It uses generated data to create a [CafeteriaState] which is then
/// used to call [LoadedEvent]
class LoadTestDataEvent extends CafeteriaEvent {
  @override
  CafeteriaState _getNextState(CafeteriaBloc bloc) =>
      LoadingCafeteriaState.fromState(bloc.state);

  @override
  Future<void> _performAction(CafeteriaBloc bloc) async {
    var loadingState = CafeteriaState._fromList(
      CafeteriaGenerator.generateMultipleCafeteria(),
    );
    bloc.add(LoadedEvent(loadingState));
  }

  @override
  List<Object> get props => [];
}

/// [LoadedEvent] takes a [CafeteriaState] and uses it to
/// move into [LoadedCafeteriaState], which serves as an idle State
class LoadedEvent extends CafeteriaEvent {
  final CafeteriaState loadedState;
  LoadedEvent(this.loadedState);

  @override
  LoadedCafeteriaState _getNextState(CafeteriaBloc bloc) =>
      LoadedCafeteriaState(loadedState);

  @override
  Future<void> _performAction(CafeteriaBloc bloc) => Future.value();

  @override
  List<Object> get props => [loadedState];
}

class CafeteriaResetEvent extends CafeteriaEvent {
  const CafeteriaResetEvent();

  @override
  CafeteriaState _getNextState(CafeteriaBloc bloc) => InitialCafeteriaState();

  @override
  Future<void> _performAction(CafeteriaBloc bloc) async {}
}
