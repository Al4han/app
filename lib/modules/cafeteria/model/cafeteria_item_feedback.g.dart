// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cafeteria_item_feedback.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension CafeteriaItemFeedbackCopyWith on CafeteriaItemFeedback {
  CafeteriaItemFeedback copyWith({
    List<CafeteriaItemComment>? comments,
    CafeteriaItemRating? rating,
  }) {
    return CafeteriaItemFeedback(
      comments: comments ?? this.comments,
      rating: rating ?? this.rating,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CafeteriaItemFeedback _$CafeteriaItemFeedbackFromJson(Map json) =>
    CafeteriaItemFeedback(
      rating: json['rating'] == null
          ? const CafeteriaItemRating.initialData()
          : CafeteriaItemRating.fromJson(
              Map<String, dynamic>.from(json['rating'] as Map)),
      comments: (json['comments'] as List<dynamic>?)
              ?.map((e) => CafeteriaItemComment.fromJson(
                  Map<String, dynamic>.from(e as Map)))
              .toList() ??
          const [],
    );

Map<String, dynamic> _$CafeteriaItemFeedbackToJson(
        CafeteriaItemFeedback instance) =>
    <String, dynamic>{
      'rating': instance.rating.toJson(),
      'comments': instance.comments.map((e) => e.toJson()).toList(),
    };
