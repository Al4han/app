// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cafeteria_opening_hours.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension CafeteriaOpeningHoursCopyWith on CafeteriaOpeningHours {
  CafeteriaOpeningHours copyWith({
    int? cafeteriaId,
    List<Map<String, String>>? closures,
    String? description,
    Map<String, List<Map<String, String>>>? eatingHours,
    String? image,
    String? latitude,
    String? longitude,
    String? name,
    String? news,
    Map<String, List<Map<String, String>>>? openingHours,
  }) {
    return CafeteriaOpeningHours(
      cafeteriaId: cafeteriaId ?? this.cafeteriaId,
      closures: closures ?? this.closures,
      description: description ?? this.description,
      eatingHours: eatingHours ?? this.eatingHours,
      image: image ?? this.image,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      name: name ?? this.name,
      news: news ?? this.news,
      openingHours: openingHours ?? this.openingHours,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CafeteriaOpeningHours _$CafeteriaOpeningHoursFromJson(Map json) =>
    CafeteriaOpeningHours(
      cafeteriaId: json['cafeteriaId'] as int? ?? 0,
      name: json['name'] as String? ?? '',
      news: json['news'] as String? ?? '',
      description: json['description'] as String? ?? '',
      image: json['image'] as String? ?? '',
      latitude: json['latitude'] as String? ?? '',
      longitude: json['longitude'] as String? ?? '',
      openingHours: (json['openingHours'] as Map?)?.map(
            (k, e) => MapEntry(
                k as String,
                (e as List<dynamic>)
                    .map((e) => Map<String, String>.from(e as Map))
                    .toList()),
          ) ??
          {},
      eatingHours: (json['eatingHours'] as Map?)?.map(
            (k, e) => MapEntry(
                k as String,
                (e as List<dynamic>)
                    .map((e) => Map<String, String>.from(e as Map))
                    .toList()),
          ) ??
          {},
      closures: (json['closures'] as List<dynamic>?)
              ?.map((e) => Map<String, String>.from(e as Map))
              .toList() ??
          [],
    );

Map<String, dynamic> _$CafeteriaOpeningHoursToJson(
        CafeteriaOpeningHours instance) =>
    <String, dynamic>{
      'cafeteriaId': instance.cafeteriaId,
      'name': instance.name,
      'news': instance.news,
      'description': instance.description,
      'image': instance.image,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'openingHours': instance.openingHours,
      'eatingHours': instance.eatingHours,
      'closures': instance.closures,
    };
