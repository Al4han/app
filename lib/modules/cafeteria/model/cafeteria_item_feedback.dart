/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import 'cafeteria_item_comment.dart';
import 'cafeteria_item_rating.dart';

part 'cafeteria_item_feedback.g.dart';

/// A [CafeteriaItemFeedback] holds a [CafeteriaItemRating] and a [List]
/// of [comments] for a [CafeteriaItem]
@CopyWith()
@JsonSerializable(anyMap: true, explicitToJson: true)
class CafeteriaItemFeedback extends Equatable {
  /// The rating for the item
  final CafeteriaItemRating rating;

  /// The [List] of comments for the item
  final List<CafeteriaItemComment> comments;

  /// Default constructor
  const CafeteriaItemFeedback({
    this.rating = const CafeteriaItemRating.initialData(),
    this.comments = const [],
  });

  /// Initial empty constructor
  CafeteriaItemFeedback.initialData()
      : rating = CafeteriaItemRating.initialData(),
        comments = [];

  factory CafeteriaItemFeedback.fromJson(Map<String, dynamic> json) =>
      _$CafeteriaItemFeedbackFromJson(json);

  Map<String, dynamic> toJson() => _$CafeteriaItemFeedbackToJson(this);

  @override
  List<Object> get props => [rating, comments];
}
