// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'building.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Building _$BuildingFromJson(Map json) => Building(
      name: json['name'] as String,
      locality: json['locality'] as String,
      postalCode: json['postalCode'] as String,
      streetAdress: json['streetAdress'] as String,
    );

Map<String, dynamic> _$BuildingToJson(Building instance) => <String, dynamic>{
      'name': instance.name,
      'locality': instance.locality,
      'postalCode': instance.postalCode,
      'streetAdress': instance.streetAdress,
    };
