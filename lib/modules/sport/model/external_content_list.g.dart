// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'external_content_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExternalContentList _$ExternalContentListFromJson(Map<String, dynamic> json) =>
    ExternalContentList(
      entries: (json['entries'] as List<dynamic>)
          .map((e) => ExternalContent.fromJson(e as Map<String, dynamic>))
          .toList(),
      name: Map<String, String>.from(json['name'] as Map),
    );

Map<String, dynamic> _$ExternalContentListToJson(
        ExternalContentList instance) =>
    <String, dynamic>{
      'entries': instance.entries,
      'name': instance.name,
    };
