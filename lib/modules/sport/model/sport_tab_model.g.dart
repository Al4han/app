// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sport_tab_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SportTabModel _$SportTabModelFromJson(Map<String, dynamic> json) =>
    SportTabModel(
      description: Map<String, String>.from(json['description'] as Map),
      segment: (json['segment'] as List<dynamic>)
          .map((e) => ExternalContentList.fromJson(e as Map<String, dynamic>))
          .toList(),
      headline: Map<String, String>.from(json['headline'] as Map),
      tab: Map<String, String>.from(json['tab'] as Map),
    );

Map<String, dynamic> _$SportTabModelToJson(SportTabModel instance) =>
    <String, dynamic>{
      'description': instance.description,
      'segment': instance.segment,
      'headline': instance.headline,
      'tab': instance.tab,
    };
