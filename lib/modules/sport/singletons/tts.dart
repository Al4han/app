/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';
import 'dart:io' show Platform;

import 'package:flutter/foundation.dart' show VoidCallback, debugPrint, kIsWeb;
import 'package:flutter_tts/flutter_tts.dart';

import '../../../common/styled_print.dart';
import 'audio_player.dart';
// import 'package:flutter_tts/flutter_tts_web.dart';

class Tts {
  static final Tts _instance = Tts._internal();

  factory Tts() => _instance;

  Tts._internal() {
    initTts();
  }

  late FlutterTts flutterTts;
  late dynamic languages;
  late String language;

  // double _volumeCache = 0.0;

  double _volume = 1.0;

  double get volume => _volume;

  set volume(double value) {
    if (value < 0.0) value = 0.0;
    if (value > 1.0) value = 1.0;
    _volume = value;
    flutterTts.setVolume(_volume);
  }

  double pitch = 1.05;
  double rate = 0.5;
  bool isCurrentLanguageInstalled = false;

  TtsState ttsState = TtsState.stopped;

  bool get isPlaying => ttsState == TtsState.playing;

  bool get isStopped => ttsState == TtsState.stopped;

  bool get isPaused => ttsState == TtsState.paused;

  bool get isContinued => ttsState == TtsState.continued;

  bool get isIOS => !kIsWeb && Platform.isIOS;

  bool get isAndroid => !kIsWeb && Platform.isAndroid;

  bool get isWeb => kIsWeb;

  bool get isOn => _volume > 0;

  // Methods, variables ...
  Future<void> initTts() async {
    flutterTts = FlutterTts();

    await _getLanguages();

    if (isAndroid) {
      await _getEngines();
    }

    flutterTts.setStartHandler(() {
      debugPrint("Playing");
      ttsState = TtsState.playing;
    });

    flutterTts.setCompletionHandler(() {
      debugPrint("Complete");
      onComplete();
      ttsState = TtsState.stopped;
    });

    flutterTts.setCancelHandler(() {
      debugPrint("Cancel");
      ttsState = TtsState.stopped;
    });

    if (isWeb || isIOS) {
      flutterTts.setPauseHandler(() {
        debugPrint("Paused");
        ttsState = TtsState.paused;
      });

      flutterTts.setContinueHandler(() {
        debugPrint("Continued");
        ttsState = TtsState.continued;
      });
    }

    flutterTts.setErrorHandler((msg) {
      debugPrint("error: $msg");
      ttsState = TtsState.stopped;
    });

    await flutterTts.setLanguage("de-DE");
  }

  VoidCallback? onEnd;

  void clearOnEnd() {
    onEnd = null;
  }

  void onComplete() {
    try {
      sprint(
        "ON COMPLETE CALLED",
        prefix: '[SPORT]',
      );
      onEnd?.call();
    } on Exception catch (e) {
      eprint("Tried to call oncomplete in tts, but was not able to");
      eprint(e);
    }
  }

  void setLanguage(String? code) {
    /// [TODO] implement
    // FlutterI18n.currentLocale(context).languageCode
    sprint(
      "Setting Language for TTS: ${code ?? "de-DE"}",
      prefix: '[SPORT]',
    );
    flutterTts.setLanguage(code ?? "de-DE");
  }

  Future<void> _getLanguages() async {
    languages = await flutterTts.getLanguages;
  }

  Future<void> _getEngines() async {
    var engines = await flutterTts.getEngines;
    if (engines != null) {
      for (dynamic engine in engines) {
        debugPrint(engine);
      }
    }
  }

  Future<void> speak(String text) async {
    ttsState = TtsState.playing;
    await flutterTts.setVolume(1);
    // await flutterTts.setVolume(_volume); // regardless of volume
    await flutterTts.setSpeechRate(rate);
    await flutterTts.setPitch(pitch);

    if (text.isNotEmpty) {
      if (AudioPlayer().isPlaying) {
        AudioPlayer().setVolume(0.4);
      }
      await flutterTts.awaitSpeakCompletion(true);
      await flutterTts.speak(text);
      if (AudioPlayer().isPlaying) {
        AudioPlayer().setVolume(0.8);
      }
    }
  }

  Future<void> stop() async {
    var result = await flutterTts.stop();
    AudioPlayer().setVolume(0.8);
    if (result == 1) ttsState = TtsState.stopped;
  }

  Future<void> pause() async {
    if (Platform.isIOS) {
      var result = await flutterTts.pause();
      if (result == 1) ttsState = TtsState.paused;
    } else {
      eprint("[Flutter TTS] Can only pause on iOS");
    }
  }

  Future<void> toggleVolume() async {
    // pause();
    if (_volume == 0.0) {
      eprint("MUTE!");
      //await flutterTts.setVolume(_volumeCache);
      // volume = _volumeCache;
      // _volumeCache = 0.0;
      _volume = 1.0;
      flutterTts.setVolume(_volume);
    } else {
      eprint("UN-MUTE!");
      //await flutterTts.setVolume(0.0);
      // _volumeCache = volume;
      // volume = 0.0;
      _volume = 0.0;
      flutterTts.setVolume(_volume);
    }
    // return volume != 0.0;
  }

  void dispose() {
    flutterTts.stop();
  }
}

enum TtsState { playing, stopped, paused, continued }
