/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:meta/meta.dart';

import '../../model/exercise_enums.dart';
import 'exercise_settings_state.dart';

export 'exercise_settings_state.dart';

part 'exercise_settings_bloc.g.dart';
part 'exercise_settings_event.dart';

class ExerciseSettingsBloc
    extends HydratedBloc<ExerciseSettingsEvent, ExerciseSettingsState> {
  ExerciseSettingsBloc() : super(ExerciseSettingsState());

  @override
  Stream<ExerciseSettingsState> mapEventToState(
      ExerciseSettingsEvent event) async* {
    yield await event._interaction(this);
  }

  @override
  ExerciseSettingsState fromJson(Map<String, dynamic> json) {
    try {
      return ExerciseSettingsState.fromJson(json);
    } on Exception catch (_) {
      clear();
      return ExerciseSettingsState();
    }
  }

  @override
  Map<String, dynamic> toJson(ExerciseSettingsState state) {
    try {
      return state.toJson();
    } on Exception catch (_) {
      return ExerciseSettingsState().toJson();
    }
  }
}
