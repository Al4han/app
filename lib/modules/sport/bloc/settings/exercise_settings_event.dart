/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
part of 'exercise_settings_bloc.dart';

@immutable
@CopyWith()
class ExerciseSettingsEvent extends Equatable {
  final bool? automaticExercises;
  final bool? automaticInstruction;
  final bool? automaticPauses;
  final bool? enableExercises;
  final bool? enableInstruction;
  final bool? enablePauses;
  final bool? enableTextToSpeech;
  final Duration? pauseDuration;
  final ExerciseCategory? selectedExerciseCategory;
  final Duration? totalDuration;
  final bool? showEffectDesc;
  final bool? showStartingPositionDesc;
  final bool? showMotionSequenceDesc;
  final bool? showMusic;
  final bool? showRepetitionDesc;
  final bool? showVariantDesc;

  const ExerciseSettingsEvent({
    this.automaticExercises,
    this.automaticInstruction,
    this.automaticPauses,
    this.enableExercises,
    this.enableInstruction,
    this.enablePauses,
    this.enableTextToSpeech,
    this.pauseDuration,
    this.selectedExerciseCategory,
    this.totalDuration,
    this.showEffectDesc,
    this.showStartingPositionDesc,
    this.showMotionSequenceDesc,
    this.showMusic,
    this.showRepetitionDesc,
    this.showVariantDesc,
  });

  factory ExerciseSettingsEvent.fromState(ExerciseSettingsState state) =>
      ExerciseSettingsEvent(
        automaticExercises: state.automaticExercises,
        automaticInstruction: state.automaticInstruction,
        automaticPauses: state.automaticPauses,
        enableExercises: state.enableExercises,
        enableInstruction: state.enableInstruction,
        enablePauses: state.enablePauses,
        enableTextToSpeech: state.enableTextToSpeech,
        pauseDuration: state.pauseDuration,
        selectedExerciseCategory: state.selectedExerciseCategory,
        totalDuration: state.totalDuration,
        showEffectDesc: state.showEffectDesc,
        showStartingPositionDesc: state.showStartingPositionDesc,
        showMotionSequenceDesc: state.showMotionSequenceDesc,
        showMusic: state.showMusic,
        showRepetitionDesc: state.showRepetitionDesc,
        showVariantDesc: state.showVariantDesc,
      );

  Future<ExerciseSettingsState> _interaction(ExerciseSettingsBloc bloc) async =>
      bloc.state.copyWith(
        automaticExercises: automaticExercises,
        automaticInstruction: automaticInstruction,
        automaticPauses: automaticPauses,
        enableExercises: enablePauses,
        enableInstruction: enableInstruction,
        enablePauses: enablePauses,
        enableTextToSpeech: enableTextToSpeech,
        pauseDuration: pauseDuration,
        selectedExerciseCategory: selectedExerciseCategory,
        totalDuration: totalDuration,
        showEffectDesc: showEffectDesc,
        showStartingPositionDesc: showStartingPositionDesc,
        showMotionSequenceDesc: showMotionSequenceDesc,
        showMusic: showMusic,
        showRepetitionDesc: showRepetitionDesc,
        showVariantDesc: showVariantDesc,
      );

  @override
  List<Object?> get props => [
        automaticExercises,
        automaticInstruction,
        automaticPauses,
        enableExercises,
        enableInstruction,
        enablePauses,
        enableTextToSpeech,
        pauseDuration,
        selectedExerciseCategory,
        totalDuration,
        showEffectDesc,
        showStartingPositionDesc,
        showMotionSequenceDesc,
        showMusic,
        showRepetitionDesc,
        showVariantDesc,
      ];

  @override
  bool get stringify => true;
}

class ExerciseSettingsResetEvent extends ExerciseSettingsEvent {
  const ExerciseSettingsResetEvent();

  @override
  Future<ExerciseSettingsState> _interaction(ExerciseSettingsBloc bloc) async =>
      ExerciseSettingsState();
}
