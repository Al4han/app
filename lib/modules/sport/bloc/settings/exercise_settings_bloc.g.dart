// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'exercise_settings_bloc.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension ExerciseSettingsEventCopyWith on ExerciseSettingsEvent {
  ExerciseSettingsEvent copyWith({
    bool? automaticExercises,
    bool? automaticInstruction,
    bool? automaticPauses,
    bool? enableExercises,
    bool? enableInstruction,
    bool? enablePauses,
    bool? enableTextToSpeech,
    Duration? pauseDuration,
    ExerciseCategory? selectedExerciseCategory,
    bool? showEffectDesc,
    bool? showMotionSequenceDesc,
    bool? showMusic,
    bool? showRepetitionDesc,
    bool? showStartingPositionDesc,
    bool? showVariantDesc,
    Duration? totalDuration,
  }) {
    return ExerciseSettingsEvent(
      automaticExercises: automaticExercises ?? this.automaticExercises,
      automaticInstruction: automaticInstruction ?? this.automaticInstruction,
      automaticPauses: automaticPauses ?? this.automaticPauses,
      enableExercises: enableExercises ?? this.enableExercises,
      enableInstruction: enableInstruction ?? this.enableInstruction,
      enablePauses: enablePauses ?? this.enablePauses,
      enableTextToSpeech: enableTextToSpeech ?? this.enableTextToSpeech,
      pauseDuration: pauseDuration ?? this.pauseDuration,
      selectedExerciseCategory:
          selectedExerciseCategory ?? this.selectedExerciseCategory,
      showEffectDesc: showEffectDesc ?? this.showEffectDesc,
      showMotionSequenceDesc:
          showMotionSequenceDesc ?? this.showMotionSequenceDesc,
      showMusic: showMusic ?? this.showMusic,
      showRepetitionDesc: showRepetitionDesc ?? this.showRepetitionDesc,
      showStartingPositionDesc:
          showStartingPositionDesc ?? this.showStartingPositionDesc,
      showVariantDesc: showVariantDesc ?? this.showVariantDesc,
      totalDuration: totalDuration ?? this.totalDuration,
    );
  }
}
