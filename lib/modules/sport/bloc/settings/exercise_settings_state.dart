/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

import '../../model/exercise_enums.dart';

part 'exercise_settings_state.g.dart';

@immutable
@CopyWith()
@JsonSerializable(anyMap: true, explicitToJson: true, checked: true)
class ExerciseSettingsState extends Equatable {
  final ExerciseCategory selectedExerciseCategory;

  @JsonKey(fromJson: _durationFromJson)
  final Duration totalDuration;

  @JsonKey(fromJson: _durationFromJson)
  final Duration pauseDuration;
  final bool enableInstruction;
  final bool automaticInstruction;
  final bool enableExercises;
  final bool automaticExercises;
  final bool enablePauses;
  final bool automaticPauses;
  final bool enableTextToSpeech;
  final bool showEffectDesc;
  final bool showStartingPositionDesc;
  final bool showMotionSequenceDesc;
  final bool showMusic;
  final bool showRepetitionDesc;
  final bool showVariantDesc;

  ExerciseSettingsState({
    this.totalDuration = const Duration(minutes: 10),
    this.pauseDuration = const Duration(seconds: 10),
    this.selectedExerciseCategory = ExerciseCategory.mobility,
    this.enableInstruction = true,
    this.automaticInstruction = true,
    this.enableExercises = true,
    this.automaticExercises = true,
    this.enablePauses = true,
    this.automaticPauses = true,
    this.enableTextToSpeech = false,
    this.showEffectDesc = true,
    this.showStartingPositionDesc = true,
    this.showMotionSequenceDesc = true,
    this.showMusic = true,
    this.showRepetitionDesc = true,
    this.showVariantDesc = true,
  });

  factory ExerciseSettingsState.fromJson(Map<String, dynamic> json) =>
      _$ExerciseSettingsStateFromJson(json);

  Map<String, dynamic> toJson() => _$ExerciseSettingsStateToJson(this);

  @override
  List<Object> get props => [
        selectedExerciseCategory,
        totalDuration,
        pauseDuration,
        enableInstruction,
        automaticInstruction,
        enableExercises,
        automaticExercises,
        enablePauses,
        automaticPauses,
        enableTextToSpeech,
        showEffectDesc,
        showStartingPositionDesc,
        showMotionSequenceDesc,
        showMusic,
        showRepetitionDesc,
        showVariantDesc,
      ];

  @override
  bool get stringify => true;

  static Duration _durationFromJson(dynamic n) =>
      Duration(microseconds: n as int);
}
