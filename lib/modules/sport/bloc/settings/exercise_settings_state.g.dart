// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'exercise_settings_state.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension ExerciseSettingsStateCopyWith on ExerciseSettingsState {
  ExerciseSettingsState copyWith({
    bool? automaticExercises,
    bool? automaticInstruction,
    bool? automaticPauses,
    bool? enableExercises,
    bool? enableInstruction,
    bool? enablePauses,
    bool? enableTextToSpeech,
    Duration? pauseDuration,
    ExerciseCategory? selectedExerciseCategory,
    bool? showEffectDesc,
    bool? showMotionSequenceDesc,
    bool? showMusic,
    bool? showRepetitionDesc,
    bool? showStartingPositionDesc,
    bool? showVariantDesc,
    Duration? totalDuration,
  }) {
    return ExerciseSettingsState(
      automaticExercises: automaticExercises ?? this.automaticExercises,
      automaticInstruction: automaticInstruction ?? this.automaticInstruction,
      automaticPauses: automaticPauses ?? this.automaticPauses,
      enableExercises: enableExercises ?? this.enableExercises,
      enableInstruction: enableInstruction ?? this.enableInstruction,
      enablePauses: enablePauses ?? this.enablePauses,
      enableTextToSpeech: enableTextToSpeech ?? this.enableTextToSpeech,
      pauseDuration: pauseDuration ?? this.pauseDuration,
      selectedExerciseCategory:
          selectedExerciseCategory ?? this.selectedExerciseCategory,
      showEffectDesc: showEffectDesc ?? this.showEffectDesc,
      showMotionSequenceDesc:
          showMotionSequenceDesc ?? this.showMotionSequenceDesc,
      showMusic: showMusic ?? this.showMusic,
      showRepetitionDesc: showRepetitionDesc ?? this.showRepetitionDesc,
      showStartingPositionDesc:
          showStartingPositionDesc ?? this.showStartingPositionDesc,
      showVariantDesc: showVariantDesc ?? this.showVariantDesc,
      totalDuration: totalDuration ?? this.totalDuration,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExerciseSettingsState _$ExerciseSettingsStateFromJson(Map json) =>
    $checkedCreate(
      'ExerciseSettingsState',
      json,
      ($checkedConvert) {
        final val = ExerciseSettingsState(
          totalDuration: $checkedConvert(
              'totalDuration',
              (v) => v == null
                  ? const Duration(minutes: 10)
                  : ExerciseSettingsState._durationFromJson(v)),
          pauseDuration: $checkedConvert(
              'pauseDuration',
              (v) => v == null
                  ? const Duration(seconds: 10)
                  : ExerciseSettingsState._durationFromJson(v)),
          selectedExerciseCategory: $checkedConvert(
              'selectedExerciseCategory',
              (v) =>
                  $enumDecodeNullable(_$ExerciseCategoryEnumMap, v) ??
                  ExerciseCategory.mobility),
          enableInstruction:
              $checkedConvert('enableInstruction', (v) => v as bool? ?? true),
          automaticInstruction: $checkedConvert(
              'automaticInstruction', (v) => v as bool? ?? true),
          enableExercises:
              $checkedConvert('enableExercises', (v) => v as bool? ?? true),
          automaticExercises:
              $checkedConvert('automaticExercises', (v) => v as bool? ?? true),
          enablePauses:
              $checkedConvert('enablePauses', (v) => v as bool? ?? true),
          automaticPauses:
              $checkedConvert('automaticPauses', (v) => v as bool? ?? true),
          enableTextToSpeech:
              $checkedConvert('enableTextToSpeech', (v) => v as bool? ?? false),
          showEffectDesc:
              $checkedConvert('showEffectDesc', (v) => v as bool? ?? true),
          showStartingPositionDesc: $checkedConvert(
              'showStartingPositionDesc', (v) => v as bool? ?? true),
          showMotionSequenceDesc: $checkedConvert(
              'showMotionSequenceDesc', (v) => v as bool? ?? true),
          showMusic: $checkedConvert('showMusic', (v) => v as bool? ?? true),
          showRepetitionDesc:
              $checkedConvert('showRepetitionDesc', (v) => v as bool? ?? true),
          showVariantDesc:
              $checkedConvert('showVariantDesc', (v) => v as bool? ?? true),
        );
        return val;
      },
    );

Map<String, dynamic> _$ExerciseSettingsStateToJson(
        ExerciseSettingsState instance) =>
    <String, dynamic>{
      'selectedExerciseCategory':
          _$ExerciseCategoryEnumMap[instance.selectedExerciseCategory],
      'totalDuration': instance.totalDuration.inMicroseconds,
      'pauseDuration': instance.pauseDuration.inMicroseconds,
      'enableInstruction': instance.enableInstruction,
      'automaticInstruction': instance.automaticInstruction,
      'enableExercises': instance.enableExercises,
      'automaticExercises': instance.automaticExercises,
      'enablePauses': instance.enablePauses,
      'automaticPauses': instance.automaticPauses,
      'enableTextToSpeech': instance.enableTextToSpeech,
      'showEffectDesc': instance.showEffectDesc,
      'showStartingPositionDesc': instance.showStartingPositionDesc,
      'showMotionSequenceDesc': instance.showMotionSequenceDesc,
      'showMusic': instance.showMusic,
      'showRepetitionDesc': instance.showRepetitionDesc,
      'showVariantDesc': instance.showVariantDesc,
    };

const _$ExerciseCategoryEnumMap = {
  ExerciseCategory.mobility: 'mobility',
  ExerciseCategory.strength: 'strength',
  ExerciseCategory.stretch: 'stretch',
};
