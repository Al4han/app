/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import '../../../common/styled_print.dart';
import '../bloc/settings/exercise_settings_state.dart';
import '../model/exercise.dart';
import '../ui/exercise/exercise_constants.dart';

class SettingsAdapter {
  final ExerciseSettingsState state;
  final List<Exercise> exerciseList;
  final String locale;

  SettingsAdapter({
    required this.state,
    required this.exerciseList,
    required this.locale,
  });

  List<Exercise> computeExerciseProgram() {
    var copiedExercises = [...exerciseList];

    sprint(
      "Initial List Size: ${copiedExercises.length.toString()}",
      style: PrintStyle.sport,
      prefix: '[SPORT]',
    );

    copiedExercises = removeUnfittingCategories(copiedExercises);
    copiedExercises =
        fillListWithDesiredTime(state.totalDuration, copiedExercises);

    sprint(
      "Final List Size: ${copiedExercises.length.toString()}",
      style: PrintStyle.sport,
      prefix: '[SPORT]',
    );
    return copiedExercises;
  }

  /// [Remove unfitting categories]
  List<Exercise> removeUnfittingCategories(List<Exercise> editableList) {
    if (editableList.isEmpty) {
      sprint(
        "[removeUnfittingCategories] Editable list is empty",
        prefix: '[SPORT]',
      );
      return editableList;
    }
    final catString = state.selectedExerciseCategory.toString().split(".").last;
    // sprint("Selected Category: $catString", style: PrintStyle.sport);

    editableList.removeWhere((val) => val.category != catString);
    return editableList;
  }

  List<Exercise> fillListWithDesiredTime(
      Duration duration, List<Exercise> referenceList) {
    if (referenceList.isEmpty) {
      sprint(
        "[fillListWithDesiredTime] Reference list is empty",
        prefix: '[SPORT]',
      );
      return referenceList;
    }
    var desiredTime = state.totalDuration.inMilliseconds;
    var computedTotalMilliseconds = 0.0;
    final timedList = <Exercise>[];
    var shuffledList = [...referenceList]..shuffle();

    while (computedTotalMilliseconds < desiredTime) {
      if (shuffledList.isEmpty) {
        shuffledList = [...referenceList]..shuffle();
      }
      final exercise = shuffledList.first;

      /// [Instruction calculation]
      if (state.enableInstruction) {
        if (state.showEffectDesc && exercise.instructions.effect.isNotEmpty) {
          computedTotalMilliseconds += ExerciseConstants.instructionDuration;
        }
        if (state.showMotionSequenceDesc &&
            exercise.instructions.motionSequence.isNotEmpty) {
          computedTotalMilliseconds += ExerciseConstants.instructionDuration;
        }
        if (state.showRepetitionDesc &&
            exercise.instructions.repetitions.isNotEmpty) {
          computedTotalMilliseconds += ExerciseConstants.instructionDuration;
        }
        if (state.showStartingPositionDesc &&
            exercise.instructions.startingPosition.isNotEmpty) {
          computedTotalMilliseconds += ExerciseConstants.instructionDuration;
        }
        if (state.showVariantDesc && exercise.instructions.variant.isNotEmpty) {
          computedTotalMilliseconds += ExerciseConstants.instructionDuration;
        }
      }

      /// [Exercise calculation]
      computedTotalMilliseconds += exercise.totalDuration;
      sprint(
        exercise.totalDuration,
        prefix: '[SPORT]',
      );

      /// [Pause calculation]
      if (state.enablePauses) {
        computedTotalMilliseconds += state.pauseDuration.inSeconds;
      }
      timedList.add(exercise);
      shuffledList.remove(exercise);
      // sprint(computedTotalMilliseconds);
      // sprint("$desiredTime seconds remaining");
    }

    sprint(
      "Overshot desired time by an additional "
      "${(computedTotalMilliseconds - desiredTime) / 1000} seconds",
      style: PrintStyle.attention,
      prefix: '[SPORT]',
    );

    return timedList;
  }
}
