/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../../../common/tc_theme.dart';
import '../../../../common/tc_utility_functions.dart';
import '../../model/external_content_list.dart';
import '../../model/sport_tab_model.dart';
import 'external_content/external_entry.dart';
import 'external_content/external_list_view.dart';

class ExternalTab extends StatelessWidget {
  final SportTabModel tabModel;
  final List<ExternalContentList> segment;

  ExternalTab({
    Key? key,
    required this.tabModel,
    required this.segment,
  }) : super(key: key);

  // const ExternalTab.video()
  //     : headline = '$sportVideos.current_videos',
  //       descriptionText = '$sportVideos.description',
  //       super(key: const ValueKey('sport_tab_videos'));

  // const ExternalTab.audioBook()
  //     : headline = '$sportAudioBook.current_courses',
  //       descriptionText = '$sportAudioBook.description',
  //       super(key: const ValueKey('sport_tab_audio_book'));

  // const ExternalTab.actionBounds()
  //     : headline = '$sportActionBounds.current_action_bounds',
  //       descriptionText = '$sportActionBounds.description',
  //       super(key: const ValueKey('sport_tab_action_bounds'));

  // const ExternalTab.links()
  //     : headline = '$sportLinks.headline',
  //       descriptionText = '$sportLinks.description',
  //       super(key: const ValueKey('sport_tab_links'));

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
        child: Theme(
          data: Theme.of(context).copyWith(
              cardTheme: CardTheme(
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  margin: EdgeInsets.symmetric(vertical: 6, horizontal: 12))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(height: 8),
              Card(
                child: Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(height: 4),
                            Text(
                              read(tabModel.headline, context),
                              style:
                                  CurrentTheme().themeData.textTheme.headline2,
                            ),
                            Container(height: 24),
                            Text(
                              read(tabModel.description, context),
                              style:
                                  CurrentTheme().themeData.textTheme.bodyText1,
                            ),
                            Container(height: 8),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              for (var i = 0; i < segment.length; i++)
                Card(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (hasContent(segment[i].name, context))
                        Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Text(
                            read(segment[i].name, context),
                            style: CurrentTheme()
                                .themeData
                                .textTheme
                                .headline3
                                ?.copyWith(color: CurrentTheme().textSoftWhite),
                          ),
                        ),
                      Container(height: 12),
                      ExternalListView(entries: [
                        // Column(children: [
                        for (var j = 0; j < segment[i].entries.length; j++)
                          ExternalEntry(
                            externalContent: segment[i].entries[j],
                          )
                      ]),
                      Container(height: 12),
                    ],
                  ),
                ),
              Container(height: 72),
            ],
          ),
        ),
      );
}
