// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pp_user_progress_bloc.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension PpUserProgressStateCopyWith on PpUserProgressState {
  PpUserProgressState copyWith({
    Set<int>? entryCheckedSet,
    Map<int, int>? entryQuestionMap,
    int? phaseSelection,
    Set<int>? skippedGroups,
    Map<int, DateTime>? successfulSent,
  }) {
    return PpUserProgressState(
      entryCheckedSet: entryCheckedSet ?? this.entryCheckedSet,
      entryQuestionMap: entryQuestionMap ?? this.entryQuestionMap,
      phaseSelection: phaseSelection ?? this.phaseSelection,
      skippedGroups: skippedGroups ?? this.skippedGroups,
      successfulSent: successfulSent ?? this.successfulSent,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PpUserProgressState _$PpUserProgressStateFromJson(Map json) =>
    PpUserProgressState(
      phaseSelection: json['phaseSelection'] as int? ?? -1,
      skippedGroups: (json['skippedGroups'] as List<dynamic>?)
              ?.map((e) => e as int)
              .toSet() ??
          const {},
      entryCheckedSet: (json['entryCheckedSet'] as List<dynamic>?)
              ?.map((e) => e as int)
              .toSet() ??
          const {},
      entryQuestionMap: (json['entryQuestionMap'] as Map?)?.map(
            (k, e) => MapEntry(int.parse(k as String), e as int),
          ) ??
          const {},
      successfulSent: (json['successfulSent'] as Map?)?.map(
            (k, e) =>
                MapEntry(int.parse(k as String), DateTime.parse(e as String)),
          ) ??
          const {},
    );

Map<String, dynamic> _$PpUserProgressStateToJson(
        PpUserProgressState instance) =>
    <String, dynamic>{
      'phaseSelection': instance.phaseSelection,
      'skippedGroups': instance.skippedGroups.toList(),
      'entryCheckedSet': instance.entryCheckedSet.toList(),
      'entryQuestionMap':
          instance.entryQuestionMap.map((k, e) => MapEntry(k.toString(), e)),
      'successfulSent': instance.successfulSent
          .map((k, e) => MapEntry(k.toString(), e.toIso8601String())),
    };
