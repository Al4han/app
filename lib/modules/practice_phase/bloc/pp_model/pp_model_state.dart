/*
 * Copyright 2020-2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
part of 'pp_model_bloc.dart';

@CopyWith()
@JsonSerializable(anyMap: true, explicitToJson: true)
class PpModelState extends Equatable {
  final List<PpPhase> phases;
  final DateTime? lastUpdated;

  const PpModelState({
    this.phases = const [],
    this.lastUpdated,
  });

  factory PpModelState.initial() => const PpModelState(
        // On purpose null. Means it never got updates from server
        lastUpdated: null,
      );

  @override
  List<Object?> get props => [phases, lastUpdated];

  factory PpModelState.fromJson(Map<String, dynamic> json) =>
      _$PpModelStateFromJson(json);

  Map<String, dynamic> toJson() => _$PpModelStateToJson(this);
}
