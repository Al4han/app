/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:json_annotation/json_annotation.dart';

part 'pp_address.g.dart';

/// A [PpAddress] contains a [street], [city], [postal], [phone],
/// [email], [latitude], [longitude] of a [PpPerson]
///
/// The data of this class is used on [PpAboutPage]
/// for the contact information of a [PpPerson]
@CopyWith()
@JsonSerializable(explicitToJson: true, anyMap: true)
class PpAddress {
  /// the [street] part of the [PpAddress]
  final String street;

  /// the [city] part of the [PpAddress], also contains house number
  final String city;

  /// the [postal] of the [PpAddress]
  final String postal;

  /// the [phone] number of the [PpPerson]
  final String phone;

  /// the [email] adress of the [PpPerson]
  final String email;

  /// the [latitude] of the [PpAddress] for map intents
  final String latitude;

  /// the [longitude] of the [PpAddress] for map intents
  final String longitude;

  /// Constructor of [PpAddress]
  PpAddress({
    this.street = "",
    this.city = "",
    this.postal = "",
    this.phone = "",
    this.email = "",
    this.latitude = "",
    this.longitude = "",
  });

  factory PpAddress.fromJson(Map<String, dynamic> json) =>
      _$PpAddressFromJson(json);

  Map<String, dynamic> toJson() => _$PpAddressToJson(this);
}
