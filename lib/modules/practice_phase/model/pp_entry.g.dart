// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pp_entry.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension PpEntryCopyWith on PpEntry {
  PpEntry copyWith({
    Map<String, String>? body,
    int? entryId,
    int? order,
    Map<String, String>? title,
  }) {
    return PpEntry(
      body: body ?? this.body,
      entryId: entryId ?? this.entryId,
      order: order ?? this.order,
      title: title ?? this.title,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PpEntry _$PpEntryFromJson(Map json) => PpEntry(
      entryId: json['entryId'] as int? ?? -1,
      order: json['order'] as int? ?? -1,
      title: (json['title'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
      body: (json['body'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
    );

Map<String, dynamic> _$PpEntryToJson(PpEntry instance) => <String, dynamic>{
      'entryId': instance.entryId,
      'order': instance.order,
      'title': instance.title,
      'body': instance.body,
    };
