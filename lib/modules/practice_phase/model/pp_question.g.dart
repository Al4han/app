// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pp_question.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension PpQuestionCopyWith on PpQuestion {
  PpQuestion copyWith({
    List<PpAnswer>? answers,
    Map<String, String>? body,
    Map<String, String>? bodyCorrect,
    Map<String, String>? bodyWrong,
    int? correctAnswerId,
    String? image,
    Map<String, String>? imageAlt,
    int? questionId,
  }) {
    return PpQuestion(
      answers: answers ?? this.answers,
      body: body ?? this.body,
      bodyCorrect: bodyCorrect ?? this.bodyCorrect,
      bodyWrong: bodyWrong ?? this.bodyWrong,
      correctAnswerId: correctAnswerId ?? this.correctAnswerId,
      image: image ?? this.image,
      imageAlt: imageAlt ?? this.imageAlt,
      questionId: questionId ?? this.questionId,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PpQuestion _$PpQuestionFromJson(Map json) => PpQuestion(
      questionId: json['questionId'] as int? ?? -1,
      answers: (json['answers'] as List<dynamic>?)
              ?.map(
                  (e) => PpAnswer.fromJson(Map<String, dynamic>.from(e as Map)))
              .toList() ??
          const [],
      correctAnswerId: json['correctAnswerId'] as int? ?? -1,
      body: (json['body'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
      bodyCorrect: (json['bodyCorrect'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
      bodyWrong: (json['bodyWrong'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
      image: json['image'] as String? ?? "",
      imageAlt: (json['imageAlt'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
    );

Map<String, dynamic> _$PpQuestionToJson(PpQuestion instance) =>
    <String, dynamic>{
      'questionId': instance.questionId,
      'answers': instance.answers.map((e) => e.toJson()).toList(),
      'correctAnswerId': instance.correctAnswerId,
      'body': instance.body,
      'bodyCorrect': instance.bodyCorrect,
      'bodyWrong': instance.bodyWrong,
      'image': instance.image,
      'imageAlt': instance.imageAlt,
    };
