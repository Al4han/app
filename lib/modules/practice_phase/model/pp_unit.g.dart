// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pp_unit.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension PpUnitCopyWith on PpUnit {
  PpUnit copyWith({
    List<PpEntry>? entries,
    String? image,
    Map<String, String>? imageAlt,
    Map<String, String>? markdown,
    List<PpMedia>? medias,
    List<PpQuestion>? questions,
    Map<String, String>? subtitle,
    Map<String, String>? title,
    int? unitId,
  }) {
    return PpUnit(
      entries: entries ?? this.entries,
      image: image ?? this.image,
      imageAlt: imageAlt ?? this.imageAlt,
      markdown: markdown ?? this.markdown,
      medias: medias ?? this.medias,
      questions: questions ?? this.questions,
      subtitle: subtitle ?? this.subtitle,
      title: title ?? this.title,
      unitId: unitId ?? this.unitId,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PpUnit _$PpUnitFromJson(Map json) => PpUnit(
      unitId: json['unitId'] as int? ?? -1,
      title: (json['title'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
      subtitle: (json['subtitle'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
      markdown: (json['markdown'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
      image: json['image'] as String? ?? "",
      imageAlt: (json['imageAlt'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
      medias: (json['medias'] as List<dynamic>?)
              ?.map(
                  (e) => PpMedia.fromJson(Map<String, dynamic>.from(e as Map)))
              .toList() ??
          const [],
      entries: (json['entries'] as List<dynamic>?)
              ?.map(
                  (e) => PpEntry.fromJson(Map<String, dynamic>.from(e as Map)))
              .toList() ??
          const [],
      questions: (json['questions'] as List<dynamic>?)
              ?.map((e) =>
                  PpQuestion.fromJson(Map<String, dynamic>.from(e as Map)))
              .toList() ??
          const [],
    );

Map<String, dynamic> _$PpUnitToJson(PpUnit instance) => <String, dynamic>{
      'unitId': instance.unitId,
      'title': instance.title,
      'subtitle': instance.subtitle,
      'markdown': instance.markdown,
      'image': instance.image,
      'imageAlt': instance.imageAlt,
      'medias': instance.medias.map((e) => e.toJson()).toList(),
      'entries': instance.entries.map((e) => e.toJson()).toList(),
      'questions': instance.questions.map((e) => e.toJson()).toList(),
    };
