/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/tc_markdown_stylesheet.dart';
import '../../../../common/tc_theme.dart';
import '../../../../common/tc_utility_functions.dart' as tc_util;
import '../../../../common/widgets/dialog/tc_dialog.dart';
import '../../../../common/widgets/tc_button.dart';
import '../../../qanda/bloc/category/category_bloc.dart';
import '../../../qanda/model/category.dart';
import '../../../qanda/qanda_question_screen.dart';
import '../../bloc/pp_user_progress/pp_user_progress_bloc.dart';
import '../../model/pp_media.dart';
import '../../model/pp_unit.dart';
import '../../utility/pp_bloc_util.dart';
import '../pp_unit/pp_sliver_unit_wrapper.dart';
import '../pp_unit/widgets/pp_elem_cards.dart';

class PpUnitPageArguments {
  PpUnitPageArguments(this.unit);

  final PpUnit unit;
}

class PpUnitPage extends StatefulWidget {
  const PpUnitPage({
    Key? key,
    required this.unit,
  }) : super(key: key);

  final PpUnit unit;

  @override
  _PpUnitPageState createState() => _PpUnitPageState();
}

class _PpUnitPageState extends State<PpUnitPage> {
  final _scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<CategoryBloc>(context).add(LoadCategoriesEvent());
    return ScaffoldMessenger(
      key: _scaffoldMessengerKey,
      child: Scaffold(
          body: PpSliverUnitWrapper(
        unit: widget.unit,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 6),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                child: BlocBuilder<PpUserProgressBloc, PpUserProgressState>(
                  builder: (context, state) {
                    final unitState =
                        PpBlocUtil.getDynamicUnitState(widget.unit);
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        if (widget.unit.entries.isNotEmpty)
                          Expanded(
                            child: PPElemCards(
                              body: FlutterI18n.translate(
                                context,
                                'modules.practice_phase'
                                '.pp_unit.learn_everything',
                              ),
                              level: 1,
                              unit: widget.unit,
                              title: FlutterI18n.translate(
                                context,
                                'modules.practice_phase'
                                '.pp_unit.learn',
                              ),
                              unitState: unitState,
                              navigationCallbackOnPop: () => setState(() {}),
                            ),
                          ),
                        if (widget.unit.questions.isNotEmpty &&
                            widget.unit.entries.isNotEmpty)
                          Container(width: 12),
                        if (widget.unit.questions.isNotEmpty)
                          Expanded(
                            child: PPElemCards(
                              body: FlutterI18n.translate(
                                context,
                                'modules.practice_phase'
                                '.pp_unit.test_knowledge',
                              ),
                              level: 2,
                              title: FlutterI18n.translate(
                                context,
                                'modules.practice_phase.quiz',
                              ),
                              unit: widget.unit,
                              unitState: unitState,
                              navigationCallbackOnPop: () => setState(() {}),
                            ),
                          ),
                      ],
                    );
                  },
                ),
              ),
              Card(
                margin: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                clipBehavior: Clip.antiAlias,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                elevation: 4,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 16, top: 12),
                      child: Text(
                        FlutterI18n.translate(
                          context,
                          'modules.practice_phase'
                          '.pp_unit.information_material',
                        ),
                        style: Theme.of(context)
                            .textTheme
                            .headline2
                            ?.copyWith(fontWeight: FontWeight.w700),
                      ),
                    ),
                    Markdown(
                      shrinkWrap: true,
                      data: tc_util.read(widget.unit.markdown, context),
                      styleSheet: TinyCampusMarkdownStylesheet(context),
                      physics: BouncingScrollPhysics(),
                    ),
                    buildMediaWidgets(widget.unit.medias, context),
                    Container(height: 16),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 4,
                ),
                child: TCButton(
                  onPressedCallback: () {
                    var catId = PpBlocUtil.getUserSelectedPhase().categoryId;
                    var cat = _searchCategory(context, catId);
                    if (cat == const Category.placeholder()) {
                      final snackBar = SnackBar(
                        duration: Duration(seconds: 3),
                        content: Text(
                          FlutterI18n.translate(
                            context,
                            'modules.practice_phase'
                            '.pp_unit.q_and_a_error',
                          ),
                        ),
                      );
                      _scaffoldMessengerKey.currentState
                          ?.showSnackBar(snackBar);
                    } else {
                      BlocProvider.of<CategoryBloc>(context)
                          .add(ChangeCurrentCategoryEvent(current: cat));
                      Navigator.pushNamed(
                        context,
                        qaAllQuestions,
                        arguments: QAndAQuestionScreenArguments(
                          showCategory: true,
                          openAskQuestion: true,
                          initialQuestionText: _buildStartQuestionText(context),
                        ),
                      );
                    }
                  },
                  elevation: 4,
                  buttonLabel: FlutterI18n.translate(
                    context,
                    'modules.q_and_a.ask',
                  ),
                ),
              ),
              Container(height: 90),
            ],
          ),
        ),
      )),
    );
  }

  String _buildStartQuestionText(BuildContext context) => FlutterI18n.translate(
        context,
        'modules.practice_phase'
        '.pp_unit.q_and_a_prefab_text',
        translationParams: {
          "unit": tc_util.read(widget.unit.title, context),
        },
      );

  Category _searchCategory(BuildContext context, int searchID) =>
      BlocProvider.of<CategoryBloc>(context)
          .state
          .all
          .expand((e) => [e, ...e.subcategories])
          .where((e) => e.subcategories.isEmpty)
          .singleWhere(
            (e) => e.id == searchID,
            orElse: () => const Category.placeholder(),
          );

  Widget buildMediaWidgets(List<PpMedia> medias, BuildContext context) =>
      Column(
        children: medias
            .map((e) => ListTile(
                  onTap: () {
                    TCDialog.showCustomDialog(
                      context: context,
                      onConfirm: () => _launchUrl(e.url),
                      functionActionText: FlutterI18n.translate(
                          context, 'common.actions.continue'),
                      headlineText: FlutterI18n.translate(
                          context,
                          'common.external_links.'
                          'dialog_headline'),
                      functionActionColor: CurrentTheme().tcBlue,
                      bodyText: '${e.url}\n\n${FlutterI18n.translate(
                        context,
                        'common.external_links.dialog_body',
                      )}',
                    );
                  },
                  leading: _idToIcon(e.type),
                  title: Text(_idToString(e.type),
                      style: Theme.of(context)
                          .textTheme
                          .headline3
                          ?.copyWith(fontSize: 14)),
                  subtitle: Text(
                    tc_util.read(e.title, context),
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        ?.copyWith(fontSize: 18),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ))
            .toList(),
      );

  Future<void> _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  String _idToString(int mediaId) {
    switch (mediaId) {
      case 0:
        {
          return FlutterI18n.translate(
            context,
            'modules.practice_phase'
            '.pp_unit.medias.link',
          );
        }
      case 1:
        {
          return FlutterI18n.translate(
            context,
            'modules.practice_phase'
            '.pp_unit.medias.document',
          );
        }
      case 2:
        {
          return FlutterI18n.translate(
            context,
            'modules.practice_phase'
            '.pp_unit.medias.video',
          );
        }
      default:
        {
          return FlutterI18n.translate(
            context,
            'modules.practice_phase'
            '.pp_unit.medias.error',
          );
        }
    }
  }

  Icon _idToIcon(int mediaId) {
    switch (mediaId) {
      case 0:
        {
          return Icon(
            Icons.language,
            size: 32,
            color: CurrentTheme().tcBlue,
          );
        }
      case 1:
        {
          return Icon(
            Icons.file_present,
            size: 32,
            color: CurrentTheme().tcBlue,
          );
        }
      case 2:
        {
          return Icon(
            Icons.video_collection,
            size: 32,
            color: CurrentTheme().tcBlue,
          );
        }
      default:
        {
          return Icon(
            Icons.error,
            size: 32,
            color: CurrentTheme().tcBlue,
          );
        }
    }
  }
}
