/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';
import '../../../../common/widgets/tc_button.dart';
import '../../model/pp_phase.dart';
import '../../utility/pp_bloc_util.dart';
import '../pp_send_data/pp_send_verify.dart';

class PpVerifyDataArguments {
  PpVerifyDataArguments({
    required this.matNumber,
    required this.lastName,
    required this.selectedPhase,
  });

  final String matNumber;
  final String lastName;
  final PpPhase selectedPhase;
}

class PpVerifyDataPage extends StatefulWidget {
  final String matNumber;
  final String lastName;
  final PpPhase selectedPhase;

  PpVerifyDataPage({
    Key? key,
    required this.matNumber,
    required this.lastName,
    required this.selectedPhase,
  }) : super(key: key);

  @override
  _PpVerifyDataPageState createState() => _PpVerifyDataPageState();
}

class _PpVerifyDataPageState extends State<PpVerifyDataPage> {
  var _checked = false;
  ValueNotifier<bool> loading = ValueNotifier<bool>(false);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(
            FlutterI18n.translate(
              context,
              'modules.practice_phase.pp_finished',
            ),
          ),
        ),
        body: ValueListenableBuilder(
          valueListenable: loading,
          builder: (context, value, child) => loading.value
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : SafeArea(
                  bottom: true,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        child: SingleChildScrollView(
                          physics: const BouncingScrollPhysics(),
                          child: Column(
                            children: [
                              Center(
                                child: Container(
                                  margin: EdgeInsets.symmetric(
                                      vertical: 32, horizontal: 12.0),
                                  child: Text(
                                    FlutterI18n.translate(
                                      context,
                                      'modules.practice_phase.correct_data',
                                    ),
                                    style:
                                        Theme.of(context).textTheme.headline2,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                              PpSendVerifyWidget(
                                  matNumber: widget.matNumber,
                                  lastName: widget.lastName),
                            ],
                          ),
                        ),
                      ),
                      TCPressableWrapper(
                        widget: CheckboxListTile(
                          value: _checked,
                          onChanged: (value) {
                            setState(() {
                              // This Checkbox is not tristate, so this is fine.
                              _checked = value!;
                            });
                          },
                          title: Text(
                            FlutterI18n.translate(
                              context,
                              'modules.practice_phase.checked_data',
                            ),
                            style: Theme.of(context).textTheme.headline4,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Container(
                          margin: const EdgeInsets.symmetric(horizontal: 12.0),
                          child: TCButton(
                            onPressedCallback: _checked
                                ? () async {
                                    switchLoader();
                                    if (await PpBlocUtil.userSendData(
                                        widget.matNumber,
                                        widget.lastName,
                                        widget.selectedPhase.phaseId)) {
                                      switchLoader();
                                      var i = 0;
                                      Navigator.popUntil(
                                          context, (_) => i++ >= 2);
                                    } else {
                                      switchLoader();
                                      final snackBar = SnackBar(
                                        duration: Duration(seconds: 3),
                                        content: Text(
                                          FlutterI18n.translate(
                                            context,
                                            'modules.practice_phase.send_error',
                                          ),
                                        ),
                                      );
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(snackBar);
                                    }
                                  }
                                : null,
                            elevation: 4,
                            noAnimationDuration: _checked ? false : true,
                            buttonLabel: FlutterI18n.translate(
                              context,
                              'modules.practice_phase.send_one_time',
                            ),
                            backgroundColor: CurrentTheme().tcBlue,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
        ),
      );

  void switchLoader() => loading.value = !loading.value;
}
