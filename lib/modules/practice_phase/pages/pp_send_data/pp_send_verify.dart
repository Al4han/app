/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';

class PpSendVerifyWidget extends StatelessWidget {
  final String matNumber;
  final String lastName;

  PpSendVerifyWidget({
    Key? key,
    required this.matNumber,
    required this.lastName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        padding: const EdgeInsets.symmetric(vertical: 32),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              FlutterI18n.translate(
                context,
                'modules.practice_phase.matriculation_number',
              ),
              style: Theme.of(context)
                  .textTheme
                  .headline4
                  ?.copyWith(color: CurrentTheme().textPassive),
              textAlign: TextAlign.center,
            ),
            Text(
              matNumber,
              style: Theme.of(context)
                  .textTheme
                  .headline1
                  ?.copyWith(fontWeight: FontWeight.w800),
              textAlign: TextAlign.center,
            ),
            Divider(
              color: Theme.of(context).primaryColor,
            ),
            Text(
              FlutterI18n.translate(
                context,
                'modules.practice_phase.last_name',
              ),
              style: Theme.of(context)
                  .textTheme
                  .headline4
                  ?.copyWith(color: CurrentTheme().textPassive),
              textAlign: TextAlign.center,
            ),
            Text(
              lastName,
              style: Theme.of(context)
                  .textTheme
                  .headline1
                  ?.copyWith(fontWeight: FontWeight.w800),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      );
}
