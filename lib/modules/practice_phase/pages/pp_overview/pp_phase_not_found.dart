/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/constants/routing_constants.dart';

class PpPhaseNotFound extends StatelessWidget {
  const PpPhaseNotFound({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text("Error"),
        ),
        body: TextButton(
            onPressed: () {
              Navigator.pushReplacementNamed(context, ppSelectionRoute);
            },
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    FlutterI18n.translate(
                      context,
                      'modules.practice_phase.could_not_find_data',
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Container(height: 12),
                  Text(
                    FlutterI18n.translate(
                      context,
                      'modules.practice_phase.write_message_on_error',
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Container(height: 12),
                  Text(
                    "😢",
                    style: TextStyle(fontSize: 32),
                  ),
                ],
              ),
            )),
      );
}
