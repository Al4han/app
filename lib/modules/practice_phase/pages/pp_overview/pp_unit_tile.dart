/*
 * Copyright 2020-2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../../../common/tc_utility_functions.dart' as tc_util;
import '../../../../common/widgets/tc_button.dart';
import '../../../../common/widgets/tc_cached_image.dart';
import '../../bloc/pp_user_progress/pp_user_progress_bloc.dart';
import '../../model/pp_unit.dart';
import '../../utility/pp_bloc_util.dart';
import '../pp_unit/pp_unit_page.dart';

class PpUnitTile extends StatefulWidget {
  const PpUnitTile({
    Key? key,
    required this.unit,
    required this.showDebug,
    required this.highlighted,
    required this.navigatorCallback,
    required this.removeFinished,
  }) : super(key: key);

  final PpUnit unit;
  final ValueNotifier<bool> showDebug;
  final bool highlighted;
  final VoidCallback navigatorCallback;
  final VoidCallback removeFinished;

  @override
  _PpUnitTileState createState() => _PpUnitTileState();
}

class _PpUnitTileState extends State<PpUnitTile>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  late Animation<double> animation;
  late Animation<double> transition;

  @override
  void initState() {
    super.initState();
    initAnimation();
  }

  void initAnimation() {
    controller = AnimationController(
        vsync: this,
        duration: Duration(
          milliseconds: 1000,
        ));

    controller.addStatusListener(
      (status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      },
    );
    final curve = CurvedAnimation(parent: controller, curve: Curves.easeInOut);
    animation = Tween<double>(begin: 0.8, end: 1.18).animate(curve);
    transition = Tween<double>(begin: 1.0, end: 1.4).animate(curve);
    controller.reset();
    controller.forward();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => ValueListenableBuilder<bool>(
        valueListenable: widget.showDebug,
        builder: (context, value, child) => Column(
          children: [
            child!,
            if (value) debugOptions(),
          ],
        ),
        child: BlocBuilder<PpUserProgressBloc, PpUserProgressState>(
          builder: (context, state) {
            final unitState = PpBlocUtil.getDynamicUnitState(widget.unit).state;
            final isFinished = unitState == UnitEnum.level2Complete ||
                unitState == UnitEnum.level2Failed;
            return TCPressableWrapper(
              widget: ListTile(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
                leading: !widget.highlighted
                    ? Hero(
                        tag: widget.unit.image + widget.unit.unitId.toString(),
                        child: TcCachedImage(
                          greyedOut: isFinished,
                          key: Key(widget.unit.image +
                              widget.unit.unitId.toString()),
                          isRounded: true,
                          borderRadius: 56,
                          url: widget.unit.image,
                          height: 56,
                          width: 56,
                        ),
                      )
                    : RepaintBoundary(
                        child: AnimatedBuilder(
                          animation: controller,
                          builder: (context, child) => Stack(
                            clipBehavior: Clip.none,
                            children: [
                              if (widget.highlighted)
                                Positioned.fill(
                                  child: Transform.scale(
                                    scale: transition.value,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: CurrentTheme().passiveIconColor,
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                  ),
                                ),
                              Transform.scale(
                                scale: widget.highlighted
                                    ? animation.value / 8 + (1)
                                    : 1.0,
                                child: Hero(
                                  tag: widget.unit.image +
                                      widget.unit.unitId.toString(),
                                  child: TcCachedImage(
                                    greyedOut: isFinished,
                                    key: Key(widget.unit.image +
                                        widget.unit.unitId.toString()),
                                    isRounded: true,
                                    borderRadius: 56,
                                    url: widget.unit.image,
                                    height: 56,
                                    width: 56,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                title: Container(
                  margin: EdgeInsets.symmetric(horizontal: 12.0),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          tc_util.read(widget.unit.title, context),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: CurrentTheme()
                              .themeData
                              .textTheme
                              .headline4
                              ?.copyWith(
                                  color: isFinished
                                      ? CurrentTheme().textPassive
                                      : CurrentTheme().textSoftWhite,
                                  fontWeight: isFinished
                                      ? FontWeight.w600
                                      : FontWeight.w700),
                        ),
                        Text(
                          tc_util.read(widget.unit.subtitle, context),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: CurrentTheme()
                              .themeData
                              .textTheme
                              .headline4
                              ?.copyWith(
                                  color: isFinished
                                      ? CurrentTheme().textPassive
                                      : CurrentTheme().textPassive,
                                  fontWeight: FontWeight.w400),
                        ),
                        if (unitState == UnitEnum.level2Failed)
                          Text(
                            FlutterI18n.translate(
                              context,
                              'modules.practice_phase.quiz_point_info',
                            ).toUpperCase(),
                            style: CurrentTheme()
                                .themeData
                                .textTheme
                                .bodyText1
                                ?.copyWith(
                                    color: CorporateColors.tinyCampusOrange,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14.0),
                          )
                      ],
                    ),
                  ),
                ),
                trailing: isFinished
                    ? Container(
                        padding: EdgeInsets.all(12.0),
                        child: Icon(Icons.check),
                      )
                    : null,
                onTap: () {
                  widget.removeFinished();
                  Navigator.pushNamed(
                    context,
                    ppUnitRoute,
                    arguments: PpUnitPageArguments(widget.unit),
                  ).then(
                    (value) => widget.navigatorCallback(),
                  );
                },
              ),
            );
          },
        ),
      );

  Widget debugOptions() => Container(
        margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  width: 72,
                  child: Text(
                      PpBlocUtil.getDynamicUnitState(widget.unit)
                          .state
                          .toString(),
                      style: CurrentTheme()
                          .themeData
                          .textTheme
                          .headline4
                          ?.copyWith(fontSize: 16.0)),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        "Noch nicht abgeschlossene Entries: "
                        "${PpBlocUtil.getDynamicUnitState(
                          widget.unit,
                        ).amountUncheckedEntries}",
                      ),
                      Text(
                        "Anzahl Fragen: ${widget.unit.questions.length}",
                      ),
                      Text("Richtig beantwortete Fragen: "
                          "${PpBlocUtil.getDynamicUnitState(
                        widget.unit,
                      ).amountCorrectAnswers}"),
                      Text("Falsch beantwortete Fragen: "
                          "${PpBlocUtil.getDynamicUnitState(
                        widget.unit,
                      ).amountWrongAnswers}"),
                      Container(height: 16.0),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Flexible(
                  child: TextButton(
                    child: Text("Init"),
                    onPressed: () {
                      PpBlocUtil.addProgressEvent(
                          PpResetUnitProgressionEvent(widget.unit));
                    },
                  ),
                ),
                Flexible(
                  child: TextButton(
                    child: Text("- L1"),
                    onPressed: () {
                      PpBlocUtil.addProgressEvent(
                          PpUncheckAllUnitEntriesEvent(widget.unit));
                    },
                  ),
                ),
                Flexible(
                  child: TextButton(
                    child: Text("+ L1"),
                    onPressed: () {
                      PpBlocUtil.addProgressEvent(
                          PpCheckAllUnitEntriesEvent(widget.unit));
                    },
                  ),
                ),
                Flexible(
                  child: TextButton(
                    child: Text("~ L2"),
                    onPressed: () {
                      PpBlocUtil.addProgressEvent(
                          PpRandomQuestionResultsEvent(widget.unit));
                    },
                  ),
                ),
                Flexible(
                  child: TextButton(
                    child: Text("- L2"),
                    onPressed: () {
                      PpBlocUtil.addProgressEvent(
                          PpRemoveQuestionResultsEvent(widget.unit));
                    },
                  ),
                ),
                Flexible(
                  child: TextButton(
                    child: Text("+ L2"),
                    onPressed: () {
                      PpBlocUtil.addProgressEvent(
                          PpSetAllQuestionsToCorrectEvent(widget.unit));
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      );
}
