/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../../../common/widgets/dialog/tc_dialog.dart';
import '../../bloc/pp_user_progress/pp_user_progress_bloc.dart';
import '../../model/pp_group.dart';
import '../../model/pp_phase.dart';
import '../../utility/pp_bloc_util.dart';
import '../pp_about/pp_about_page.dart';
import 'pp_group_body_debug_options.dart';
import 'pp_unit_tile.dart';

class PpGroupBody extends StatelessWidget {
  final PpGroup group;
  final GroupState state;
  final PpPhase selectedPhase;
  final ValueNotifier<bool> showDebug;
  final ValueNotifier<int> highlightedUnitId;
  final VoidCallback checkIfEverythingIsFinished;
  final VoidCallback removeFinishedForAnimation;

  const PpGroupBody({
    Key? key,
    required this.group,
    required this.state,
    required this.selectedPhase,
    required this.showDebug,
    required this.highlightedUnitId,
    required this.checkIfEverythingIsFinished,
    required this.removeFinishedForAnimation,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => state.isLocked
      ? Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            IconButton(
              icon: Icon(Icons.lock),
              onPressed: () {
                TCDialog.showCustomDialog(
                  context: context,
                  headlineText: FlutterI18n.translate(
                    context,
                    'modules.practice_phase.unlock_group_header',
                  ),
                  bodyText: FlutterI18n.translate(
                    context,
                    'modules.practice_phase.unlock_group_body',
                  ),
                  functionActionText: FlutterI18n.translate(
                    context,
                    'modules.practice_phase.unlock',
                  ),
                  onConfirm: () => PpBlocUtil.addProgressEvent(
                    PpUnlockGroupEvent(group.groupId),
                  ),
                );
              },
            ),
            SizedBox(
              width: 144,
              child: Text(
                FlutterI18n.translate(
                  context,
                  'modules.practice_phase.complete_groups_before',
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        )
      : SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              PpGroupBodyDebugOptions(
                showDebug: showDebug,
                selectedPhase: selectedPhase,
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 64),
                width: 240,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    if (state.amountCompletedUnits > 0)
                      Text(
                        FlutterI18n.translate(
                          context,
                          'modules.practice_phase.finished_percentage',
                          translationParams: {
                            "percent": (state.amountCompletedUnits *
                                    100 /
                                    group.units.length)
                                .floor()
                                .toString(),
                          },
                        ),
                        textAlign: TextAlign.center,
                        style: CurrentTheme().themeData.textTheme.headline2,
                      ),
                    if (state.amountCompletedUnits == 0)
                      Text(
                        FlutterI18n.translate(
                          context,
                          'modules.practice_phase.complete_all_groups',
                        ),
                        textAlign: TextAlign.center,
                        style: CurrentTheme().themeData.textTheme.bodyText1,
                      ),
                    Container(height: 12),
                    ElevatedButton(
                      onPressed: () => Navigator.pushNamed(
                        context,
                        ppAboutRoute,
                        arguments: PpAboutPageArguments(selectedPhase),
                      ).then(
                        (value) {
                          checkIfEverythingIsFinished();
                        },
                      ),
                      child: Text(
                        FlutterI18n.translate(
                          context,
                          'modules.practice_phase.contact_and_questions',
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
              for (var unit in group.units)
                ValueListenableBuilder(
                  valueListenable: highlightedUnitId,
                  builder: (context, value, child) => PpUnitTile(
                    unit: unit,
                    showDebug: showDebug,
                    // finished: finished,
                    highlighted:
                        highlightedUnitId.value == unit.unitId ? true : false,
                    navigatorCallback: checkIfEverythingIsFinished,
                    removeFinished: removeFinishedForAnimation,
                  ),
                ),
              Container(height: 120)
            ],
          ),
        );
}
