/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../common/map_functions.dart';
import '../../../../common/tc_markdown_stylesheet.dart';
import '../../../../common/tc_theme.dart';
import '../../../../common/tc_utility_functions.dart' as tc_util;
import '../../../../common/widgets/tc_button.dart';
import '../../../../common/widgets/tc_cached_image.dart';
import '../../model/pp_address.dart';
import '../../model/pp_person.dart';
import '../../model/pp_phase.dart';
import 'pp_about_sliver_wrapper.dart';

class PpAboutPageArguments {
  PpAboutPageArguments(this.phase);
  final PpPhase phase;
}

class PpAboutPage extends StatelessWidget {
  const PpAboutPage({
    Key? key,
    required this.phase,
  }) : super(key: key);

  final PpPhase phase;

  @override
  Widget build(BuildContext context) => Scaffold(
        body: PpAboutSliverWrapper(
          child: Column(
            children: [
              Container(height: 6),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                child: Material(
                  shadowColor: CurrentTheme().textPassive.withOpacity(0.4),
                  borderRadius: BorderRadius.circular(8.0),
                  elevation: 4,
                  color: CurrentTheme().themeData.primaryColor,
                  child: Markdown(
                    shrinkWrap: true,
                    data: tc_util.read(phase.description, context),
                    styleSheet: TinyCampusMarkdownStylesheet(context),
                    physics: BouncingScrollPhysics(),
                  ),
                ),
              ),
              TCPressableWrapper(
                widget: ListTile(
                  contentPadding: EdgeInsets.zero,
                  onTap: () {
                    launchCaller(phase.faqUrl);
                  },
                  title: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(24.0),
                        child: Icon(
                          Icons.web,
                          size: 32,
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 16.0, horizontal: 12.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                FlutterI18n.translate(
                                  context,
                                  'modules.practice_phase.'
                                  'frequently_questions',
                                ),
                                style: Theme.of(context)
                                    .textTheme
                                    .headline3
                                    ?.copyWith(fontWeight: FontWeight.w700),
                              ),
                              Text(
                                  FlutterI18n.translate(
                                    context,
                                    'modules.practice_phase.'
                                    'access_web',
                                  ),
                                  style: Theme.of(context).textTheme.bodyText1),
                            ],
                            //  ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              for (var person in phase.persons)
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 12.0, vertical: 6.0),
                  child: Material(
                    borderRadius: BorderRadius.circular(8.0),
                    color: CurrentTheme().themeData.primaryColor,
                    clipBehavior: Clip.antiAlias,
                    elevation: 4,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: AspectRatio(
                                aspectRatio: 1,
                                child: TcCachedImage(url: person.image),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 24.0, horizontal: 16.0),
                          child: Text(
                            tc_util.read(person.fullName, context),
                            style: Theme.of(context)
                                .textTheme
                                .headline2
                                ?.copyWith(fontWeight: FontWeight.w700),
                            textAlign: TextAlign.start,
                          ),
                        ),
                        Markdown(
                          shrinkWrap: true,
                          data: tc_util.read(person.description, context),
                          styleSheet: TinyCampusMarkdownStylesheet(context),
                          physics: BouncingScrollPhysics(),
                        ),
                        if (person.addresses.isNotEmpty)
                          ListView.separated(
                            itemCount: person.addresses.length,
                            shrinkWrap: true,
                            padding: EdgeInsets.zero,
                            itemBuilder: (context, index) {
                              final address = person.addresses[index];
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  if (address.email.isNotEmpty)
                                    ListTile(
                                      onTap: () => launchCaller(address.email),
                                      title: Text(
                                        address.email.replaceAll("mailto:", ""),
                                      ),
                                      leading: Icon(Icons.mail),
                                    ),
                                  if (address.phone.isNotEmpty)
                                    ListTile(
                                      onTap: () => launchCaller(address.phone),
                                      title: Text(
                                        address.phone.replaceAll("tel:", ""),
                                      ),
                                      leading: Icon(Icons.phone),
                                    ),
                                  if (hasAnyContent(address))
                                    ListTile(
                                      onTap: address.latitude.isEmpty ||
                                              address.longitude.isEmpty
                                          // This has to be null to deactivate
                                          // the button
                                          ? null
                                          : () => openAddressMap(
                                                context,
                                                person,
                                                address,
                                              ),
                                      title: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(address.street),
                                          Row(
                                            children: [
                                              Text(address.postal),
                                              Container(width: 4),
                                              Text(address.city),
                                            ],
                                          ),
                                        ],
                                      ),
                                      leading: Icon(Icons.location_city),
                                    ),
                                ],
                              );
                            },
                            separatorBuilder: (context, index) => Divider(
                              color: Theme.of(context).backgroundColor,
                              height: 16,
                              thickness: 1,
                              indent: 16,
                              endIndent: 16,
                            ),
                            physics: NeverScrollableScrollPhysics(),
                          ),
                        Container(height: 12.0),
                      ],
                    ),
                  ),
                ),
            ],
          ),
        ),
      );

  void openAddressMap(
    BuildContext context,
    PpPerson person,
    PpAddress address,
  ) =>
      MapFunctions.openMapsSheet(
          context,
          double.parse(address.latitude),
          double.parse(address.longitude),
          [
            tc_util.read(person.fullName, context),
            address.postal,
            address.city,
            address.street,
          ].join(", "));

  Future<void> launchCaller(
    String url,
  ) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  bool hasAnyContent(PpAddress address) =>
      address.city.isNotEmpty &&
      address.postal.isNotEmpty &&
      address.street.isNotEmpty;
}
