/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/assets_adapter.dart';
import '../../../../common/tc_theme.dart';
import '../../../../common/widgets/tc_cached_image.dart';
import '../pp_sliver_wrapper.dart';

class PpAboutSliverWrapper extends StatelessWidget {
  const PpAboutSliverWrapper({
    Key? key,
    required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) => PpSliverWrapper(
        child: child,
        appBarBackground: TcCachedImage(
          localPath: AssetAdapter.ppAboutPageTable(),
          stackForeground: Stack(
            children: [
              Positioned.fill(
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: [
                        Colors.black87,
                        Colors.black45,
                        Colors.black12,
                        Colors.transparent
                      ],
                    ),
                  ),
                ),
              ),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    margin: EdgeInsets.only(bottom: 16),
                    child: Text(
                      FlutterI18n.translate(
                        context,
                        'modules.practice_phase.'
                        'contact_and_questions',
                      ),
                      textAlign: TextAlign.center,
                      style: CurrentTheme()
                          .themeData
                          .textTheme
                          .headline2
                          ?.copyWith(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
}
