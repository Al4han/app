/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../../../common/tc_markdown_stylesheet.dart';
import '../../../../common/tc_theme.dart';
import '../../../../common/tc_utility_functions.dart' as tc_util;
import '../../../../common/widgets/tc_button.dart';
import '../../bloc/pp_user_progress/pp_user_progress_bloc.dart';
import '../../model/pp_unit.dart';
import '../../utility/pp_bloc_util.dart';

class PpEntryPageArguments {
  PpEntryPageArguments(this.unit);
  final PpUnit unit;
}

class PpEntryPage extends StatefulWidget {
  const PpEntryPage({
    Key? key,
    required this.unit,
  }) : super(key: key);

  final PpUnit unit;

  @override
  _PpEntryPageState createState() => _PpEntryPageState();
}

class _PpEntryPageState extends State<PpEntryPage>
    with SingleTickerProviderStateMixin {
  final ValueNotifier<bool> isLocked = ValueNotifier<bool>(false);
  late PageController controller;
  late AnimationController animController;
  late EntryButtonAnimation animation;

  @override
  void initState() {
    super.initState();
    controller = PageController(viewportFraction: 1.0);
    animController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 360));

    animation = EntryButtonAnimation(controller: animController);
  }

  @override
  void dispose() {
    animController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(
            FlutterI18n.translate(
              context,
              'modules.practice_phase.learning_unit',
            ),
          ),
        ),
        body: Container(
          color: CurrentTheme().themeData.backgroundColor,
          child: Column(
            children: <Widget>[
              Expanded(
                child: Stack(
                  fit: StackFit.expand,
                  children: [
                    Positioned.fill(
                      child: PageView(
                        controller: controller,
                        reverse: false,
                        physics: NeverScrollableScrollPhysics(),
                        children: List.generate(
                          widget.unit.entries.length,
                          (index) => Scrollbar(
                            child: SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              child: Column(
                                children: [
                                  Card(
                                    elevation: 2,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 12, vertical: 16),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8),
                                        // TODO: Evaluate if design fits
                                        // image: DecorationImage(
                                        //   image: NetworkImage(
                                        //       widget.unit.image),
                                        //   fit: BoxFit.cover,
                                        //   colorFilter:
                                        // ColorFilter.mode(
                                        //       Colors.black
                                        //           .withOpacity(0.06),
                                        //       BlendMode.dstATop),
                                        // ),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(24.0),
                                        child: Column(
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  bottom: 24.0),
                                              child: Text(
                                                tc_util.read(
                                                    widget.unit.entries[index]
                                                        .title,
                                                    context),
                                                style: CurrentTheme()
                                                    .themeData
                                                    .textTheme
                                                    .headline1
                                                    ?.copyWith(
                                                      fontWeight:
                                                          FontWeight.w700,
                                                    ),
                                              ),
                                            ),
                                            Markdown(
                                              padding: EdgeInsets.zero,
                                              shrinkWrap: true,
                                              data: tc_util.read(
                                                  widget
                                                      .unit.entries[index].body,
                                                  context),
                                              styleSheet:
                                                  TinyCampusMarkdownStylesheet(
                                                context,
                                              ),
                                              physics: BouncingScrollPhysics(),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(height: 42),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      height: 32,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              end: Alignment.bottomCenter,
                              begin: Alignment.topCenter,
                              colors: [
                                CurrentTheme()
                                    .themeData
                                    .backgroundColor
                                    .withOpacity(0.0),
                                CurrentTheme().themeData.backgroundColor,
                                CurrentTheme().themeData.backgroundColor,
                              ]),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 42,
                child: SmoothPageIndicator(
                  controller: controller,
                  count: widget.unit.entries.length,
                  effect: ExpandingDotsEffect(
                    expansionFactor: 6,
                    activeDotColor:
                        CurrentTheme().themeData.colorScheme.secondary,
                    strokeWidth: 2,
                    dotHeight: 12,
                    dotWidth: 12,
                  ),
                ),
              ),
              AnimatedBuilder(
                animation: animController,
                builder: (context, child) => SafeArea(
                  bottom: true,
                  child: Container(
                    margin: EdgeInsets.all(12),
                    child: Transform.scale(
                      scale: animation.indicatorSize.value,
                      child: AnimatedBuilder(
                        animation: controller,
                        builder: (context, child) => AbsorbPointer(
                          absorbing: (animController.value % 1) != 0,
                          child: TCButton(
                            backgroundColor: animation.buttonColor.value,
                            buttonLabel: isLastPage()
                                ? FlutterI18n.translate(
                                    context,
                                    'modules.practice_phase.lock',
                                  )
                                : FlutterI18n.translate(
                                    context,
                                    'modules.practice_phase.check_and_continue',
                                  ),
                            onPressedCallback: onPressedLogic,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );

  void onPressedLogic() {
    if (!animController.isAnimating) {
      animController.forward().then((value) => animController.reset());

      if (isLastPage()) {
        PpBlocUtil.addProgressEvent(PpCheckAllUnitEntriesEvent(widget.unit));
        Navigator.pop(context);
        return;
      } else {
        // TODO: Check if neccessary in future
        // PpBlocUtil.addProgressEvent(UpbEntryCheckedEvent(
        //     widget.unit.entries[controller.page.toInt()].entryId));
        controller.nextPage(
            duration: Duration(milliseconds: 260), curve: Curves.easeOutQuad);
      }
    }
  }

  bool isLastPage() {
    if (controller.hasClients) {
      return (controller.page ?? 0) > widget.unit.entries.length - 1.5;
    } else {
      return false;
    }
  }
}

class EntryButtonAnimation {
  EntryButtonAnimation({
    required this.controller,
  })  : buttonColor = TweenSequence<Color?>(
          [
            TweenSequenceItem(
              tween: ColorTween(
                begin: CurrentTheme().themeData.colorScheme.secondary,
                end: CurrentTheme().themeData.iconTheme.color,
              ),
              weight: 30.0,
            ),
            TweenSequenceItem(
              tween: ConstantTween<Color?>(
                CurrentTheme().themeData.iconTheme.color,
              ),
              weight: 60.0,
            ),
            TweenSequenceItem(
              tween: ColorTween(
                begin: CurrentTheme().themeData.iconTheme.color,
                end: CurrentTheme().themeData.colorScheme.secondary,
              ),
              weight: 30.0,
            ),
          ],
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.easeOut),
          ),
        ),
        indicatorOpacity = TweenSequence(
          <TweenSequenceItem<double>>[
            TweenSequenceItem<double>(
              tween: Tween<double>(begin: 1.0, end: 0.0)
                  .chain(CurveTween(curve: Curves.easeOut)),
              weight: 40.0,
            ),
            TweenSequenceItem<double>(
              tween: ConstantTween<double>(0.0),
              weight: 20.0,
            ),
            TweenSequenceItem<double>(
              tween: Tween<double>(begin: 0.0, end: 1.0)
                  .chain(CurveTween(curve: Curves.easeOut)),
              weight: 40.0,
            ),
          ],
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.easeOut),
          ),
        ),
        indicatorSize = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 1.0, end: 0.86)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 20.0,
          ),
          TweenSequenceItem<double>(
            tween: ConstantTween<double>(0.86),
            weight: 60.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.86, end: 1.0)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 20.0,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.easeOut),
          ),
        ),
        totalOpacity = TweenSequence(
          <TweenSequenceItem<double>>[
            TweenSequenceItem<double>(
              tween: ConstantTween<double>(1.0),
              weight: 70.0,
            ),
            TweenSequenceItem<double>(
              tween: Tween(begin: 1.0, end: 0.2),
              weight: 30.0,
            ),
          ],
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.easeOut),
          ),
        ),
        dotOpacity = TweenSequence(
          <TweenSequenceItem<double>>[
            TweenSequenceItem<double>(
              tween: ConstantTween<double>(1.0),
              weight: 70.0,
            ),
            TweenSequenceItem<double>(
              tween: Tween(begin: 1.0, end: 0.2),
              weight: 10.0,
            ),
            TweenSequenceItem<double>(
              tween: ConstantTween<double>(0.0),
              weight: 20.0,
            ),
          ],
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.easeOut),
          ),
        );

  final AnimationController controller;
  final Animation<double> indicatorOpacity;
  final Animation<double> dotOpacity;
  final Animation<double> indicatorSize;
  final Animation<double> totalOpacity;
  final Animation<Color?> buttonColor;
}
