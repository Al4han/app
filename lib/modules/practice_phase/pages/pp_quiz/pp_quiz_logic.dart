/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vibration/vibration.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/styled_print.dart';
import '../../bloc/pp_user_progress/pp_user_progress_bloc.dart';
import '../../model/pp_answer.dart';
import '../../model/pp_question.dart';
import '../../model/pp_unit.dart';
import '../../utility/pp_bloc_util.dart';
import '../pp_result/pp_result_page.dart';
import 'pp_quiz_animations.dart';
import 'pp_quiz_page.dart';

extension QuizLogic on PpQuizPageState {
  void initQuiz() {
    initializeSettings();
    deepCopyAndRandomize();
    initAnimation();
    final faultyQuestionIds = checkQuestionsForErrors(
      widget.originalUnit.questions,
    );

    if (faultyQuestionIds.isNotEmpty) {
      eprint(errors.toString(), caller: PpQuizPage);
    }

    /// checks if any question has faulty attributes
    /// if yes, the questions get removed and flagged as [1] in the
    /// [resultMap], since the user is not able to answer the question
    /// in any way
    for (var entry in errors.entries) {
      copiedUnit.questions.removeWhere((e) => entry.key == e.questionId);
      resultMap[entry.key] = 1;
    }

    if (faultyQuestionIds.length == widget.originalUnit.questions.length) {
      for (var question in widget.originalUnit.questions) {
        resultMap[question.questionId] = 1;
      }
      navigateToResultScreen();
    }
    if (copiedUnit.questions.isEmpty) {
      navigateToResultScreen();
    }
    initializeFirstQuestion();
  }

  Set<int> checkQuestionsForErrors(List<PpQuestion> questions) {
    final faultyQuestions = <int>{};

    for (final question in questions) {
      final questionId = question.questionId;
      final errorSet = <FaultyQuestion>{};
      final answerIds = question.answers.map((e) => e.answerId).toList();

      if (!answerIds.contains(question.correctAnswerId)) {
        errorSet.add(FaultyQuestion.noCorrectAnswerFound);

        eprint(
          "Question $questionId has no corresponding correct answer.\n"
          "${question.correctAnswerId} not found in $answerIds",
          caller: PpQuizPage,
        );
      }

      if (answerIds.isEmpty) {
        errorSet.add(FaultyQuestion.noAnswersFound);

        eprint("Question $questionId has no answers!", caller: PpQuizPage);
      } else if (!const [2, 4].contains(answerIds.length)) {
        errorSet.add(FaultyQuestion.unsupportedAnswerCount);

        eprint(
          "Question $questionId has ${answerIds.length} answers, "
          "which is not well-suited for the UI",
          caller: PpQuizPage,
        );
      }

      if (errorSet.isNotEmpty) {
        faultyQuestions.add(questionId);

        errors.update(
          questionId,
          (value) => value..addAll(errorSet),
          ifAbsent: () => errorSet,
        );
      }
    }

    return faultyQuestions;
  }

  void deepCopyAndRandomize() {
    // NOTICE: We do not want to mess with the BLoC-References
    // That's why we create a copy, so we can freely manipulate the content
    copiedUnit = widget.originalUnit.copyWith();
    copiedUnit.questions
      ..shuffle()
      ..forEach((e) => e.answers.shuffle());
  }

  void updateAnimationSettings() {
    animController.duration = mainAnimationDuration;
    translateController.duration = translateDuration;
    quizFlipController.duration = quizFlipDuration;
  }

  void startAnimation() {
    if (mounted) {
      translateController.reset();
      animController.reset();
      quizFlipController.reset();
      animController.reset();

      translateController.animateTo(0.5).whenComplete(
        () {
          if (mounted) {
            Future.delayed(
              waitForFlipDuration,
              () => quizFlipController.animateTo(1.0).whenComplete(
                () {
                  if (mounted) {
                    animController.value = 0.35;
                    animController.animateTo(0.5);
                  }
                },
              ),
            );
          }
        },
      );
    }
  }

  Future<void> setSettings() async {
    await sp.setBool(
        ppSettingsFastAnimationsString, ppSettingsFastAnimations.value);
    await sp.setBool(
        ppSettingsEnableVibrationsString, ppSettingsEnableVibrations.value);
  }

  void initializeFirstQuestion() {
    if (copiedUnit.questions.isNotEmpty) {
      noQuestionsAvailable.value = false;
    }
    selectedQuestion = 0;
    initQuestionData();
  }

  void reset() {
    // TODO: DON'T DO THIS!
    // ignore: invalid_use_of_protected_member
    setState(() {
      selectedQuestion = 0;
    });

    resultMap.clear();
    initializeFirstQuestion();
    animController.reset();
    animController.animateTo(0.5);
  }

  void initAnimation() {
    animController =
        AnimationController(vsync: this, duration: mainAnimationDuration);
    animation = QuizAnimation(controller: animController);

    translateController =
        AnimationController(vsync: this, duration: translateDuration);
    animController.removeListener(mainAnimationListener);
    animController.addListener(mainAnimationListener);
    translateController.removeListener(translateAnimationListener);
    translateController.addListener(translateAnimationListener);
    translateAnimation = TweenSequence<double>(<TweenSequenceItem<double>>[
      TweenSequenceItem<double>(
        tween: Tween<double>(begin: 1.0, end: 0.0)
            .chain(CurveTween(curve: Curves.easeOutCubic)),
        weight: 50.0,
      ),
      TweenSequenceItem<double>(
        tween: Tween<double>(begin: 0.0, end: -1.0)
            .chain(CurveTween(curve: Curves.easeInCubic)),
        weight: 50.0,
      ),
    ]).animate(
      CurvedAnimation(
        parent: translateController,
        curve: Interval(0.0, 1.0, curve: Curves.linear),
      ),
    );

    quizFlipController =
        AnimationController(vsync: this, duration: quizFlipDuration);
    quizFlipAnimation = QuizRotationAnimation(controller: quizFlipController);

    Future.delayed(waitForFlipDuration, startAnimation);
  }

  void translateAnimationListener() {
    if (translateController.status == AnimationStatus.completed &&
        translateController.value == 1) {
      nextQuestion();
    }
  }

  void mainAnimationListener() {
    if (animController.status == AnimationStatus.completed &&
        animController.value == 1) {
      translateController.forward();
    }
  }

  Future<void> initializeSettings() async {
    sp = await SharedPreferences.getInstance();
    try {
      ppSettingsFastAnimations.value =
          sp.getBool(ppSettingsFastAnimationsString) ?? false;
    } on Exception catch (e) {
      eprint("Could not load ppSettingsFastAnimationss: $e",
          caller: PpQuizPage);
    }
    try {
      ppSettingsEnableVibrations.value =
          sp.getBool(ppSettingsEnableVibrationsString) ?? false;
    } on Exception catch (e) {
      eprint("Could not load ppSettingsEnableVibrationsString: $e",
          caller: PpQuizPage);
    }
    updateAnimationSettings();
  }

  Future<bool> mayVibrate() async =>
      ppSettingsEnableVibrations.value &&
      (await Vibration.hasCustomVibrationsSupport() ?? false);

  Future<void> vibrateWin() async {
    if (await mayVibrate()) {
      Vibration.vibrate(duration: 10);
    }
  }

  Future<void> vibrateWrong() async {
    if (await mayVibrate()) {
      Vibration.vibrate(duration: 16, amplitude: 255);
      await Future.delayed(Duration(milliseconds: 160));
      Vibration.vibrate(duration: 16, amplitude: 255);
    }
  }

  void navigateToResultScreen() {
    PpBlocUtil.addProgressEvent(PpUpdateQuestionResultsEvent(resultMap));
    Future.delayed(Duration(milliseconds: 1), () {
      Navigator.pushReplacementNamed(context, ppResultRoute,
          arguments: PpResultPageArguments(widget.originalUnit, resultMap));
    });
  }

  void nextQuestion() {
    if ((selectedQuestion + 1) == copiedUnit.questions.length) {
      navigateToResultScreen();
      return;
    } else {
      // TODO: DON'T DO THIS!
      // ignore: invalid_use_of_protected_member
      setState(() {
        startAnimation();
        selectedQuestion++;
        initQuestionData();
      });
    }
  }

  void initQuestionData() {
    questionHasImage.value =
        copiedUnit.questions[selectedQuestion].image.isNotEmpty;
    answerChosen.value = false;
    answerIdChosen.value = -1;
  }

  Set<FaultyQuestion> isFaultyQuestion() =>
      errors[copiedUnit.questions[selectedQuestion].questionId] ?? {};

  void previousQuestion() {
    // TODO: DON'T DO THIS!
    // ignore: invalid_use_of_protected_member
    setState(() {
      selectedQuestion = --selectedQuestion % copiedUnit.questions.length;
      startAnimation();
    });
  }

  bool isCorrectAnswer(PpQuestion question, PpAnswer answer) =>
      question.correctAnswerId == answer.answerId;

  VoidCallback chooseAnswer(PpAnswer? answer) {
    final question = copiedUnit.questions[selectedQuestion];
    final correctOrNull =
        answer == null || question.correctAnswerId == answer.answerId;
    final chosenAnswerId = answer?.answerId ?? -1;
    final duration = answer == null ? Duration.zero : null;
    final maybeVibrate = answer == null
        ? () {}
        : correctOrNull
            ? vibrateWin
            : vibrateWrong;

    return () {
      correctAnswerChosen.value = correctOrNull;
      answerChosen.value = true;
      answerIdChosen.value = chosenAnswerId;
      resultMap[question.questionId] = correctOrNull ? 1 : 0;
      animController.animateTo(1.0, duration: duration);
      maybeVibrate();
    };
  }
}
