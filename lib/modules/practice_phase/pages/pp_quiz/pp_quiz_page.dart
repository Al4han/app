/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../model/pp_question.dart';
import '../../model/pp_unit.dart';
import 'pp_quiz_animations.dart';
import 'pp_quiz_logic.dart';
import 'pp_quiz_widgets.dart';

class PpQuizPageArguments {
  PpQuizPageArguments(this.originalUnit);
  final PpUnit originalUnit;
}

enum QuizPopUpSelection {
  animationSpeed,
  vibration,
  reset,
}

enum FaultyQuestion {
  unsupportedAnswerCount,
  noCorrectAnswerFound,
  noAnswersFound,
}

class PpQuizPage extends StatefulWidget {
  const PpQuizPage({
    Key? key,
    required this.originalUnit,
  }) : super(key: key);

  final PpUnit originalUnit;

  @override
  PpQuizPageState createState() => PpQuizPageState();
}

// THIS IS INCREDIBLY DANGEROUS!
// SETSTATE SHOULD ONLY BE CALLED BE THE STATE ITSELF!
// SEE PP_QUIZ_LOGIC AND PP_QUIZ_WIDGETS!
class PpQuizPageState extends State<PpQuizPage> with TickerProviderStateMixin {
  final noQuestionsAvailable = ValueNotifier<bool>(true);
  final answerChosen = ValueNotifier<bool>(false);
  final answerIdChosen = ValueNotifier<int>(-1);
  final correctAnswerChosen = ValueNotifier<bool>(false);
  final questionHasImage = ValueNotifier<bool>(false);
  final ppSettingsFastAnimations = ValueNotifier<bool>(false);
  final ppSettingsEnableVibrations = ValueNotifier<bool>(true);

  final resultMap = <int, int>{};
  final errors = <int, Set<FaultyQuestion>>{};
  final copiedRandomizedQuestions = <PpQuestion>[];

  final durationWaitForFlipShort = Duration(milliseconds: 40);
  final durationWaitForFlip = Duration(milliseconds: 600);
  final durationQuizFlipShort = Duration(milliseconds: 300);
  final durationQuizFlip = Duration(milliseconds: 1400);
  final durationTranslateShort = Duration(milliseconds: 160);
  final durationTranslate = Duration(milliseconds: 600);
  final durationMainAnimationShort = Duration(milliseconds: 1900);
  final durationMainAnimation = Duration(milliseconds: 5200);
  final durationAnimatedSizeShort = Duration(milliseconds: 60);
  final durationAnimatedSize = Duration(milliseconds: 180);
  final ppSettingsFastAnimationsString = "ppSettingsFastAnimations";
  final ppSettingsEnableVibrationsString = "ppSettingsEnableVibrations";

  final perspective = Matrix4.fromList(
    const [
      [1.0, 0.0, 0.0, 0.0],
      [0.0, 1.0, 0.0, 0.0],
      [0.0, 0.0, 1.0, 0.001],
      [0.0, 0.0, 0.0, 1.0],
    ].expand((e) => e).toList(),
  );

  late final SharedPreferences sp;
  late final AnimationController animController;
  late final AnimationController translateController;
  late final Animation<double> translateAnimation;
  late final AnimationController quizFlipController;
  late final QuizRotationAnimation quizFlipAnimation;
  late final QuizAnimation animation;
  late final PpUnit copiedUnit;

  int selectedQuestion = 0;

  bool get useFastAnimations => ppSettingsFastAnimations.value;

  Duration get waitForFlipDuration =>
      useFastAnimations ? durationWaitForFlipShort : durationWaitForFlip;

  Duration get quizFlipDuration =>
      useFastAnimations ? durationQuizFlipShort : durationQuizFlip;

  Duration get translateDuration =>
      useFastAnimations ? durationTranslateShort : durationTranslate;

  Duration get mainAnimationDuration =>
      useFastAnimations ? durationMainAnimationShort : durationMainAnimation;

  Duration get animatedSizeDuration =>
      useFastAnimations ? durationAnimatedSizeShort : durationAnimatedSize;

  @override
  void initState() {
    super.initState();
    initQuiz();
  }

  @override
  void dispose() {
    animController.dispose();
    quizFlipController.dispose();
    translateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          actions: [buildQuizPopup(context)],
          title: Text(
            FlutterI18n.translate(
              context,
              'modules.practice_phase.quiz',
            ),
          ),
        ),
        body: SafeArea(
          bottom: true,
          child: ValueListenableBuilder<bool>(
            valueListenable: noQuestionsAvailable,
            builder: (context, value, child) => noQuestionsAvailable.value
                ? buildNoQuestionsFound(context)
                : isFaultyQuestion().isNotEmpty
                    ? buildUnableToDisplayQuestionWidget(context)
                    : buildQuizContent(),
          ),
        ),
      );
}
