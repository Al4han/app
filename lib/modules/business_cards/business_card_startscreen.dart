/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../common/constants/routing_constants.dart';
import '../../common/tc_theme.dart';
import '../../common/tinycampus_icons.dart';
import 'contacts/vcard/vcard.dart';
import 'qr_code_screen.dart';
import 'v_card/v_card_bloc.dart';
import 'widgets/vcard_contact_preview.dart';
import 'widgets/vcard_preview.dart';

class BusinessCardStartScreen extends StatefulWidget {
  BusinessCardStartScreen({Key? key}) : super(key: key);

  @override
  _BusinessCardStartScreenState createState() =>
      _BusinessCardStartScreenState();
}

class _BusinessCardStartScreenState extends State<BusinessCardStartScreen> {
  final _slidableController = SlidableController();

  @override
  Widget build(BuildContext context) {
    _askForPermission();
    return BlocBuilder<VCardBloc, VCardState>(
      builder: (context, state) => Scaffold(
        backgroundColor: Color.fromARGB(255, 243, 243, 243),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.green,
          onPressed: () {
            _closeSlidables();
            Navigator.pushNamed(
              context,
              vCardQRCodeRoute,
              arguments: QRScreenArguments(isQrGenerationScreen: false),
            );
          },
          foregroundColor: Colors.white,
          child: Icon(Icons.add),
        ),
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: I18nText('modules.business_cards.start_screen.title'),
          actions: <Widget>[
            Builder(
              builder: (context) => PopupMenuButton<String>(
                onSelected: (value) {
                  if (value == "import") {
                    _importContacts(state.contacts).then(
                      (_) {
                        var contactCount = state.contacts.length;
                        return ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: I18nPlural(
                                'modules.business_cards.'
                                'start_screen.imported_contacts.times',
                                contactCount),
                          ),
                        );
                      },
                    );
                  }
                },
                itemBuilder: (context) => <PopupMenuEntry<String>>[
                  PopupMenuItem<String>(
                    value: 'import',
                    enabled: state.contacts.isNotEmpty,
                    child: I18nPlural(
                        'modules.business_cards.'
                        'start_screen.import_contacts.times',
                        state.contacts.length),
                  ),
                ],
              ),
            ),
          ],
        ),
        body: ListView(
          shrinkWrap: true,
          // padding: EdgeInsets.all(8.0),
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(vertical: 8.0),
              child: VCardPreview(
                personal: state.personal,
                slidableController: _slidableController,
              ),
            ),
            Center(
              child: Padding(
                padding: EdgeInsets.only(bottom: 14.0),
                child: I18nText(
                  'modules.business_cards.start_screen.tip',
                  child: Text(
                    '',
                    style: TextStyle(color: CorporateColors.tinyCampusIconGrey),
                  ),
                ),
              ),
            ),
            Container(
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 12.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 12.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              I18nText(
                                'modules.business_cards.'
                                'start_screen.your_contacts',
                                child: Text(
                                  '',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 20,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 4.0),
                                child: I18nPlural(
                                  'modules.business_cards.'
                                  'start_screen.contacts.times',
                                  state.contacts.length,
                                  child: Text(
                                    '',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            _closeSlidables();
                            var t = state.sort;
                            BlocProvider.of<VCardBloc>(context)
                                .add(ContactListSortEvent(
                              // TODO: Translate?? Check back with Maurice
                              contacts: t == 'newest'
                                  ? _sortAlphabetical(state.contacts)
                                  : _sortDate(_migrateOldDate(state.contacts)),
                              sort: t == 'newest' ? 'a_to_z' : 'newest',
                            ));
                          },
                          child: Column(
                            children: <Widget>[
                              Icon(
                                TinyCampusIcons.filter_2,
                                color: Color.fromARGB(255, 1, 68, 87),
                              ),
                              I18nText(
                                'modules.business_cards.'
                                'start_screen.sort.${state.sort}',
                                child: Text(
                                  '',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Divider(
                    indent: 12.0,
                    endIndent: 12.0,
                    color: Colors.grey[200],
                    thickness: 1.5,
                  ),
                  state.contacts.isEmpty
                      ? Container(
                          margin: EdgeInsets.only(top: 16.0, bottom: 36),
                          child: I18nText(
                            'modules.business_cards.'
                            'start_screen.no_contacts_available',
                            child: Text(
                              '',
                              style: TextStyle(color: Colors.grey),
                            ),
                          ),
                        )
                      : Column(
                          children: state.contacts
                              .map((contact) => VCardContactPreview(
                                    contact: contact,
                                    pos: state.contacts.indexOf(contact),
                                    slidableController: _slidableController,
                                  ))
                              .toList()),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _closeSlidables() {
    if (_slidableController.activeState != null) {
      _slidableController.activeState!.close();
    }
  }

  Future<void> _askForPermission() async {
    await Permission.contacts.request();
    await Permission.camera.request();
  }

  /// This function is needed so the old DateFormat could be
  /// transformed into ISO8601 format
  List<VCard> _migrateOldDate(List<VCard> contacts) {
    var format = DateFormat("dd.MM.yyyy, hh:mm");
    return contacts
      ..where((element) => element.date.contains("Uhr")).forEach((element) {
        contacts.add(VCard(
            formattedName: element.formattedName,
            categories: element.categories,
            role: element.role,
            title: element.title,
            organization: element.organization,
            email: element.email,
            url: element.url,
            otherPhone: element.otherPhone,
            date: format.parse(element.date).toString()));
        contacts.remove(element);
      });
  }

  List<VCard> _sortAlphabetical(List<VCard> contacts) =>
      contacts..sort((a, b) => a.formattedName.compareTo(b.formattedName));

  List<VCard> _sortDate(List<VCard> contacts) => contacts
    ..sort((a, b) => DateTime.parse(b.date).compareTo(DateTime.parse(a.date)));

  Future<void> _importContacts(List<VCard> contacts) async {
    for (var c in contacts) {
      await ContactsService.addContact(Contact(
        familyName: " ",
        givenName: c.formattedName,
        phones: [Item(label: "", value: c.otherPhone)],
        emails: [Item(label: "", value: c.email)],
        jobTitle: c.title,
        company: c.organization,
      ));
    }
  }
}
