/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../common/assets_adapter.dart';
import '../../common/tinycampus_icons.dart';
import 'contacts/vcard_sharing.dart';
import 'qr_code_read.dart';
import 'v_card/v_card_bloc.dart';

/// The QrCodeScreen shows the personal QrCode generate with the personal VCard
class QrCodeScreen extends StatefulWidget {
  /// The arguments which are needed
  final QRScreenArguments qrArguments;

  /// Constructor of [QrCodeScreen]
  QrCodeScreen({Key? key, required this.qrArguments}) : super(key: key);

  @override
  _QrCodeScreenState createState() => _QrCodeScreenState();
}

class _QrCodeScreenState extends State<QrCodeScreen> {
  final double iconSize = 50;
  bool isQrGenerationScreen = false;

  @override
  void initState() {
    super.initState();
    isQrGenerationScreen = widget.qrArguments.isQrGenerationScreen;
  }

  @override
  Widget build(BuildContext context) => BlocBuilder<VCardBloc, VCardState>(
        builder: (context, state) => Scaffold(
          appBar: AppBar(
            title: I18nText('modules.business_cards.qr.title'),
            actions: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.share,
                  color: Color.fromARGB(255, 25, 140, 178),
                ),
                onPressed: () => VCardSharing().share(state.personal),
              ),
            ],
          ),
          resizeToAvoidBottomInset: false,
          body: ListView(
            children: <Widget>[
              Column(
                children: <Widget>[
                  _buildTopText(),
                  isQrGenerationScreen ? _buildQRCode(context) : QrReadScreen(),
                  _buildBottomSwitchButtonRow(),
                ],
              ),
            ],
          ),
        ),
      );

  Widget _buildTopText() => Container(
        margin: EdgeInsets.only(
          top: 15.0,
          bottom: 15.0,
        ),
        child: I18nText(
          'modules.business_cards.qr.'
          '${isQrGenerationScreen ? 'show_own_code' : 'scan_code'}',
          child: Text(
            '',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ),
      );

  Widget _buildQRCode(BuildContext context) =>
      BlocBuilder<VCardBloc, VCardState>(
        builder: (context, state) => SizedBox(
          height: 300,
          child: Column(
            children: <Widget>[
              QrImage(
                data: state.personal.getFormattedString(),
                errorCorrectionLevel: QrErrorCorrectLevel.L,
                version: QrVersions.auto,
                size: 300,
                gapless: true,
                foregroundColor: Color.fromARGB(255, 1, 68, 78),
                embeddedImage:
                    AssetImage(AssetAdapter.tinyCampusLogoWithPadding()),
                embeddedImageStyle: QrEmbeddedImageStyle(
                  size: Size(64, 64),
                ),
              ),
            ],
          ),
        ),
      );

  Widget _buildBottomSwitchButtonRow() =>
      _buildSwitchButtons(isQrGenerationScreen);

  Widget _buildSwitchButtons(bool isQrGenerationScreen) => Container(
        margin: EdgeInsets.symmetric(vertical: 40.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            GestureDetector(
              onTap: !this.isQrGenerationScreen
                  ? () {
                      setState(() {
                        this.isQrGenerationScreen = !this.isQrGenerationScreen;
                      });
                    }
                  : () {},
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    TinyCampusIcons.qr,
                    size: iconSize,
                    color: this.isQrGenerationScreen
                        ? Color.fromARGB(255, 25, 140, 178)
                        : Colors.grey,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 16.0),
                    width: 110.0,
                    child: I18nText(
                      'modules.business_cards.qr.generate_code',
                      child: Text(
                        '',
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: this.isQrGenerationScreen
                  ? () {
                      setState(() {
                        this.isQrGenerationScreen = !this.isQrGenerationScreen;
                      });
                    }
                  : () {},
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    TinyCampusIcons.camera,
                    size: iconSize,
                    color: !this.isQrGenerationScreen
                        ? Color.fromARGB(255, 25, 140, 178)
                        : Colors.grey,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 16.0),
                    width: 110.0,
                    child: I18nText(
                      'modules.business_cards.qr.scan_another_code',
                      child: Text(
                        '',
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
}

/// The arguments which are needed by [QrCodeScreen]
class QRScreenArguments {
  /// Checks if you are on [QrCodeScreen] or [QrReadScreen]
  final bool isQrGenerationScreen;

  /// Constructor of [QRScreenArguments]
  QRScreenArguments({required this.isQrGenerationScreen});
}
