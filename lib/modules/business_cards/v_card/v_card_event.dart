/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:meta/meta.dart';

import '../contacts/vcard/vcard.dart';
import 'v_card_bloc.dart';

/// All events which could happen in this module
@immutable
abstract class VCardEvent {
  const VCardEvent();

  /// The method which all events need
  /// @param the state shows what is changing
  VCardState performAction(VCardState currentState);
}

/// Is used when the user edits his own [VCard]
/// It only changes the personal [VCard]
class PersonalVCardChangeEvent extends VCardEvent {
  /// The new personal [VCard]
  final VCard personal;

  /// Constructor of [PersonalVCardChangeEvent]
  /// Needs the new [VCard]
  PersonalVCardChangeEvent({required this.personal});

  @override
  VCardState performAction(VCardState currentState) => PersonalVCardChangeState(
      personal, currentState.contacts, currentState.sort);
}

/// Is used when a new contact is added to the contact list
/// It only changes the contact list
class ContactListAddEvent extends VCardEvent {
  /// The new contact which will be added
  final VCard contact;

  /// Constructor of [ContactListAddEvent]
  /// Needs the new contact
  ContactListAddEvent({required this.contact});

  @override
  VCardState performAction(VCardState currentState) => ContactListAddState(
        currentState.personal,
        (currentState.contacts..insert(0, contact)),
        currentState.sort,
      );
}

/// Is used when a contact is deleted from the contact list
/// It only changes the contact list
class ContactListDeleteEvent extends VCardEvent {
  /// The contact which will be deleted
  final VCard contact;

  /// Constructor of [ContactListDeleteEvent]
  /// Needs the contact to be deleted
  ContactListDeleteEvent({required this.contact});

  @override
  VCardState performAction(VCardState currentState) => ContactListDeleteState(
        currentState.personal,
        (currentState.contacts..remove(contact)),
        currentState.sort,
      );
}

/// Is used when the user write a comment for one contact
/// It only changes the contact list
class ContactListEditEvent extends VCardEvent {
  /// The contact which gets the comment
  final VCard contact;

  /// The position of [contact]
  final int pos;

  /// Constructor of [ContactListEditEvent]
  /// Needs the contact and the position of the contact
  ContactListEditEvent({required this.contact, required this.pos});

  @override
  VCardState performAction(VCardState currentState) {
    currentState.contacts.removeAt(pos);
    return ContactListEditState(
      currentState.personal,
      currentState.contacts..insert(pos, contact),
      currentState.sort,
    );
  }
}

/// Is used when the contact list is sorted
/// It changes the contact list and the [sort] string
class ContactListSortEvent extends VCardEvent {
  /// New sorted list
  final List<VCard> contacts;

  /// String of how the list is sorted
  final String sort;

  /// Constructor of [ContactListSortEvent]
  /// Needs the new list and the [sort] string
  ContactListSortEvent({required this.contacts, required this.sort});

  @override
  VCardState performAction(VCardState currentState) => ContactListSortState(
        currentState.personal,
        contacts,
        sort,
      );
}

class VCardResetEvent extends VCardEvent {
  const VCardResetEvent();

  @override
  VCardState performAction(VCardState currentState) => InitialVCardState();
}
