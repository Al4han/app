/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

import 'vcard_formatter.dart';

part 'vcard.g.dart';

/// vCard, Valery Colong, May 2019
/// forked from https://github.com/valerycolong/vcard
@JsonSerializable(anyMap: true, explicitToJson: true)
@immutable
class VCard extends Equatable {
  /// The address for private electronic mail communication
  final String email;

  /// Formatted name string associated with the vCard object
  /// (will automatically populate if not set)
  final String formattedName;

  /// Categories
  final String categories;

  /// Specifies supplemental information or
  /// a comment that is associated with the vCard
  final String note;

  /// The name and optionally the unit(s)
  /// of the organization associated with the vCard object
  final String organization;

  /// The role, occupation, or business category
  /// of the vCard object within an organization
  final String role;

  /// Specifies the job title, functional position or function
  /// of the individual within an organization
  final String title;

  /// URL pointing to a website that represents the person in some way
  final String url;

  /// Other phone
  final String otherPhone;

  /// vCard version
  final String version;

  /// Date when a vCard was added to Contacts
  final String date;

  /// Get formatted vCard
  /// @return {String} Formatted vCard in VCF format
  String getFormattedString() => VCardFormatter().getFormattedString(this);

  VCard({
    this.email = '',
    this.formattedName = '',
    this.categories = '',
    this.note = '',
    this.organization = '',
    this.role = '',
    this.title = '',
    this.url = '',
    this.otherPhone = '',
    this.version = '2.1',
    this.date = '',
  });

  factory VCard.fromJson(Map<String, dynamic> json) => _$VCardFromJson(json);

  Map<String, dynamic> toJson() => _$VCardToJson(this);

  @override
  List<Object> get props => [
        email,
        formattedName,
        categories,
        note,
        organization,
        role,
        title,
        url,
        otherPhone,
        version,
        date
      ];
}
