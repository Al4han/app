/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/widgets/I18nText.dart';

import '../v_card/v_card_bloc.dart';
import '../widgets/v_card_form_field.dart';

/// This class represents the arguments which are needed by [VCardContactNote].
class VCardContactNoteArguments {
  /// The constructor of [VCardContactNoteArguments]
  VCardContactNoteArguments(this.pos);

  /// The position in the VCard list
  int pos;
}

/// In this screen the user can write a comment for a specific contact
class VCardContactNote extends StatelessWidget {
  /// The position of the [vCard] in the contact list
  final int pos;

  /// The constructor of [VCardContactNote]
  ///
  /// @param the contact [VCard] and the position
  VCardContactNote({
    Key? key,
    required this.pos,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => BlocBuilder<VCardBloc, VCardState>(
        builder: (context, state) => Scaffold(
          appBar: AppBar(
            title: I18nText(
              'modules.business_cards.vcard.contacts.notes_on',
              translationParams: <String, String>{
                'name': state.contacts.elementAt(pos).formattedName,
              },
            ),
          ),
          body: ListView(
            children: <Widget>[VCardFormField.getNote(state, context, pos)],
          ),
        ),
      );
}
