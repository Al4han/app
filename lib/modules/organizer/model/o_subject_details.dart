/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:json_annotation/json_annotation.dart';

import 'o_lesson.dart';
import 'o_person.dart';
import 'o_subject.dart';

part 'o_subject_details.g.dart';

@JsonSerializable()
class OSubjectDetails {
  OSubjectDetails({
    this.id,
    this.name,
    this.departmentID,
    this.lessons,
    this.moduleCode,
    this.executors,
    this.teachers,
    this.description,
    this.objective,
    this.content,
    this.expertise,
    this.methodCompetence,
    this.socialCompetence,
    this.selfCompetence,
    this.duration,
    this.instructionLanguage,
    this.expenditure,
    this.sws,
    this.method,
    this.preliminaryWork,
    this.proof,
    this.evaluation,
    this.availability,
    this.literature,
    this.aids,
    this.prerequisites,
    this.preRequisiteModules,
    this.recommendedPrerequisites,
    this.prerequisiteFor,
    this.postRequisiteModules,
  });

  int? id;
  String? name;
  int? departmentID;
  List<OLesson>? lessons;
  String? moduleCode;
  List<OPerson>? executors;
  List<OPerson>? teachers;
  String? description;
  String? objective;
  List<String>? content;
  String? expertise;
  String? methodCompetence;
  String? socialCompetence;
  String? selfCompetence;
  int? duration;
  String? instructionLanguage;
  String? expenditure;
  int? sws;
  String? method;
  String? preliminaryWork;
  String? proof;
  String? evaluation;
  String? availability;
  String? literature;
  String? aids;
  String? prerequisites;
  List<OSubject>? preRequisiteModules;
  String? recommendedPrerequisites;
  String? prerequisiteFor;
  List<OSubject>? postRequisiteModules;

  factory OSubjectDetails.fromJson(Map<String, dynamic> json) =>
      _$OSubjectDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$OSubjectDetailsToJson(this);
}
