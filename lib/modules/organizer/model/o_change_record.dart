/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:json_annotation/json_annotation.dart';

part 'o_change_record.g.dart';

@JsonSerializable(anyMap: true, explicitToJson: true)
class OChangeRecord {
  OChangeRecord({
    this.userChecked = false,
    this.oldRef, // can and must be nullable
    this.newRef, // can and must be nullable
    this.type = "",
    this.contextId = "",
  }) : delta = <ODelta>[];

  String contextId;
  List<ODelta> delta;
  bool userChecked;
  Map<String, dynamic>? oldRef;
  Map<String, dynamic>? newRef;
  String type;

  factory OChangeRecord.fromJson(Map<String, dynamic> json) =>
      _$OChangeRecordFromJson(json);

  Map<String, dynamic> toJson() => _$OChangeRecordToJson(this);

  @override
  String toString() => delta.toString();
}

enum ODelta {
  added,
  nameChanged,
  amountSubtreeIncreased,
  amountSubtreeDecreased,
  removed,
  descriptionChanged,
}
