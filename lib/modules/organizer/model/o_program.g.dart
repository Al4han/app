// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_program.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OProgram _$OProgramFromJson(Map json) => OProgram(
      name: json['name'] as String? ?? "",
      id: json['id'] as int? ?? -1,
      assumedDegree: json['assumedDegree'] as String? ?? "",
      pools: (json['pools'] as List<dynamic>?)
          ?.map((e) => OPool.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
    );

Map<String, dynamic> _$OProgramToJson(OProgram instance) => <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'assumedDegree': instance.assumedDegree,
      'pools': instance.pools.map((e) => e.toJson()).toList(),
    };
