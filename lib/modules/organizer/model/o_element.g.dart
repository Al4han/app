// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_element.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OElement _$OElementFromJson(Map json) => OElement(
      id: json['id'] as int?,
      comment: json['comment'] as String?,
      date:
          json['date'] == null ? null : DateTime.parse(json['date'] as String),
      start: json['start'] == null
          ? null
          : DateTime.parse(json['start'] as String),
      end: json['end'] == null ? null : DateTime.parse(json['end'] as String),
      subjectId: json['subjectId'] as int?,
      subjectNo: json['subjectNo'] as String?,
      lessonId: json['lessonId'] as int?,
      abbr: json['abbr'] as String?,
      name: json['name'] as String?,
      fullname: json['fullname'] as String?,
      type: json['type'] as String? ?? "",
      typeLong: json['typeLong'] as String? ?? "",
      cyclic: json['cyclic'] as bool?,
    )
      ..organization = json['organization'] as String?
      ..organizationId = OCommonFunctions.forceInt(json['organizationId'])
      ..resource = (json['resource'] as List<dynamic>)
          .map((e) => OResource.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList();

Map<String, dynamic> _$OElementToJson(OElement instance) => <String, dynamic>{
      'id': instance.id,
      'comment': instance.comment,
      'date': instance.date?.toIso8601String(),
      'start': instance.start?.toIso8601String(),
      'end': instance.end?.toIso8601String(),
      'subjectId': instance.subjectId,
      'lessonId': instance.lessonId,
      'subjectNo': instance.subjectNo,
      'abbr': instance.abbr,
      'fullname': instance.fullname,
      'name': instance.name,
      'type': instance.type,
      'typeLong': instance.typeLong,
      'organization': instance.organization,
      'organizationId': instance.organizationId,
      'cyclic': instance.cyclic,
      'resource': instance.resource.map((e) => e.toJson()).toList(),
    };
