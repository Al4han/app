// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_lesson.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OLesson _$OLessonFromJson(Map json) => OLesson(
      name: json['name'] as String?,
      id: OCommonFunctions.forceInt(json['id']),
      comment: json['comment'] as String?,
      subjectID: OCommonFunctions.forceInt(json['subjectID']),
      method: json['method'] as String?,
      subjectName: json['subjectName'] as String?,
      fullname: json['fullname'] as String?,
      enabled: json['enabled'] as bool?,
    );

Map<String, dynamic> _$OLessonToJson(OLesson instance) => <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'comment': instance.comment,
      'subjectID': instance.subjectID,
      'method': instance.method,
      'subjectName': instance.subjectName,
      'fullname': instance.fullname,
      'enabled': instance.enabled,
    };
