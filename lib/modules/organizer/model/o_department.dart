/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:json_annotation/json_annotation.dart';

import '../utility/organizer_common_functions.dart';

part 'o_department.g.dart';

@JsonSerializable()
class ODepartment {
  ODepartment({
    this.id,
    this.name,
  });

  @JsonKey(fromJson: OCommonFunctions.forceInt)
  int? id;
  String? name;

  factory ODepartment.fromJson(Map<String, dynamic> json) =>
      _$ODepartmentFromJson(json);

  Map<String, dynamic> toJson() => _$ODepartmentToJson(this);
}
