// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_subject.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OSubject _$OSubjectFromJson(Map json) => OSubject(
      name: json['name'] as String? ?? "",
      id: json['id'] as int? ?? -1,
      enabled: json['enabled'] as bool? ?? false,
      lessons: (json['lessons'] as List<dynamic>?)
          ?.map((e) => OLesson.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
    );

Map<String, dynamic> _$OSubjectToJson(OSubject instance) => <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'lessons': instance.lessons.map((e) => e.toJson()).toList(),
      'enabled': instance.enabled,
    };
