// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_block_intervals.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OBlockIntervals _$OBlockIntervalsFromJson(Map json) => OBlockIntervals(
      json['abbr'] as String,
      json['fullName'] as String,
      (json['blocks'] as List<dynamic>)
          .map((e) => OBlock.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
    );

Map<String, dynamic> _$OBlockIntervalsToJson(OBlockIntervals instance) =>
    <String, dynamic>{
      'abbr': instance.abbr,
      'fullName': instance.fullName,
      'blocks': instance.blocks.map((e) => e.toJson()).toList(),
    };
