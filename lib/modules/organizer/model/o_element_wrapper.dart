/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:json_annotation/json_annotation.dart';

import 'o_element.dart';

part 'o_element_wrapper.g.dart';

@JsonSerializable(anyMap: true, explicitToJson: true)
class OElementWrapper {
  OElementWrapper({
    this.layer = 0,
    required this.ele,
  });

  int layer;
  bool isIntersected = false;
  OElement ele; // TODO NNBD MIGRATION
  OElementWrapper? intersectsWith;
  int countIntersections = 0;

  void init() {
    layer = 0;
    isIntersected = false;
    countIntersections = 0;
    intersectsWith = null;
  }

  factory OElementWrapper.fromJson(Map<String, dynamic> json) =>
      _$OElementWrapperFromJson(json);

  Map<String, dynamic> toJson() => _$OElementWrapperToJson(this);
}
