/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:json_annotation/json_annotation.dart';

import 'o_change_record.dart';
import 'o_lesson.dart';

part 'o_subject.g.dart';

@JsonSerializable(anyMap: true, explicitToJson: true)
class OSubject {
  OSubject({
    this.name = "",
    this.id = -1,
    this.enabled = false,
    List<OLesson>? lessons,
  }) : lessons = lessons ?? <OLesson>[];

  String name;
  int id;
  List<OLesson> lessons;
  bool enabled;

  factory OSubject.fromJson(Map<String, dynamic> json) =>
      _$OSubjectFromJson(json);

  Map<String, dynamic> toJson() => _$OSubjectToJson(this);

  OLesson? findLessonById(int lessonId) {
    if (lessons.isEmpty) {
      return null;
    }
    for (var i = 0; i < lessons.length; i++) {
      if (lessons[i].id == lessonId) {
        return lessons[i];
      }
    }
    // if nothing was found
    return null;
  }

  bool hasLessonById(int lessonId) {
    if (lessons.isEmpty) {
      return false;
    }
    for (var i = 0; i < lessons.length; i++) {
      if (lessons[i].id == lessonId) {
        return true;
      }
    }
    // if nothing was found
    return false;
  }

  /// [other] is regarded as the most recent compared object
  OChangeRecord? identifyChanges(
      OSubject? other, List<OSubject> siblings, List<OSubject> otherSiblings) {
    var record = OChangeRecord();
    record.type = runtimeType.toString();
    record.oldRef = shallowCopy().toJson();
    record.userChecked = false;
    record.delta = <ODelta>[];
    if (other == null) {
      record.delta.add(ODelta.removed);
      return record;
    }
    record.newRef = other.shallowCopy().toJson();
    record.contextId = other.toContextString();
    record.newRef = other.shallowCopy().toJson();
    // check if name is the same
    if (name != other.name) {
      record.delta.add(ODelta.nameChanged);
    }
    if (lessons.isNotEmpty && other.lessons.isNotEmpty) {
      // check if new subtree length is smaller
      if (lessons.length > other.lessons.length) {
        record.delta.add(ODelta.amountSubtreeDecreased);
      }
      // check if new subtree length is bigger
      if (lessons.length < other.lessons.length) {
        record.delta.add(ODelta.amountSubtreeIncreased);
      }
    }
    // check if it was added
    if (siblings.isNotEmpty && otherSiblings.isNotEmpty) {
      var list = siblings.map((e) => e.id).toList();
      var otherList = otherSiblings.map((e) => e.id).toList();
      if (!list.contains(other.id)) {
        record.delta.add(ODelta.added);
      }
      if (!otherList.contains(id)) {
        record.delta.add(ODelta.removed);
      }
    }
    if (siblings.isEmpty && otherSiblings.isNotEmpty) {
      record.delta.add(ODelta.added);
    }
    // check if there are changes
    if (record.delta.isEmpty) {
      return null;
    }
    return record;
  }

  static OChangeRecord wasAdded(OSubject a) {
    // here, b is the "older reference", since we need to track backwards
    var record = OChangeRecord();
    record.contextId = a.toContextString();
    record.type = a.runtimeType.toString();
    record.oldRef = null;
    record.newRef = a.shallowCopy().toJson();
    record.userChecked = false;
    record.delta = <ODelta>[];
    record.delta.add(ODelta.added);
    return record;
  }

  OSubject shallowCopy() =>
      OSubject(id: id, name: name, enabled: enabled, lessons: lessons);

  String toContextString() => id.toString();
}
