// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_element_wrapper.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OElementWrapper _$OElementWrapperFromJson(Map json) => OElementWrapper(
      layer: json['layer'] as int? ?? 0,
      ele: OElement.fromJson(Map<String, dynamic>.from(json['ele'] as Map)),
    )
      ..isIntersected = json['isIntersected'] as bool
      ..intersectsWith = json['intersectsWith'] == null
          ? null
          : OElementWrapper.fromJson(
              Map<String, dynamic>.from(json['intersectsWith'] as Map))
      ..countIntersections = json['countIntersections'] as int;

Map<String, dynamic> _$OElementWrapperToJson(OElementWrapper instance) =>
    <String, dynamic>{
      'layer': instance.layer,
      'isIntersected': instance.isIntersected,
      'ele': instance.ele.toJson(),
      'intersectsWith': instance.intersectsWith?.toJson(),
      'countIntersections': instance.countIntersections,
    };
