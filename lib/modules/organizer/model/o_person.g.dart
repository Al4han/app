// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_person.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OPerson _$OPersonFromJson(Map<String, dynamic> json) => OPerson(
      name: json['name'] as String? ?? "",
      id: json['id'] as int? ?? -1,
      role: json['role'] as String? ?? "",
    );

Map<String, dynamic> _$OPersonToJson(OPerson instance) => <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'role': instance.role,
    };
