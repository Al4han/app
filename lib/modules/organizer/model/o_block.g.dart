// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_block.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OBlock _$OBlockFromJson(Map<String, dynamic> json) => OBlock(
      json['label'] as String,
      DateTime.parse(json['start'] as String),
      DateTime.parse(json['end'] as String),
    );

Map<String, dynamic> _$OBlockToJson(OBlock instance) => <String, dynamic>{
      'start': instance.start.toIso8601String(),
      'end': instance.end.toIso8601String(),
      'label': instance.label,
    };
