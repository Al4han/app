/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

import '../ui/organizer/o_utility_functions.dart';
import 'o_change_record.dart';
import 'o_lesson.dart';
import 'o_pool.dart';
import 'o_subject.dart';

part 'o_program.g.dart';

@JsonSerializable(anyMap: true, explicitToJson: true)
class OProgram {
  OProgram({
    this.name = "",
    this.id = -1,
    this.assumedDegree = "",
    List<OPool>? pools,
  }) : pools = pools ?? <OPool>[];

  String name;
  int id;
  String assumedDegree;
  List<OPool> pools;

  factory OProgram.fromJson(Map<String, dynamic> json) =>
      _$OProgramFromJson(json);

  Map<String, dynamic> toJson() => _$OProgramToJson(this);

  int getDistinctSubjectsInProgram() {
    var distinctSubjects = <int>[];

    if (pools.isNotEmpty) {
      for (var pool in pools) {
        if (pool.subjects.isNotEmpty) {
          for (var subject in pool.subjects) {
            distinctSubjects.add(subject.id);
          }
        }
      }
    }
    // }
    // remove duplicates, make a list, check the length
    return distinctSubjects.toSet().toList().length;
  }

  List<int> getDistinctLessonsInProgram() {
    var lessonIds = <int>[];
    try {
      if (pools.isNotEmpty) {
        for (var pool in pools) {
          if (pool.subjects.isNotEmpty) {
            for (var subject in pool.subjects) {
              for (var lesson in subject.lessons) {
                if (lesson.id != null) {
                  lessonIds.add(lesson.id!);
                }
              }
            }
          }
        }
      }
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
    // }
    // remove duplicates, make a list, check the length
    return lessonIds.toSet().toList();
  }

  OPool? findPoolById(int poolId) {
    if (pools.isEmpty) {
      return null;
    }
    for (var i = 0; i < pools.length; i++) {
      if (pools[i].id == poolId) {
        return pools[i];
      }
    }
    return null;
  }

  bool hasPoolById(int poolId) {
    if (pools.isEmpty) {
      return false;
    }
    for (var i = 0; i < pools.length; i++) {
      if (pools[i].id == poolId) {
        return true;
      }
    }
    // if nothing was found
    return false;
  }

  OLesson? findLessonByIds(int poolId, int subjectId, int lessonId) {
    var pool = findPoolById(poolId);
    if (pool == null) return null;
    var subject = pool.findSubjectById(subjectId);
    if (subject == null) return null;
    return subject.findLessonById(lessonId);
  }

  bool hasLessonByIds(int poolId, int subjectId, int lessonId) {
    var pool = findPoolById(poolId);
    if (pool == null) return false;
    var subject = pool.findSubjectById(subjectId);
    if (subject == null) return false;
    return subject.hasLessonById(lessonId);
  }

  OSubject? findSubjectByIds(int poolId, int subjectId) {
    var pool = findPoolById(poolId);
    if (pool == null) return null;
    return pool.findSubjectById(subjectId);
  }

  List<OSubject> findSubjectsByLessonId(int lesson) => pools
      .expand((e) => e.subjects)
      .where((e) => e.hasLessonById(lesson))
      .toList();

  bool hasSubjectByIds(int poolId, int subjectId) {
    var pool = findPoolById(poolId);
    if (pool == null) return false;
    return pool.hasSubjectById(subjectId);
  }

  Map<String, OChangeRecord> changeReport(OProgram otherProgram) {
    var map = <String, OChangeRecord>{};
    // check program
    /// TODO: Program is commented because it does not have a specific
    /// user interaction linked to resolving the change
    // map[toContextString()] = identifyChanges(otherProgram);

    // check pool
    // could be: nameChanged, amountSubtreeIncreased, amountSubtreeDecreased,
    // removed
    try {
      for (var i = 0; i < pools.length; i++) {
        try {
          var newChange = pools[i].identifyChanges(
            otherProgram.findPoolById(pools[i].id),
            pools,
            otherProgram.pools,
          );
          if (newChange != null) {
            map[OUtil.concatPP(this, pools[i])] = newChange;
          }
        } on Exception catch (e) {
          debugPrint("${"|POOLS| --- i=$i "}$e");
        }
        for (var j = 0; j < pools[i].subjects.length; j++) {
          try {
            var newChangePool = pools[i].subjects[j].identifyChanges(
                  otherProgram.findSubjectByIds(
                    pools[i].id,
                    pools[i].subjects[j].id,
                  ),
                  pools[i].subjects,
                  otherProgram.findPoolById(pools[i].id)?.subjects ??
                      <OSubject>[],
                );
            if (newChangePool != null) {
              map[OUtil.concatPPS(this, pools[i], pools[i].subjects[j])] =
                  newChangePool;
            }
          } on Exception catch (e) {
            debugPrint("${"|SUBJECTS| --- i=$i j=$j "}$e");
          }
          for (var k = 0; k < pools[i].subjects[j].lessons.length; k++) {
            try {
              var newLessonChange =
                  pools[i].subjects[j].lessons[k].identifyChanges(
                        otherProgram.findLessonByIds(
                          pools[i].id,
                          pools[i].subjects[j].id,
                          pools[i].subjects[j].lessons[k].id ?? -1,
                        ),
                        pools[i].subjects[j].lessons,
                        otherProgram
                            .findSubjectByIds(
                              pools[i].id,
                              pools[i].subjects[j].id,
                            )
                            ?.lessons,
                      );
              if (newLessonChange != null) {
                map[OUtil.concatPPSL(this, pools[i], pools[i].subjects[j],
                    pools[i].subjects[j].lessons[k])] = newLessonChange;
              }
            } on Exception catch (e) {
              debugPrint("${"|LESSONS| --- i=$i j=$j k=$k"} FIRST $e");
            }
          }
        }
      }

      // could be: could be: added
      for (var i = 0; i < otherProgram.pools.length; i++) {
        if (!hasPoolById(otherProgram.pools[i].id)) {
          map[OUtil.concatPP(this, otherProgram.pools[i])] =
              OPool.wasAdded(otherProgram.pools[i]);
        }

        for (var j = 0; j < otherProgram.pools[i].subjects.length; j++) {
          if (!hasSubjectByIds(
              otherProgram.pools[i].id, otherProgram.pools[i].subjects[j].id)) {
            map[OUtil.concatPPS(
              this,
              otherProgram.pools[i],
              otherProgram.pools[i].subjects[j],
            )] = OSubject.wasAdded(otherProgram.pools[i].subjects[j]);
          }

          for (var k = 0;
              k < otherProgram.pools[i].subjects[j].lessons.length;
              k++) {
            try {
              if (!hasLessonByIds(
                otherProgram.pools[i].id,
                otherProgram.pools[i].subjects[j].id,
                otherProgram.pools[i].subjects[j].lessons[k].id ?? -1,
              )) {
                map[OUtil.concatPPSL(
                        this,
                        otherProgram.pools[i],
                        otherProgram.pools[i].subjects[j],
                        otherProgram.pools[i].subjects[j].lessons[k])] =
                    OLesson.wasAdded(
                        otherProgram.pools[i].subjects[j].lessons[k]);
              }
            } on Exception catch (e) {
              debugPrint("${"|LESSONS| --- i=$i j=$j k=$k"} SECOND $e");
            }
          }
        }
      }
    } on Exception catch (e) {
      debugPrint("|GENERAL| --- $e");
    }
    // map.removeWhere((key, value) => value == null);
    return map;
  }

  /// [other] is regarded as the most recent compared object
  OChangeRecord? identifyChanges(OProgram? other) {
    var record = OChangeRecord();

    record.type = runtimeType.toString();
    record.oldRef = shallowCopy().toJson();
    record.type = runtimeType.toString();
    record.userChecked = false;
    record.delta = <ODelta>[];
    record.contextId = toContextString();
    if (other == null) {
      record.delta.add(ODelta.removed);
      return record;
    }
    record.newRef = other.shallowCopy().toJson();
    // check if name is the same
    if (name != other.name) {
      record.delta.add(ODelta.nameChanged);
    }
    if (pools.isNotEmpty && other.pools.isNotEmpty) {
      // check if new subtree length is smaller
      if (pools.length > other.pools.length) {
        record.delta.add(ODelta.amountSubtreeDecreased);
      }
      // check if new subtree length is bigger
      if (pools.length < other.pools.length) {
        record.delta.add(ODelta.amountSubtreeIncreased);
      }
    }

    // check if there are changes
    if (record.delta.isEmpty) {
      return null;
    }
    return record;
  }

  OProgram shallowCopy() =>
      OProgram(id: id, name: name, assumedDegree: assumedDegree, pools: pools);

  String toContextString() => id.toString();
}
