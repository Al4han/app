/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:json_annotation/json_annotation.dart';

import 'o_block.dart';

part "o_block_intervals.g.dart";

@JsonSerializable(anyMap: true, explicitToJson: true)
class OBlockIntervals {
  OBlockIntervals(this.abbr, this.fullName, this.blocks);
  final String abbr;
  final String fullName;
  final List<OBlock> blocks;

  factory OBlockIntervals.fromJson(Map<String, dynamic> json) =>
      _$OBlockIntervalsFromJson(json);

  Map<String, dynamic> toJson() => _$OBlockIntervalsToJson(this);
}
