/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../utility/organizer_data_loader.dart';
import 'o_utility_functions.dart';
import 'organizer_enums.dart';
import 'organizer_pageview.dart';

class OrganizerPage extends StatefulWidget {
  OrganizerPage({Key? key}) : super(key: key);

  @override
  _OrganizerPageState createState() => _OrganizerPageState();
}

OrganizerInterval organizerInterval = OrganizerInterval.weekly;

class _OrganizerPageState extends State<OrganizerPage> {
  CurrentOrganizerState localState = CurrentOrganizerState.noLessonsSelected;

  @override
  Widget build(BuildContext context) => OPageView();

  @override
  void initState() {
    super.initState();
    OUtil.initBlocs(context);
    OUtil.initPageValues();
    localState = CurrentOrganizerState.initialState;
    try {
      if (mounted) {
        ODataLoader.loadData();
      }
      OUtil.isUpdating = true;
      if (mounted) {
        OUtil.updateAllSelectedPrograms();
      }
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
  }

  @override
  void dispose() {
    OUtil.disposePageValues();
    super.dispose();
  }
}
