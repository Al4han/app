/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../common/tc_theme.dart';
import '../../../../../common/widgets/tc_button.dart' show TCButton;
import '../../../blocs/o_color_mapping_bloc/o_color_mapping_bloc.dart';
import '../../../blocs/o_drawer_bloc/o_drawer_bloc.dart';
import '../../../model/o_element_wrapper.dart';
import '../o_utility_functions.dart';
import 'i18n_common.dart';
import 'o_drawer_menu.dart';

class ODrawerMColor extends StatefulWidget {
  const ODrawerMColor({
    Key? key,
    required this.oElementWrapper,
  }) : super(key: key);
  final OElementWrapper oElementWrapper; // TODO NNBD MIGRATION

  @override
  _ODrawerMColorState createState() => _ODrawerMColorState();
}

class _ODrawerMColorState extends State<ODrawerMColor> {
  final double opacity = 1.0;
  Color selectedColor = Colors.grey; // TODO NNBD MIGRATION
  List<Color> colors = <Color>[];
  final FixedExtentScrollController scrollController =
      FixedExtentScrollController();

  @override
  void initState() {
    super.initState();
    colors = [
      Color(0x198CB2.toInt()).withOpacity(opacity),
      Color(0x4BE3D4.toInt()).withOpacity(opacity),
      Color(0x4BE3B4.toInt()).withOpacity(opacity),
      Color(0x4BE383.toInt()).withOpacity(opacity),
      Color(0x5FE34B.toInt()).withOpacity(opacity),
      Color(0xB6E34B.toInt()).withOpacity(opacity),
      Color(0xE3DE4B.toInt()).withOpacity(opacity),
      Color(0xE3AB4B.toInt()).withOpacity(opacity),
      Color(0xD99100.toInt()).withOpacity(opacity),
      Color(0xFC5A49.toInt()).withOpacity(opacity),
      Color(0xE34B4B.toInt()).withOpacity(opacity),
      Color(0xE34BA6.toInt()).withOpacity(opacity),
      Color(0xA14BE3.toInt()).withOpacity(opacity),
      Color(0x9C4BE3.toInt()).withOpacity(opacity),
      Color(0x5E4BE3.toInt()).withOpacity(opacity),
      Color(0x014457.toInt()).withOpacity(opacity),
      Color(0x000000.toInt()).withOpacity(opacity),
    ];
  }

  @override
  Widget build(BuildContext context) => Column(
        children: <Widget>[
          ODrawerHeader(oElementWrapper: widget.oElementWrapper),
          Container(
            color: Colors.white,
            height: 50,
            child: _generateList(
              widget.oElementWrapper.ele.subjectId ?? 0, // TODO NNBD MIGRATION
              colors,
            ),
          ),
          Container(
            color: Colors.white,
            padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 16.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: TCButton(
                    buttonLabel:
                        FlutterI18n.translate(context, 'common.actions.back'),
                    elevation: 0.0,
                    borderColor: CorporateColors.tinyCampusBlue,
                    backgroundColor: Theme.of(context).primaryColor,
                    borderThickness: 2,
                    foregroundColor: CorporateColors.tinyCampusBlue,
                    onPressedCallback: () {
                      OUtil.addODrawerEvent(OSelectEvent(
                          oElementWrapper: widget.oElementWrapper));
                    },
                  ),
                ),
                // Container(
                //   width: 16.0,
                // ),
                // Expanded(flex: 1, child: Container()),
              ],
            ),
          ),
        ],
      );

  Widget _generateList(int subjectId, List<Color> colors) => ListView.builder(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemCount: colors.length,
        itemBuilder: (context, index) => ColorSelectorWidget(
          color: colors[index],
          selected: checkIfSelectedColor(colors[index], selectedColor),
          onColorSelected: _selectColor,
        ),
      );

  bool checkIfSelectedColor(Color a, Color b) => a.value == b.value;

  void _selectColor(Color color) {
    var colorBloc = BlocProvider.of<OColorMappingBloc>(context);
    var currentState = colorBloc.state;
    if (currentState is OColorMappingReadyState) {
      var previousList = currentState.previousColorList;
      colorBloc.add(MapOSubjectToColorEvent(
        subject:
            widget.oElementWrapper.ele.subjectId ?? 0, // TODO NNBD MIGRATION
        previousColorList: previousList,
        color: color.value.toRadixString(16),
      ));
    }
    setState(() {
      selectedColor = color;
    });
  }
}

class ColorSelectorWidget extends StatelessWidget {
  ColorSelectorWidget({
    Key? key,
    required this.color,
    required this.selected,
    required this.onColorSelected,
  }) : super(key: key);
  final double size = 60;
  final double margin = 2;
  final bool selected;
  final Color color;
  final void Function(Color) onColorSelected;
  @override
  Widget build(BuildContext context) => GestureDetector(
        // behavior: HitTestBehavior.translucent,
        onTap: () => onColorSelected(color),
        child: Transform(
          alignment: Alignment.center,
          transform: Matrix4.identity()..scale(selected ? 1.00 : 0.85),
          child: Card(
            elevation: 0.0,
            margin: EdgeInsets.symmetric(vertical: margin, horizontal: margin),
            color: color,
            child: SizedBox(
              // duration: Duration(
              //   milliseconds: 16*4,
              // ),
              width: 40.0,
              height: 40.0,
              child: Opacity(
                opacity: selected ? 1.0 : 0.0,
                // duration: Duration(
                //   milliseconds: 16*4,
                // ),
                child: Center(
                  child: Icon(
                    Icons.check,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
        ),
      );
}
