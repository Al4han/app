/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../common/tc_theme.dart';
import '../../../blocs/o_animation_bloc/o_animation_bloc.dart';
import '../../../blocs/o_color_mapping_bloc/o_color_mapping_bloc.dart';
import '../../../blocs/o_drawer_bloc/o_drawer_bloc.dart';
import "../../../model/o_element_wrapper.dart";
import '../o_utility_functions.dart';
import 'i18n_common.dart';
import 'o_drawer_widget_components.dart';

class ODrawerMenu extends StatefulWidget {
  const ODrawerMenu({
    Key? key,
    required this.oElementWrapper,
    required this.drawerMenuContent,
    this.showColorTopBar = true,
  }) : super(key: key);
  final OElementWrapper oElementWrapper;
  final Widget drawerMenuContent;
  final bool showColorTopBar;

  // final ODrawerButtons oElementWrapper;

  @override
  _ODrawerMenuState createState() => _ODrawerMenuState();
}

class _ODrawerMenuState extends State<ODrawerMenu>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) => Container(
        margin: EdgeInsets.only(bottom: 8.0),
        child: Column(
          children: <Widget>[
            widget.drawerMenuContent,
            Container(
              height: 150,
            ),
          ],
        ),
      );
}

class ODrawerButtons extends StatelessWidget {
  const ODrawerButtons({
    Key? key,
    required this.oElementWrapper,
    required this.color,
  }) : super(key: key);

  final OElementWrapper oElementWrapper;
  final Color color;

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          ODrawerCardButton(
            text: FlutterI18n.translate(context, '$i18nDrawerKey.remove'),
            icon: Icon(Icons.delete_outline),
            callBack: () {
              //start animation
              final oAnimationBloc = BlocProvider.of<OAnimationBloc>(context);
              oAnimationBloc.add(
                OSetAnimationStateEvent(
                  OStartSingleShakingAnimationState(
                    oElementId:
                        oElementWrapper.ele.id ?? -1, // TODO NNBD MIGRATION
                  ),
                ),
              );
              OUtil.addODrawerEvent(ODeleteEvent());
            },
          ),
          ODrawerCardButton(
            text: FlutterI18n.translate(context, '$i18nDrawerKey.change_color'),
            icon: Icon(
              Icons.invert_colors,
              color: color,
            ),
            callBack: () => OUtil.addODrawerEvent(
              OColorEvent(oElementWrapper: oElementWrapper),
            ),
          ),
          // ODrawerCardButton(
          //     text: "mehr Details",
          //     icon: Icon(Icons.perm_device_information),
          //     callBack: () => ODrawerMenu
          //     .callDetails(context, OInfoEvent())),
        ],
      );
}

class ODrawerContent extends StatelessWidget {
  const ODrawerContent({
    Key? key,
    required this.oElementWrapper,
  }) : super(key: key);

  final OElementWrapper oElementWrapper;

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<OColorMappingBloc, OColorMappingState>(
        builder: (context, colorState) => Column(
          children: <Widget>[
            ODrawerHeader(oElementWrapper: oElementWrapper),
            ODrawerInformationRow(element: oElementWrapper.ele),
            // ODrawerButtons(oElementWrapper: oElementWrapper, color: color),
          ],
        ),
      );
}

class ODrawerHeader extends StatelessWidget {
  const ODrawerHeader({
    Key? key,
    required this.oElementWrapper,
  }) : super(key: key);

  final OElementWrapper oElementWrapper;

  @override
  Widget build(BuildContext context) {
    final hasComment = oElementWrapper.ele.comment != "";
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 12.0),
      // height: 100.0,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 6,
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Flexible(
                                child: Text(
                                  // oElementWrapper.ele.name.replaceAll("", "\u{200B}"),
                                  oElementWrapper.ele.name ?? "",
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 3,
                                  style: TextStyle(
                                      fontSize: 22.0,
                                      letterSpacing: -1.2,
                                      // letterSpacing: -0.7,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                            ],
                          ),
                          hasComment
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Flexible(
                                      child: SelectableText(
                                        oElementWrapper.ele.comment ?? "",
                                        scrollPhysics: BouncingScrollPhysics(),
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            letterSpacing: -0.5,
                                            color: CorporateColors
                                                .tinyCampusOrange),
                                      ),
                                    ),
                                  ],
                                )
                              : Container(),
                        ],
                      ),
                    ),
                    IconButton(
                      icon: AnimatedContainer(
                        width: 30,
                        height: 30,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            // border: Border.all(
                            //   color: Colors.red[500],
                            // ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 0,
                                blurRadius: 2,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                            borderRadius: BorderRadius.all(Radius.circular(4))),
                        duration: Duration(milliseconds: 240),
                        child: Icon(
                          Icons.delete,
                          size: 20.0,
                          color: CorporateColors.tinyCampusIconGrey,
                        ),
                      ),
                      // color: color,
                      onPressed: () {
                        // OUtil.getAffectingLessons(
                        //     oLessonId: [oElementWrapper.ele.lessonId]);
                        OUtil.addODrawerEvent(
                            ODeleteEvent(oElementWrapper: oElementWrapper));
                        OUtil.addOAnimationEvent(
                          OSetAnimationStateEvent(
                              OStartSingleShakingAnimationState(
                                  oElementId: oElementWrapper.ele.id ?? -1)),
                        );
                      },
                    ),
                    BlocBuilder<OColorMappingBloc, OColorMappingState>(
                      builder: (context, state) {
                        Color color;
                        // if (oElementWrapper != null) {
                        if (state is OColorMappingReadyState) {
                          color = state.getColor(
                            subjectId: oElementWrapper.ele.subjectId,
                          );
                        } else {
                          color = Colors.grey;
                        }
                        // } else {
                        //   color = Colors.grey;
                        // }
                        return IconButton(
                          icon: AnimatedContainer(
                            width: 30,
                            height: 30,
                            decoration: BoxDecoration(
                                color: color,
                                // border: Border.all(
                                //   color: Colors.red[500],
                                // ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4))),
                            duration: Duration(milliseconds: 240),
                            child: Icon(
                              Icons.invert_colors,
                              size: 20.0,
                              color: Colors.white,
                            ),
                          ),
                          // color: color,
                          onPressed: () {
                            OUtil.stopAllShakingAnimationIfNotAlready();
                            OUtil.addODrawerEvent(
                                OColorEvent(oElementWrapper: oElementWrapper));
                          },
                        );
                      },
                    ),
                  ],
                ),

                Divider(
                  height: 20,
                ),
                // ODrawerInformationRow(element: oElementWrapper.ele),
                // )
              ],
            ),
          ),
          // Expanded(
          //   flex: 1,
          //   child: CircularProgressIndicator(),
          // ),
        ],
      ),
    );
  }
}

class ODrawerCardButton extends StatelessWidget {
  const ODrawerCardButton({
    Key? key,
    required this.text,
    required this.icon,
    required this.callBack,
  }) : super(key: key);

  final String text;
  final Icon icon;
  final void Function() callBack;

  @override
  Widget build(BuildContext context) => Card(
        elevation: 0.0,
        child: FlatButton(
          padding: EdgeInsets.all(8.0),
          onPressed: callBack,
          // onPressed: () =>
          // addAllSubjectLessons(widget.args),
          child: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              icon,
              Text(
                text,
                softWrap: true,
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.w300, fontSize: 14.0),
              ),
            ],
          )),
        ),
      );
}

class ODrawerColorTop extends StatelessWidget {
  const ODrawerColorTop({
    Key? key,
    required this.oElementWrapper,
  }) : super(key: key);

  final OElementWrapper? oElementWrapper;

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<OColorMappingBloc, OColorMappingState>(
        builder: (context, state) {
          Color color;
          if (oElementWrapper != null) {
            if (state is OColorMappingReadyState) {
              color = state.getColor(subjectId: oElementWrapper?.ele.subjectId);
            } else {
              color = Colors.grey;
            }
          } else {
            color = Colors.grey;
          }
          return Row(
            children: <Widget>[
              Expanded(
                  child: AnimatedContainer(
                duration: Duration(milliseconds: 240),
                height: 4,
                color: color,
                // decoration: BoxDecoration(
                //   gradient: LinearGradient(
                //     begin: Alignment.topCenter,
                //     end: Alignment.bottomCenter,
                //     colors: [color, Colors.white],
                //   ),
                // ),
              )),
            ],
          );
        },
      );
}
