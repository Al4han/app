/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../blocs/o_loading_bloc/o_loading_bloc.dart';
import 'o_drawer_widget.dart';
import 'o_top_parallax_bar.dart';

class BottomSheetDrawer extends StatelessWidget {
  const BottomSheetDrawer({
    Key? key,
    required this.front,
    required this.drawer,
    required this.pageController,
  }) : super(key: key);

  final Widget front;
  final Widget drawer;
  final PageController pageController;

  @override
  Widget build(BuildContext context) => Container(
          child: (Stack(children: <Widget>[
        Positioned(
          left: 6,
          right: 6,
          top: 4,
          child: OTopParallaxBar(
            pageController: pageController,
          ),
        ),
        Positioned(
          child: front,
        ),
        ODrawerWidget(),
        buildLoadingOverlay(context),
      ])));

  Widget buildLoadingOverlay(BuildContext context) =>
      BlocBuilder<OLoadingBloc, OLoadingState>(
        builder: (context, state) {
          var showOverlay = false;
          if (state is OLoadMultipleState) {
            showOverlay = true;
          }
          return Positioned.fill(
              child: AnimatedSwitcher(
            duration: Duration(milliseconds: 260),
            child: showOverlay
                ? Container(
                    key: Key("0"),
                    color: Theme.of(context).backgroundColor,
                    child: Center(child: CircularProgressIndicator()))
                : Container(key: Key("1")),
          ));
        },
      );
}
