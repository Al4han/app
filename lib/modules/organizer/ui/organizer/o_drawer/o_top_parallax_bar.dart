/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../common/tc_theme.dart';
import '../../../blocs/o_loading_bloc/o_loading_bloc.dart';

class OTopParallaxBar extends StatefulWidget {
  const OTopParallaxBar({
    Key? key,
    required this.pageController,
  }) : super(key: key);
  final PageController pageController;

  @override
  _OTopParallaxBarState createState() => _OTopParallaxBarState();
}

class _OTopParallaxBarState extends State<OTopParallaxBar>
    with TickerProviderStateMixin {
  late AnimationController controller;
  late StaggeredParallaxAnimation animation;

  @override
  void initState() {
    super.initState();
    initAnimations();
  }

  void initAnimations() {
    controller = AnimationController(
        duration: const Duration(milliseconds: 2100), vsync: this);
    animation = StaggeredParallaxAnimation(controller: controller);
    animation.controller.animateTo(1.0, duration: Duration(milliseconds: 1200));
    controller.addStatusListener((status) {
      switch (status) {
        case AnimationStatus.dismissed:
          controller.forward();
          break;
        case AnimationStatus.forward:
          break;
        case AnimationStatus.reverse:
          break;
        case AnimationStatus.completed:
          break;
      }
    });
    controller.forward();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void animateToBegin() {
    controller.animateBack(0,
        duration: Duration(milliseconds: 320), curve: Curves.easeOut);
    setState(() {});
  }

  bool isPageControllerActive() {
    // if (widget.pageController != null) {
    if (widget.pageController.hasClients) {
      return true;
    }
    // }
    return false;
  }

  @override
  Widget build(BuildContext context) =>
      BlocListener<OLoadingBloc, OLoadingState>(
        listener: (context, state) {
          animateToBegin();
        },
        child: !isPageControllerActive()
            ? Container()
            : ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(2.0)),
                child: Stack(
                  alignment: AlignmentDirectional.center,
                  children: [
                    AnimatedBuilder(
                      animation: animation.controller,
                      builder: (context, child) => Positioned(
                        child: Opacity(
                          opacity: animation.totalOpacity.value,
                          child: Container(
                            height: 4,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(2.0),
                              color: CorporateColors.tinyCampusIconGrey
                                  .withOpacity(0.14),
                            ),
                          ),
                        ),
                      ),
                    ),
                    AnimatedBuilder(
                      animation: animation.dotOpacity,
                      builder: (context, child) => Opacity(
                        opacity: animation.dotOpacity.value,
                        child: Container(
                          height: 4,
                          width: 4,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(2.0),
                            color: CurrentTheme().tcBlueFont.withOpacity(0.2),
                          ),
                        ),
                      ),
                    ),
                    AnimatedBuilder(
                      animation: animation.indicatorSize,
                      builder: (context, child) => SizedBox(
                        width: 32 + animation.indicatorSize.value,
                        height: 4,
                        child: AnimatedBuilder(
                          animation: widget.pageController,
                          builder: (context, child) => Transform.translate(
                            offset: Offset(
                                ((widget.pageController.page ?? 0.0) -
                                        (widget.pageController.initialPage)) *
                                    20.0,
                                0.0),
                            child: AnimatedBuilder(
                              animation: animation.indicatorOpacity,
                              builder: (context, child) => Opacity(
                                opacity: animation.indicatorOpacity.value,
                                child: Container(
                                  height: 4,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(2.0),
                                    color: Theme.of(context).backgroundColor,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    AnimatedBuilder(
                      animation: animation.indicatorSize,
                      builder: (context, child) => SizedBox(
                        width: 24 + animation.indicatorSize.value,
                        height: 4,
                        child: AnimatedBuilder(
                          animation: widget.pageController,
                          builder: (context, child) => Transform.translate(
                            offset: Offset(
                                ((widget.pageController.page ?? 0.0) -
                                        (widget.pageController.initialPage)) *
                                    20.0,
                                0.0),
                            child: AnimatedBuilder(
                              animation: animation.indicatorOpacity,
                              builder: (context, child) => Opacity(
                                opacity: animation.indicatorOpacity.value,
                                child: Container(
                                  height: 4,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(2.0),
                                    color: CorporateColors.tinyCampusBlue,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      );
}

class StaggeredParallaxAnimation {
  StaggeredParallaxAnimation({required this.controller})
      : indicatorOpacity = TweenSequence(
          <TweenSequenceItem<double>>[
            TweenSequenceItem<double>(
              tween: ConstantTween<double>(1.0),
              weight: 80.0,
            ),
            TweenSequenceItem<double>(
              tween: Tween(begin: 1.0, end: 0.2),
              weight: 20.0,
            ),
          ],
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.easeOut),
          ),
        ),
        indicatorSize = TweenSequence(
          <TweenSequenceItem<double>>[
            TweenSequenceItem<double>(
              tween: ConstantTween<double>(4.0),
              weight: 80.0,
            ),
            TweenSequenceItem<double>(
              tween: Tween(begin: 4.0, end: 0.0),
              weight: 20.0,
            ),
          ],
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.easeOut),
          ),
        ),
        totalOpacity = TweenSequence(
          <TweenSequenceItem<double>>[
            TweenSequenceItem<double>(
              tween: ConstantTween<double>(1.0),
              weight: 70.0,
            ),
            TweenSequenceItem<double>(
              tween: Tween(begin: 1.0, end: 0.2),
              weight: 30.0,
            ),
          ],
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.easeOut),
          ),
        ),
        dotOpacity = TweenSequence(
          <TweenSequenceItem<double>>[
            TweenSequenceItem<double>(
              tween: ConstantTween<double>(1.0),
              weight: 70.0,
            ),
            TweenSequenceItem<double>(
              tween: Tween(begin: 1.0, end: 0.2),
              weight: 10.0,
            ),
            TweenSequenceItem<double>(
              tween: ConstantTween<double>(0.0),
              weight: 20.0,
            ),
          ],
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.easeOut),
          ),
        );

  final AnimationController controller;
  final Animation<double> indicatorOpacity;
  final Animation<double> dotOpacity;
  final Animation<double> indicatorSize;
  final Animation<double> totalOpacity;
}
