/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../../blocs/o_drawer_bloc/o_drawer_bloc.dart';
import '../../../model/o_element_wrapper.dart';
import '../o_utility_functions.dart';
import 'i18n_common.dart';
import 'o_drawer_menu.dart';

class ODrawerMInfo extends StatelessWidget {
  const ODrawerMInfo({
    Key? key,
    required this.oElementWrapper,
  }) : super(key: key);
  final OElementWrapper oElementWrapper;

  @override
  Widget build(BuildContext context) => Container(
        child: buildInfoBlock(context),
      );

  Widget buildInfoBlock(BuildContext context) => Column(
        children: <Widget>[
          SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: <Widget>[
                ODrawerHeader(oElementWrapper: oElementWrapper),
                ODrawerButtons(
                    oElementWrapper: oElementWrapper, color: Colors.black),
              ],
            ),
          ),
          Row(
            children: <Widget>[
              Expanded(flex: 1, child: Container()),
              Expanded(
                flex: 1,
                child: FlatButton(
                  child: I18nText(
                    '$i18nDrawerKey.back_caps',
                    child: Text(
                      "ZURÜCK",
                      style: TextStyle(fontWeight: FontWeight.w400),
                    ),
                  ),
                  onPressed: () {
                    OUtil.addODrawerEvent(
                        OSelectEvent(oElementWrapper: oElementWrapper));
                  },
                ),
              ),
            ],
          ),
        ],
      );

  bool checkIfSelectedColor(Color a, Color b) => a.value == b.value;
}
