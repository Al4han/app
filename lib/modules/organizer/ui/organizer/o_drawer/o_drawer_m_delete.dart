/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../common/tc_theme.dart';
import '../../../../../common/widgets/tc_button.dart' show TCButton;
import '../../../blocs/o_animation_bloc/o_animation_bloc.dart';
import '../../../blocs/o_drawer_bloc/o_drawer_bloc.dart';
import '../../../blocs/o_lesson_selection_bloc/o_lesson_selection_bloc.dart';
import '../../../blocs/o_message_bloc/o_message_bloc.dart';
import '../../../blocs/o_settings_bloc/o_settings_bloc.dart';
import '../../../model/o_element.dart';
import '../../../model/o_element_wrapper.dart';
import '../../../model/o_program.dart';
import '../../../model/o_subject.dart';
import '../o_utility_functions.dart';
import '../organizer_constants.dart';
import 'i18n_common.dart';

class ODrawerMDelete extends StatefulWidget {
  const ODrawerMDelete({
    Key? key,
    required this.oElementWrapper,
  }) : super(key: key);
  final OElementWrapper oElementWrapper;

  @override
  _ODrawerMDeleteState createState() => _ODrawerMDeleteState();
}

class _ODrawerMDeleteState extends State<ODrawerMDelete> {
  int deleteOption = 0;
  List<DeleteAction> deleteActions = <DeleteAction>[];
  bool hasDifferentModules = false;
  bool hasDifferentPrograms = false;
  String? currentlySelectedSubject;
  String? currentlySelectedProgram;
  String? currentlySelectedComment;
  String? currentlySelectedTypeLong;

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() {
    // create first DeleteAction
    deleteActions = <DeleteAction>[];
    deleteActions.clear();
    deleteActions.add(
      DeleteAction(
        lessonId: widget.oElementWrapper.ele.lessonId ?? -1,
        // TODO: NULL-SAFETY CRITICAL
        referredProgram: null,
        referredSubject: null,
        type: DeleteType.element,
      ),
    );
    deleteActions.add(
      DeleteAction(
        lessonId: widget.oElementWrapper.ele.lessonId ?? -1,
        // TODO: NULL-SAFETY CRITICAL
        referredProgram: null,
        referredSubject: null,
        type: DeleteType.lesson,
      ),
    );
    // create subject DeleteActions, if possible
    var programs = OUtil.getAllSelectedPrograms();
    var alreadyAddedSubjectsLessons = <String>[];
    if (programs != null) {
      for (var i = 0; i < programs.length; i++) {
        var subjects = OUtil.getSubjectsFromProgramInferredByLesson(
            programs[i], widget.oElementWrapper.ele.lessonId ?? -1);
        if (subjects != null) {
          for (var j = 0; j < subjects.length; j++) {
            if (!alreadyAddedSubjectsLessons
                .contains(subjects[j].lessons.toString())) {
              deleteActions.add(DeleteAction(
                lessonId: widget.oElementWrapper.ele.lessonId ?? -1,
                referredProgram: programs[i],
                referredSubject: subjects[j],
                type: DeleteType.subject,
              ));
              alreadyAddedSubjectsLessons.add(subjects[j].lessons.toString());
            } else {
              hasDifferentModules = true;
            }
          }
        }
      }
    }
    indexDeleteActionsAndRequestAffectedLessons();
  }

  void indexDeleteActionsAndRequestAffectedLessons() {
    for (var i = 0; i < deleteActions.length; i++) {
      deleteActions[i].index = i;

      /// [fillDeleteActionsWithAffectedLessons] is unstable right now at the
      /// endpoint, use again when fully ready and stable
      // fillDeleteActionsWithAffectedLessons(deleteActions[i]);
    }
  }

  void requestAffectedLessonsCached() {}

  void reinitialize() {
    setState(init);
  }

  Future<void> fillDeleteActionsWithAffectedLessons(
    DeleteAction deleteAction,
  ) async {
    try {
      switch (deleteAction.type) {
        case DeleteType.lesson:
          deleteAction.affectedLessons = await OUtil.getAffectingLessons(
              oLessonId: [deleteAction.lessonId]);
          break;
        case DeleteType.subject:
          deleteAction.affectedLessons = await OUtil.getAffectingLessons(
              oLessonId: deleteAction.referredSubject?.lessons
                      .map((e) => e.id ?? -1)
                      .toList() ??
                  <int>[]);
          break;
        case DeleteType.element:
          // this is different, there are no affectedLessons here, because it's
          // a single date. Will be handled in the execute method
          break;
      }
    } on Exception catch (e) {
      debugPrint("[DELETE OPTIONS AFFECTED LESSONS LOADING] Error caught: $e");
    }
    if (mounted) {
      setState(() {});
    }
  }

  bool checkIfAnyDescriptionIsNotNull() {
    if (currentlySelectedSubject != null ||
        currentlySelectedProgram != null ||
        currentlySelectedComment != null ||
        currentlySelectedTypeLong != null) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) => BlocListener<ODrawerBloc, ODrawerState>(
        // depending on selected action, CHANGE HEADER TITLE!
        listener: (context, state) {
          reinitialize();
        },
        child: Container(
          margin: EdgeInsets.only(top: 16.0),
          child: Column(
            children: [
              if (checkIfAnyDescriptionIsNotNull())
                Container(
                    margin: EdgeInsets.only(bottom: 6.0),
                    child: Column(
                      children: [
                        if (currentlySelectedComment != null)
                          Container(
                            margin: EdgeInsets.all(0.0),
                            // elevation: 0.0,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8.0),
                              color: Theme.of(context).primaryColor,
                            ),
                            child: Text(currentlySelectedComment ?? "",
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: -0.8,
                                    color: CorporateColors.tinyCampusOrange)),
                          ),
                        if (currentlySelectedSubject != null)
                          Text(
                            currentlySelectedSubject ?? "",
                            maxLines: 2,
                            style: Theme.of(context)
                                .textTheme
                                .headline4
                                ?.copyWith(
                                    color: CorporateColors.tinyCampusIconGrey),
                            textAlign: TextAlign.center,
                          ),
                        if (currentlySelectedProgram != null)
                          Text(
                            currentlySelectedProgram ?? "",
                            maxLines: 2,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                ?.copyWith(
                                    color: CorporateColors.tinyCampusIconGrey
                                        .withOpacity(0.5)),
                            textAlign: TextAlign.center,
                          ),
                        if (currentlySelectedTypeLong != null)
                          Text(
                            currentlySelectedTypeLong ?? "",
                            style: Theme.of(context)
                                .textTheme
                                .headline4
                                ?.copyWith(
                                    color: CorporateColors.tinyCampusIconGrey),
                            textAlign: TextAlign.center,
                          ),
                      ],
                    )),
              SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                child: Padding(
                  padding: const EdgeInsets.only(top: 6.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      ...deleteActions.map((e) => _buildDeleteOption(
                          // e.referredProgram,
                          // e.referredSubject,
                          e.lessonId,
                          e.type,
                          e))
                    ],
                  ),
                ),
              ),
              Container(height: 16),
              _buildButtons(),
            ],
          ),
        ),
      );

  String determineDeleteLabel(DeleteAction action, OElement ele) {
    switch (action.type) {
      case DeleteType.element:
        return FlutterI18n.translate(context, '$i18nODKey.date');
      case DeleteType.lesson:
        if (ele.type == "?") {
          return FlutterI18n.translate(
              context, 'modules.organizer.data.unknown');
        }
        return ele.typeLong;
      case DeleteType.subject:
        return action.referredSubject?.name ?? "";
    }
  }

  String determineDeleteLabelShort(DeleteAction action, OElement ele) {
    switch (action.type) {
      case DeleteType.element:
        return FlutterI18n.translate(
            context, '$i18nODKey.delete_options_short.single_date');
      case DeleteType.lesson:
        if (ele.type == "?") {
          return FlutterI18n.translate(
              context, '$i18nODKey.delete_options_short.lesson_type');
        }
        return ele.typeLong;
      case DeleteType.subject:
        return FlutterI18n.translate(
            context, '$i18nODKey.delete_options_short.entire_subject');
    }
  }

  String determineDeleteDescription(DeleteAction action, OElement ele) {
    switch (action.type) {
      case DeleteType.element:
        return FlutterI18n.translate(
            context, '$i18nODKey.delete_options.single_date');
      case DeleteType.lesson:
        // return ele.type;
        return FlutterI18n.translate(
            context, '$i18nODKey.delete_options.lesson_type');
      case DeleteType.subject:
        return FlutterI18n.translate(
            context, '$i18nODKey.delete_options.entire_subject');
    }
  }

  Widget _buildDeleteOption(
    // OProgram program,
    // OSubject subject,
    int lessonId,
    DeleteType type,
    DeleteAction deleteAction,
  ) =>
      InkWell(
        onTap: () => changeValue(deleteAction),
        child: SizedBox(
          width: deleteActions.length > 3 ? 90 : 120,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Stack(
                clipBehavior: Clip.none,
                children: [
                  Text(
                    determineDeleteLabelShort(
                      deleteAction,
                      widget.oElementWrapper.ele,
                    ),
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        ?.copyWith(color: CorporateColors.cafeteriaCautionRed),
                  ),
                  if (deleteActions.length > 3 && type == DeleteType.subject)
                    Positioned(
                      right: -14,
                      top: -4,
                      // bottom: 0,
                      width: 14,
                      height: 14,
                      child: Container(
                        decoration: BoxDecoration(
                          color: CorporateColors.tinyCampusIconGrey,
                          shape: BoxShape.circle,
                        ),
                        child: Center(
                          child: Text(
                            ((deleteAction.index ?? 0) -
                                    (deleteActions.length - 3))
                                .toString(),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 10,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                ],
              ),
              // if (deleteAction.type == DeleteType.subject)
              //   Text(
              //     deleteAction.referredSubject.name,
              //     textAlign: TextAlign.center,
              //     maxLines: 2,
              //     overflow: TextOverflow.ellipsis,
              //     style: Theme.of(context)
              //         .textTheme
              //         .bodyText2
              //         .copyWith(color: CorporateColors.tinyCampusBlue),
              //   ),
              // if (deleteAction.type == DeleteType.subject)
              //   Text(
              //     deleteAction.referredProgram.name,
              //     maxLines: 1,
              //     textAlign: TextAlign.center,
              //     overflow: TextOverflow.ellipsis,
              //     style: Theme.of(context)
              //         .textTheme
              //         .bodyText2
              //         .copyWith(color: CorporateColors.tinyCampusIconGrey),
              //   ),
              // _buildAffectedLessons(deleteAction),
              Radio(
                groupValue: deleteOption,
                value: deleteAction.index ?? 0, // TODO NNBD MIGRATION
                onChanged: (value) => changeValue(deleteAction),
              ),
            ],
          ),
        ),
      );

  /// [_buildAffectedLessons] is unstable right now at the endpoint, use again
  /// when fully ready and stable
  // Text _buildAffectedLessons(DeleteAction deleteAction) => Text(
  //       deleteAction.type == DeleteType.element
  //           ? FlutterI18n.plural(
  //              context,
  //              '$i18nODKey.oelement_count.times',
  //              1,
  //              )
  //           : deleteAction.affectedLessons.isEmpty
  //               ? FlutterI18n.translate(context, 'common.messages.loading')
  //               : FlutterI18n.plural(
  //                  context,
  //                  '$i18nODKey.oelement_count.times',
  //                   deleteAction.affectedLessons.length,
  //                   ),
  //       maxLines: 1,
  //       overflow: TextOverflow.ellipsis,
  //       style: Theme.of(context)
  //           .textTheme
  //           .bodyText2
  //           ?.copyWith(color: CorporateColors.tinyCampusIconGrey),
  //     );

  void nullDescriptionStrings() {
    currentlySelectedSubject = null;
    currentlySelectedProgram = null;
    currentlySelectedComment = null;
    currentlySelectedTypeLong = null;
  }

  void changeValue(DeleteAction deleteAction) {
    setState(() {
      deleteOption = deleteAction.index ?? 0; // TODO NNBD MIGRATION
      nullDescriptionStrings();
      if (deleteAction.type == DeleteType.subject) {
        currentlySelectedSubject = deleteAction.referredSubject?.name;
        currentlySelectedProgram = deleteAction.referredProgram?.name;
      }
      if (deleteAction.type == DeleteType.lesson) {
        currentlySelectedComment = widget.oElementWrapper.ele.comment;
        currentlySelectedProgram = widget.oElementWrapper.ele.name;
      }
      currentlySelectedSubject =
          (currentlySelectedSubject != "" ? currentlySelectedSubject : null);
      currentlySelectedProgram =
          (currentlySelectedProgram != "" ? currentlySelectedProgram : null);
      currentlySelectedComment =
          (currentlySelectedComment != "" ? currentlySelectedComment : null);
      currentlySelectedTypeLong =
          (currentlySelectedTypeLong != "" ? currentlySelectedTypeLong : null);
    });

    OUtil.addOAnimationEvent(
        OSetAnimationStateEvent(OStopAllShakingAnimationState()));
    if (deleteAction.type == DeleteType.element) {
      OUtil.addOAnimationEvent(OSetAnimationStateEvent(
          OStartSingleShakingAnimationState(
              oElementId: widget.oElementWrapper.ele.id ?? -1)));
      return;
    }
    var lessons = (deleteAction.type == DeleteType.lesson
        ? [deleteAction.lessonId]
        : deleteAction.referredSubject?.lessons
                .map((e) => e.id ?? -1)
                .toList() ??
            <int>[]);

    OUtil.addOAnimationEvent(
        OSetAnimationStateEvent(OStartShakingAnimationState(lessons: lessons)));
  }

  Widget _buildButtons() => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        child: Row(
          children: <Widget>[
            Expanded(
              child: TCButton(
                buttonLabel:
                    FlutterI18n.translate(context, 'common.actions.back'),
                onPressedCallback: () {
                  OUtil.addODrawerEvent(
                      OSelectEvent(oElementWrapper: widget.oElementWrapper));
                  final oAnimationBloc =
                      BlocProvider.of<OAnimationBloc>(context);
                  oAnimationBloc.add(
                      OSetAnimationStateEvent(OStopAllShakingAnimationState()));
                },
                borderColor: CorporateColors.tinyCampusBlue,
                backgroundColor: Colors.white,
                borderThickness: 2,
                elevation: 0,
                foregroundColor: CorporateColors.tinyCampusBlue,
              ),
            ),
            Container(width: 16),
            Expanded(
              child: TCButton(
                buttonLabel:
                    FlutterI18n.translate(context, '$i18nDrawerKey.remove'),
                onPressedCallback: executeOption,
                backgroundColor: CorporateColors.cafeteriaCautionRed,
              ),
            ),
          ],
        ),
      );

  void executeOption() {
    if (deleteActions[deleteOption].type == DeleteType.element) {
      // remove single entry
      OUtil.addOSettingsEvent(DeleteSingleOLesson(widget.oElementWrapper.ele));
      OUtil.addOAnimationEvent(OSetAnimationStateEvent(
          ODeleteSingleAnimationState(
              oElementId: widget.oElementWrapper.ele.id ?? -1)));
      OUtil.addOMessageEvent(OResolvedMessage());
      OUtil.addOMessageEvent(
          OElementChanged(widget.oElementWrapper.ele.id ?? -1));
    } else {
      var lessonConfigBloc = BlocProvider.of<OLessonSelectionBloc>(context);
      var state = lessonConfigBloc.state;
      var previousLessons = state.lessons;

      // remove lessons
      var selectedAction = deleteActions[deleteOption];
      var toBeDeletedLessons = [selectedAction.lessonId];
      if (selectedAction.type == DeleteType.subject) {
        toBeDeletedLessons.addAll(selectedAction.referredSubject?.lessons
                .map((e) => e.id ?? -1)
                .toList() ??
            <int>[]);
      }
      toBeDeletedLessons = toBeDeletedLessons.toSet().toList();
      lessonConfigBloc.add(
        RemoveMultipleOLessonsEvent(
          lessons: toBeDeletedLessons,
          previousLessonList: previousLessons,
        ),
      );
      OUtil.addOMessageEvent(OResolvedMessage());
      OUtil.addOMessageEvent(OLessonsChanged(toBeDeletedLessons));
      OUtil.addOAnimationEvent(OSetAnimationStateEvent(
          ODeleteAnimationState(lessons: toBeDeletedLessons)));
    }

    OUtil.addODrawerEvent(
        ODeselectEvent(oElementWrapper: widget.oElementWrapper));
  }
}

class DeleteAction {
  DeleteAction({
    required this.type,
    required this.lessonId,
    this.referredSubject,
    this.referredProgram,
    this.index,
  });

  final DeleteType type;
  final int lessonId;
  final OSubject? referredSubject;
  final OProgram? referredProgram;
  List<OElement> affectedLessons = <OElement>[];
  int? index;
}

enum DeleteType { element, lesson, subject }
