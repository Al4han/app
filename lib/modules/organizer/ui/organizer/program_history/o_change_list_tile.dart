/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

// TODO: Rework this pls! This dynamic usage is a nightmare for static analysis!
// ignore_for_file: avoid_dynamic_calls

import 'package:flutter/material.dart';

import '../../../../../common/tc_theme.dart';
import '../../../blocs/o_program_history_bloc/o_program_history_bloc.dart';
import '../../../model/o_change_record.dart';
import '../../../model/o_lesson.dart';
import '../../../model/o_pool.dart';
import '../../../model/o_program.dart';
import '../../../model/o_subject.dart';
import '../o_utility_functions.dart';

class OChangeListTile extends StatelessWidget {
  const OChangeListTile(
    this.entry,
    this.type, {
    Key? key,
  }) : super(key: key);
  final MapEntry<String, OChangeRecord> entry;
  final ODelta type;

  @override
  Widget build(BuildContext context) => Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        elevation: 0.0,
        margin: EdgeInsets.symmetric(horizontal: 12.0, vertical: 4.0),
        child: buildContent(
            entry.value,
            context,
            () => OUtil.addOHistoryBlocEvent(UserCheckedChangeEvent(
                  entry.key,
                ))),
      );

  Widget buildContent(
      OChangeRecord record, BuildContext context, VoidCallback? callback) {
    try {
      // return CircularProgressIndicator();
      final oldRef = determineType(record.oldRef, record.type);
      final newRef = determineType(record.newRef, record.type);
      final ts = Theme.of(context).textTheme.bodyText1;
      switch (type) {
        case ODelta.added:
          // TODO: Handle this case.

          return ListTile(
            onTap: callback,
            leading: (record.type == "OLesson"
                ? CircleAvatar(
                    child: Text(newRef?.method?.toString() ?? ""),
                    backgroundColor: Colors.transparent)
                : null),
            trailing: displayHierarchy(record.type),
            title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(newRef?.name, style: ts),
                if (record.type == "OLesson")
                  if (newRef?.comment != "") Text(newRef?.comment, style: ts),

                // Text(record.type),
              ],
            ),
          );
        case ODelta.nameChanged:
          final removedStyle = ts?.copyWith(
              color: CorporateColors.tinyCampusIconGrey,
              decoration: TextDecoration.lineThrough);
          return ListTile(
            onTap: callback,
            contentPadding: EdgeInsets.zero,
            title: Row(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      ListTile(
                        leading: (record.type == "OLesson"
                            ? CircleAvatar(
                                child: Text(newRef?.method?.toString() ?? "",
                                    style: ts),
                                backgroundColor: Colors.transparent)
                            : null),
                        title: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              newRef?.name,
                              // newRef.name,
                              style: ts,
                            ),
                            if (record.type == "OLesson")
                              if (newRef?.comment != "")
                                Text(newRef?.comment,
                                    style: ts?.copyWith(
                                        color:
                                            CorporateColors.tinyCampusOrange)),
                          ],
                        ),
                      ),
                      ListTile(
                        leading: (record.type == "OLesson"
                            ? CircleAvatar(
                                child: Text(oldRef?.method.toString() ?? "",
                                    style: removedStyle),
                                backgroundColor: Colors.transparent)
                            : null),
                        title: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              oldRef?.name,
                              // newRef.name,
                              style: removedStyle,
                            ),
                            if (record.type == "OLesson")
                              if (newRef?.comment != "")
                                Text(newRef?.comment,
                                    style: removedStyle?.copyWith(
                                        color: CorporateColors
                                            .tinyCampusIconGrey
                                            .withOpacity(0.6))),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: displayHierarchy(record.type),
                ),
              ],
            ),
          );
        case ODelta.amountSubtreeIncreased:
          return ListTile(
            onTap: callback,
            leading: Icon(Icons.add_box),
            trailing: displayHierarchy(record.type),
            title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(newRef?.name, style: ts),
                if (record.type == "OLesson")
                  if (newRef?.comment != "") Text(newRef?.comment, style: ts),

                // Text(record.type),
              ],
            ),
          );
        case ODelta.amountSubtreeDecreased:
          return ListTile(
            onTap: callback,
            leading: Icon(Icons.indeterminate_check_box),
            trailing: displayHierarchy(record.type),
            title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(newRef?.name, style: ts),
                if (record.type == "OLesson")
                  if (newRef?.comment != "") Text(newRef?.comment, style: ts),
              ],
            ),
          );
        case ODelta.removed:
          final removedStyle = ts?.copyWith(
              color: CorporateColors.tinyCampusIconGrey,
              decoration: TextDecoration.lineThrough);
          return ListTile(
            onTap: callback,
            leading: (record.type == "OLesson"
                ? CircleAvatar(
                    child: Text(oldRef?.method?.toString() ?? "",
                        style: removedStyle),
                    backgroundColor: Colors.transparent)
                : null),
            trailing: displayHierarchy(record.type),
            title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(),
                Text(
                  oldRef?.name,
                  style: removedStyle,
                ),
                if (record.type == "OLesson")
                  if (oldRef?.comment != "")
                    Text(oldRef?.comment, style: removedStyle),
              ],
            ),
          );
        case ODelta.descriptionChanged:
          final removedStyle = ts?.copyWith(
            color: CorporateColors.tinyCampusIconGrey,
          );
          return ListTile(
            onTap: callback,
            contentPadding: EdgeInsets.zero,
            title: Row(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      ListTile(
                        leading: (record.type == "OLesson"
                            ? CircleAvatar(
                                child: Text(newRef?.method?.toString() ?? "",
                                    style: ts),
                                backgroundColor: Colors.transparent)
                            : null),
                        title: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              newRef?.name,
                              style: ts,
                            ),
                            if (record.type == "OLesson")
                              if (newRef?.comment != "")
                                Text(newRef?.comment, style: ts),
                          ],
                        ),
                      ),
                      ListTile(
                        leading: (record.type == "OLesson"
                            ? CircleAvatar(
                                child: Text(oldRef?.method?.toString() ?? "",
                                    style: removedStyle),
                                backgroundColor: Colors.transparent)
                            : null),
                        title: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              oldRef?.name,
                              style: removedStyle,
                            ),
                            if (record.type == "OLesson")
                              if (oldRef?.comment != "")
                                Text(oldRef?.comment, style: removedStyle),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: displayHierarchy(record.type),
                ),
              ],
            ),
          );
      }
    } on Exception catch (e) {
      debugPrint(e.toString());
      return CircularProgressIndicator();
    }
  }

  Widget displayHierarchy(String type) {
    switch (type) {
      case "OProgram":
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            buildSingleHierarchy(activated: true),
            buildSingleHierarchy(activated: false),
            buildSingleHierarchy(activated: false),
          ],
        );
      case "OPool":
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            buildSingleHierarchy(activated: true),
            buildSingleHierarchy(activated: false),
            buildSingleHierarchy(activated: false),
          ],
        );
      case "OSubject":
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            buildSingleHierarchy(activated: false),
            buildSingleHierarchy(activated: true),
            buildSingleHierarchy(activated: false),
          ],
        );
      case "OLesson":
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            buildSingleHierarchy(activated: false),
            buildSingleHierarchy(activated: false),
            buildSingleHierarchy(activated: true),
          ],
        );
    }
    return Container();
  }

  Widget buildSingleHierarchy({
    required bool activated,
    bool semiActivated = false,
  }) =>
      Container(
        width: 22,
        height: 4,
        margin: EdgeInsets.symmetric(vertical: 2),
        decoration: BoxDecoration(
          color: activated
              ? CorporateColors.cafeteriaCautionRed
              : CorporateColors.tinyCampusIconGrey.withOpacity(0.4),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(16.0),
            topRight: Radius.circular(16.0),
          ),
        ),
      );

  dynamic determineType(Map<String, dynamic>? instance, String type) {
    if (instance == null) return null;
    switch (type) {
      case "OProgram":
        return OProgram.fromJson(instance);
      case "OPool":
        return OPool.fromJson(instance);
      case "OSubject":
        return OSubject.fromJson(instance);
      case "OLesson":
        return OLesson.fromJson(instance);
    }
    return null;
  }
}
