/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../../../common/tc_theme.dart';
import '../../../../common/tinycampus_icons.dart';
import '../../blocs/o_animation_bloc/o_animation_bloc.dart';
import '../../blocs/o_calendar_bloc/o_calendar_bloc.dart';
import '../../blocs/o_loading_bloc/o_loading_bloc.dart';
import '../../blocs/o_message_bloc/o_message_bloc.dart';
import '../../blocs/o_settings_bloc/o_settings_bloc.dart';
import '../../blocs/o_view_mode_bloc/o_view_mode_bloc.dart';
import '../../model/o_element.dart';
import '../../model/o_element_wrapper.dart';
import '../../model/o_week.dart';
import 'o_calendar_entry.dart';
import 'o_utility_functions.dart';
import 'organizer_constants.dart';
import 'organizer_day_widget.dart';
import 'organizer_enums.dart';
import 'organizer_reload_widget.dart';

class OrganizerWeekWidget extends StatelessWidget {
  const OrganizerWeekWidget({
    Key? key,
    required this.position,
    // required this.animation,
  }) : super(key: key);

  final int position;
  // final double animation;

  @override
  Widget build(BuildContext context) => _buildLayout(context);

  Widget _buildDataReloadDelayed(BuildContext context) => FutureBuilder(
        future: Future.delayed(Duration(milliseconds: 1600)),
        builder: (context, snapshot) => Stack(children: <Widget>[
          AnimatedSwitcher(
            duration: Duration(milliseconds: 420),
            child: (snapshot.connectionState == ConnectionState.done)
                ? OrganizerReloadWidget()
                : Column(
                    children: [
                      I18nText(
                        'common.messages.loading',
                        child: Text(
                          "",
                          style: TextStyle(
                              color: CorporateColors.tinyCampusIconGrey
                                  .withOpacity(0.5)),
                        ),
                      ),
                    ],
                  ),
          )
        ]),
      );

  Widget _buildNoDataFoundWidget(BuildContext context, OWeek oWeek) => Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            I18nText(
              '$i18nOKey.error_messages.no_data_found',
              child: Text('', style: Theme.of(context).textTheme.headline2),
            ),
            Container(
              height: 32,
            ),
            Row(
              children: [
                Expanded(
                  child: Icon(
                    TinyCampusIcons.tcOutline,
                    size: 96.0,
                    color: CorporateColors.tinyCampusIconGrey.withOpacity(0.5),
                  ),
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.only(top: 32.0, bottom: 24.0),
              child: I18nText(
                '$i18nOWKey.data_from_here',
                child: Text(
                  "aber hier sind noch Daten vorhanden:",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 14, color: CorporateColors.tinyCampusIconGrey),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  oWeek.past != null
                      ? RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          elevation: 0.0,
                          color: Theme.of(context).primaryColor,
                          padding: EdgeInsets.all(8.0),
                          child: Column(
                            children: <Widget>[
                              Container(
                                  margin: EdgeInsets.symmetric(vertical: 20.0),
                                  child: Icon(
                                    Icons.chevron_left,
                                    color: CurrentTheme().tcBlueFont,
                                    size: 40.0,
                                  )),
                              I18nText(
                                '$i18nOWKey.past',
                                child: Text(
                                  "Vergangenheit:",
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: CorporateColors.tinyCampusIconGrey,
                                  ),
                                ),
                              ),
                              Text(
                                DateFormat('dd.MM.yyyy')
                                    .format(oWeek.past ?? DateTime.now()),
                                style: TextStyle(
                                    fontSize: 16,
                                    color: CorporateColors.tinyCampusIconGrey),
                              ),
                            ],
                          ),
                          onPressed: () {
                            OUtil.setOAnimationState(OSetPageState(
                                OUtil.absoluteWeekSince1970(
                                    oWeek.past ?? DateTime.now())));
                          },
                        )
                      : Container(),
                  oWeek.future != null
                      ? RaisedButton(
                          elevation: 0.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          color: Theme.of(context).primaryColor,
                          padding: EdgeInsets.all(8.0),
                          child: Column(
                            children: <Widget>[
                              Container(
                                  margin: EdgeInsets.symmetric(vertical: 20.0),
                                  child: Icon(
                                    Icons.chevron_right,
                                    color: CurrentTheme().tcBlueFont,
                                    size: 40.0,
                                  )),
                              I18nText(
                                '$i18nOWKey.future',
                                child: Text(
                                  "Zukunft:",
                                  style: TextStyle(
                                      fontSize: 12,
                                      color:
                                          CorporateColors.tinyCampusIconGrey),
                                ),
                              ),
                              Text(
                                DateFormat('dd.MM.yyyy')
                                    .format(oWeek.future ?? DateTime.now()),
                                style: TextStyle(
                                    fontSize: 16,
                                    color: CorporateColors.tinyCampusIconGrey),
                              ),
                            ],
                          ),
                          onPressed: () {
                            OUtil.setOAnimationState(OSetPageState(
                                OUtil.absoluteWeekSince1970(
                                    (oWeek.future?.add(Duration(days: 1)) ??
                                        DateTime.now()))));
                          },
                        )
                      : Container(),
                ],
              ),
            ),
          ],
        ),
      );

  Widget _buildLayout(BuildContext context) =>
      BlocBuilder<OCalendarBloc, OCalendarState>(
        builder: (context, state) {
          if (state is OCalendarReadyState) {
            final oWeek = state.weekMap[position];
            if (oWeek == null) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _buildDataReloadDelayed(context),
                  ],
                ),
              );
            } else {
              var containsData = oWeek.containsData;
              if (oWeek.containsData == null) {
                if (oWeek.containingWeekdays != null) {
                  switch (oWeek.containingWeekdays) {
                    case ContainingWeekdays.none:
                      containsData = false;
                      break;
                    case ContainingWeekdays.error:
                      containsData = false;
                      break;
                    case ContainingWeekdays.noneWithFuture:
                      containsData = false;
                      break;
                    case ContainingWeekdays.noneWithPast:
                      containsData = false;
                      break;
                    case ContainingWeekdays.noneWithPastAndFuture:
                      containsData = false;
                      break;
                    case ContainingWeekdays.normal:
                      containsData = true;
                      break;
                    case ContainingWeekdays.normalIncludingSaturday:
                      containsData = true;
                      break;
                    case ContainingWeekdays.normalIncludingWeekend:
                      containsData = true;
                      break;
                    default:
                      containsData = false;
                      break;
                  }
                  debugPrint(oWeek.containingWeekdays.toString());
                }
              }
              if (containsData ?? false) {
                return _buildDataFoundWidget(oWeek);
              } else {
                return _buildNoDataFoundWidget(context, oWeek);
              }
            }
          } else {
            return Text("insert here");
          }
        },
      );

  Column _buildDataFoundWidget(OWeek oWeek) => Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: GestureDetector(
              onTap: OUtil.resetOElementWrapperSelection,
              child: FutureBuilder(
                future: Future.delayed(Duration(milliseconds: 50)),
                builder: (context, snapshot) => Card(
                  margin: EdgeInsets.only(
                      top: 12.0, bottom: 6.0, left: 6.0, right: 6.0),
                  clipBehavior: Clip.antiAlias,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  elevation: 1.0,
                  child: Stack(
                    alignment: Alignment.center,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              BlocBuilder<OViewModeBloc, OViewModeState>(
                                builder: (context, state) => Expanded(
                                  flex: 2,
                                  child: state is ODailyViewState
                                      ? Container()
                                      : InkWell(
                                          onTap: () => OUtil.addOSettingsEvent(
                                            CycleOBlockIntervalEvent(),
                                          ),
                                          child: BlocBuilder<OSettingsBloc,
                                              OSettingsState>(
                                            builder: (context, state) => Text(
                                              state.getCurrentIntervals().abbr,
                                              textAlign: TextAlign.center,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline2
                                                  ?.copyWith(
                                                    color: CorporateColors
                                                        .tinyCampusIconGrey
                                                        .withOpacity(0.15),
                                                  ),
                                            ),
                                          ),
                                        ),
                                ),
                              ),
                              _buildDayHeading(context, oWeek, DateTime.monday),
                              _buildDayHeading(
                                  context, oWeek, DateTime.tuesday),
                              _buildDayHeading(
                                  context, oWeek, DateTime.wednesday),
                              _buildDayHeading(
                                  context, oWeek, DateTime.thursday),
                              _buildDayHeading(context, oWeek, DateTime.friday),
                              _buildDayHeading(
                                  context, oWeek, DateTime.saturday),
                              _buildDayHeading(context, oWeek, DateTime.sunday),
                            ],
                          ),
                          Expanded(
                            child: Stack(
                              clipBehavior: Clip.antiAlias,
                              alignment: Alignment.bottomCenter,
                              children: [
                                FutureBuilder(
                                    future: Future.delayed(Duration(
                                        milliseconds:
                                            forcedDelayInMilliseconds)),
                                    builder: (context, snapshot) =>
                                        Stack(children: <Widget>[
                                          DelayedBodyWidget(
                                            oWeek: oWeek,
                                          )
                                        ])),
                                DataAge(
                                  oWeek: oWeek,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      // Positioned(
                      //     child: AnimatedSwitcher(
                      //         duration: Duration(milliseconds: 360),
                      //         child: (snapshot.connectionState !=
                      //                 ConnectionState.done)
                      //             ? CircularProgressIndicator()
                      //             : Container())),
                      // Positioned.fill(
                      //     child: AnimatedSwitcher(
                      //         duration:
                      // Duration(milliseconds: 9000),
                      //         child: (snapshot.connectionState !=
                      //                 ConnectionState.done)
                      //             ? Container(
                      //                 color: Colors.white,
                      //               )
                      //             : SizedBox.shrink())),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      );

// Widget createTodayLine() => Row(
  //       crossAxisAlignment: CrossAxisAlignment.center,
  //       children: [
  //         Container(height: 4, width: 1, color: Colors.red),
  //         ...List.generate(
  //           90 ~/ 10,
  //           (index) => Expanded(
  //             flex: index % 2 == 0 ? 1 : 2,
  //             child: Container(
  //               color: index % 2 == 0 ? Colors.transparent : Colors.red,
  //               height: 1,
  //             ),
  //           ),
  //         ),
  //         Card(
  //                       child: Container(
  //             child: Text(
  //               DateFormat("hh:mm").format(
  //                 DateTime.now(),
  //               ),
  //               style: TextStyle(
  //                 fontSize: 12.0,
  //               ),
  //             ),
  //           ),
  //         ),
  //         ...List.generate(
  //           90 ~/ 10,
  //           (index) => Expanded(
  //             flex: index % 2 == 0 ? 1 : 2,
  //             child: Container(
  //               color: index % 2 == 0 ? Colors.transparent : Colors.red,
  //               height: 1,
  //             ),
  //           ),
  //         ),
  //         Container(height: 5, width: 1, color: Colors.red),
  //       ],
  //     );

  // Widget createTodayIndicator() => Positioned(
  //       child: Container(
  //           child: Column(
  //         children: <Widget>[
  //           createTodayLine(),
  //         ],
  //       )),
  //       left: 0,
  //       right: 0,
  //       top: calculateHeight(
  //           DateTime.now(),
  //           DateTime.now().add(Duration(
  //             minutes: 5,
  //           )),
  //           maxHeight,
  //           oWeek),
  //     );

  Widget _buildDayHeading(BuildContext context, OWeek oWeek, int weekday) {
    // if (oWeek != null) {
    if (weekday == 6 && !oWeek.containsEntriesOn(DateTime.saturday)) {
      return Container();
    }
    if (weekday == 7 && !oWeek.containsEntriesOn(DateTime.sunday)) {
      return Container();
    }
    // }

    //by default, only show 1-5 / Mo-Fr, weekend exluded
    // if (oWeek == null && weekday >= 6) {
    // return Container();
    // }

    // if (oWeek == null && weekday <= 5) {
    // return Container();
    // }
    var date = OUtil.getDateTimeFromWeekSince1970(
        oWeek.weekSinceEpoch1970 ?? 0, weekday); // TODO NNBD MIGRATION

    final today = DateTime.now();
    final isToday = today.difference(date).inDays == 0 && today.day == date.day;
    return Expanded(
      flex: 3,
      child: GestureDetector(
        onTap: () {
          OUtil.resetOElementWrapperSelection();
          OUtil.switchToDailyViewMode(weekday);
        },
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                DateFormat.E(FlutterI18n.currentLocale(context)?.languageCode)
                    .format(date),
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline3?.copyWith(
                    color: isToday
                        ? CorporateColors.tinyCampusBlue
                        : CorporateColors.tinyCampusIconGrey),
              ),

              // TextStyle(
              //     fontSize: 20,
              //     fontWeight: FontWeight.w400,
              //     letterSpacing: -1.0,
              //     color: isToday
              //         ? Theme.of(context).secondaryHeaderColor
              //         // ? CorporateColors.tinyCampusBlue
              //         : CorporateColors.tinyCampusIconGrey)),
              Text(
                  isToday
                      ? FlutterI18n.translate(context, '$i18nOWKey.today')
                      : DateFormat('dd.MM').format(date),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: isToday ? 14.0 : 12.0,
                      color: isToday
                          ? CurrentTheme().tcBlueFont
                          : CorporateColors.tinyCampusIconGrey,
                      fontWeight: isToday ? FontWeight.w600 : FontWeight.w400,
                      letterSpacing: -0.5)),
            ],
          ),
        ),
      ),
    );
  }
}

class DataAge extends StatefulWidget {
  const DataAge({
    Key? key,
    required this.oWeek,
  }) : super(key: key);

  final OWeek oWeek;

  @override
  _DataAgeState createState() => _DataAgeState();
}

class _DataAgeState extends State<DataAge> with TickerProviderStateMixin {
  late AnimationController controller;
  late Animation<double> animation;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 2600), vsync: this);
    animation = TweenSequence(
      <TweenSequenceItem<double>>[
        TweenSequenceItem<double>(
          tween: Tween(begin: -42.0 - bottomAbsoluteMargin, end: 0.0),
          weight: 15.0,
        ),
        TweenSequenceItem<double>(
          tween: ConstantTween<double>(0.0),
          weight: 82.5,
        ),
        TweenSequenceItem<double>(
          tween: Tween(begin: 0.0, end: -42.0 - bottomAbsoluteMargin),
          weight: 2.5,
        ),
      ],
    ).animate(
      CurvedAnimation(
        parent: controller,
        curve: Interval(0.0, 1.0, curve: Curves.easeOut),
      ),
    );
    controller.forward();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  final bottomAbsoluteMargin = 22.0;

  @override
  Widget build(BuildContext context) {
    final oldDate = widget.oWeek.lastUpdated;
    final newDate = DateTime.now();
    final differenceInMinutes =
        oldDate?.difference(newDate).inMilliseconds.abs() ?? 0;
    // final differenceInSeconds = oldDate.difference(newDate).inSeconds.abs();
    var calculatedOpacity = differenceInMinutes / datedThreshold;
    if (calculatedOpacity > 1) {
      calculatedOpacity = 1;
    }
    final checkIfDated = differenceInMinutes > datedThreshold;
    return AnimatedBuilder(
      animation: animation,
      builder: (context, child) => Positioned(
        bottom: animation.value + bottomAbsoluteMargin,
        child: BlocListener<OLoadingBloc, OLoadingState>(
          listener: (context, state) {
            controller
                .animateBack(0.15, duration: Duration(milliseconds: 320))
                .then((value) => {controller.forward()});
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 2.0, horizontal: 12.0),
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                border: Border.all(
                  color: checkIfDated
                      ? CorporateColors.tinyCampusOrange
                      : CorporateColors.tinyCampusIconGrey,
                ),
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: AnimatedSize(
              duration: Duration(milliseconds: 360),
              curve: Curves.easeOut,
              child: _buildDataAgeRow(context, widget.oWeek, checkIfDated),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildDataAgeRow(
          BuildContext context, OWeek oWeek, bool checkIfDated) =>
      Container(
        // width: 190,
        padding: EdgeInsets.symmetric(vertical: 4.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(checkIfDated ? Icons.hourglass_full : Icons.hourglass_empty,
                size: 12,
                color: checkIfDated
                    ? CorporateColors.tinyCampusOrange
                    : CorporateColors.tinyCampusIconGrey),
            RichText(
                text: TextSpan(
              children: [
                TextSpan(
                    text: FlutterI18n.translate(
                        context, '$i18nOWKey.last_updated'),
                    style: TextStyle(
                        fontSize: 12.0,
                        letterSpacing: -0.5,
                        color: checkIfDated
                            ? CorporateColors.tinyCampusOrange
                            : CorporateColors.tinyCampusIconGrey)),
                TextSpan(
                    text: " ",
                    style: TextStyle(
                      fontSize: 12.0,
                    )),
                TextSpan(
                    text: timeago.format(
                      oWeek.lastUpdated ?? DateTime.now(),
                      locale: FlutterI18n.currentLocale(context)?.languageCode,
                    ),
                    style: TextStyle(
                        fontSize: 12.0,
                        letterSpacing: -0.5,
                        color: checkIfDated
                            ? CorporateColors.tinyCampusOrange
                            : CorporateColors.tinyCampusIconGrey)),
              ],
            )),
          ],
        ),
      );
}

class DelayedBodyWidget extends StatefulWidget {
  const DelayedBodyWidget({
    Key? key,
    required this.oWeek,
  }) : super(key: key);

  final OWeek oWeek;

  @override
  _DelayedBodyWidgetState createState() => _DelayedBodyWidgetState();
}

class _DelayedBodyWidgetState extends State<DelayedBodyWidget> {
  int maxLayers = 0;
  List<int> temporaryDeletedLessons = <int>[];
  bool isVisible = false;

  @override
  void initState() {
    super.initState();
    calculateLayeringAndIntersections();
    Future.delayed(Duration(milliseconds: forcedDelayInMilliseconds))
        .then((value) {
      if (mounted) {
        setState(() {
          isVisible = true;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) => AnimatedSwitcher(
        duration: Duration(milliseconds: switcherDelayInMilliseconds),
        child: !isVisible
            ? Container()
            : BlocConsumer<OViewModeBloc, OViewModeState>(
                listener: (context, state) {
                  // if (state is ODailyViewState) {
                  calculateLayeringAndIntersections();
                  // }
                },
                builder: (context, state) {
                  if (state is OWeeklyViewState) {
                    return SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: BlocListener<OMessageBloc, OMessageState>(
                        listener: (context, state) {
                          if (state is OElementChangedState) {
                            calculateLayeringAndIntersections(
                                shouldDelay: true);
                          }
                          if (state is OMessageInitial) {
                            calculateLayeringAndIntersections(
                                shouldDelay: false);
                          }
                          if (state is OLessonsChangedState) {
                            temporaryDeletedLessons.addAll(state.lessons);
                            calculateLayeringAndIntersections(
                                shouldDelay: true);
                          }
                        },
                        child: BlocListener<OAnimationBloc, OAnimationState>(
                          listener: (context, state) {
                            if (state is ORevertDeleteAnimationState) {
                              for (var e in state.lessons) {
                                temporaryDeletedLessons.remove(e);
                              }
                            } else if (state
                                is ORevertDeleteMultiOElementsAnimationState) {
                              calculateLayeringAndIntersections(
                                shouldDelay: true,
                              );
                            }
                          },
                          child: SizedBox(
                            height: maxHeight + weekBodyExtraHeight,
                            child: WeekBody(
                                maxHeight: maxHeight, oWeek: widget.oWeek),
                          ),
                        ),
                      ),
                      // ),
                    );
                  } else if (state is ODailyViewState) {
                    return OverviewDay(
                      oWeek: widget.oWeek,
                      weekday: state.weekday,
                      temporaryDeletedLessons: temporaryDeletedLessons,
                    );
                  }
                  return Container();
                },
              ),
      );

  void calculateLayeringAndIntersections({bool shouldDelay = false}) async {
    // filter by deletedElements
    debugPrint(
        "calculateLayeringAndIntersections${DateTime.now().toIso8601String()}");
    Timer(Duration(milliseconds: shouldDelay ? 460 : 0), () {
      var elements = OUtil.getDeletedOElementIds();

      // if (widget.oWeek != null) {
      for (var k = 1; k <= DateTime.sunday; k++) {
        if (widget.oWeek.days[k]?.isEmpty ?? false) continue;
        for (var i = 0; i < (widget.oWeek.days[k]?.length ?? 0); i++) {
          widget.oWeek.days[k]?[i].init();
        }
      }

      // return;
      for (var k = 1; k <= DateTime.sunday; k++) {
        if (widget.oWeek.days[k]?.isEmpty ?? false) continue;
        for (var i = 0; i < (widget.oWeek.days[k]?.length ?? 0); i++) {
          // if (temporaryDeletedLessons != null) {
          //   if (temporaryDeletedLessons
          //       .contains(widget.oWeek.days[k][i].ele.lessonId)) {
          //     widget.oWeek.days[k][i].countIntersections = -1;
          //     // continue;
          //   }
          // }
          for (var j = 0; j < (widget.oWeek.days[k]?.length ?? 0); j++) {
            if (i != j) {
              if (elements.contains(widget.oWeek.days[k]?[i].ele.id) ||
                  elements.contains(widget.oWeek.days[k]?[j].ele.id)) {
                continue;
              }
              var hiddenIdentified = false;
              if (temporaryDeletedLessons
                  .contains(widget.oWeek.days[k]?[i].ele.lessonId)) {
                // widget.oWeek.days[k][i].init();
                widget.oWeek.days[k]?[i].countIntersections = -1;
                hiddenIdentified = true;
              }
              if (temporaryDeletedLessons
                  .contains(widget.oWeek.days[k]?[j].ele.lessonId)) {
                // widget.oWeek.days[k][i].init();
                widget.oWeek.days[k]?[j].countIntersections = -1;
                hiddenIdentified = true;
              }
              if (hiddenIdentified) {
                continue;
              }
              if (intersectsIncludingLayer(
                  widget.oWeek.days[k]?[i], widget.oWeek.days[k]?[j])) {
                if (!((widget.oWeek.days[k]?[i].countIntersections) == -1)) {
                  widget.oWeek.days[k]?[j].layer++;
                  widget.oWeek.days[k]?[i].isIntersected = true;
                  widget.oWeek.days[k]?[j].isIntersected = true;
                  widget.oWeek.days[k]?[j].intersectsWith =
                      widget.oWeek.days[k]?[i];
                  widget.oWeek.days[k]?[i].countIntersections++;
                  widget.oWeek.days[k]?[j].countIntersections++;
                }
                if (maxLayers < (widget.oWeek.days[k]?[j].layer ?? 0)) {
                  maxLayers++;
                }
              }
              // count++;
            }
          }
        }
      }
      // }
      // OUtil.addOCalendarEvent(OCalendarAddOWeek(

      // ));
      if (mounted) {
        setState(() {});
      }
    });
  }

  bool intersectsIncludingLayer(OElementWrapper? a, OElementWrapper? b) {
    if (a != null && b != null) {
      if (a.layer != b.layer) return false;
      return intersects(a.ele, b.ele);
    }
    return false;
  }

  bool intersects(OElement a, OElement b) {
    if (a.start != null && a.end != null && b.start != null && b.end != null) {
      return ((a.start!.millisecondsSinceEpoch <=
              b.end!.millisecondsSinceEpoch) &
          (b.start!.millisecondsSinceEpoch <= a.end!.millisecondsSinceEpoch));
    }
    return false;
  }
}

class WeekBody extends StatefulWidget {
  const WeekBody({
    Key? key,
    required this.maxHeight,
    required this.oWeek,
  }) : super(key: key);

  final double maxHeight;
  final OWeek oWeek;

  @override
  _WeekBodyState createState() => _WeekBodyState();
}

class _WeekBodyState extends State<WeekBody> {
  int selectedSubject = 0;

  int selectedOElement = 0;

  @override
  Widget build(BuildContext context) => Row(
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: BlocBuilder<OSettingsBloc, OSettingsState>(
                    builder: (context, state) => Stack(
                      clipBehavior: Clip.none,
                      children: _buildLeftTimeAxis(widget.maxHeight),
                    ),
                  ),
                ),
              ],
            ),
          ),
          _buildDayStack(widget.oWeek, DateTime.monday, widget.maxHeight),
          _buildDayStack(widget.oWeek, DateTime.tuesday, widget.maxHeight),
          _buildDayStack(widget.oWeek, DateTime.wednesday, widget.maxHeight),
          _buildDayStack(widget.oWeek, DateTime.thursday, widget.maxHeight),
          _buildDayStack(widget.oWeek, DateTime.friday, widget.maxHeight),
          _buildDayStack(widget.oWeek, DateTime.saturday, widget.maxHeight),
          _buildDayStack(widget.oWeek, DateTime.sunday, widget.maxHeight),
        ],
      );

  List<Widget> _buildLeftTimeAxis(double maxHeight) => OUtil.getBlocks()
      .map((e) => _buildBlockElement(e.start, e.end, e.label, maxHeight))
      .expand((e) => e)
      .toList();

  List<Widget> _buildBlockElement(
      DateTime start, DateTime end, String label, double maxHeight) {
    const _fontSize = 11.0;
    const milDuration = 680;
    var starting = AnimatedPositioned(
      duration: Duration(milliseconds: milDuration),
      curve: Curves.elasticOut,
      left: 0.0,
      right: 0.0,
      top: OUtil.calculateBegin(start, maxHeight, extraSpace, widget.oWeek),
      child: Text(
        DateFormat('kk:mm').format(start),
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: _fontSize, color: Colors.grey[400]),
        overflow: TextOverflow.ellipsis,
      ),
    );
    var middle = AnimatedPositioned(
      duration: Duration(milliseconds: milDuration),
      curve: Curves.elasticOut,
      top: ((OUtil.calculateBegin(start, maxHeight, extraSpace, widget.oWeek) +
                  OUtil.calculateBegin(
                      end, maxHeight, extraSpace, widget.oWeek)) /
              2) -
          9,
      left: 0.0,
      right: 0.0,
      child: Text(
        label,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 18.0,
        ),
        overflow: TextOverflow.ellipsis,
      ),
    );
    var ending = AnimatedPositioned(
      duration: Duration(milliseconds: milDuration),
      curve: Curves.elasticOut,
      top: OUtil.calculateBegin(end, maxHeight, extraSpace, widget.oWeek) - 10,
      left: 0.0,
      right: 0.0,
      child: Text(
        DateFormat('kk:mm').format(end),
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: _fontSize, color: Colors.grey[400]),
        overflow: TextOverflow.ellipsis,
      ),
    );
    return [starting, middle, ending];
  }

  Widget _buildDayStack(OWeek oWeek, int weekday, double maxHeight) {
    // if (oWeek == null) {
    //   return Expanded(flex: weekday <= 5 ? 3 : 0, child: Container());
    // }
    if (weekday == 6 && !oWeek.containsEntriesOn(DateTime.saturday)) {
      return Expanded(flex: 0, child: Container());
    }
    if (weekday == 7 && !oWeek.containsEntriesOn(DateTime.sunday)) {
      return Expanded(flex: 0, child: Container());
    }
    var date = OUtil.getDateTimeFromWeekSince1970(
        oWeek.weekSinceEpoch1970 ?? 0, weekday);

    final today = DateTime.now();
    final isToday = today.difference(date).inDays == 0 && today.day == date.day;

    return Expanded(
      flex: 3,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 2.0),
        child: Column(
          children: <Widget>[
            Expanded(
              child: LayoutBuilder(
                builder: (context, constraints) => Stack(
                  clipBehavior: Clip.none,
                  children: [
                    ..._buildWeeklyOElements(weekday, oWeek, maxHeight,
                        constraints.maxWidth, isToday),
                    // isToday ? createTodayIndicator() : Container()
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> _buildWeeklyOElements(
    int weekday,
    OWeek oWeek,
    double maxHeight,
    double maxWidth,
    bool isToday,
  ) {
    // return [Container()];
    // if (oWeek == null) {
    //   return [
    //     Center(
    //       child: Column(
    //         children: <Widget>[
    //           LinearProgressIndicator(),
    //         ],
    //       ),
    //     )
    //   ];
    // }
    if (oWeek.days[weekday]?.isEmpty ?? false) {
      return [Container()]; // no content this day
    }
    var oElements = oWeek.days[weekday];
    var positionedElements = <Widget>[];
    for (var i = 0; i < (oElements?.length ?? 0); i++) {
      if (!OUtil.isDeleted(oElements?[i])) {
        positionedElements.add(_buildSingleOElement(
          oElements![i],
          (oWeek),
          maxHeight,
          // !OUtil.isDeleted(oElements[i]) ?
          maxWidth,
          // : 4
          // ,
          isToday,
        ));
      }
    }
    return positionedElements;
  }

  Widget _buildSingleOElement(
    OElementWrapper eW,
    OWeek oWeek,
    double maxHeight,
    double maxWidth,
    bool isToday,
  ) {
    Color oElementColor = Colors.grey;
    var _top = OUtil.calculateBegin(
        eW.ele.start ?? DateTime.now(), maxHeight, extraSpace, oWeek);

    var directSelected = eW.ele.id == selectedOElement;

    var _width = (eW.isIntersected && eW.countIntersections > 0
        ? maxWidth * (1 / (eW.countIntersections + 1))
        : maxWidth);
    return AnimatedPositioned(
      key: Key(eW.ele.id.toString()),
      curve: Curves.easeOut,
      duration: Duration(milliseconds: 260),
      top: _top,
      left: (eW.isIntersected && eW.countIntersections > 0
          ? maxWidth * (eW.layer / (eW.countIntersections + 1))
          : 0),
      width: _width,
      child: eW.countIntersections >= 0
          ? OCalendarEntry(
              key: Key(eW.ele.id.toString()),
              eW: eW,
              maxHeight: maxHeight,
              oElementColor: oElementColor,
              width: _width,
              top: _top,
              isToday: isToday,
              height: calculateHeight(
                  eW.ele.start ?? DateTime.now(),
                  eW.ele.end ??
                      DateTime.now().add(Duration(hours: 1, minutes: 30)),
                  maxHeight,
                  oWeek),
              // determineOpacity: determineOpacity,
              directSelected: directSelected,
            )
          : Container(),
    );
  }

  double calculateHeight(
          DateTime start, DateTime end, double maxHeight, OWeek oWeek) =>
      end.difference(start).inMinutes /
      (oWeek.latestMinuteForEntireWeek - oWeek.firstMinuteForEntireWeek) *
      maxHeight;
}
