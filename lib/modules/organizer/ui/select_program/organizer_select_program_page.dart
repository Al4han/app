/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart';

import '../../../../common/tc_theme.dart';
import '../../../../repositories/http/http_repository.dart';
import '../../blocs/o_color_mapping_bloc/o_color_mapping_bloc.dart';
import '../../blocs/o_program_configuration_bloc/o_program_configuration_bloc.dart';
import '../../model/o_program.dart';
import '../../utility/organizer_common_classes.dart';
import '../../utility/organizer_program_functions.dart';
import '../organizer/o_utility_functions.dart';
import '../organizer/organizer_constants.dart';

const String _i18nKey = "modules.organizer.ui.selected_program";

class OrganizerSelectProgramPage extends StatefulWidget {
  OrganizerSelectProgramPage({
    Key? key,
  }) : super(key: key);

  @override
  _OrganizerSelectProgramPageState createState() =>
      _OrganizerSelectProgramPageState();
}

enum TileState {
  idle,
  loading,
  noData,
  error,
  success,
}

class _OrganizerSelectProgramPageState
    extends State<OrganizerSelectProgramPage> {
  // of the TextField.
  final _myController = TextEditingController();
  String _filterString = "";
  List<OProgram> programs = <OProgram>[];
  late OProgram _selectedProgram;
  CurrentOperation status = CurrentOperation.fetchingSelectedProgram;
  late FocusNode focusNode;
  Map<int, TileState> tileMap = <int, TileState>{};
  bool hasAlreadyPopped = false;

  @override
  void dispose() {
    _myController.dispose();
    focusNode.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    status = CurrentOperation.fetchingPrograms;
    focusNode = FocusNode();
    _fetchPrograms();
  }

  Future<void> _fetchPrograms() async {
    setState(() {
      status = CurrentOperation.fetchingPrograms;
    });

    var res = await OProgramFunctions.getAllProgramsUpdated(
      RepositoryProvider.of<HttpRepository>(context),
    ).catchError((e) {
      // TODO: correct error handling
      // addOMessageEvent(OErrorMessage());

      switch (e.runtimeType) {
        case SocketException:
        case InternalServerErrorException:
        case ClientException:
          debugPrint(e.runtimeType.toString());
          break;
        default:
          debugPrint(e);
      }

      debugPrint("error in _loadData() - but no action triggered: "
          "${e.toString}");

      setState(() {
        status = CurrentOperation.errorConnection;
      });
    });

    if (status == CurrentOperation.errorConnection) {
      return;
    }
    setState(() {
      programs = res?.toList() ?? <OProgram>[];

      // if (programs == null) {
      //   status = CurrentOperation.errorLoadingProgram;
      // }
      try {
        // TODO NNBD MIGRATION
        // var blacklist = OUtil.oSettingsBloc.state.programBlacklist;
        // if (blacklist.isNotEmpty) {
        //   for (var i = 0; i < blacklist.length; i++) {
        //     programs.removeWhere((element) => element.name == blacklist[i]);
        //   }
        // }

        /// filtered by already selected [OProgram]s by the user
        var selectedPrograms = OUtil.getAllSelectedPrograms();
        if (selectedPrograms != null) {
          for (var i = 0; i < selectedPrograms.length; i++) {
            programs
                .removeWhere((element) => element.id == selectedPrograms[i].id);
          }
        }
      } on Exception catch (e) {
        debugPrint("[ORGANIZER LOADING PROGRAMS ERROR] Error: $e");
      }

      status = CurrentOperation.loadedPrograms;
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: I18nText('$_i18nKey.select_study_course',
              child: Text('Studiengang auswählen')),
        ),
        body: _buildSelectProgramBody(context),
      );

  Widget _buildSelectProgramBody(BuildContext context) {
    switch (status) {
      case CurrentOperation.fetchingPrograms:
        return _buildProgramLoading();
      case CurrentOperation.loadedPrograms:
        return _buildSearchBody(context);
      case CurrentOperation.fetchingSelectedProgram:
        return _buildSearchBody(context);
      case CurrentOperation.errorLoadingProgram:
        return _buildSearchBody(context);
      // return Center(
      //   child: _buildErrorLoadingButton(),
      // );
      case CurrentOperation.errorConnection:
        return Center(
          child: _buildErrorConnectionButton(),
        );
      case CurrentOperation.loadedSelectedProgram:
        // TODO: take a look on that
        Timer(const Duration(milliseconds: 1), onCloseDialog);
        return _buildSearchBody(context);
      case CurrentOperation.noDataFound:
        return Center(
          child: _buildNoDataFoundButton(),
        );
    }
  }

  Widget _buildNoDataFoundButton() => FlatButton(
        onPressed: () {
          setState(() {
            status = CurrentOperation.loadedPrograms;
          });
        },
        child: I18nText(
          '$_i18nKey.no_data',
          child: Text(
            "Keine Daten gefunden, bitte etwas anderes auswählen",
            textAlign: TextAlign.center,
          ),
        ),
      );

  Widget _buildErrorConnectionButton() => Center(
          child: Card(
        child: FlatButton(
          onPressed: _fetchPrograms,
          child: SizedBox(
            width: 170,
            height: 110,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    I18nText(
                      '$i18nOKey.error_messages.connection_error',
                      child: Text(
                        "Verbindungsfehler",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.w400),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: I18nText(
                    '$i18nOKey.reload',
                    child: Text(
                      "Neu laden",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 20,
                          color: CorporateColors.tinyCampusOrange),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ));

  void onCloseDialog() {
    if (mounted) {
      gracefulPop(context);
    }
  }

  Widget _buildProgramLoading() => Center(child: CircularProgressIndicator());

  Widget _buildSearchBody(BuildContext context) => Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal: 12.0),
            child: Stack(
              children: <Widget>[
                TextField(
                  focusNode: focusNode,
                  style: TextStyle(
                      // color: Colors.white,
                      fontSize: 22.0,
                      fontStyle: FontStyle.normal,
                      color: CorporateColors.tinyCampusBlue),
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      contentPadding: EdgeInsets.symmetric(vertical: 24.0),
                      // contentPadding: EdgeInsets.only(
                      // left: 15, bottom: 11, top: 11, right: 15),
                      hintText:
                          FlutterI18n.translate(context, '$_i18nKey.search'),
                      hintStyle:
                          TextStyle(color: CorporateColors.tinyCampusIconGrey)),
                  controller: _myController,
                  onChanged: (inputText) {
                    setState(() {
                      _filterString = inputText;
                    });
                  },
                ),
                Positioned(
                    top: 0,
                    bottom: 0,
                    right: _myController.text.isEmpty ? 0 : 0,
                    child: AnimatedSwitcher(
                      duration: Duration(
                        milliseconds: 210,
                      ),
                      child: _myController.text.isNotEmpty
                          ? IconButton(
                              onPressed: () {
                                _myController.clear();
                                _filterString = "";
                                setState(() {});
                              },
                              icon: Icon(Icons.backspace,
                                  size: 24.0,
                                  color: _myController.text.isEmpty
                                      ? CorporateColors.tinyCampusIconGrey
                                      : CorporateColors.tinyCampusBlue),
                            )
                          : IconButton(
                              onPressed: () => focusNode.requestFocus(),
                              icon: Icon(Icons.search,
                                  size: 32.0,
                                  color: CorporateColors.tinyCampusIconGrey),
                            ),
                    )),
              ],
            ),
          ),
          Expanded(
            child: _buildProgramList(context),
          ),
        ],
      );

  Widget _buildProgramList(BuildContext context) {
    // _filterString ??= "";
    if (programs.isEmpty) return Center(child: CircularProgressIndicator());
    var filteredPrograms = List.of(programs);
    for (var i = 0; i < programs.length; i++) {
      if (!programs[i]
          .name
          .toLowerCase()
          .contains(_filterString.toLowerCase())) {
        filteredPrograms.remove(programs[i]);
      }
    }
    return ListView.builder(
      itemCount: filteredPrograms.length,
      physics: BouncingScrollPhysics(),
      itemBuilder: (context, index) {
        var tileState = tileMap[filteredPrograms[index].id];
        if (tileMap[filteredPrograms[index].id] == null) {
          tileState = TileState.idle;
        }
        return Container(
          margin: EdgeInsets.symmetric(horizontal: 8.0),
          child: Card(
            // elevation: 4.0,
            child: ListTile(
              title: Material(
                color: Colors.transparent,
                child: Text(
                  filteredPrograms[index].name,
                  style: tileState == TileState.noData
                      ? Theme.of(context).textTheme.headline4?.copyWith(
                          color: CorporateColors.cafeteriaCautionYellow)
                      : Theme.of(context).textTheme.headline4,
                ),
              ),
              subtitle: tileState == TileState.noData
                  ? I18nText(
                      '$_i18nKey.no_data',
                      child: Text(
                        "Keine Daten gefunden, bitte etwas anderes auswählen",
                        textAlign: TextAlign.left,
                      ),
                    )
                  : tileState == TileState.error
                      ? I18nText(
                          '$i18nOKey.error_messages.connection_error',
                          child: Text(
                            "Verbindungsfehler",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontWeight: FontWeight.w400),
                          ),
                        )
                      : null,
              trailing: programTrailingState(filteredPrograms[index].id),
              onTap: tileState == TileState.noData ||
                      status == CurrentOperation.fetchingSelectedProgram
                  ? null
                  : () {
                      tileMap[filteredPrograms[index].id] = TileState.loading;
                      setState(() {
                        status = CurrentOperation.fetchingSelectedProgram;
                      });

                      OProgramFunctions
                              .fillEntireProgramWithPoolsSubjectsLessonsUpdated(
                                  filteredPrograms[index].id,
                                  filteredPrograms[index].name,
                                  RepositoryProvider.of<HttpRepository>(
                                      context))
                          .then((content) {
                        if (content == null) {
                          setState(() {
                            status = CurrentOperation.errorLoadingProgram;
                          });
                          return;
                        }
                        setState(() {
                          _selectedProgram = content;
                          _addOProgram(_selectedProgram);
                          tileMap[filteredPrograms[index].id] =
                              TileState.success;
                          status = CurrentOperation.loadedSelectedProgram;
                        });
                      }).catchError((e) {
                        debugPrint(e.runtimeType.toString());
                        if (e is EmptyDataException) {
                          debugPrint("found no data");
                          setState(() {
                            tileMap[filteredPrograms[index].id] =
                                TileState.noData;
                            status = CurrentOperation.loadedPrograms;
                          });
                          return;
                        } else {
                          debugPrint("other error");
                          debugPrint(e.runtimeType.toString());
                          setState(() {
                            tileMap[filteredPrograms[index].id] =
                                TileState.error;
                            status = CurrentOperation.errorLoadingProgram;
                          });
                          return;
                        }
                      });
                    },
            ),
          ),
        );
      },
    );
  }

  void gracefulPop(BuildContext context) {
    if (!hasAlreadyPopped) {
      setState(() {
        hasAlreadyPopped = true;
      });
      Navigator.of(context).pop();
    } else {
      //nothing
    }
  }

  Widget programTrailingState(int programID) {
    // if (tileMap == null) {
    //   return SizedBox(width: 1, height: 1);
    // }
    final tileState = tileMap[programID];
    if (tileState == null) {
      return SizedBox(width: 1, height: 1);
    } else {
      switch (tileState) {
        case TileState.idle:
          return SizedBox(width: 1, height: 1);
        case TileState.loading:
          return SizedBox(
              height: 20,
              width: 20,
              child: CircularProgressIndicator(
                strokeWidth: 1.5,
              ));
        case TileState.noData:
          return Icon(Icons.warning);
        case TileState.success:
          return Icon(
            Icons.check,
            color: CorporateColors.cafeteriaVeganGreen,
          );
        case TileState.error:
          return Icon(Icons.signal_cellular_connected_no_internet_4_bar);
      }
    }
  }

  void _addOProgram(OProgram program) {
    var programConfigBloc = BlocProvider.of<OProgramConfigurationBloc>(context);
    var state = programConfigBloc.state;
    debugPrint(state.toString());
    if (state is OProgramConfigurationEmptyState) {
      programConfigBloc.add(
        // TODO: NULL-SAFETY CRITICAL
        AddOProgramEvent(
          program: _selectedProgram,
          previousProgramList: [],
        ),
      );
    } else if (state is OProgramConfigurationReadyState) {
      programConfigBloc.add(
        // TODO: NULL-SAFETY CRITICAL
        AddOProgramEvent(
          program: _selectedProgram,
          previousProgramList: state.programs,
        ),
      );
    }

    var colorConfigBloc = BlocProvider.of<OColorMappingBloc>(context);
    var colorState = colorConfigBloc.state;

    //get all subjects
    var subjects = <int>[];
    //transfer subjects to list
    subjects = getDistinctLessonsInPool(_selectedProgram);

    if (colorState is OColorMappingReadyState) {
      colorConfigBloc.add(
        MapMultiOSubjectToRandomColorEvent(
          subjects: subjects,
          previousColorList: colorState.previousColorList,
        ),
      );
    }
    //
  }

  List<int> getDistinctLessonsInPool(OProgram program) {
    var distinctSubjectsInProgram = <int>[];
    if (program.pools.isNotEmpty) {
      for (var pool in program.pools) {
        if (pool.subjects.isNotEmpty) {
          for (var subjectLesson in pool.subjects) {
            if (!distinctSubjectsInProgram.contains(subjectLesson.id)) {
              distinctSubjectsInProgram.add(subjectLesson.id);
            }
          }
        }
      }
    }
    return distinctSubjectsInProgram;
  }
}

enum CurrentOperation {
  fetchingPrograms,
  loadedPrograms,
  fetchingSelectedProgram,
  loadedSelectedProgram,
  errorLoadingProgram,
  errorConnection,
  noDataFound,
}
