/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../common/tc_theme.dart';
import '../../blocs/o_color_mapping_bloc/o_color_mapping_bloc.dart';
import '../../blocs/o_lesson_selection_bloc/o_lesson_selection_bloc.dart';
import '../../blocs/o_program_history_bloc/o_program_history_bloc.dart';
import '../../model/o_pool.dart';
import '../../model/o_program.dart';
import '../../model/o_subject.dart';
import '../organizer/o_utility_functions.dart';
import '../shared_widgets/delta_widget.dart';

class OrganizerSubjectConfigPage extends StatefulWidget {
  const OrganizerSubjectConfigPage({
    Key? key,
    required this.subject,
    required this.pool,
    required this.program,
  }) : super(key: key);
  final OSubject subject;
  final OPool pool;
  final OProgram program;

  @override
  _OrganizerSubjectConfigPageState createState() =>
      _OrganizerSubjectConfigPageState();
}

class _OrganizerSubjectConfigPageState
    extends State<OrganizerSubjectConfigPage> {
  @override
  void initState() {
    super.initState();
    if (widget.subject.lessons.isNotEmpty) {
      try {
        for (var i = 0; i < widget.subject.lessons.length; i++) {
          Timer(Duration(milliseconds: 1600), () {
            if (mounted) {
              OUtil.addOHistoryBlocEvent(
                UserCheckedChangeEvent(OUtil.concatPPSL(widget.program,
                    widget.pool, widget.subject, widget.subject.lessons[i])),
              );
            }
          });
        }
      } on Exception catch (e) {
        debugPrint("Tried to emit an event, but was not mounted? Error: $e");
      }
    }
  }
  // final ColorSwatch _mainColor = Colors.blue;
  // void _openDialog(String title, Widget content) {
  //   showDialog(
  //     context: context,
  //     builder: (_) => AlertDialog(
  //       contentPadding: const EdgeInsets.all(6.0),
  //       title: Text(title),
  //       content: content,
  //       actions: [
  //         FlatButton(
  //           child: Text('ABBRECHEN'),
  //           onPressed: Navigator.of(context).pop,
  //         ),
  //         FlatButton(
  //           child: Text('OK'),
  //           onPressed: () {
  //             Navigator.of(context).pop();
  //           },
  //         ),
  //       ],
  //     ),
  //   );
  // }

  // void _openFullMaterialColorPicker() {
  //   _openDialog(
  //     "Farbauswahl",
  //     MaterialColorPicker(
  //       colors: fullMaterialColors,
  //       selectedColor: _mainColor,
  //       onColorChange: (color) => setState(() => {
  //             _setNewSubjectColorInBloc(
  //               color: color,
  //             ),
  //           }),
  //     ),
  //   );
  // }

  // void _setNewSubjectColorInBloc({Color color}) {
  //   var colorBloc = BlocProvider.of<OColorMappingBloc>(context);
  //   var currentState = colorBloc.state;
  //   if (currentState is OColorMappingReadyState) {
  //     var previousList = currentState.previousColorList;
  //     colorBloc.add(MapOSubjectToColorEvent(
  //       subject: widget.subject.id,
  //       previousColorList: previousList,
  //       color: color.value.toRadixString(16),
  //     ));
  //   }
  // }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: _buildSubjectLessons(context),
      );

  // TextStyle _estimateBrightness(Color color) {
  //   final theme = Theme.of(context);
  //   var textStyle = theme.primaryTextTheme.subtitle1.copyWith(color: color);
  //   final shadow = <Shadow>[
  //     Shadow(
  //       blurRadius: 3.0,
  //       color: Colors.white54,
  //     )
  //   ];
  //   switch (ThemeData.estimateBrightnessForColor(color)) {
  //     case Brightness.dark:
  //       textStyle = textStyle.copyWith(
  //         color: Colors.white,
  //         shadows: shadow,
  //         letterSpacing: -1.0,
  //       );
  //       break;
  //     case Brightness.light:
  //       textStyle = textStyle.copyWith(
  //         color: Colors.black,
  //         shadows: shadow,
  //       );
  //       break;
  //   }
  //   return textStyle;
  // }

  Widget _buildSubjectLessons(BuildContext context) =>
      BlocBuilder<OLessonSelectionBloc, OLessonSelectionState>(
          builder: (context, state) {
        if (state is OLessonSelectionReadyState) {
          return BlocBuilder<OColorMappingBloc, OColorMappingState>(
              builder: (context, colorState) {
            if (colorState is OColorMappingReadyState) {
              // final foregroundColor =
              //     colorState.getColor(subjectId: widget.subject.id);
              return Column(
                children: <Widget>[
                  SizedBox(
                    height: 150,
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                          top: 0,
                          left: 0,
                          right: 0,
                          bottom: 0,
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              // borderRadius: BorderRadius.only(
                              //     bottomLeft: Radius.circular(12),
                              //     bottomRight: Radius.circular(12)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black12,
                                  spreadRadius: 0,
                                  blurRadius: 3,
                                  offset: Offset(0, 3),
                                ),
                              ],
                            ),
                            child: Container(
                              margin: EdgeInsets.only(top: 20),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Hero(
                                    tag: "BACKBUTTON",
                                    child: Material(
                                      color: Colors.transparent,
                                      child: BackButton(
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 5,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Hero(
                                          tag: widget.pool.id,
                                          child: Material(
                                            type: MaterialType.transparency,
                                            child: Text(widget.pool.name,
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                    color: CorporateColors
                                                        .tinyCampusIconGrey,
                                                    fontSize: 16.0,
                                                    letterSpacing: -0.6)),
                                          ),
                                        ),
                                        Hero(
                                          tag: widget.subject.id,
                                          child: Material(
                                            type: MaterialType.transparency,
                                            child: Text(widget.subject.name,
                                                textAlign: TextAlign.left,
                                                style: CorporateTextStyles
                                                    .organizerHeadline),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  // Expanded(
                                  //   flex: 3,
                                  //   child: Container(
                                  //     margin:
                                  //
                                  //  EdgeInsets.symmetric(vertical: 14.0),
                                  //     child: Card(
                                  //       elevation: 0.0,
                                  //       color: foregroundColor,
                                  //       margin: EdgeInsets.symmetric(
                                  //
                                  //  vertical: 12.0, horizontal: 18.0),
                                  //       child: FlatButton(
                                  //         onPressed:
                                  //             _openFullMaterialColorPicker,
                                  //         child: Center(
                                  //             child: Column(
                                  //           mainAxisAlignment:
                                  //               MainAxisAlignment.center,
                                  //           children: <Widget>[
                                  //             Icon(
                                  //               Icons.invert_colors,
                                  //               color: _estimateBrightness(
                                  //                       foregroundColor)
                                  //                   .color,
                                  //             ),
                                  //             Text(
                                  //               "Farbe ändern",
                                  //               style: _estimateBrightness(
                                  //                 foregroundColor,
                                  //               ),
                                  //               textAlign: TextAlign.center,
                                  //             ),
                                  //           ],
                                  //         )),
                                  //       ),
                                  //     ),
                                  //   ),
                                  // ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: ListView.builder(
                      physics: BouncingScrollPhysics(),
                      itemCount: widget.subject.lessons.length,
                      itemBuilder: (context, index) {
                        var isSelected = state.isInSelection(
                            lessonId: widget.subject.lessons[index].id);
                        return Material(
                          color: Colors.white,
                          child: CheckboxListTile(
                            value: isSelected,
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 8.0, horizontal: 18.0),
                            onChanged: (value) {
                              setState(() {
                                var lessonConfigBloc =
                                    BlocProvider.of<OLessonSelectionBloc>(
                                        context);
                                var previousLessons = state.lessons;
                                if (state.isInSelection(
                                        lessonId:
                                            widget.subject.lessons[index].id) ==
                                    true) {
                                  //remove program
                                  lessonConfigBloc.add(
                                    RemoveOLessonEvent(
                                      lesson:
                                          widget.subject.lessons[index].id ??
                                              -1,
                                      previousLessonList: previousLessons,
                                    ),
                                  );
                                } else {
                                  //add Program
                                  lessonConfigBloc.add(
                                    AddOLessonEvent(
                                      lesson:
                                          widget.subject.lessons[index].id ??
                                              -1,
                                      previousLessonList: previousLessons,
                                    ),
                                  );
                                }
                              });
                            },
                            title: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(widget.subject.lessons[index].name ?? ""),
                                DeltaWidget(
                                  program: widget.program,
                                  pool: widget.pool,
                                  subject: widget.subject,
                                  lesson: widget.subject.lessons[index],
                                ),
                              ],
                            ),
                            // checkColor:
                            //     _estimateBrightness(foregroundColor).color,
                            activeColor: CorporateColors.cafeteriaVeganGreen,
                            subtitle: widget.subject.lessons[index].comment !=
                                    null
                                ? widget.subject.lessons[index].comment == ""
                                    ? null
                                    : Text(
                                        widget.subject.lessons[index].comment ??
                                            "",
                                        style: TextStyle(
                                          fontSize: 16,
                                          letterSpacing: -0.8,
                                          color: CorporateColors
                                              .tinyCampusDarkBlue,
                                        ))
                                : null,
                            secondary: Hero(
                              tag: widget.subject.lessons[index].id ?? -1,
                              child: CircleAvatar(
                                child: Text(
                                  widget.subject.lessons[index].method ?? "",
                                ),
                                backgroundColor: isSelected
                                    ? CorporateColors.cafeteriaVeganGreen
                                    : CorporateColors.passiveBackgroundLight,
                                foregroundColor: isSelected
                                    ? Colors.white
                                    : CorporateColors.tinyCampusBlue,
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              );
            }
            return Container();
          });
        }
        return Container();
      });
}
