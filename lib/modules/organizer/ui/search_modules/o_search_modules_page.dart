/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../model/o_pool.dart';
import '../../model/o_program.dart';
import '../../model/o_subject.dart';
import '../pool_config/o_subject_list_tile.dart';
import '../search_modules/o_search_widget.dart';

class OSearchModulesPageArguments {
  OSearchModulesPageArguments({required this.program});

  OProgram program;
}

class OSearchModulesPage extends StatefulWidget {
  const OSearchModulesPage({Key? key, required this.program}) : super(key: key);
  final OProgram program;

  @override
  _OSearchModulesPageState createState() => _OSearchModulesPageState();
}

class _OSearchModulesPageState extends State<OSearchModulesPage> {
  final TextEditingController _searchController = TextEditingController();
  String _filterString = "";

  void resetFilter() {
    _searchController.clear();
    _filterString = "";
    setState(() {});
  }

  PreferredSize _buildCustomAppBar() => PreferredSize(
      preferredSize: Size.fromHeight(90.0), // here the desired height
      child: SafeArea(
        child: Row(
          children: <Widget>[
            Hero(
              tag: "BACKBUTTON",
              child: Material(
                color: Colors.transparent,
                child: BackButton(
                  color: Colors.black,
                ),
              ),
            ),
            Expanded(
              child: OSearchWidget(
                myController: _searchController,
                resetFilter: resetFilter,
                onChanged: (input) {
                  setState(() {
                    _filterString = _searchController.text;
                  });
                },
              ),
            ),
          ],
        ),
      ));

  @override
  Widget build(BuildContext context) => Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () => Navigator.of(context).pop(),
          label: Padding(
            padding: EdgeInsets.symmetric(horizontal: 32.0),
            child: I18nText(
              'modules.organizer.ui.config.done',
              child: Text(
                "Fertig",
                style: Theme.of(context).textTheme.headline4,
                textAlign: TextAlign.left,
              ),
            ),
          ),
          backgroundColor: Theme.of(context).primaryColor,
          foregroundColor: CorporateColors.tinyCampusBlue,
        ),
        appBar: _buildCustomAppBar(),
        body: _buildBody(),
      );

  Widget _buildBody() => Scrollbar(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 12.0),
                      child: FlatButton(
                        // elevation: 0.0,
                        onPressed: () => Navigator.pushNamed(
                            context, orgaMultipleModulesExplanation),
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              vertical: 24.0, horizontal: 12.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Icon(
                                  Icons.info,
                                  color: CorporateColors.tinyCampusIconGrey
                                      .withOpacity(0.5),
                                  size: 24.0,
                                ),
                              ),
                              Expanded(
                                child: I18nText(
                                  'modules.organizer.ui.explanations.'
                                  'multiple_shown',
                                  child: Text(
                                    "Warum werden bestimmte Veranstaltungen "
                                    "mehrfach angezeigt?",
                                    style:
                                        Theme.of(context).textTheme.headline4,
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: widget.program.pools.length,
                  itemBuilder: (context, index) {
                    final item = widget.program.pools[index];
                    // if (widget.program.pools[index].subjects != null) {
                    if (widget.program.pools[index].subjects.isNotEmpty) {
                      return Container(
                        margin: EdgeInsets.symmetric(
                          horizontal: 12.0,
                          vertical: 6.0,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 16.0),
                              child: Text(widget.program.pools[index].name,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline2
                                      ?.copyWith(fontSize: 22)),
                            ),
                            ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(12.0)),
                                child: _buildPools(item)),
                          ],
                        ),
                      );
                    } else {
                      return Container(
                        height: 5,
                      );
                    }
                    // } else {
                    //   return Container(
                    //     height: 5,
                    //   );
                    // }
                  }),
              Container(
                height: 80,
              ),
            ],
          ),
        ),
      );

  bool determineVisibility(OSubject subject) {
    if (_filterString.isEmpty) {
      return true;
    }
    if (subject.name.toLowerCase().contains(_filterString.toLowerCase())) {
      return true;
    }
    return false;
  }

  Widget _buildPools(OPool pool) {
    if (pool.subjects.isEmpty) {
      return Container();
    }
    // return Text(pool.name);
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: pool.subjects.length,
      itemBuilder: (context, index) {
        final item = pool.subjects[index];
        var visible = determineVisibility(pool.subjects[index]);
        if (visible) {
          return OSubjectListTile(
            // arguments: OrganizerSubjectConfigPageArguments(
            //     pool: pool, subject: pool.subjects[index]),
            // context: context,
            // index: index,
            subject: item,
            pool: pool,
            program: widget.program,

            // hasSelectedLessons: false,
          );
        } else {
          return Container();
        }
      },
    );
  }
}
