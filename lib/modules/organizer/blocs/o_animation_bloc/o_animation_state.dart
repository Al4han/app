/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_animation_bloc.dart';

abstract class OAnimationState extends Equatable {
  const OAnimationState();
}

class OAnimationInitial extends OAnimationState {
  @override
  List<Object> get props => [];
}

class OAnnounceNewAnimationState extends OAnimationState {
  @override
  List<Object> get props => [];
}

class OClearAllAnimationState extends OAnimationState {
  @override
  List<Object> get props => [];
}

class ONextPageState extends OAnimationState {
  @override
  List<Object> get props => [];
}

class OPreviousPageState extends OAnimationState {
  @override
  List<Object> get props => [];
}

class OSetPageState extends OAnimationState {
  final int setPageValue;

  OSetPageState(this.setPageValue);

  @override
  List<Object> get props => [setPageValue];
}

class OInitialPage extends OAnimationState {
  @override
  List<Object> get props => [];
}

class OAnimiationErrorState extends OAnimationState {
  OAnimiationErrorState();

  @override
  List<Object> get props => [];
}

class ODeleteAnimationState extends OAnimationState {
  ODeleteAnimationState({required this.lessons});
  final List<int> lessons;

  @override
  List<Object> get props => [lessons];
}

class ODeleteSingleAnimationState extends OAnimationState {
  ODeleteSingleAnimationState({required this.oElementId});
  final int oElementId;

  @override
  List<Object> get props => [oElementId];
}

class OStartShakingAnimationState extends OAnimationState {
  OStartShakingAnimationState({required this.lessons});
  final List<int> lessons;

  @override
  List<Object> get props => [lessons];
}

class OStartSingleShakingAnimationState extends OAnimationState {
  OStartSingleShakingAnimationState({required this.oElementId});
  final int oElementId;

  @override
  List<Object> get props => [oElementId];
}

class OStopAllShakingAnimationState extends OAnimationState {
  OStopAllShakingAnimationState();

  @override
  List<Object> get props => [];
}

class ORevertDeleteAnimationState extends OAnimationState {
  ORevertDeleteAnimationState({required this.lessons});
  final List<int> lessons;

  @override
  List<Object> get props => [lessons];
}

class ORevertDeleteSingleAnimationState extends OAnimationState {
  ORevertDeleteSingleAnimationState({required this.oElementId});
  final int oElementId;

  @override
  List<Object> get props => [oElementId];
}

class ORevertDeleteMultiOElementsAnimationState extends OAnimationState {
  ORevertDeleteMultiOElementsAnimationState(this.oElements);

  final List<int> oElements;

  @override
  List<Object> get props => [oElements];
}
