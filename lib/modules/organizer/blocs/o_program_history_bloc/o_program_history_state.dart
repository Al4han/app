/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_program_history_bloc.dart';

/// To save a history tree of a program
@CopyWith()
@JsonSerializable(anyMap: true, explicitToJson: true)
class OProgramHistoryState extends Equatable {
  /// Initial constructor
  OProgramHistoryState.initialSettings()
      : lastUpdated = DateTime.now(),
        viewedHistory = false,
        changeRecordMap = <String, OChangeRecord>{};

  OProgramHistoryState({
    Map<String, OChangeRecord>? changeRecordMap,
    DateTime? lastUpdated,
    bool? viewedHistory,
  })  : changeRecordMap = changeRecordMap ?? <String, OChangeRecord>{},
        lastUpdated = lastUpdated ?? DateTime.fromMicrosecondsSinceEpoch(0),
        viewedHistory = viewedHistory ?? false;

  final Map<String, OChangeRecord> changeRecordMap;
  final DateTime lastUpdated;
  final bool viewedHistory;

  factory OProgramHistoryState.fromJson(Map<String, dynamic> json) =>
      _$OProgramHistoryStateFromJson(json);

  Map<String, dynamic> toJson() => _$OProgramHistoryStateToJson(this);

  @override
  List<Object> get props => [
        changeRecordMap,
        lastUpdated,
        viewedHistory,
      ];

  bool hasChanges() {
    var hasChanges = false;
    // if (changeRecordMap == null) return false;
    if (changeRecordMap.isNotEmpty) {
      hasChanges = true;
    }
    return hasChanges;
  }

  bool hasUncheckedChanges() =>
      changeRecordMap.values.any((e) => !e.userChecked);

  int countChanges(String contextId) {
    var changes = 0;
    // if (changeRecordMap == null) return 0;
    changeRecordMap.forEach((key, value) {
      if (key.startsWith(contextId)) {
        changes++;
      }
    });
    return changes;
  }

  Map<String, OChangeRecord> getChangesByProgram(int programId) {
    // if (changeRecordMap == null) {
    //   return null;
    // }
    var list = <String, OChangeRecord>{};
    changeRecordMap.forEach((key, value) {
      if (key.startsWith(programId.toString())) {
        list[key] = (value);
      }
    });
    return list;
  }

  /// a lesson can have 3 deltas but only 1 change
  int countUncheckedChanges(String contextId) {
    var changes = 0;
    // if (changeRecordMap == null) return 0;
    changeRecordMap.forEach((key, value) {
      if (key.startsWith(contextId) && value.userChecked == false) {
        changes++;
      }
    });
    return changes;
  }

  /// a lesson can have 1 change but 3 deltas (added, namechanged etc)
  int countUncheckedDeltas(String contextId) {
    var changes = 0;
    // if (changeRecordMap == null) return 0;
    changeRecordMap.forEach((key, value) {
      if (key.startsWith(contextId) && value.userChecked == false) {
        changes = changes + value.delta.length;
      }
    });
    return changes;
  }

  int countAllUncheckedDeltas() {
    var changes = 0;
    // if (changeRecordMap == null) return 0;
    changeRecordMap.forEach((key, value) {
      changes = changes + value.delta.length;
    });
    return changes;
  }

  List<bool> hasChanged(String? contextId, {bool showIndirectRemoved = false}) {
    var subtreeChange = false;
    var directChange = false;
    var subtreeChecked = true;
    var directUserChecked = false;
    if (contextId == null) return [false, false, false];
    if (contextId == "") return [false, false, false];
    try {
      changeRecordMap.forEach((key, value) {
        if (key.startsWith(contextId) && key != contextId) {
          if (showIndirectRemoved || !value.delta.contains(ODelta.removed)) {
            subtreeChange = true;
            // if one of the subtree is NOT checked, set it to false forever
            if (value.userChecked == false) subtreeChecked = false;
          }
        }
        if (key == contextId) {
          directChange = true;
          if (value.userChecked == true) directUserChecked = true;
        }
      });
    } on Exception catch (e) {
      debugPrint(e.toString());
      debugPrint(
        "Something went wrong int hasChanged(String contextId) "
        "in OProgramHistoryState",
      );
    }
    return [subtreeChange, directChange, subtreeChecked, directUserChecked];
  }

  // "13-456-5522-13330"
  OChangeRecord? getChange(String contextId) {
    OChangeRecord? change;
    changeRecordMap.forEach((key, value) {
      if (key == contextId) {
        change = value;
      }
    });
    return change;
  }

  List<OChangeRecord> getSubChanges(String contextId) {
    var changes = <OChangeRecord>[];
    changeRecordMap.forEach((key, value) {
      if (key.startsWith(contextId)) {
        changes.add(value);
      }
    });
    return changes;
  }

  Map<String, OChangeRecord> getSubChangesMap(String contextId) {
    var changes = <String, OChangeRecord>{};
    changeRecordMap.forEach((key, value) {
      if (key.startsWith(contextId)) {
        changes[key] = value;
      }
    });
    return changes;
  }
}
