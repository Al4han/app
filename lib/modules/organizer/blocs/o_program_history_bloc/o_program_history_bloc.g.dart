// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_program_history_bloc.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension OProgramHistoryStateCopyWith on OProgramHistoryState {
  OProgramHistoryState copyWith({
    Map<String, OChangeRecord>? changeRecordMap,
    DateTime? lastUpdated,
    bool? viewedHistory,
  }) {
    return OProgramHistoryState(
      changeRecordMap: changeRecordMap ?? this.changeRecordMap,
      lastUpdated: lastUpdated ?? this.lastUpdated,
      viewedHistory: viewedHistory ?? this.viewedHistory,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OProgramHistoryState _$OProgramHistoryStateFromJson(Map json) =>
    OProgramHistoryState(
      changeRecordMap: (json['changeRecordMap'] as Map?)?.map(
        (k, e) => MapEntry(k as String,
            OChangeRecord.fromJson(Map<String, dynamic>.from(e as Map))),
      ),
      lastUpdated: json['lastUpdated'] == null
          ? null
          : DateTime.parse(json['lastUpdated'] as String),
      viewedHistory: json['viewedHistory'] as bool?,
    );

Map<String, dynamic> _$OProgramHistoryStateToJson(
        OProgramHistoryState instance) =>
    <String, dynamic>{
      'changeRecordMap':
          instance.changeRecordMap.map((k, e) => MapEntry(k, e.toJson())),
      'lastUpdated': instance.lastUpdated.toIso8601String(),
      'viewedHistory': instance.viewedHistory,
    };
