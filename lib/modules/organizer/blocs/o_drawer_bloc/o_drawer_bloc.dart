/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../model/o_element_wrapper.dart';

part 'o_drawer_event.dart';
part 'o_drawer_state.dart';

class ODrawerBloc extends Bloc<ODrawerEvent, ODrawerState> {
  ODrawerBloc() : super(ODrawerInitial(null));

  @override
  Stream<ODrawerState> mapEventToState(
    ODrawerEvent event,
  ) async* {
    if (event is OSelectEvent) {
      yield ODrawerMenuState(event.oElementWrapper);
    } else if (event is ODeselectEvent) {
      yield ODrawerInitial(null);
    } else if (event is ODeleteEvent) {
      yield ODrawerDeleteState(event.oElementWrapper);
    } else if (event is OColorEvent) {
      yield ODrawerColorState(event.oElementWrapper);
    } else if (event is OInfoEvent) {
      yield ODrawerInfoState(event.oElementWrapper);
    }
  }
}
