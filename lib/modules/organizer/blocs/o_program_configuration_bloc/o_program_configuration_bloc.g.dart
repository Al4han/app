// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_program_configuration_bloc.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OProgramConfigurationState _$OProgramConfigurationStateFromJson(Map json) =>
    OProgramConfigurationState(
      programs: (json['programs'] as List<dynamic>?)
          ?.map((e) => OProgram.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
    );

Map<String, dynamic> _$OProgramConfigurationStateToJson(
        OProgramConfigurationState instance) =>
    <String, dynamic>{
      'programs': instance.programs.map((e) => e.toJson()).toList(),
    };
