/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../../bloc/local_user_hyd_bloc/local_user_hyd_bloc.dart';
import '../../model/o_program.dart';
import '../../utility/organizer_state_converter.dart';

part 'o_program_configuration_bloc.g.dart';
part 'o_program_configuration_event.dart';
part 'o_program_configuration_state.dart';
part 'o_program_configuration_state_converter.dart';

class OProgramConfigurationBloc extends LocalUserHydBloc<
    OProgramConfigurationEvent, OProgramConfigurationState> {
  OProgramConfigurationBloc() : super(OProgramConfigurationEmptyState());

  @override
  Stream<OProgramConfigurationState> mapEventToState(
    OProgramConfigurationEvent event,
  ) async* {
    yield OProgramConfigurationEmptyState();
    if (event is AddOProgramEvent) {
      var newList = _addOProgramAction(event);
      yield OProgramConfigurationReadyState(
        programs: newList,
      );
    } else if (event is DeleteSingleOProgram) {
      var listWithRemovedProgram = _deleteSingleOProgramAction(event);
      yield OProgramConfigurationReadyState(programs: listWithRemovedProgram);
    } else if (event is OProgramConfigurationResetEvent) {
      yield OProgramConfigurationEmptyState();
    }
  }

  @override
  OProgramConfigurationState fromJson(Map<String, dynamic> json) {
    try {
      return _OProgramConfigurationStateConverter().fromJson(json);
    } on Exception catch (e) {
      debugPrint("[OProgramConfigurationBloc] SEVERE fromJson ERROR: $e");
      return OProgramConfigurationEmptyState();
    }
  }

  @override
  Map<String, dynamic> toJson(OProgramConfigurationState state) =>
      _OProgramConfigurationStateConverter().toJson(state);
}

List<OProgram> _addOProgramAction(AddOProgramEvent event) {
  // get Program that should be added
  var previousProgramList = event.previousProgramList;
  var addThisProgram = event.program; //to be added

  if (previousProgramList.isNotEmpty) {
    // check if already exists in previous list<OProgram>
    for (var i = 0; i < previousProgramList.length; i++) {
      if (previousProgramList[i].id == addThisProgram.id) {
        //TODO: identify delta and show user result
        previousProgramList[i] = addThisProgram;
        return previousProgramList;
      }
    }
  }
  // did not previously exist, so add as new at the beginning
  previousProgramList.insert(0, addThisProgram);
  return previousProgramList;
}

List<OProgram> _deleteSingleOProgramAction(DeleteSingleOProgram event) {
  // get Program that should be added
  var previousProgramList = event.previousProgramList;
  var deleteThisProgram = event.program; //to be added
  // check if List is null
  // for (int i = 0; i < previousProgramList.length; i++) {
  //   if (previousProgramList[i].id == deleteThisProgram.id) {
  //     //TODO: identify delta and show user result
  //     previousProgramList[i] = deleteThisProgram;
  //   }
  // }
  previousProgramList.remove(deleteThisProgram);
  return previousProgramList;
}

// class ReplaceException implements Exception {
//   ReplaceException();
// }
