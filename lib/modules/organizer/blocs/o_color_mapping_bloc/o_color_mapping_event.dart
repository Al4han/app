/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_color_mapping_bloc.dart';

abstract class OColorMappingEvent extends Equatable {
  const OColorMappingEvent();
}

class MapOSubjectToColorEvent extends OColorMappingEvent {
  final int subject;
  final String color;
  final Map<int, String> previousColorList;
  MapOSubjectToColorEvent({
    required this.subject,
    required this.color,
    required this.previousColorList,
  });

  @override
  List<Object> get props => [subject, color, previousColorList];
}

class MapMultiOSubjectToRandomColorEvent extends OColorMappingEvent {
  final List<int> subjects;
  final Map<int, String> previousColorList;
  MapMultiOSubjectToRandomColorEvent({
    required this.subjects,
    required this.previousColorList,
  });

  @override
  List<Object> get props => [subjects, previousColorList];
}

class OColorMappingResetEvent extends OColorMappingEvent {
  const OColorMappingResetEvent();

  @override
  List<Object> get props => [];
}
