/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_lesson_selection_bloc.dart';

enum _OLessonSelectionStateType { base, ready }

class _OLessonSelectionStateConverter
    extends OStateConverter<OLessonSelectionState, _OLessonSelectionStateType> {
  const _OLessonSelectionStateConverter() : super();

  @override
  _OLessonSelectionStateType fromTypeJsonValueInternal(dynamic jsonValue) =>
      _OLessonSelectionStateType.values[jsonValue as int];

  @override
  int toTypeJsonValueInternal(OLessonSelectionState object) {
    _OLessonSelectionStateType type;

    switch (object.runtimeType) {
      case OLessonSelectionReadyState:
        type = _OLessonSelectionStateType.ready;
        break;
      default:
        type = _OLessonSelectionStateType.base;
    }

    return type.index;
  }

  @override
  OLessonSelectionState fromJsonInternal(
      _OLessonSelectionStateType type, Map<String, dynamic> json) {
    final base = OLessonSelectionState.fromJson(json);

    switch (type) {
      case _OLessonSelectionStateType.base:
        return base;
      case _OLessonSelectionStateType.ready:
        return OLessonSelectionReadyState(
          lessons: base.lessons,
        );
    }
  }

  @override
  Map<String, dynamic> toJsonInternal(OLessonSelectionState object) =>
      object.toJson();

  @override
  OLessonSelectionState migrate(Map<String, dynamic> json) {
    final tmp = OLessonSelectionState.fromJson(json);
    return OLessonSelectionReadyState(lessons: tmp.lessons);
  }
}
