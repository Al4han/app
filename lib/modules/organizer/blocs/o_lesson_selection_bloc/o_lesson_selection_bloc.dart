/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../../bloc/local_user_hyd_bloc/local_user_hyd_bloc.dart';
import '../../model/o_lesson.dart';
import '../../model/o_pool.dart';
import '../../model/o_program.dart';
import '../../model/o_subject.dart';
import '../../utility/organizer_state_converter.dart';

part 'o_lesson_selection_bloc.g.dart';
part 'o_lesson_selection_event.dart';
part 'o_lesson_selection_state.dart';
part 'o_lesson_selection_state_converter.dart';

class OLessonSelectionBloc
    extends LocalUserHydBloc<OLessonSelectionEvent, OLessonSelectionState> {
  OLessonSelectionBloc() : super(OLessonSelectionReadyState(lessons: <int>[]));

  @override
  OLessonSelectionState fromJson(Map<String, dynamic> json) {
    try {
      return _OLessonSelectionStateConverter().fromJson(json);
    } on Exception {
      return OLessonSelectionState(lessons: <int>[]);
    }
  }

  @override
  Map<String, dynamic> toJson(OLessonSelectionState state) =>
      _OLessonSelectionStateConverter().toJson(state);

  @override
  Stream<OLessonSelectionState> mapEventToState(
    OLessonSelectionEvent event,
  ) async* {
    yield OLessonSelectionReadyState(lessons: <int>[]);
    if (event is AddOLessonEvent) {
      var listWithAddedLesson = _addOLessonAction(event);
      yield OLessonSelectionReadyState(
        lessons: listWithAddedLesson,
      );
    } else if (event is AddMultipleOLessonsEvent) {
      var listWithAddedLesson = _addMultipleOLessonAction(event);
      yield OLessonSelectionReadyState(
        lessons: listWithAddedLesson,
      );
    } else if (event is RemoveOLessonEvent) {
      var listWithRemovedLesson = _deleteSingleOLessonAction(event);
      yield OLessonSelectionReadyState(lessons: listWithRemovedLesson);
    } else if (event is RemoveMultipleOLessonsEvent) {
      var listWithRemovedLesson = _deleteMultipleOLessonAction(event);
      yield OLessonSelectionReadyState(lessons: listWithRemovedLesson);
    } else if (event is OLessonSelectionResetEvent) {
      var newList = <int>[];
      yield OLessonSelectionReadyState(lessons: newList);
    } else if (event is RemoveOLessonsFromProgramEvent) {
      var newList = _removeOLessonsFromProgram(event);
      yield OLessonSelectionReadyState(lessons: newList);
    }
  }

  List<int> _addOLessonAction(AddOLessonEvent event) {
    var previousLessonList = event.previousLessonList;
    var addThisLesson = event.lesson; //to be added
    // check if List is null
    if (previousLessonList.isEmpty) {
      previousLessonList = <int>[];
    } else if (previousLessonList.isNotEmpty) {
      // check if already exists in previous list<OProgram>
      for (var i = 0; i < previousLessonList.length; i++) {
        if (previousLessonList[i] == addThisLesson) {
          //TODO: identify delta and show user result
          previousLessonList[i] = addThisLesson;
          return previousLessonList;
        }
      }
    }
    previousLessonList.add(addThisLesson);
    return previousLessonList;
  }

  List<int> _removeOLessonsFromProgram(RemoveOLessonsFromProgramEvent event) {
    var previousLessonList = event.previousLessonList;
    // check if List is null
    if (previousLessonList.isEmpty) {
      previousLessonList = <int>[];
    } else if (previousLessonList.isNotEmpty) {
      // delete only lessons that are in this program
      var programLessons = <int>[];
      try {
        if (event.program.pools.isNotEmpty) {
          for (var pool in event.program.pools) {
            for (var subject in pool.subjects) {
              for (var lesson in subject.lessons) {
                if (lesson.id != null) {
                  programLessons.add(lesson.id!);
                }
              }
            }
          }
        }
        // programLessons.forEach((toBeDeletedLesson) {
        for (var toBeDeletedLesson in programLessons) {
          previousLessonList.remove(toBeDeletedLesson);
        }
      } on Exception {
        debugPrint("couldnt remove lessons from program");
      }
      // for (int i = 0; i < previousLessonList.length; i++) {
      //   if (previousLessonList[i] == addThisLesson) {
      //     //TODO: identify delta and show user result
      //     previousLessonList[i] = addThisLesson;
      //     return previousLessonList;
      //   }
      // }
    }
    return previousLessonList;
  }

  List<int> _addMultipleOLessonAction(AddMultipleOLessonsEvent event) {
    var previousLessonList = event.previousLessonList;
    var addTheseLessons = event.lessons; //to be added
    // check if List is null
    if (addTheseLessons.isEmpty) return previousLessonList;
    // if (previousLessonList.isEmpty) {
    //   previousLessonList = <int>[];
    // } else if (previousLessonList.isNotEmpty) {
    // check if already exists in previous list<OProgram>
    for (var j = 0; j < addTheseLessons.length; j++) {
      if (previousLessonList.contains(addTheseLessons[j]) == false) {
        //TODO: identify delta and show user result
        debugPrint("adding ${addTheseLessons[j]}");
        previousLessonList.add(addTheseLessons[j]);
      }
    }
    // }
    // previousLessonList.add(addThisLesson);
    return previousLessonList;
  }

  List<int> _deleteSingleOLessonAction(RemoveOLessonEvent event) {
    // get Program that should be added
    var previousLessonList = event.previousLessonList;
    var deleteThisLesson = event.lesson; //to be added
    // check if List is null
    if (previousLessonList.isEmpty) {
      previousLessonList = <int>[];
    }
    var toBeDeletedLesson = -1;
    for (var i = 0; i < previousLessonList.length; i++) {
      if (previousLessonList[i] == deleteThisLesson) {
        toBeDeletedLesson = previousLessonList[i];
      }
    }

    /// note that it doesnt remove the lesson provided by the event
    /// it actually removes the lesson found with the same id!
    /// that's because even though some lessons have the same id
    /// but are filled differently by the organizer api (unfortunately)
    previousLessonList.remove(toBeDeletedLesson);
    return previousLessonList;
  }

  List<int> _deleteMultipleOLessonAction(RemoveMultipleOLessonsEvent event) {
    var previousLessonList = event.previousLessonList;
    var deleteTheseLessons = event.lessons; //to be added
    // check if List is null
    if (previousLessonList.isEmpty) {
      previousLessonList = <int>[];
    } else {
      if (deleteTheseLessons.isNotEmpty) {
        for (var removeThisLesson in deleteTheseLessons) {
          previousLessonList.remove(removeThisLesson);
        }
      }
    }
    return previousLessonList;
  }
}
