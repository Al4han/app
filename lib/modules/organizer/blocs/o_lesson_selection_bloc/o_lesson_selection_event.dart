/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_lesson_selection_bloc.dart';

abstract class OLessonSelectionEvent extends Equatable {
  const OLessonSelectionEvent();
}

class AddOLessonEvent extends OLessonSelectionEvent {
  final int lesson;
  final List<int> previousLessonList;
  AddOLessonEvent({required this.lesson, required this.previousLessonList});

  @override
  List<Object> get props => [lesson, previousLessonList];
}

class AddMultipleOLessonsEvent extends OLessonSelectionEvent {
  final List<int> lessons;
  final List<int> previousLessonList;
  AddMultipleOLessonsEvent(
      {required this.lessons, required this.previousLessonList});

  @override
  List<Object> get props => [lessons, previousLessonList];
}

class RemoveOLessonEvent extends OLessonSelectionEvent {
  final int lesson;
  final List<int> previousLessonList;
  RemoveOLessonEvent({required this.lesson, required this.previousLessonList});

  @override
  List<Object> get props => [lesson, previousLessonList];
}

class RemoveMultipleOLessonsEvent extends OLessonSelectionEvent {
  final List<int> lessons;
  final List<int> previousLessonList;
  RemoveMultipleOLessonsEvent(
      {required this.lessons, required this.previousLessonList});

  @override
  List<Object> get props => [lessons, previousLessonList];
}

class OLessonSelectionResetEvent extends OLessonSelectionEvent {
  const OLessonSelectionResetEvent();

  @override
  List<Object> get props => [];
}

class RemoveOLessonsFromProgramEvent extends OLessonSelectionEvent {
  RemoveOLessonsFromProgramEvent(
      {required this.program, required this.previousLessonList});
  final OProgram program;
  final List<int> previousLessonList;

  @override
  List<Object> get props => [];
}
