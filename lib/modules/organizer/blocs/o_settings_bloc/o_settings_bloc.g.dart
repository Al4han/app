// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_settings_bloc.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension OSettingsStateCopyWith on OSettingsState {
  OSettingsState copyWith({
    List<OBlockIntervals>? blockIntervals,
    List<OElement>? deletedOElements,
    List<OElement>? editedOElements,
    List<String>? programBlacklist,
    int? selectedInterval,
  }) {
    return OSettingsState(
      blockIntervals: blockIntervals ?? this.blockIntervals,
      deletedOElements: deletedOElements ?? this.deletedOElements,
      editedOElements: editedOElements ?? this.editedOElements,
      programBlacklist: programBlacklist ?? this.programBlacklist,
      selectedInterval: selectedInterval ?? this.selectedInterval,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OSettingsState _$OSettingsStateFromJson(Map json) => OSettingsState(
      selectedInterval: json['selectedInterval'] as int? ?? 0,
      blockIntervals: (json['blockIntervals'] as List<dynamic>?)
          ?.map((e) =>
              OBlockIntervals.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
      editedOElements: (json['editedOElements'] as List<dynamic>?)
          ?.map((e) => OElement.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
      deletedOElements: (json['deletedOElements'] as List<dynamic>?)
          ?.map((e) => OElement.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
      programBlacklist: (json['programBlacklist'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$OSettingsStateToJson(OSettingsState instance) =>
    <String, dynamic>{
      'selectedInterval': instance.selectedInterval,
      'blockIntervals': instance.blockIntervals.map((e) => e.toJson()).toList(),
      'editedOElements':
          instance.editedOElements.map((e) => e.toJson()).toList(),
      'deletedOElements':
          instance.deletedOElements.map((e) => e.toJson()).toList(),
      'programBlacklist': instance.programBlacklist,
    };
