/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

class AssetCacher extends StatelessWidget {
  final Widget child;

  static const List<String> _assets = [
    'assets/images/tinycampus/logofont_block_default.png',
    'assets/images/homescreen/module-qanda.png',
    'assets/images/homescreen/module-business-card.png',
    'assets/images/homescreen/module-organizer.png',
    'assets/images/homescreen/module-cafeteria.png',
    'assets/images/homescreen/module-semester-fee.png',
    'assets/images/homescreen/module-important-links.png',
    'assets/images/homescreen/module-comingnext.png',
    'assets/images/homescreen/module-zs.png',
    'assets/images/homescreen/coming-next.png',
    'assets/images/logos/thm-logo.png',
  ];

  const AssetCacher({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    for (var asset in _assets) {
      precacheImage(AssetImage(asset), context);
    }
    return child;
  }
}
