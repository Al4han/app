/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../tc_theme.dart';

ThemeData theme = ThemeData(
  primaryIconTheme: IconThemeData(color: Colors.white),
  appBarTheme: AppBarTheme(
    titleTextStyle: TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.w400,
      letterSpacing: -1.0,
      color: Colors.white,
      wordSpacing: 2.0,
    ),
    iconTheme: IconThemeData(color: Colors.white),
    actionsIconTheme: IconThemeData(color: Colors.white),
    backgroundColor: CorporateColors.darkModeContent,
  ),
  primaryColorLight: Colors.white,
  toggleableActiveColor: Colors.white,
  indicatorColor: Colors.white,
  // above is new
  iconTheme: IconThemeData(color: Colors.white),
  brightness: Brightness.dark,
  primaryColor: CorporateColors.darkModeContent,
  fontFamily: 'Lato',
  primaryTextTheme: TextTheme(
    headline6: TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.w400,
      letterSpacing: -1.0,
      color: Colors.white,
      wordSpacing: 2.0,
    ),
  ),
  backgroundColor: CorporateColors.darkModeBackground,
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      primary: CorporateColors.darkModeContent,
    ),
  ),
  textTheme: TextTheme(
    bodyText1: TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w400,
      fontSize: 16.0,
      letterSpacing: -0.4,
    ),
    bodyText2: TextStyle(
      color: Colors.white,
    ),
    button: TextStyle(
      color: Colors.white,
    ),
    overline: TextStyle(
      color: Colors.white,
    ),
    headline1: TextStyle(
      fontSize: 32,
      fontWeight: FontWeight.w500,
      letterSpacing: -1.2,
      color: Colors.white,
    ),
    headline2: TextStyle(
      fontSize: 26,
      fontWeight: FontWeight.w400,
      letterSpacing: -1.0,
      color: Colors.white,
    ),
    headline3: TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.w400,
      letterSpacing: -1.1,
      color: Colors.white,
      wordSpacing: 2.0,
    ),
    headline4: TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.w400,
      letterSpacing: -1.1,
      color: Colors.white,
      wordSpacing: 2.0,
    ),
    headline5: TextStyle(
      color: Colors.white,
    ),
    headline6: TextStyle(
      color: Colors.white,
      fontSize: 16.0,
      fontWeight: FontWeight.w600,
    ),
    caption: TextStyle(
      color: CorporateColors.passiveBackgroundDark,
    ),
    subtitle1: TextStyle(
      color: CorporateColors.passiveBackgroundDark,
    ),
    subtitle2: TextStyle(
      color: CorporateColors.passiveBackgroundDark,
    ),
  ).apply(
    bodyColor: Colors.white,
  ),
  textSelectionTheme: TextSelectionThemeData(
    cursorColor: CorporateColors.tinyCampusBlue,
  ),
  colorScheme: ColorScheme.fromSwatch(
    brightness: Brightness.dark,
    primarySwatch: createMaterialColor(Colors.white),
  ).copyWith(
    secondary: createMaterialColor(Colors.white),
  ),
  // DM ONLY
  cardTheme: CardTheme(color: CorporateColors.darkModeContent),
  buttonTheme: ButtonThemeData(
    buttonColor: CorporateColors.darkModeContent,
  ),
);
