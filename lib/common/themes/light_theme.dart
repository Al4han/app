/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../tc_theme.dart';

ThemeData theme = ThemeData(
  primaryIconTheme: IconThemeData(color: CorporateColors.tinyCampusIconGrey),
  appBarTheme: AppBarTheme(
    titleTextStyle: TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.w400,
      letterSpacing: -1.0,
      color: CorporateColors.tinyCampusBlue,
      wordSpacing: 2.0,
    ),
    iconTheme: IconThemeData(color: CorporateColors.tinyCampusTextSoft),
    actionsIconTheme: IconThemeData(color: CorporateColors.tinyCampusTextSoft),
    backgroundColor: Colors.white,
  ),
  primaryColorLight: CorporateColors.tinyCampusBlue,
  // sliderTheme: SliderThemeData(),
  toggleableActiveColor: CorporateColors.tinyCampusBlue,
  indicatorColor: CorporateColors.tinyCampusBlue,
  // above is new
  iconTheme: IconThemeData(color: CorporateColors.tinyCampusIconGrey),
  brightness: Brightness.light,
  primaryColor: Colors.white,
  fontFamily: 'Lato',
  primaryTextTheme: TextTheme(
    headline6: TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.w400,
      letterSpacing: -1.0,
      color: CorporateColors.tinyCampusBlue,
      wordSpacing: 2.0,
    ),
  ),
  backgroundColor: CorporateColors.passiveBackgroundLight,
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      primary: CorporateColors.tinyCampusBlue,
    ),
  ),
  textTheme: TextTheme(
    bodyText1: TextStyle(
      color: CorporateColors.tinyCampusTextStandard,
      fontWeight: FontWeight.w400,
      fontSize: 16.0,
      letterSpacing: -0.4,
    ),
    bodyText2: TextStyle(
      color: Colors.blueGrey[800],
    ),
    button: TextStyle(
      color: CorporateColors.tinyCampusBlue,
    ),
    overline: TextStyle(
      color: Colors.blueGrey[800],
    ),
    headline1: TextStyle(
      fontSize: 32,
      fontWeight: FontWeight.w500,
      letterSpacing: -1.2,
      color: CorporateColors.tinyCampusBlue,
    ),
    headline2: TextStyle(
      fontSize: 26,
      fontWeight: FontWeight.w400,
      letterSpacing: -1.0,
      color: CorporateColors.tinyCampusBlue,
    ),
    headline3: TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.w400,
      letterSpacing: -1.1,
      color: CorporateColors.tinyCampusBlue,
      wordSpacing: 2.0,
    ),
    headline4: TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.w400,
      letterSpacing: -1.1,
      color: CorporateColors.tinyCampusBlue,
      wordSpacing: 2.0,
    ),
    headline5: TextStyle(
      color: Colors.blueGrey[500],
    ),
    headline6: TextStyle(
      color: CorporateColors.tinyCampusBlue,
      fontSize: 16.0,
      fontWeight: FontWeight.w600,
    ),
    caption: TextStyle(
      color: Colors.blueGrey[800],
    ),
    subtitle1: TextStyle(
      color: Colors.blueGrey[800],
    ),
    subtitle2: TextStyle(
      color: Colors.blueGrey[800],
    ),
  ).apply(
    bodyColor: CorporateColors.tinyCampusTextStandard,
  ),
  textSelectionTheme: TextSelectionThemeData(
    cursorColor: CorporateColors.tinyCampusBlue,
  ),
  colorScheme: ColorScheme.fromSwatch(
    brightness: Brightness.light,
    primarySwatch: createMaterialColor(CorporateColors.tinyCampusBlue),
  ).copyWith(
    secondary: CorporateColors.accentColor,
  ),
  // DM ONLY
  // cardTheme: CardTheme(color: CorporateColors.darkModeContent),
  // buttonTheme: ButtonThemeData(
  //   buttonColor: CorporateColors.darkModeContent,
  // ),
);
