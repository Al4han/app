/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../tc_theme.dart';
import 'tc_form_field.dart';

/// Noch nicht final

typedef OnTap = void Function();

/// This class contains the two Buttons which are displayed at
/// one [TCFormField] when it's in focus
class TCFormFieldButtons extends StatelessWidget {
  final OnTap _del;
  final OnTap _save;
  final bool deleteButtonVisibility;
  final bool saveButtonVisibility;

  /// The constructor gets the Functions of the buttons, when they are pressed.
  TCFormFieldButtons({
    Key? key,
    required OnTap del,
    required OnTap save,
    this.saveButtonVisibility = false,
    this.deleteButtonVisibility = false,
  })  : _del = del,
        _save = save,
        super(key: key);

  @override
  Widget build(BuildContext context) => Positioned(
        bottom: -15,
        right: 17,
        child: Row(
          children: <Widget>[
            if (deleteButtonVisibility)
              Container(
                child: FloatingActionButton(
                  elevation: 0,
                  heroTag: "delete",
                  onPressed: _del,
                  backgroundColor: Colors.white,
                  shape: CircleBorder(
                    side: BorderSide(
                      color: CorporateColors.tinyCampusBlue,
                      width: 2,
                    ),
                  ),
                  child: Icon(
                    Icons.delete,
                    color: CorporateColors.tinyCampusBlue,
                  ),
                ),
                height: 40,
                width: 40,
                margin: EdgeInsets.only(right: 10),
              ),
            if (saveButtonVisibility)
              SizedBox(
                child: FloatingActionButton(
                  elevation: 0,
                  heroTag: "okay",
                  onPressed: _save,
                  child: Icon(
                    Icons.check,
                    color: Colors.white,
                  ),
                ),
                height: 40,
                width: 40,
              ),
          ],
        ),
      );
}

/// This class containsthe Head part of one [TCFormField]
class TCFormFieldHeader extends StatelessWidget {
  /// The Constructor of this class
  const TCFormFieldHeader({
    Key? key,
    required this.widget,
    required bool wasEdited,
    required bool error,
    required bool wasSaved,
  })  : _wasEdited = wasEdited,
        _error = error,
        _wasSaved = wasSaved,
        super(key: key);

  /// Must be public through this.
  final TCFormField widget;
  final bool _wasEdited;
  final bool _error;
  final bool _wasSaved;

  @override
  Widget build(BuildContext context) => Container(
        color: Theme.of(context).primaryColor,
        child: Row(
          children: <Widget>[
            Container(
              color: Theme.of(context).primaryColor,
              padding: EdgeInsets.symmetric(horizontal: 5.0),
              child: Text(
                widget.formFieldModel.title,
                style: Theme.of(context).textTheme.headline4,
              ),
              alignment: Alignment.centerLeft,
            ),
            if (_wasEdited)
              Container(
                margin: EdgeInsets.only(left: 2, right: 4),
                decoration: BoxDecoration(
                  color: _error
                      ? CorporateColors.ppAnswerWrongRed
                      : CorporateColors.ppAnswerCorrectGreen,
                  shape: BoxShape.circle,
                ),
                child: Padding(
                  padding: EdgeInsets.all(2.0),
                  child: Icon(
                    _error ? Icons.close : Icons.check,
                    size: 12,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
            if (widget.formFieldModel.isPersistent && _wasEdited)
              Container(
                margin: EdgeInsets.only(left: 6),
                padding: EdgeInsets.only(right: 6),
                child: I18nText(
                  'modules.business_cards.'
                  'vcard.form.personal.'
                  '${_wasSaved && !_error ? '' : 'not_'}saved',
                  child: Text(
                    '',
                    style: TextStyle(
                      fontSize: 12,
                      color: Color.fromARGB(255, 122, 122, 122),
                    ),
                  ),
                ),
              ),
          ],
        ),
      );
}

/// This class contains the counter of one [TCFormField]
class TCFormFieldTextCounter extends StatelessWidget {
  /// The Constructor of this class
  const TCFormFieldTextCounter({
    Key? key,
    required int counter,
    required this.widget,
  })  : _counter = counter,
        super(key: key);

  final int _counter;

  /// Must be public through this.
  final TCFormField widget;

  @override
  Widget build(BuildContext context) => Positioned(
        bottom: -6,
        left: 12.0,
        child: Container(
          color: Theme.of(context).primaryColor,
          padding: EdgeInsets.symmetric(horizontal: 5.0),
          child: I18nText(
            'modules.business_cards.vcard.form.personal.chars_of_available',
            translationParams: <String, String>{
              'count': '${widget.formFieldModel.maxLength - _counter}',
              'max': '${widget.formFieldModel.maxLength}',
            },
            child: Text(
              '',
              style: TextStyle(
                fontSize: 14.0,
                letterSpacing: -0.4,
                color: Color.fromARGB(255, 122, 122, 122),
              ),
            ),
          ),
        ),
      );
}
