/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../tc_theme.dart';

// TODO: Cleanup
class TCDialog {
  static void showCustomDialog({
    required BuildContext context,
    VoidCallback? onConfirm,
    ValueListenable<bool>? onConfirmEnabled,
    VoidCallback? onCancel,

    /// Default value needs context, see [generateDialog]
    String? functionActionText,
    Color functionActionColor = CorporateColors.tinyCampusBlue,
    Widget? alternativeWidget,
    bool popAfterFunction = true,
    required String headlineText,
    required String bodyText,
  }) {
    var alert = generateDialog(
      bodyText: bodyText,
      context: context,
      onConfirm: onConfirm,
      onConfirmEnabled: onConfirmEnabled,
      onCancel: onCancel,
      functionActionText: functionActionText,
      headlineText: headlineText,
      functionActionColor: functionActionColor,
      alternativeWidget: alternativeWidget,
      popAfterFunction: popAfterFunction,
    );

    showDialog(
      context: context,
      builder: (context) => alert,
    );
  }

  static AlertDialog generateDialog({
    required BuildContext context,
    VoidCallback? onConfirm,
    ValueListenable<bool>? onConfirmEnabled,

    /// Default value needs context, see [generateDialog]
    String? functionActionText,
    VoidCallback? onCancel,
    Color functionActionColor = CorporateColors.tinyCampusBlue,
    TextStyle headlineFontStyle = CorporateTextStyles.dialogHeadlineStyle,
    TextStyle bodyFontStyle = CorporateTextStyles.dialogBodyStyle,
    Widget? alternativeWidget,
    required String headlineText,
    required String bodyText,
    bool popAfterFunction = true,
  }) {
    Widget cancelButton = FlatButton(
      child: Text(
        FlutterI18n.translate(context, 'common.actions.cancel').toUpperCase(),
      ),
      onPressed: () {
        if (onCancel == null && kDebugMode) {
          debugPrint(
              "[TC DIALOG] No function 'onCancel' was passed or executed");
        }
        onCancel?.call();
        Navigator.of(context).pop();
      },
    );
    Widget confirmButton = ValueListenableBuilder<bool>(
      valueListenable: onConfirmEnabled ?? ValueNotifier(true),
      builder: (_, value, __) => FlatButton(
        textColor: functionActionColor,
        disabledTextColor: Colors.blueGrey,
        child: Text(
          functionActionText?.toUpperCase() ??
              FlutterI18n.translate(context, 'common.actions.confirm'),
          textAlign: TextAlign.start,
        ),
        onPressed: !value
            ? null
            : () {
                if (onConfirm == null) {
                  debugPrint(
                    "[TC DIALOG] No function 'onConfirm' "
                    "was passed or executed",
                  );
                }
                onConfirm?.call();

                if (popAfterFunction) Navigator.of(context).pop();
              },
      ),
    );
    return AlertDialog(
      backgroundColor: Theme.of(context).backgroundColor,
      content: IntrinsicHeight(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                // Icon(Icons.delete),
                Flexible(
                  child: Text(
                    headlineText,
                    textAlign: TextAlign.center,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style: headlineFontStyle.copyWith(
                        color: CurrentTheme().tcBlue),
                  ),
                ),
              ],
            ),
            alternativeWidget != null
                ? Expanded(child: alternativeWidget)
                : Container(
                    constraints: BoxConstraints(
                      maxHeight: 200.0,
                    ),
                    margin: EdgeInsets.symmetric(vertical: 38.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Text(
                        bodyText,
                        style: bodyFontStyle,
                      ),
                    ),
                  ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                cancelButton,
                confirmButton,
              ],
            )
          ],
        ),
      ),
    );
  }
}
