/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

import '../../tc_markdown_stylesheet.dart';

class CodeOfConductPage extends StatelessWidget {
  const CodeOfConductPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: I18nText('common.coc.title'),
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        body: Card(
          margin: EdgeInsets.all(4.0),
          color: Theme.of(context).primaryColor,
          child: Markdown(
            data: FlutterI18n.translate(
              context,
              'common.coc.text',
            ),
            styleSheet: TinyCampusMarkdownStylesheet(context),
            physics: BouncingScrollPhysics(),
          ),
        ),
      );
}
