/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter_linkify/flutter_linkify.dart';

var _urlRegex = RegExp(
  // There are multiple solutions, try to find the best fitting

  // Passed 30/36 tests
  r'^((?:.|\n)*?)(www.[^\s\/$.?#].[^\s]*|(?:https?):\/\/[^\s/$.?#].[^\s]*)',

  // Passed 13/36 tests
  // r'^([\.\/:a-zA-Z0-9#_?=-]+%\(([a-zA-Z0-9_-]+)\)s)+[a-zA-Z0-9_-]*$',

  // Passed 21/36 tests
  // r'^((?:.|\n)*?|(?:[.]{2,}))(www.[^\s\/$.?#].[^\s]*|(?:(?:https?):\/\/)]?[^\s\/$.?#].[^\s]*|(?:(?:[^\s\/$.?\d#]+)(?:[.])(?![.]))[^\s]+)',

  caseSensitive: false,
);

class TCUrlLinkifier extends Linkifier {
  const TCUrlLinkifier();

  @override
  List<LinkifyElement> parse(
      List<LinkifyElement> elements, LinkifyOptions options) {
    final list = <LinkifyElement>[];

    for (final element in elements) {
      if (element is TextElement) {
        var match = _urlRegex.firstMatch(element.text);
        // var match = _urlRegex.firstMatch(element.text);

        if (match == null) {
          list.add(element);
        } else {
          final text = element.text.replaceFirst(match.group(0)!, '');

          if (match.group(1) != null && match.group(1)!.isNotEmpty) {
            list.add(TextElement(match.group(1)!));
          }

          if (match.group(2) != null && match.group(2)!.isNotEmpty) {
            var originalUrl = match.group(2)!;
            String? end;

            if (options.excludeLastPeriod &&
                originalUrl[originalUrl.length - 1] == ".") {
              end = ".";
              originalUrl = originalUrl.substring(0, originalUrl.length - 1);
            }

            var url = originalUrl;

            // TODO: discuss if this is needed at all
            // if (originalUrl.contains("http://") && options.defaultToHttps ??
            //     false) {
            //   originalUrl = originalUrl.replaceAll("http://", "https://");
            // }
            // if (options.defaultToHttps ?? false) {
            //   if (!originalUrl.contains("https://")) {
            //     originalUrl = "https://$originalUrl";
            //   }
            // } else {
            //   if (!originalUrl.contains("http://")) {
            //     originalUrl = "http://$originalUrl";
            //   }
            // }

            if (options.humanize || options.removeWww) {
              if (options.humanize) {
                url = url.replaceFirst(RegExp(r'https?://'), '');
              }
              if (options.removeWww) {
                url = url.replaceFirst(RegExp(r'www\.'), '');
              }

              list.add(UrlElement(
                originalUrl,
                url,
              ));
            } else {
              list.add(UrlElement(originalUrl));
            }

            if (end != null) {
              list.add(TextElement(end));
            }
          }

          if (text.isNotEmpty) {
            list.addAll(parse([TextElement(text)], options));
          }
        }
      } else {
        list.add(element);
      }
    }

    return list;
  }
}
