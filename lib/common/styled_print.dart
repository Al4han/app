/*
 * Copyright 2020-2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
import 'package:colorize/colorize.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';

void eprint(
  dynamic input, {
  Object? caller,
}) {
  if (kReleaseMode) {
    return;
  }
  var content = "";
  if (input is String) {
    content = input;
  } else {
    try {
      content = input?.toString() ?? "null";
    } on Exception catch (e) {
      eprint("Could not print: $e");
    }
  }
  _cprint(
    content,
    bgColor: Styles.BG_RED,
    caller: caller,
    color: Styles.WHITE,
    prefix: "[ERROR]",
  );
}

void sprint(
  dynamic input, {
  PrintStyle? style,
  String? prefix,
  Object? caller,
}) {
  if (kReleaseMode) {
    return;
  }
  var content = "";
  if (input is String) {
    content = input;
  } else {
    try {
      content = input?.toString() ?? "null";
    } on Exception catch (e) {
      eprint("Could not print: $e");
    }
  }
  switch (style) {
    case PrintStyle.praxisPhase:
      _cprint(
        content,
        bgColor: Styles.BG_LIGHT_GREEN,
        caller: caller,
        color: Styles.BLACK,
        prefix: prefix ?? "[PP]",
      );
      break;
    case PrintStyle.sport:
      _cprint(
        content,
        bgColor: Styles.BG_BLUE,
        caller: caller,
        color: Styles.WHITE,
        prefix: prefix ?? "[PP]",
      );
      break;
    case PrintStyle.error:
      _cprint(
        content,
        bgColor: Styles.BG_RED,
        caller: caller,
        color: Styles.WHITE,
        prefix: prefix ?? "[ERROR]",
      );
      break;
    case PrintStyle.attention:
      _cprint(
        content,
        bgColor: Styles.BG_YELLOW,
        caller: caller,
        color: Styles.BLACK,
        prefix: prefix ?? "[ATTENTION]",
      );
      break;
    case PrintStyle.bloc:
      _cprint(
        content,
        bgColor: Styles.BG_LIGHT_MAGENTA,
        caller: caller,
        color: Styles.BLACK,
        prefix: prefix ?? "[BLOC]",
      );
      break;
    case PrintStyle.blocEvent:
      _cprint(
        content,
        bgColor: Styles.BG_MAGENTA,
        caller: caller,
        color: Styles.BLACK,
        prefix: prefix ?? "[BLOC_EVENT]",
      );
      break;
    default:
      _cprint(
        content,
        bgColor: Styles.BG_BLACK,
        caller: caller,
        color: Styles.WHITE,
        prefix: prefix ?? "[UNSTYLED]",
      );
      break;
  }
}

void _cprint(
  String input, {
  Object? caller,
  String prefix = "[NO_PREFIX]",
  Styles color = Styles.BLACK,
  Styles bgColor = Styles.BG_LIGHT_GREEN,
}) {
  if (kReleaseMode) {
    return;
  }
  var timeString = DateFormat('HH:mm:ss SSS').format(DateTime.now());
  var cTime = Colorize(timeString)
    ..bgDarkGray()
    ..black();

  var optionalCaller =
      caller == null ? Colorize("") : Colorize(" (Caller: $caller)")
        ..white();

  var cInput = Colorize(input)
    ..apply(color)
    ..apply(bgColor);

  var domainPrefix = Colorize(prefix)
    ..apply(color)
    ..apply(bgColor);

  debugPrint('$domainPrefix $cTime : $cInput $optionalCaller');
}

enum PrintStyle { praxisPhase, sport, error, attention, bloc, blocEvent }
