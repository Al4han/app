/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

// TODO: Cleanup
class AssetAdapter {
  static const _logoList = [
    'assets/images/kim/KIM_logo_illustration_blue.jpg',
    'assets/images/kim/KIM_logo_illustration_red.jpg',
    'assets/images/kim/KIM_logo_illustration_green.jpg',
  ];

  // TODO: find a better way to do this (see also: `lib/Pages/home/model/module.dart`)!
  static const _moduleList = [
    'assets/images/homescreen/module-qanda.png',
    'assets/images/homescreen/module-business-card.png',
    'assets/images/homescreen/module-organizer.png',
    'assets/images/homescreen/module-cafeteria.png',
    'assets/images/homescreen/module-semester-fee.png',
    'assets/images/homescreen/module-comingnext.png',
    'assets/images/homescreen/module-important-links.png',
    'assets/images/homescreen/module-zs.png',
    'assets/images/homescreen/module-practice-phase.png',
    'assets/images/homescreen/module-sport.png',
    'assets/images/homescreen/module-bouncy-ball.png'
  ];

  static String kimLogo(KIMLogo identifier) => _logoList[identifier.index];

  static String kimLogoRandom() =>
      _logoList[Random().nextInt(_logoList.length)];

  // TODO: find a better way to do this (see also: `lib/Pages/home/model/module.dart`)!
  static String module(ModuleModel identifier) => _moduleList[identifier.index];

  static String moduleRandom() =>
      _moduleList[Random().nextInt(_moduleList.length)];

  static String thmLogo() => 'assets/images/logos/thm-logo.png';

  static String comingSoonCanteen() =>
      'assets/images/comingsoon/coming_soon_canteen.png';

  static String comingSoonOrganizer() =>
      'assets/images/comingsoon/coming_soon_organizer.png';

  static String tinyCampusLogo() =>
      'assets/images/tinycampus/logofont_horizontal.png';

  static String tinyCampusLogoDarkMode() =>
      'assets/images/tinycampus/logofont_white_horizontalxxhdpi.png';

  static String tinyCampusLogoWhite() =>
      'assets/images/tinycampus/logo_whitexhdpi.png';

  static String tinyCampusLogoWithPadding() =>
      'assets/images/tinycampus/logo_with_padding.png';
  static String ppAboutPageTable() => 'assets/images/praxis_phase/table.jpg';

  static String exercisesJson() => 'assets/json/sport.exercises.json';
}

enum KIMLogo { blue, red, green }

// TODO: find a better way to do this (see also: `lib/Pages/home/model/module.dart`)!
enum ModuleModel {
  qAndA, //       'assets/images/homescreen/module-qanda.png',
  businessCard, //     '    'assets/images/homescreen/module-business-card.png',
  organizer, //         'assets/images/homescreen/module-organizer.png',
  cafeteria, //         'assets/images/homescreen/module-cafeteria.png',
  semesterFee, //     '    'assets/images/homescreen/module-semester-fee.png',
  comingNext, //         'assets/images/homescreen/module-comingnext.png',
  importantLinks, //     '    'assets/images/homescreen/module-important-links.png',
  zs, //         'assets/images/homescreen/module-zs.png',
  practicePhase, //     '    'assets/images/homescreen/module-practice-phase.png',
  sport, // 'assets/images/homescreen/module-sport.png'
  bouncyBall,
}
