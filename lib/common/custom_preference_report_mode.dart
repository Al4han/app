/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:io';

import 'package:catcher/mode/dialog_report_mode.dart';
import 'package:catcher/model/report.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'tc_theme.dart';
import 'widgets/dialog/tc_dialog.dart';

class CustomPreferenceReportMode extends DialogReportMode {
  @override
  void requestAction(Report report, BuildContext? context) {
    handleError(report, context);
  }

  Future<void> handleError(Report report, BuildContext? context) async {
    // Only if user accepted to send reports at all
    // Otherwise, do nothing
    if (await userAcceptsToReportSilently()) {
      final error = report.error;
      if (error is String) {
        if (error.contains("LocalizationException")) {
          super.onActionConfirmed(report);
          return;
        }
      }
      if (error is SocketException) {
        // User is offline, so we can'ts end it anyway.
        // TODO: Schedule this report, send later. Implement this functionality
        debugPrint(report.toString());
        return;
      }
      if (await shouldOpenDialog()) {
        if (context != null) {
          _showDialog(report, context);
        }
      } else {
        super.onActionConfirmed(report);
      }
    }
  }

// TODO: this can be optimized, because it always loads the disk
  Future<bool> userAcceptsToReportSilently() async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.getBool('accept_reporting') == null) {
      await prefs.setBool('accept_reporting', kDebugMode);
    }
    return prefs.getBool('accept_reporting') ?? false;
  }

// TODO: this can be optimized, because it always loads the disk
  Future<bool> shouldOpenDialog() async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.getBool('dialog_mode') == null) {
      await prefs.setBool('dialog_mode', kDebugMode);
    }
    var show = prefs.getBool('dialog_mode') ?? kDebugMode;
    return show;
  }

  Future<T?> _showDialog<T>(Report report, BuildContext context) =>
      showDialog<T>(
        context: context,
        barrierDismissible: true,
        builder: (context) => _buildMaterialDialog(report, context),
      );

  AlertDialog _buildTCDialog(Report report, BuildContext context) =>
      TCDialog.generateDialog(
        context: context,
        onConfirm: () => _onAcceptReportClicked(context, report),
        functionActionText:
            FlutterI18n.translate(context, 'common.actions.send'),
        functionActionColor: CorporateColors.tinyCampusBlue,
        headlineText: report.error.toString(),
        bodyText: report.stackTrace.toString(),
        bodyFontStyle: Theme.of(context).textTheme.bodyText2 ??
            CorporateTextStyles.dialogBodyStyle,
        popAfterFunction: false,
      );

  Widget _buildMaterialDialog(Report report, BuildContext context) =>
      _buildTCDialog(report, context);

  void _onAcceptReportClicked(BuildContext context, Report report) {
    super.onActionConfirmed(report);
    Navigator.pop(context);
  }

  @override
  bool isContextRequired() => true;
}
