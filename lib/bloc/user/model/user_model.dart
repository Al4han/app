/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:equatable/equatable.dart';
import 'package:jdenticon_dart/jdenticon_dart.dart';
import 'package:json_annotation/json_annotation.dart';

import 'badge_model.dart';

part 'user_model.g.dart';

// TODO: Add fields: campus and program?
/// A model that describes the User.
@JsonSerializable(anyMap: true, explicitToJson: true)
class User extends Equatable {
  /// Display name of this.
  final String displayName;

  /// Registered account name
  final String userName;

  /// Level of this.
  final int level;

  /// Overall count of hearts received
  final int totalHearts;

  /// Hearts sorted by Category
  final Map<String, int> hearts;

  /// Badges available
  final List<Badge> badges;

  /// Is the user verified
  final bool verified;

  /// Has the user got an email
  final bool emailSent;

  /// Get all achieved badges
  List<Badge> get achievedBadges =>
      badges.where((badge) => badge.unlockDate != null).toList();

  /// Get all locked badges
  List<Badge> get lockedBadges =>
      badges.where((badge) => badge.unlockDate == null).toList();

  /// Getter for the generated avatar
  @JsonKey(ignore: true)
  String get avatar => Jdenticon.toSvg(
        userName,
        size: 110,
      );

  /// Constructor for [User].
  const User({
    required this.displayName,
    required this.userName,
    required this.level,
    required this.totalHearts,
    required this.hearts,
    required this.badges,
    required this.verified,
    required this.emailSent,
  });

  /// Creates the placeholder [User].
  factory User.placeholder() => const User(
        displayName: "Max Mustermann",
        userName: 'musterMax',
        level: 0,
        totalHearts: 0,
        hearts: {},
        badges: [],
        verified: false,
        emailSent: false,
      );

  /// Creates a [User] from Json.
  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  /// Creates a Json from [User].
  Map<String, dynamic> toJson() => _$UserToJson(this);

  @override
  List<Object> get props => [
        displayName,
        level,
        totalHearts,
        hearts,
        badges,
        userName,
      ];
}
