// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'badge_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Badge _$BadgeFromJson(Map json) => Badge(
      badgeId: json['badgeId'] as int,
      asset: json['asset'] as String,
      animationLevel: json['animationLevel'] as String,
      title: json['title'] as String,
      description: json['description'] as String,
      category: json['category'] as String,
      unlockDate: json['unlockDate'] == null
          ? null
          : DateTime.parse(json['unlockDate'] as String),
      conditions: (json['conditions'] as List<dynamic>)
          .map((e) => Conditions.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
    );

Map<String, dynamic> _$BadgeToJson(Badge instance) => <String, dynamic>{
      'badgeId': instance.badgeId,
      'asset': instance.asset,
      'animationLevel': instance.animationLevel,
      'title': instance.title,
      'description': instance.description,
      'category': instance.category,
      'unlockDate': instance.unlockDate?.toIso8601String(),
      'conditions': instance.conditions.map((e) => e.toJson()).toList(),
    };
