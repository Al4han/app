/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'user_bloc.dart';

/// Enumerate all the types of errors we may encounter.
enum UserError {
  /// No error detected.
  none,

  /// Error while sending data to backend.
  post,

  /// Error while retrieving data from backend.
  fetch,

  /// Error while retrieving data from local storage.
  read,

  /// Error while writing data to local storage.
  write,

  /// Offline while trying to retrieve/send data from/to backend.
  offline,
}

/// A state object for [UserBloc].
@immutable
@JsonSerializable(anyMap: true, explicitToJson: true)
class UserState extends Equatable {
  final User user;
  final UserError errorType;
  final String errorMsg;

  /// Creates a new instance of a [UserState].
  UserState(this.user, [this.errorType = UserError.none, this.errorMsg = ""]);

  /// Creates a [UserState] from a placeholder [User.baseUser].
  UserState.baseState([this.errorType = UserError.none, this.errorMsg = ""])
      : user = User.placeholder();

  /// Creates a [UserState] from a Json.
  factory UserState.fromJson(Map<String, dynamic> json) =>
      _$UserStateFromJson(json);

  /// Maps a [UserState] to a Json.
  Map<String, dynamic> toJson() => _$UserStateToJson(this);

  @override
  String toString() => errorType == UserError.none
      ? '${runtimeType.toString()}: { ${user.toString()} }'
      : '${runtimeType.toString()}: { ${user.toString()}, '
          '${errorType.toString()}, errorMsg: $errorMsg }';

  @override
  List<Object> get props => [
        user,
        errorType,
        errorMsg,
      ];
}

/// A [UserState] indicating that a synchronization is running.
class UserLoadingState extends UserState {
  /// Creates a new instance of [UserLoadingState].
  UserLoadingState(User user) : super(user);
}

/// A [UserState] indicating that loading succeeded.
class UserLoggedInState extends UserState {
  UserLoggedInState(User user) : super(user);
}

/// A [UserState] that resets the current state to baseState when logging out.
class UserLoggedOutState extends UserState {
  /// Sets the current [UserState] to [UserState.baseState]
  UserLoggedOutState() : super.baseState();
}

/// A [UserState] indicating that an error occurred.
class UserErrorState extends UserState {
  /// Creates a new instance of [UserErrorState] providing the [User],
  /// the [errorType] and the [errorMsg].
  UserErrorState(User user, UserError errorType, String errorMsg)
      : super(user, errorType, errorMsg);

  /// The basic [UserErrorState] that just provides the [errorType]
  /// and the [errorMsg].
  UserErrorState.baseState(UserError errorType, String errorMsg)
      : super.baseState(errorType, errorMsg);
}
