/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'user_bloc.dart';

/// A quasi Interface for [UserBloc]s supported events.
@immutable
abstract class UserEvent extends Equatable {
  UserState _performAction(UserBloc bloc);

  @override
  String toString() => runtimeType.toString();

  @override
  List<Object> get props => [];
}

/// An event to fetch the [UserState] from the backend.
class UserFetchEvent extends UserEvent {
  @override
  UserLoadingState _performAction(UserBloc bloc) {
    _fetch(bloc);
    return UserLoadingState(bloc.state.user);
  }

  /// Tries to load previous [UserState] from backend for synchronization.
  /// Adds a [UserErrorEvent] should an exception be caught.
  Future<void> _fetch(UserBloc bloc) async {
    try {
      var response = await bloc._httpRepo.read(profileUrl);
      var user = User.fromJson(jsonDecode(response));
      bloc.add(UserFetchedEvent(user));
    } on OfflineException catch (e) {
      // TODO: offline mode?
      bloc.add(UserErrorEvent.offline(e.message));
    } on Exception catch (e) {
      bloc.add(UserErrorEvent.fetch(e.toString()));
      if (e is! UnauthorizedException && e is! RefreshTokenExpiredException) {
        rethrow;
      }
    }
  }
}

/// An event to return a new [UserState] with fetched [User]
class UserFetchedEvent extends UserEvent {
  final User _user;

  UserFetchedEvent(this._user);

  @override
  UserLoggedInState _performAction(UserBloc bloc) => UserLoggedInState(_user);

  @override
  String toString() => '${runtimeType.toString()}: '
      '{ ${_user.toString()} }';

  @override
  List<Object> get props => [_user];
}

/// An event that returns a [UserLoggedOutState]
class UserLogoutEvent extends UserEvent {
  @override
  UserLoggedOutState _performAction(UserBloc bloc) => UserLoggedOutState();
}

/// An event to handle errors occurring during synchronization
class UserErrorEvent extends UserEvent {
  final UserError _errorType;
  final String _errorMsg;
  UserErrorEvent.post(this._errorMsg) : _errorType = UserError.post;
  UserErrorEvent.fetch(this._errorMsg) : _errorType = UserError.fetch;
  UserErrorEvent.read(this._errorMsg) : _errorType = UserError.read;
  UserErrorEvent.write(this._errorMsg) : _errorType = UserError.write;
  UserErrorEvent.offline(this._errorMsg) : _errorType = UserError.offline;

  @override
  UserErrorState _performAction(UserBloc bloc) {
    debugPrint("UserErrorEvent: ${_errorType.toString()}, \n"
        "errorMsg: $_errorMsg\n"
        "State was: ${bloc.state.user.toString()}");
    return UserErrorState(bloc.state.user, _errorType, _errorMsg);
  }

  @override
  String toString() => '${runtimeType.toString()}: '
      '{ ${_errorType.toString()}, _errorMsg: $_errorMsg }';

  @override
  List<Object> get props => [_errorType, _errorMsg];
}

/// An event to update the [User]s data.
class UserUpdateEvent extends UserEvent {
  final User _newUser;

  /// Expects the new [User]s data.
  UserUpdateEvent(this._newUser);

  // TODO: Implement updating backend data as well!
  @override
  UserLoggedInState _performAction(UserBloc bloc) =>
      UserLoggedInState(_newUser);

  @override
  String toString() => '${runtimeType.toString()}: '
      '{ ${_newUser.toString()} }';

  @override
  List<Object> get props => [_newUser];
}
