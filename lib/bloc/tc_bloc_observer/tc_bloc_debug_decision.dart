/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:json_annotation/json_annotation.dart';

part 'tc_bloc_debug_decision.g.dart';

const _envKey = 'BLOC_DEBUG';

enum BlocAction {
  create,
  event,
  change,
  transition,
  persistenceReading,
  persistenceWriting,
  error,
  close,
}

extension Prepend on String {
  String prepend(String prefix) => '$prefix$this';
}

extension AsString on BlocAction {
  String asString() => toString().splitMapJoin(
        RegExp(r'(BlocAction\.)|([A-Z])'),
        onMatch: (match) => match.group(2)?.toUpperCase().prepend(' ') ?? '',
        onNonMatch: (nonMatch) => nonMatch.toUpperCase(),
      );
}

extension CheckDebug on BlocBase {
  void debugIfAllowed(BlocAction action, String message) {
    if (shouldLogOn(action)) {
      debugPrint(
        "${TCBlocDebugDecision.debugHeader(action.asString())} $message",
      );
    }
  }

  bool shouldLogOn(BlocAction action) {
    final name = runtimeType.toString();
    if (TCBlocDebugDecision.debugBLoC.isEmpty) return true;
    final value = TCBlocDebugDecision.debugBLoC[name];
    switch (value.runtimeType) {
      case Null:
        return false;
      case bool:
        return value;
      default:
        try {
          return TCBlocDebugDecision.fromJson(value).resolve(action);
        } on Exception catch (_) {
          debugPrint(
            "${TCBlocDebugDecision.debugHeader(action.asString())} "
            "could not resolve logging decision",
          );
        }
        return false;
    }
  }
}

@JsonSerializable()
class TCBlocDebugDecision {
  /// Header used for [debugPrint]s.
  static String debugHeader(String info) => "[ ${DateTime.now().toString()} "
      "| TCBlocDelegate "
      "| ${info.toUpperCase()} ]";

  static Map<String, dynamic> debugBLoC = {};

  static void loadDecisionMap() {
    try {
      if (dotenv.isEveryDefined({_envKey})) {
        debugBLoC.addAll(jsonDecode(dotenv.get(_envKey)));
      }
    } on Exception catch (e) {
      debugPrint(
        "${debugHeader('loading')} Loading Config failed:\n"
        "${e.toString()}",
      );
    } finally {
      debugPrint(
        "${debugHeader('loading')} Config is: "
        "${debugBLoC.isNotEmpty ? debugBLoC.toString() : 'Default'}",
      );
    }
  }

  @JsonKey(defaultValue: false)
  final bool onCreate;
  @JsonKey(defaultValue: false)
  final bool onEvent;
  @JsonKey(defaultValue: false)
  final bool onChange;
  @JsonKey(defaultValue: false)
  final bool onTransition;
  @JsonKey(defaultValue: false)
  final bool onPersistence;
  @JsonKey(defaultValue: false)
  final bool onError;
  @JsonKey(defaultValue: false)
  final bool onClose;

  const TCBlocDebugDecision({
    required this.onCreate,
    required this.onEvent,
    required this.onChange,
    required this.onTransition,
    required this.onPersistence,
    required this.onError,
    required this.onClose,
  });

  bool resolve(BlocAction action) {
    switch (action) {
      case BlocAction.create:
        return onCreate;
      case BlocAction.event:
        return onEvent;
      case BlocAction.change:
        return onChange;
      case BlocAction.transition:
        return onTransition;
      case BlocAction.persistenceReading:
      case BlocAction.persistenceWriting:
        return onPersistence;
      case BlocAction.error:
        return onError;
      case BlocAction.close:
        return onClose;
    }
  }

  factory TCBlocDebugDecision.fromJson(Map<String, dynamic> json) =>
      _$TCBlocDebugDecisionFromJson(json);

  Map<String, dynamic> toJson() => _$TCBlocDebugDecisionToJson(this);
}
