#!/usr/bin/env sh

# Create release builds for Android and iOS with the correct .env file
#
# For the script to work, two files must exist:
#   - .env/.env.dev
#   - .env/.env.production
#
# If you still have .env files under 'assets/env/' (besides the 'examples/'
# directory), move them to '.env/' instead.
#
# Usage: Run the script from the project root directory for the target platform:
#
# Android:
#   $ scripts/build-release.sh appbundle
#
# iOS:
#   $ scripts/build-release.sh ios
#

ENV_DEV=".env/.env.dev"
ENV_PRODUCTION=".env/.env.production"
ENV_TARGET="assets/env/.env"
TWO_LEVELS_UP="../../"

error() {
    RED='\033[0;31m'
    NC='\033[0m'

    printf >&2 "%b%s: Error: %s%b\n" "$RED" "$0" "$1" "$NC"
    exit "$2"
}

info() {
    GREEN='\033[0;32m'
    NC='\033[0m'

    printf "%b%s%b\n" "$GREEN" "$1" "$NC"
}

usage() {
    RED='\033[0;31m'
    NC='\033[0m'

    printf >&2 "%bUsage: %s %s\n%s%b\n" "$RED" "$0" "TYPE" "'TYPE' must be either 'appbundle' or 'ios'." "$NC"
    exit 1
}

# Print usage on invalid args
test $# -ne 1 &&
    usage
test $1 = 'appbundle' -o $1 = 'ios' ||
    usage

# Check whether '.env.dev' and '.env.production' exist
test -f "$ENV_DEV" ||
    error "Missing .env file: '$ENV_DEV'" "1"
test -f "$ENV_PRODUCTION" ||
    error "Missing .env file: '$ENV_PRODUCTION'" "1"

# Delete old symlink
test -h "$ENV_TARGET" &&
    rm -f "$ENV_TARGET" &&
    info "Old symlink '$ENV_TARGET' deleted"

# Check for file that would be overwritten by symlink
test -f "$ENV_TARGET" &&
    error "Regular file '$ENV_TARGET' already exists" "2"

# Set symlink to '.env.production'
ln -s "$TWO_LEVELS_UP$ENV_PRODUCTION" "$ENV_TARGET" &&
    info "'$ENV_TARGET' set to production:" &&
    cat "$ENV_TARGET" &&
    echo

# Perform clean release build
info "Running 'flutter clean' ..."
flutter clean
info "Running 'flutter build $1' ..."
flutter build "$1"
echo

# Set symlink back to '.env.dev'
ln -sf "$TWO_LEVELS_UP$ENV_DEV" "$ENV_TARGET" &&
    info "$ENV_TARGET set back to dev:" &&
    cat "$ENV_TARGET" &&
    echo

info "Done"
exit 0
